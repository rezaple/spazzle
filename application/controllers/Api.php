<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'third_party/REST_Controller.php';
require APPPATH . 'third_party/Format.php';

use Restserver\Libraries\REST_Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Api extends REST_Controller {

    public function __construct() {
        parent::__construct();
        
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);    
        $this->load->model(['query']);    
    }

    public function hello_get()
    {
        $tokenData = 'Hello World!';
        
        // Create a token
        $token = AUTHORIZATION::generateToken($tokenData);

        // Set HTTP status code
        $status = parent::HTTP_OK;

        // Prepare the response
        $response = ['status' => $status, 'token' => $token];

        // REST_Controller provide this method to send responses
        $this->response($response, $status);

    }

    public function getToken_post()
    {

        // Extract user data from POST request
        $username = $this->post('username');
        $password = $this->post('password');

        // GET USER DARI DATABASE
        $getUser  = $this->db->query("SELECT * FROM api where username='$username' and password='$password'")->result_array();
        $data     = array_shift($getUser);

        // Check if valid user
        if ($username === $data['username'] && $password === $data['password']) {
            
            // Create a token from the user data and send it as reponse
            $token = AUTHORIZATION::generateToken(['username' => $data['username']]);

            // Prepare the response
            $status = parent::HTTP_OK;

            $response = ['status' => $status, 'token' => $token];

            $this->response($response, $status);
        }
        else {
            $this->response(['msg' => 'Invalid username or password!'], parent::HTTP_NOT_FOUND);
        }
    }

    private function verify_request()
	{
	    // Get all the headers
	    $headers = $this->input->request_headers();

	    // Extract the token
	    $token = $headers['Authorization'];

	    // Use try-catch
	    // JWT library throws exception if the token is not valid
	    try {
	        // Validate the token
	        // Successfull validation will return the decoded user data else returns false
	        $data = AUTHORIZATION::validateToken($token);
	        if ($data === false) {
	            $status = parent::HTTP_UNAUTHORIZED;
	            $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
	            $this->response($response, $status);

	            exit();
	        } else {
	            return $data;
	        }
	    } catch (Exception $e) {
	        // Token is invalid
	        // Send the unathorized access message
	        $status = parent::HTTP_UNAUTHORIZED;
	        $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
	        $this->response($response, $status);
	    }
	}

    public function escalation_post()
	{
	    // Call the verification method and store the return value in the variable
	    $verify     = $this->verify_request();

	    // Send the return data as reponse
	    $status     = parent::HTTP_OK;

        $now            = date('ymd');

        // CREATED DOC BY
        $konseptor      = $this->post('konseptor');
        $cekKonseptor   = $this->db->query("SELECT * FROM mi.user where username='$konseptor'")->num_rows();
        $gKonseptor     = $this->db->query("SELECT * FROM mi.user where username='$konseptor'")->result_array();
        $dKonseptor     = array_shift($gKonseptor);
        if ($cekKonseptor>0) {
            $createdby  = $dKonseptor['userid'];
        } else {
            $cekMaxU    = $this->db->query("SELECT MAX(userid)+1 maxid from mi.user")->result_array();
            $gMaxU      = array_shift($cekMaxU);
            if ($gMaxU['maxid']==0) {
                $lastidU= '1';
            } else {
                $lastidU= $gMaxU['maxid'];
            }

            $getProfile = $this->getprofileLDAP($konseptor);
            $profile    = json_decode($getProfile, true);
            $imgK       = $konseptor.'.jpg';

            foreach ($profile AS $p){
                $nameK  = $p['name'];
                $emailK = $p['email'];
            }
            $insNewUser = $this->db->query("
                        INSERT INTO mi.user (userid,username,name,picture,id_role,email,ldap,level_user) values 
                        ('$lastidU','$konseptor','$nameK','$imgK','2','$emailK','1','1')
                        ");
            $createdby  = $lastidU;

        }

        $namaproject    = $this->post('namaproject');
        $subject        = $this->post('subject');
        $pelanggan      = $this->post('nipnas');
        $serviceid      = $this->post('serviceid');
        $no_order_ncx   = $this->post('no_order_ncx');
        
        $approval       = $this->post('approval');
        $levelapp       = $this->post('levelapp');

        $latarbelakang  = str_replace("'",'`',$this->post('latarbelakang'));
        $aspekstrategis = str_replace("'",'`',$this->post('aspekstrategis'));
        $aspekfinansial = str_replace("'",'`',$this->post('aspekfinansial'));
        $aspekkompetisi = str_replace("'",'`',$this->post('aspekkompetisi'));
        $konfigurasiteknis = str_replace("'",'`',$this->post('konfigurasiteknis'));

        // GET LEVEL TIM TARIFF
        $getTT          = $this->db->query("SELECT * from mi.level_user where id_level=7 order by level")->result_array();
        $dTT            = array_shift($getTT);
        $status         = '3';
        $current        = $dTT['level'];

        $cekMax         = $this->db->query("SELECT MAX(id)+1 maxid from sbrdoc")->result_array();
        $gMax           = array_shift($cekMax);
        if ($gMax['maxid']==0) {
            $lastid     = '1';
        } else {
            $lastid     = $gMax['maxid'];
        }

        $code           = sprintf("%06d", $lastid);
        $maxid          = $code;

        $norequest      = 'SBR/'.$now.'/'.$pelanggan.'/'.$maxid;
        $createdat      = date('Y-m-d H:i:s');

        // INSERT NEW PENGAJUAN
        $rows           = $this->db->query("
                        INSERT INTO sbrdoc (no_request, nipnas, nama_project, subject, latar_belakang, aspek_strategis, aspek_finansial, aspek_kompetisi, konfigurasi_teknis, status, created_by, created_at, service_id, current,no_order_ncx,cfu)
                        VALUES
                        ('$norequest', '$pelanggan', '$namaproject', '$subject', '$latarbelakang', '$aspekstrategis', '$aspekfinansial', '$aspekkompetisi', '$konfigurasiteknis', '$status', '$createdby', '$createdat'::TIMESTAMP, '$serviceid', '$current','$no_order_ncx','E')
                        ");
        
        $id             = $this->db->insert_id('mi.sbrdoc_id_seq');
        $url            = "Pengajuan SBR";
        $activity       = "INSERT FROM API";

        if($rows) {
            // INSERT LEVEL FLOW CREATED
            $insAFC     = $this->db->query("
                        INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
                        ('$norequest', '$createdby', '1', '0', 'Account Manager')
                        ");

            // INSERT LEVEL FLOW AFTER ESCALATED
            $getLevA    = $this->db->query("
                        SELECT a.*,
                        (select userid from mi.user where level_user=a.id_level limit 1) userid
                        from mi.level_user a
                        where level >=13
                        order by level
                        ")->result_array();
            foreach ($getLevA as $levA) {
                $useridAS   = $levA['userid'];
                $levelAS    = $levA['level'];
                $isfinalAS  = $levA['is_final'];
                $rolenameAS = $levA['name'];

                $insAFAS    = $this->db->query("
                            INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
                            ('$norequest', '$useridAS', '$levelAS', '$isfinalAS', '$rolenameAS')
                            ");
            }


            // INSERT LEVEL FLOW BEFORE ESCALATED
            $gjsonappr      = json_encode($approval, true);
            $jsonappr       = json_decode($gjsonappr, true);

            foreach ($jsonappr AS $dataappr){
                $apprcfue       = $dataappr['nik'];
                $apprlev        = $dataappr['level'];

                $cekAppr        = $this->db->query("SELECT * FROM mi.user where username='$apprcfue'")->num_rows();
                $gAppr          = $this->db->query("SELECT * FROM mi.user where username='$apprcfue'")->result_array();
                $dAppr          = array_shift($gAppr);
                
                if ($cekAppr>0) {
                    $useridlev  = $dAppr['userid'];
                } else {
                    $cekMaxUA   = $this->db->query("SELECT MAX(userid)+1 maxid from mi.user")->result_array();
                    $gMaxUA     = array_shift($cekMaxUA);
                    if ($gMaxUA['maxid']==0) {
                        $lastidUA= '1';
                    } else {
                        $lastidUA= $gMaxUA['maxid'];
                    }

                    $getProfileA = $this->getprofileLDAP($apprcfue);
                    $profileA    = json_decode($getProfileA, true);
                    $imgA        = $apprcfue.'.jpg';

                    foreach ($profileA AS $pA){
                        $nameA  = @$pA['name'];
                        $emailA = @$pA['email'];
                    }
                    $insNewUserA = $this->db->query("
                                INSERT INTO mi.user (userid,username,name,picture,id_role,email,ldap,level_user) values 
                                ('$lastidUA','$apprcfue','$nameA','$imgA','2','$emailA','1','$apprlev')
                                ");
                    $useridlev  = $lastidUA;

                }

                $rolename   = $dataappr['role_name'];
                $isfinal    = '0';

                $insAF      = $this->db->query("
                            INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
                            ('$norequest', '$useridlev', '$apprlev', '$isfinal', '$rolename')
                            ");
            }

            $getFS      = $this->db->query("
                        SELECT a.userid, b.level, b.is_final 
                        from mi.user a 
                        left join level_user b 
                        on a.level_user=b.id_level 
                        where  level=$current
                        order by b.level
                        limit 1
                        ")->result_array();
            foreach ($getFS as $dFS) { $firststep   = $dFS['userid']; }

            // INSERT HISTORY AND NOTIFICATIONS
            // $this->sendMailNotif($norequest,$userid,$firststep,'escalation');

            $insHis     = $this->db->query("
                        INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
                        ('$norequest', 'Eskalasi dari SBR CFU-E', '$createdby', '$createdat'::TIMESTAMP, 'escalation', '$firststep' ,'0')
                        ");

            $log = $this->query->insertlog($activity,$url,$id);
            $this->response(array('status' => '200', 'messages' => 'Success'));
        } else {
            $this->response(array('status' => '502', 'messages' => 'Failed'));
        }

	}

    public function getprofileLDAP($username){
        // $username   = trim(strip_tags(stripslashes($this->input->post('username',true))));
        $url2       = 'https://auth.telkom.co.id/api/call/'.$username.'';
        $JSON2      = file_get_contents($url2);
        
        $gdata2     = json_decode(json_encode($JSON2), True);
        $data2      = json_decode($gdata2);

        $user       = $data2->username;
        $name       = $data2->name;
        $email      = $data2->email;
        
        if (!empty($user)) {
            // $url         = 'https://myworkbook.telkom.co.id/mwb/api/index.php?r=api/photo&nik='.$user;
            $url        = 'https://pwb.telkom.co.id/index.php?r=pwbPhoto/profilePhoto&nik='.$user;
            /* Extract the filename */
            $gfilename  = substr($url, strrpos($url, '/') + 1);
            $filename   = str_replace('profilePhoto&nik=','',$gfilename);
            /* Save file wherever you want */
            @file_put_contents('images/user/'.$filename.'.jpg', file_get_contents($url));

            $ret[] = array(
                "name"      => $name,
                "username"  => $user,
                "email"     => $email
            );
        } else {
            $ret[] = "";
        }
        
        return json_encode($ret);
    }

    public function sendMailNotif($id,$sendby,$sendto,$type) {
        $norequest  = str_replace('-','/',$id);
        $subject    = 'SBR Online - '.$norequest.'';

        $getSendby  = $this->db->query("SELECT * FROM mi.user where userid='$sendby'")->result_array();
        $dataSendby = array_shift($getSendby);
        $nameby     = $dataSendby['name'];
        $emailby    = $dataSendby['email'];

        $getSendto  = $this->db->query("SELECT * FROM mi.user where userid='$sendto'")->result_array();
        $dataSendto = array_shift($getSendto);
        $nameto     = $dataSendto['name'];
        $emailto    = $dataSendto['email'];

        if ($type=='new') {
            $notiftext  = 'Pengajuan baru dari';
        } else if ($type=='republish') {
            $notiftext  = 'Perbaikan Pengajuan dari';
        } else if ($type=='escalation') {
            $notiftext  = 'Eskalasi SBR CFU-E Pengajuan dari';
        } else if ($type=='reject') {
            $notiftext  = 'Pengajuan tidak disetujui oleh';
        } else if ($type=='approve') {
            $notiftext  = 'Pengajuan disetujui oleh';
        } else if ($type=='return') {
            $notiftext  = 'Pengajuan dikembalikan oleh';
        }

        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'smtp.hostinger.co.id',
        'smtp_port' => 587,
        'smtp_user' => 'mi@parwatha.com', // change it to yours
        'smtp_pass' => 'b1sm1llah', // change it to yours
        'mailtype'  => 'html',
        'charset'   => 'iso-8859-1'
        );

        //Email content
        $htmlContent = '';
        $htmlContent .= '
            <!DOCTYPE html>
            <html>
            <head>
              <meta charset="utf-8" />
              <title>No Reply</title>
              <meta name="viewport" content="width=device-width, initial-scale=1.0" />
              <style>
              </style>
            </head>
            <body style="font-family: verdana; font-size: 14px;">
                <div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
                    <div id="logo" style="background: #FFF;"><img src="'.base_url().'images/logotel.png" style="max-height: 70px; margin-top: 20px;"></div>
                    <div id="confirmation-message">
                        <div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
                            <div class="title" style="color: #1e1e1e; font-size: 24px;">
                                <span style="text-transform:capitalize;">Dear, '.$nameto.'</span>
                            </div>
                        </div>
                        <div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
                            <div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
                                '.$notiftext.' <b>'.$nameby.'</b>  dengan No Request : <b>'.$norequest.'</b>.<br>
                                Klik <a href="'.base_url().'docdetail/'.$id.'" target="_blank" style="color: #5d78ff;">disini</a> untuk melihat detail pengajuan.
                                <br><br>

                                Hubungi Admin jika Anda butuh bantuan lebih lanjut.<br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </body>
            </html>
        ';

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('mi@parwatha.com'); // change it to yours
        $this->email->to($emailto);// change it to yours
        $this->email->subject($subject);
        $this->email->set_mailtype("html");
        $this->email->message($htmlContent);
        
        if($this->email->send()) {
            echo '';
        } else {
            show_error($this->email->print_debugger());
        }
        // echo $htmlContent;
    }
}

/* End of file Api.php */