<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctrl_innovasi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $profile;
	private $divisiUAM;
	private $segmenUAM;
	private $tregUAM;
	private $witelUAM;
	private $amUAM;
	
	public function __construct(){
		date_default_timezone_set("Asia/Bangkok");
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('query'); 
		$this->load->model('formula'); 
		$this->load->model('datatable'); 
		$this->load->model('inovasi_handler');
		
		//ini_set('max_execution_time', 123456);
		//ini_set("memory_limit","1256M");
			
		// $session = checkingsessionpwt();
		$session	 = $this->session->userdata('sesspwt'); 
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}


	public function getdatainovasi(){
		if(checkingsessionpwt()){
			error_reporting(0);
			$columnsDefault = [
				'judul'			=> true,
				'cfu_fu'		=> true,
				'url_aplikasi'	=> true,
				'stackholder'	=> true,
				'created_by'	=> true,
				'created_date'	=> true,
				'status'		=> true,
				'actions'		=> true,
			];
			$arraynya	= $columnsDefault;

			// $jsonfile	= base_url().'user/data';
			$jsonfile	= $this->inovasi_handler->data();
			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}
	public function submit_innovasi(){
		if(checkingsessionpwt()){
			error_reporting(0);
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];
			$now 			= date('ymd');

			$judul 			= trim(strip_tags(stripslashes($this->input->post('judul',true))));
			$cfu_fu 		= trim(strip_tags(stripslashes($this->input->post('cfu_fu',true))));
			$url 			= trim(strip_tags(stripslashes($this->input->post('url',true))));
			$tujuan			= trim(strip_tags(stripslashes($this->input->post('tujuan',true))));
			$pii 			= trim(strip_tags(stripslashes($this->input->post('pii',true))))?:'0';

			$deskripsi 		= str_replace("'",'`',$_POST['deskripsi']);
			$tujuan 		= str_replace("'",'`',$_POST['tujuan']);
			$jenis_data 	= str_replace("'",'`',$_POST['jenis_data']);
			$penjelasan 	= str_replace("'",'`',$_POST['penjelasan']);
			$stakholder		= str_replace("'",'`',$_POST['stakholder']);

			@$filename 		= $_FILES['diagram']['name'];
			@$filename2 		= $_FILES['diagram2']['name'];
			@$filereq 		= $_FILES['docrequirmen']['name'];
			@$filedes 		= $_FILES['docdesign']['name'];
			@$filetes 		= $_FILES['doctesreport']['name'];
			@$filesmp 		= $_FILES['docsmp']['name'];
			@$filesop 		= $_FILES['docsop']['name'];
			@$filesopd2p		= $_FILES['docsopd2p']['name'];
			@$filetou		= $_FILES['doctou']['name'];
			@$filetfa		= $_FILES['doctfa']['name'];
			@$filess 		= $_FILES['docss']['name'];
			@$filecc 		= $_FILES['doccustcons']['name'];

			$status 		= '0';
			$current 		= '1';

			$cekMax 		= $this->db->query("SELECT MAX(id)+1 maxid from data_innovasi")->result_array();
			$gMax 			= array_shift($cekMax);
			if ($gMax['maxid']==0) { $lastid 	= '1'; } else { $lastid 	= $gMax['maxid']; }

			$code 			= sprintf("%06d", $lastid);
			$maxid 			= $code;

			$Y 				= date('Y');
			$MaxCode 		= $this->db->query("SELECT MAX(id)+1 maxid from mi.data_innovasi where EXTRACT(YEAR FROM created_at) ='".$Y."'")->result_array();
			$gMaxCode 		= array_shift($MaxCode);
			$maxidbyyear 	= $gMaxCode['maxid'];

			$noinnovasi 	= 'TEL.'.$lastid.'/INNOVASI/A/'.date('m').'/'.date('Y');
			$createdate 	= date('Y-m-d H:i:s');

			$direktori 		= './attachment/';
			$lokasi_file  	= $_FILES['diagram']['tmp_name'];
			$tipe_file     	= $_FILES['diagram']['type'];
			$nama_file     	= $_FILES['diagram']['name'];
			$nama_file_unik	= str_replace(' ','_',date('Ymd').'_'.$nama_file);

			$extension = pathinfo($nama_file, PATHINFO_EXTENSION);
			
			if (!empty($lokasi_file)){
				$vfile_upload = $direktori . $nama_file_unik;
				move_uploaded_file($lokasi_file, $vfile_upload);
			}

			$direktori_2 		= './attachment/';
			$lokasi_file_2  	= $_FILES['diagram2']['tmp_name'];
			$tipe_file_2     	= $_FILES['diagram2']['type'];
			$nama_file_2     	= $_FILES['diagram2']['name'];
			$nama_file_unik_2	= str_replace(' ','_',date('Ymd').'_'.$nama_file_2);

			$extension_2 = pathinfo($nama_file_2, PATHINFO_EXTENSION);
			
			if (!empty($lokasi_file_2)){
				$vfile_upload_2 = $direktori_2 . $nama_file_unik_2;
				move_uploaded_file($lokasi_file_2, $vfile_upload_2);
			}


			if($pii == '1'){
				$direktori_pii 		= './attachment/';
				$lokasi_file_pii  	= $_FILES['docpii']['tmp_name'];
				$tipe_file_pii     	= $_FILES['docpii']['type'];
				$nama_file_pii     	= $_FILES['docpii']['name'];
				$nama_file_unik_pii	= str_replace(' ','_',date('Ymd').'_'.$nama_file_pii);

				$extension_pii = pathinfo($nama_file_pii, PATHINFO_EXTENSION);				
			}else{
				$nama_file_unik_pii = '';
			}
			
			if (!empty($lokasi_file_pii)){
				$vfile_upload_pii = $direktori_pii . $nama_file_unik_pii;
				move_uploaded_file($lokasi_file_pii, $vfile_upload_pii);
			}

			$direktori_stg 		= './attachment/';
			$lokasi_file_stg  	= $_FILES['docstg']['tmp_name'];
			$tipe_file_stg     	= $_FILES['docstg']['type'];
			$nama_file_stg     	= $_FILES['docstg']['name'];
			$nama_file_unik_stg	= str_replace(' ','_',date('Ymd').'_'.$nama_file_stg);

			$extension_stg = pathinfo($nama_file_stg, PATHINFO_EXTENSION);
			
			if (!empty($lokasi_file_stg)){
				$vfile_upload_stg = $direktori_stg . $nama_file_unik_stg;
				move_uploaded_file($lokasi_file_stg, $vfile_upload_stg);
			}

			$data1 		= array(
				"id" => $lastid,
				"judul" => $judul,				
				"cfu_fu" => $cfu_fu,
				"url_aplikasi" => $url,
				"deskripsi" => $deskripsi,
				"tujuan" => $tujuan,
				"jenis_data" => $jenis_data,
				"stakholder" => $stakholder,
				"diagram" => $nama_file_unik,
				"status" => $status,
				"created_by" => $userid,
				"created_at" => $createdate,
				"penjelasan_singkat" => $penjelasan,
				"current" => $current, 
				"noinnovasi" => $noinnovasi,
				"pii_evidance" => $nama_file_unik_pii,
				"sharing_telkom_group" => $nama_file_unik_stg,
				"diagram2" => $nama_file_unik_2
			);
			$rows = $this->db->insert('data_innovasi', $data1);
			$url 			= "Pengajuan Innovasi";
			$activity 		= "INSERT";
			if($rows) {
				// INSERT SDLC
				$jreq 		= count($filereq);
				for($ia=0;$ia<$jreq;$ia++) {
			        $direktori_req[$ia] 		= './attachment/';
					$lokasi_file_req[$ia]    	= $_FILES['docrequirmen']['tmp_name'][$ia];
					$tipe_file_req[$ia]      	= $_FILES['docrequirmen']['type'][$ia];
					$nama_file_req[$ia]      	= $_FILES['docrequirmen']['name'][$ia];
					$nama_file_unik_req[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_req[$ia]);
					$extension_req = pathinfo($nama_file_req[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_req[$ia])){
						$getmax_req = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_req[$ia] = $direktori_req[$ia] . $nama_file_unik_req[$ia];
						
						move_uploaded_file($lokasi_file_req[$ia], $vfile_upload_req[$ia]);
						
						$data3		= array(
							"id_sdlc" => $getmax_req,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_req[$ia],
							"type" => "Dok.Requirment"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data3);
					}
				}

				$jdes 		= count($filedes);
				for($ia=0;$ia<$jdes;$ia++) {
			        $direktori_des[$ia] 		= './attachment/';
					$lokasi_file_des[$ia]    	= $_FILES['docdesign']['tmp_name'][$ia];
					$tipe_file_des[$ia]      	= $_FILES['docdesign']['type'][$ia];
					$nama_file_des[$ia]      	= $_FILES['docdesign']['name'][$ia];
					$nama_file_unik_des[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_des[$ia]);
					$extension_des = pathinfo($nama_file_des[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_des[$ia])){
						$getmax_des = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_des[$ia] = $direktori_des[$ia] . $nama_file_unik_des[$ia];
						
						move_uploaded_file($lokasi_file_des[$ia], $vfile_upload_des[$ia]);
						
						$data4		= array(
							"id_sdlc" => $getmax_des,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_des[$ia],
							"type" => "Dok.Design"							
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data4);
					}
				}

				$jtes 		= count($filetes);
				for($ia=0;$ia<$jtes;$ia++) {
			        $direktori_tes[$ia] 		= './attachment/';
					$lokasi_file_tes[$ia]    	= $_FILES['doctesreport']['tmp_name'][$ia];
					$tipe_file_tes[$ia]      	= $_FILES['doctesreport']['type'][$ia];
					$nama_file_tes[$ia]      	= $_FILES['doctesreport']['name'][$ia];
					$nama_file_unik_tes[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_tes[$ia]);
					$extension_tes = pathinfo($nama_file_tes[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_tes[$ia])){
						$getmax_tes = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_tes[$ia] = $direktori_tes[$ia] . $nama_file_unik_tes[$ia];
						
						move_uploaded_file($lokasi_file_tes[$ia], $vfile_upload_tes[$ia]);
						
						$data5		= array(
							"id_sdlc" => $getmax_tes,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_tes[$ia],
							"type" => "Dok.Test Plan/Report"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data5);
					}
				}

				$jsop 		= count($filesop);
				for($ia=0;$ia<$jsop;$ia++) {
			        $direktori_sop[$ia] 		= './attachment/';
					$lokasi_file_sop[$ia]    	= $_FILES['docsop']['tmp_name'][$ia];
					$tipe_file_sop[$ia]      	= $_FILES['docsop']['type'][$ia];
					$nama_file_sop[$ia]      	= $_FILES['docsop']['name'][$ia];
					$nama_file_unik_sop[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_sop[$ia]);
					$extension_sop = pathinfo($nama_file_sop[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_sop[$ia])){
						$getmax_sop = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_sop[$ia] = $direktori_sop[$ia] . $nama_file_unik_sop[$ia];
						
						move_uploaded_file($lokasi_file_sop[$ia], $vfile_upload_sop[$ia]);
						
						$data6		= array(
							"id_sdlc" => $getmax_sop,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_sop[$ia],
							"type" => "Dok.SOP"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data6);
					}
				}

				$jsopd2p 		= count($filesopd2p);
				for($ia=0;$ia<$jsopd2p;$ia++) {
			        $direktori_sopd2p[$ia] 		= './attachment/';
					$lokasi_file_sopd2p[$ia]    	= $_FILES['docsopd2p']['tmp_name'][$ia];
					$tipe_file_sopd2p[$ia]      	= $_FILES['docsopd2p']['type'][$ia];
					$nama_file_sopd2p[$ia]      	= $_FILES['docsopd2p']['name'][$ia];
					$nama_file_unik_sopd2p[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_sopd2p[$ia]);
					$extension_sopd2p = pathinfo($nama_file_sopd2p[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_sopd2p[$ia])){
						$getmax_sopd2p = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_sopd2p[$ia] = $direktori_sopd2p[$ia] . $nama_file_unik_sopd2p[$ia];
						
						move_uploaded_file($lokasi_file_sopd2p[$ia], $vfile_upload_sopd2p[$ia]);
						
						$data7		= array(
							"id_sdlc" => $getmax_sopd2p,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_sopd2p[$ia],
							"type" => "Dok.SOP D2P"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data7);
					}
				}

				$jsmp 		= count($filesmp);
				for($ia=0;$ia<$jsmp;$ia++) {
			        $direktori_smp[$ia] 		= './attachment/';
					$lokasi_file_smp[$ia]    	= $_FILES['docsmp']['tmp_name'][$ia];
					$tipe_file_smp[$ia]      	= $_FILES['docsmp']['type'][$ia];
					$nama_file_smp[$ia]      	= $_FILES['docsmp']['name'][$ia];
					$nama_file_unik_smp[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_smp[$ia]);
					$extension_smp = pathinfo($nama_file_smp[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_smp[$ia])){
						$getmax_smp = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_smp[$ia] = $direktori_smp[$ia] . $nama_file_unik_smp[$ia];
						
						move_uploaded_file($lokasi_file_smp[$ia], $vfile_upload_smp[$ia]);
						
						$data8		= array(
							"id_sdlc" => $getmax_smp,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_smp[$ia],
							"type" => "Dok.SMP"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data8);
					}
				}
				// INSERT SECURITY
				$jtou 		= count($filetou);
				for($ia=0;$ia<$jtou;$ia++) {
			        $direktori_tou[$ia] 		= './attachment/';
					$lokasi_file_tou[$ia]    	= $_FILES['doctou']['tmp_name'][$ia];
					$tipe_file_tou[$ia]      	= $_FILES['doctou']['type'][$ia];
					$nama_file_tou[$ia]      	= $_FILES['doctou']['name'][$ia];
					$nama_file_unik_tou[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_tou[$ia]);
					$extension_tou = pathinfo($nama_file_tou[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_tou[$ia])){
						$getmax_tou = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_tou[$ia] = $direktori_tou[$ia] . $nama_file_unik_tou[$ia];
						
						move_uploaded_file($lokasi_file_tou[$ia], $vfile_upload_tou[$ia]);
						
						$data11		= array(
							"id_sdlc" => $getmax_tou,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_tou[$ia],
							"type" => "Evidance.ToU"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data11);
					}
				}

				$jtfa 		= count($filetfa);
				for($ia=0;$ia<$jtfa;$ia++) {
			        $direktori_tfa[$ia] 		= './attachment/';
					$lokasi_file_tfa[$ia]    	= $_FILES['doctfa']['tmp_name'][$ia];
					$tipe_file_tfa[$ia]      	= $_FILES['doctfa']['type'][$ia];
					$nama_file_tfa[$ia]      	= $_FILES['doctfa']['name'][$ia];
					$nama_file_unik_tfa[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_tfa[$ia]);
					$extension_tfa = pathinfo($nama_file_tfa[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_tfa[$ia])){
						$getmax_tfa = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_tfa[$ia] = $direktori_tfa[$ia] . $nama_file_unik_tfa[$ia];
						
						move_uploaded_file($lokasi_file_tfa[$ia], $vfile_upload_tfa[$ia]);
						
						$data12		= array(
							"id_sdlc" => $getmax_tfa,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_tfa[$ia],
							"type" => "Evidance.TFA"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data12);
					}
				}

				$jnss 		= count($filess);
				for($ia=0;$ia<$jnss;$ia++) {
			        $direktori_ss[$ia] 		= './attachment/';
					$lokasi_file_ss[$ia]    	= $_FILES['docss']['tmp_name'][$ia];
					$tipe_file_ss[$ia]      	= $_FILES['docss']['type'][$ia];
					$nama_file_ss[$ia]      	= $_FILES['docss']['name'][$ia];
					$nama_file_unik_ss[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_ss[$ia]);
					$extension_ss = pathinfo($nama_file_ss[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_ss[$ia])){
						$getmax_ss = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_ss[$ia] = $direktori_ss[$ia] . $nama_file_unik_ss[$ia];
						
						move_uploaded_file($lokasi_file_ss[$ia], $vfile_upload_ss[$ia]);
						
						$data13		= array(
							"id_sdlc" => $getmax_ss,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_unik_ss[$ia],
							"type" => "Evidance.NDE VA Test"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data13);
					}
				}

				@$jncc		= count($filecc);
				for($ia=0;$ia<$jncc;$ia++) {
			        $direktori_cc[$ia] 		= './attachment/';
					$lokasi_file_cc[$ia]    	= $_FILES['doccustcons']['tmp_name'][$ia];
					$tipe_file_cc[$ia]      	= $_FILES['doccustcons']['type'][$ia];
					$nama_file_cc[$ia]      	= $_FILES['doccustcons']['name'][$ia];
					$nama_file_unik_cc[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file_cc[$ia]);
					$extension_cc = pathinfo($nama_file_cc[$ia], PATHINFO_EXTENSION);
					if (!empty($lokasi_file_cc[$ia])){
						$getmax_cc = $this->fn_increment('data_innovasi_sdlc','id_sdlc');
						
						$vfile_upload_cc[$ia] = $direktori_cc[$ia] . $nama_file_unik_cc[$ia];
						
						move_uploaded_file($lokasi_file_cc[$ia], $vfile_upload_cc[$ia]);
						
						$data200 = array(
							"id_sdlc" => $getmax_cc,
							"noinnovasi" => $noinnovasi,				
							"doc" => $nama_file_cc[$ia],
							"type" => "Evidance.Customer Consent"
						);
						$rows = $this->db->insert('data_innovasi_sdlc', $data200);
					}
				}

				// INSERT UPTI
				if(isset($_POST['framework'])){
					@$framework = count($_POST['framework']);
					for($ia=0;$ia<$framework;$ia++) {
						$getmaxfm = $this->fn_increment('data_innovasi_upti','id_upti');
						$data9		= array(
							"id_upti" => $getmaxfm,
							"noinnovasi" => $noinnovasi,				
							"type" => 'Framework',
							"name_upti" => $_POST['framework'][$ia]
						);
						$rows = $this->db->insert('data_innovasi_upti', $data9);

					}					
				}
				if(isset($_POST['database'])){
					@$database = count($_POST['database']);
					for($ia=0;$ia<$database;$ia++) {
						$getmaxdb = $this->fn_increment('data_innovasi_upti','id_upti');
						$data10		= array(
							"id_upti" => $getmaxdb,
							"noinnovasi" => $noinnovasi,				
							"type" => 'Database',
							"name_upti" => $_POST['database'][$ia]
						);
						$rows = $this->db->insert('data_innovasi_upti', $data10);
					}
				}

				if(isset($_POST['otherframe'])){
					$ot_framework = count($_POST['otherframe']);
					for($ia=0;$ia<$ot_framework;$ia++) {
						if($_POST['otherframe'][$ia] !=''){
							$getmaxofm = $this->fn_increment('data_innovasi_upti','id_upti');
							$data100 = array(
								"id_upti" => $getmaxofm,
								"noinnovasi" => $noinnovasi,				
								"type" => 'Framework',
								"name_upti" => $_POST['otherframe'][$ia]
							);
							$rows = $this->db->insert('data_innovasi_upti', $data100);							
						}
					}
				}
				if(isset($_POST['otherdb'])){
					$ot_database = count($_POST['otherdb']);
					for($ia=0;$ia<$ot_database;$ia++) {
						if($_POST['otherdb'][$ia] !=''){
							$getmaxodb = $this->fn_increment('data_innovasi_upti','id_upti');
							$data101		= array(
								"id_upti" => $getmaxodb,
								"noinnovasi" => $noinnovasi,				
								"type" => 'Database',
								"name_upti" => $_POST['otherdb'][$ia]
							);
							$rows = $this->db->insert('data_innovasi_upti', $data101);
						}
					}
				}

				// INSERT HISTORY AND NOTIFICATIONS
				$cekinnovasihistory = $this->db->query("SELECT MAX(id)+1 maxid from data_innovasi_history")->result_array();
				$gmaxinnovasi 		= array_shift($cekinnovasihistory);
				if ($gmaxinnovasi['maxid']==0) { $lastinnovasi 	= '1'; } else { $lastinnovasi 	= $gmaxinnovasi['maxid']; }

				$datax		= array(
					"id"=>$lastinnovasi,
					"noinnovasi"=>$noinnovasi,
					"comment"=>'Pengajuan Innovasi Baru',
					"created_by"=>$userid,
					"created_at"=>$createdate,
					"action"=>'new',
					"send_to"=>'0',
					"is_read"=>'0',
				);
				$rows = $this->db->insert('data_innovasi_history', $datax);
			}
		}else{
		}		
	}
	public function review_innovasi(){
		if(checkingsessionpwt()){
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];
			$now 			= date('ymd');

			$noinnovasi = trim(strip_tags(stripslashes($this->input->post('noinnovasi',true))));
			$status_data_innovasi = trim(strip_tags(stripslashes($this->input->post('status_data_innovasi',true))));
			$status_sdlc = trim(strip_tags(stripslashes($this->input->post('status_sdlc',true))));
			$status_upti = trim(strip_tags(stripslashes($this->input->post('status_upti',true))));
			$status_pii = trim(strip_tags(stripslashes($this->input->post('status_pii',true))));
			$status_security = trim(strip_tags(stripslashes($this->input->post('status_security',true))));
			$status_data_telkom_group = trim(strip_tags(stripslashes($this->input->post('status_data_telkom_group',true))));

			//$status 		= '1';
			$current 		= '2';
	
			$createdate 	= date('Y-m-d H:i:s');

			if($status_data_innovasi == 1 and $status_sdlc==1 and $status_pii==1 and $status_upti==1 and $status_security==1 and $status_data_innovasi==1){
				$allstatus = 1;
			}else{
				$allstatus = 2;
			}
			$data 		= array(
				"status"	=> $allstatus,
				"current"	=> $current,
				"status_data_innovasi" => $status_data_innovasi,
				"sdlc_status" => $status_sdlc,
				"upti_status" => $status_upti,				
				"pii_status" => $status_pii,
				"security_status" => $status_security,
				"data_sharing_status" => $status_data_telkom_group,
				"approve_by" => $userid,
				"approve_at" => $createdate
			);
			$this->db->where('noinnovasi', $noinnovasi);
			$rows =$this->db->update('data_innovasi', $data);
			$url 			= "Review Pengajuan Innovasi";
			$activity 		= "INSERT";
			if($rows) {
				// INSERT HISTORY AND NOTIFICATIONS
				$cekinnovasihistory = $this->db->query("SELECT MAX(id)+1 maxid from data_innovasi_history")->result_array();
				$gmaxinnovasi 		= array_shift($cekinnovasihistory);
				if ($gmaxinnovasi['maxid']==0) { $lastinnovasi 	= '1'; } else { $lastinnovasi 	= $gmaxinnovasi['maxid']; }

				$datax		= array(
					"id"=>$lastinnovasi,
					"noinnovasi"=>$noinnovasi,
					"comment"=>'Review Pengajuan Innovasi Baru',
					"created_by"=>$userid,
					"created_at"=>$createdate,
					"action"=>'approve',
					"send_to"=>'0',
					"is_read"=>'0',
				);
				$rows = $this->db->insert('data_innovasi_history', $datax);
			}
		}else{
		}		
	}
	public function fn_increment($table,$id){
		$qmax = $this->db->query("SELECT MAX($id)+1 maxid from $table")->result_array();
		$gmax = array_shift($qmax);
		if ($gmax['maxid']==0) { $lastid 	= '1'; } else { $lastid 	= $gmax['maxid']; }
		return $lastid;
	}
	public function insert($type){
		if(checkingsessionpwt()){
			error_reporting(E_ALL);
			
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$now 		= date('ymd');

			$namaproject 	= trim(strip_tags(stripslashes($this->input->post('namaproject',true))));
			$subject 		= trim(strip_tags(stripslashes($this->input->post('subject',true))));
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('pelanggan',true))));
			$serviceid 		= trim(strip_tags(stripslashes($this->input->post('serviceid',true))));
			$no_order_ncx	= trim(strip_tags(stripslashes($this->input->post('no_order_ncx',true))));
			
			// $reviewer1 		= trim(strip_tags(stripslashes($this->input->post('reviewer1',true))));
			// $approval1 		= trim(strip_tags(stripslashes($this->input->post('approval1',true))));
			// $approval2 		= trim(strip_tags(stripslashes($this->input->post('approval2',true))));
			// $approval3 		= trim(strip_tags(stripslashes($this->input->post('approval3',true))));
			// $timtarif1 		= trim(strip_tags(stripslashes($this->input->post('timtarif1',true))));
			// $timtarif2 		= trim(strip_tags(stripslashes($this->input->post('timtarif2',true))));
			
			$appflow 		= $this->input->post('appflow',true);

			$latarbelakang 	= str_replace("'",'`',$_POST['latarbelakang']);
			$aspekstrategis = str_replace("'",'`',$_POST['aspekstrategis']);
			$aspekfinansial = str_replace("'",'`',$_POST['aspekfinansial']);
			$aspekkompetisi = str_replace("'",'`',$_POST['aspekkompetisi']);
			$konfigurasiteknis = str_replace("'",'`',$_POST['konfigurasiteknis']);

			$filename 		= $_FILES['attach']['name'];

			if ($type=='draft') {
				$status 	= '0';
				$current 	= '1';
			} else {
				$status 	= '1';
				$current 	= '2';
			}

			$cekMax 		= $this->db->query("SELECT MAX(id)+1 maxid from sbrdoc")->result_array();
			$gMax 			= array_shift($cekMax);
			if ($gMax['maxid']==0) {
				$lastid 	= '1';
			} else {
				$lastid 	= $gMax['maxid'];
			}

			$code 			= sprintf("%06d", $lastid);
			$maxid 			= $code;

			$Y 				= date('Y');
			$MaxCode 		= $this->db->query("SELECT MAX(id)+1 maxid from sbrdoc where EXTRACT(YEAR FROM created_at) = '".$Y."'")->result_array();
			$gMaxCode 			= array_shift($MaxCode);
			$maxidbyyear 		= $gMaxCode['maxid'];

			$norequest 		= 'TEL.'.$lastid.'/SBR-B/A/'.date('m').'/'.date('Y');
			$createdat 		= date('Y-m-d H:i:s');

			// INSERT ATTACHMENT
			$jmlfile 		= count($filename);
			for($ia=0;$ia<$jmlfile;$ia++) {
		        $direktori[$ia] 		= './attachment/';
				$lokasi_file[$ia]    	= $_FILES['attach']['tmp_name'][$ia];
				$tipe_file[$ia]      	= $_FILES['attach']['type'][$ia];
				$nama_file[$ia]      	= $_FILES['attach']['name'][$ia];
				$nama_file_unik[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file[$ia]);

		        // $allowed = array('pdf','doc','docx','');
				
				$extension = pathinfo($nama_file[$ia], PATHINFO_EXTENSION);

				// if(!in_array(strtolower($extension), $allowed)){
				// 	echo '{"status":"error"}';
				// 	exit;
				// }
				
				if (!empty($lokasi_file[$ia])){
					//direktori gambar
					$vfile_upload[$ia] = $direktori[$ia] . $nama_file_unik[$ia];

					//Simpan gambar dalam ukuran sebenarnya
					move_uploaded_file($lokasi_file[$ia], $vfile_upload[$ia]);
					
					// INSERT IMAGE Room
					$insAttach 	= $this->db->query("INSERT INTO sbrattach (no_request,file) values ('$norequest','$nama_file_unik[$ia]')");
				}
			}

			// INSERT NEW PENGAJUAN
			$rows 			= $this->db->query("
							INSERT INTO sbrdoc (no_request, nipnas, nama_project, subject, latar_belakang, aspek_strategis, aspek_finansial, aspek_kompetisi, konfigurasi_teknis, status, created_by, created_at, service_id, current,no_order_ncx,cfu)
							VALUES
							('$norequest', '$pelanggan', '$namaproject', '$subject', '$latarbelakang', '$aspekstrategis', '$aspekfinansial', '$aspekkompetisi', '$konfigurasiteknis', '$status', '$userid', '$createdat'::TIMESTAMP, '$serviceid', '$current','$no_order_ncx','W')
							");
			
			$id				= $this->db->insert_id('mi.sbrdoc_id_seq');
			$url 			= "Pengajuan SBR";
			$activity 		= "INSERT";

			if($rows) {
				// DELETE FIRST LEVEL FLOW
				$delFlow 	= $this->db->query("DELETE FROM sbrdoc_flow where no_request='$norequest'");

				// INSERT LEVEL FLOW CREATED
				$insAFC 	= $this->db->query("
							INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
							('$norequest', '$userid', '1', '0', 'Account Manager')
							");

				// INSERT LEVEL FLOW AUTO SELECTED
				$getLevA 	= $this->db->query("
							SELECT a.*,
							(select userid from mi.user where level_user=a.id_level limit 1) userid
							from mi.level_user a
							where is_select=0 and level not in ('0','1') 
							order by level
							")->result_array();
				foreach ($getLevA as $levA) {
					$useridAS 	= $levA['userid'];
					$levelAS 	= $levA['level'];
					$isfinalAS	= $levA['is_final'];
					$rolenameAS	= $levA['name'];

					$insAFAS 	= $this->db->query("
								INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
								('$norequest', '$useridAS', '$levelAS', '$isfinalAS', '$rolenameAS')
								");
				}


				// INSERT LEVEL FLOW SELECTED
				$jmlAppr	= count($appflow);
				for($ja=0;$ja<$jmlAppr;$ja++) {
					$useridlev 	= $appflow[$ja];

					$getLev 	= $this->db->query("
								SELECT a.userid, b.level, b.is_final, b.name
								from mi.user a 
								left join level_user b 
								on a.level_user=b.id_level 
								where userid='$useridlev'
								order by b.level
								")->result_array();
					$dLev 		= array_shift($getLev);
					$levelFS 	= $dLev['level'];
					$isfinal 	= $dLev['is_final'];
					$rolename 	= $dLev['name'];

					$insAF 		= $this->db->query("
								INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
								('$norequest', '$useridlev', '$levelFS', '$isfinal', '$rolename')
								");

					$getFS 		= $this->db->query("
								SELECT a.userid, b.level, b.is_final 
								from mi.user a 
								left join level_user b 
								on a.level_user=b.id_level 
								where  level=2 and userid='$useridlev'
								order by b.level
								")->result_array();
					foreach ($getFS as $dFS) { $firststep 	= $dFS['userid']; }
				}

				// INSERT HISTORY AND NOTIFICATIONS
				if ($type!='draft') {
					$this->sendMailNotif($norequest,$userid,$firststep,'new');

					$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', 'Pengajuan SBR Baru', '$userid', '$createdat'::TIMESTAMP, 'new', '$firststep' ,'0')
								");
				}

				// BEGIN INSERT ANSWER IN PENGAJUAN
				$layanan		= $_POST['layanan'];
				$volume			= $_POST['volume'];
				$tarif			= $_POST['tarif']; 
				$price			= $_POST['price']; 
				$analisateknis 	= str_replace("'","`",$_POST['analisateknis']);
				$ceMaxAns 		= $this->db->query("SELECT MAX(id)+1 maxid from sbranswer where EXTRACT(YEAR FROM created_at) = '".$Y."'")->result_array();
				$gMaxAns		= array_shift($ceMaxAns);
				if ($gMaxAns['maxid']==0) {
					$lastidAns 	= '1';
				} else {
					$lastidAns 	= $gMaxAns['maxid'];
				}

				$code 			= sprintf("%06d", $lastid);
				$maxid 			= $code;

				$noanswer 		= 'TEL'.$lastidAns.'/SBR-B/J/'.date('m').'/'.date('Y').'';

				$insAns 	= $this->db->query("
								INSERT INTO sbranswer (no_answer, no_request,userid, analisa_teknis,created_by, created_at, updated_at,updated_by) VALUES
								('$noanswer', '$norequest', '$userid', '$analisateknis' ,'$userid', '$createdat'::TIMESTAMP,null,null)
								"); 
				if($insAns){
					$jmLayanan = count($layanan);
					for($x=0;$x<$jmLayanan;$x++){
							$insAnsDet 	= $this->db->query("
								INSERT INTO sbranswer_detail (no_answer, layanan,volume, tarif,price) VALUES
								('$noanswer', '".$layanan[$x]."', '".$volume[$x]."', '".$tarif[$x]."' ,'".$price[$x]."')
								"); 						
					}
				}
				//END INSERT ANSWER IN PENGAJUAN
				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
		} else {
			redirect('/panel');
		}
	}	

	public function jmlsbr($status){
		if(checkingsessionpwt()){
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];

			$getUser 		= $this->db->query("
								SELECT a.*, 
									(SELECT level from mi.level_user where id_level=a.level_user) step,
									(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
								FROM mi.user a where userid='$userid'
							")->result_array();
			$dUser 			= array_shift($getUser);
			$level			= $dUser['step'];

			$cond 			= "
							and (no_request in (
								SELECT no_request from mi.sbrhistory where created_by='$userid' or send_to='$userid'
							) or created_by='$userid')
							";
			$getPending 	= $this->db->query("SELECT * from sbrdoc where status not in (4,5) $cond")->num_rows();

			echo $getPending.' <span>Record</span>';
		} else {
			redirect('/panel');
		}
	}	

	public function modal(){
		if(checkingsessionpwt()){
			
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			
			$dataUsr			= $this->db->query("
								SELECT a.* ,
									(SELECT name from mi.user where userid=a.created_by) name_am,
									(SELECT username from mi.user where userid=a.reviewer1) usernamerev,
									(SELECT name from mi.user where userid=a.reviewer1) namerev1,
									(SELECT username from mi.user where userid=a.approval1) usernameappr1,
									(SELECT name from mi.user where userid=a.approval1) nameappr1,
									(SELECT username from mi.user where userid=a.approval2) usernameappr2,
									(SELECT name from mi.user where userid=a.approval2) nameappr3,
									(SELECT username from mi.user where userid=a.approval3) usernameappr3,
									(SELECT name from mi.user where userid=a.approval3) nameappr3,
									(SELECT username from mi.user where userid=a.timtarif1) usernametrf1,
									(SELECT name from mi.user where userid=a.timtarif1) nametrf1,
									(SELECT username from mi.user where userid=a.timtarif2) usernametrf2,
									(SELECT name from mi.user where userid=a.timtarif2) nametrf2,
									(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
									(SELECT analisa_teknis from mi.sbranswer where no_request=a.no_request) analisa_teknis,
									(SELECT no_answer from mi.sbranswer where no_request=a.no_request) no_answer
								FROM sbrdoc a where id='$id'
								")->result_array();
			
			header('Content-type: application/json; charset=UTF-8');
			
			if (isset($id) && !empty($id)) {
				foreach($dataUsr as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	
	public function getlayanan($id){
		if(checkingsessionpwt()){			
			$datalayanan		= $this->db->query("
								select 
								d.*
								from mi.sbrdoc a
								left join mi.sbranswer n on n.no_request = a.no_request
								left join mi.sbranswer_detail d on d.no_answer = n.no_answer
								where a.id='".$id."'
								")->result_array();
			$form = '';
			foreach($datalayanan as $layanan){
				$form .='<div class="row"><div class="col-lg-3"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label>Layanan *</label><div class="input-group"><input type="text" name="ed_layanan[]" class="form-control" id="ed_layanan" placeholder="Layanan" value="'.$layanan['layanan'].'"></div></div></div> </div><div class="col-lg-3"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label>Volume *</label><div class="input-group"><input type="text" name="ed_volume[]" class="form-control" id="ed_volume" placeholder="Volume" value="'.$layanan['volume'].'"></div></div></div> </div><div class="col-lg-3"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label>Tarif *</label><div class="input-group"><input type="text" name="ed_tarif[]" class="form-control" id="ed_tarif" placeholder="Tarif" value="'.$layanan['tarif'].'"></div></div></div> </div><div class="col-lg-2"><div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label>Price *</label><div class=""input-group"><input type="text" name="ed_price[]" class="form-control" id="ed_price" placeholder="Price" value="'.$layanan['price'].'"></div></div></div></div><a href="#" class="ed_remove_field col-lg-1"><br><button type="button" class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="remove"><i class="la la-close"></i></button></a></div>';
			}
			echo $form;
		} else {
			redirect('/panel');
		}
	}	

	public function update($type){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$now 		= date('ymd');

			$id 			= trim(strip_tags(stripslashes($this->input->post('ed_id',true))));
			$norequest 		= trim(strip_tags(stripslashes($this->input->post('ed_norequest',true))));
			$statusbefore 	= trim(strip_tags(stripslashes($this->input->post('statusbefore',true))));
			$namaproject 	= trim(strip_tags(stripslashes($this->input->post('ed_namaproject',true))));
			$subject 		= trim(strip_tags(stripslashes($this->input->post('ed_subject',true))));
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('ed_pelanggan',true))));
			$serviceid 		= trim(strip_tags(stripslashes($this->input->post('ed_serviceid',true))));
			$no_order_ncx	= trim(strip_tags(stripslashes($this->input->post('ed_no_order_ncx',true))));
			
			$appflow 		= $this->input->post('ed_appflow',true);

			// $reviewer1 		= trim(strip_tags(stripslashes($this->input->post('ed_reviewer1',true))));
			// $approval1 		= trim(strip_tags(stripslashes($this->input->post('ed_approval1',true))));
			// $approval2 		= trim(strip_tags(stripslashes($this->input->post('ed_approval2',true))));
			// $timtarif1 		= trim(strip_tags(stripslashes($this->input->post('ed_timtarif1',true))));
			// $timtarif2 		= trim(strip_tags(stripslashes($this->input->post('ed_timtarif2',true))));
			// $approval3 		= trim(strip_tags(stripslashes($this->input->post('ed_approval3',true))));
			
			$latarbelakang 	= str_replace("'",'`',$_POST['ed_latarbelakang']);
			$aspekstrategis = str_replace("'",'`',$_POST['ed_aspekstrategis']);
			$aspekfinansial = str_replace("'",'`',$_POST['ed_aspekfinansial']);
			$aspekkompetisi = str_replace("'",'`',$_POST['ed_aspekkompetisi']);
			$konfigurasiteknis = str_replace("'",'`',$_POST['ed_konfigurasiteknis']);

			// GET CURRENT LEVEL
			$getCL 		= $this->db->query("SELECT current+1 as nextstep FROM sbrdoc where no_request='$norequest'")->result_array();
			foreach($getCL as $dCL) {
				$curlevel	= $dCL['nextstep'];
			}

			$filename 		= @$_FILES['ed_attach']['name'];


			if ($type=='draft') {
				$status 	= '0';
				$uptstatus 	= '';
				$upcurrent	= '';
			} else {
				$status 	= '1';
				$current 	= $curlevel;
				$uptstatus 	= "status = '".$status."',";
				$upcurrent	= "current = '".$current."',";
			}

			$createdat 		= date('Y-m-d H:i:s');

			if(!empty($filename)) {
				$jmlfile 		= count($filename);
				for($ia=0;$ia<$jmlfile;$ia++) {
			        $direktori[$ia] 		= './attachment/';
					$lokasi_file[$ia]    	= $_FILES['ed_attach']['tmp_name'][$ia];
					$tipe_file[$ia]      	= $_FILES['ed_attach']['type'][$ia];
					$nama_file[$ia]      	= $_FILES['ed_attach']['name'][$ia];
					$nama_file_unik[$ia] 	= str_replace(' ','_',date('Ymd').'_'.$nama_file[$ia]);

			        $allowed = array('pdf');
					
					$extension = pathinfo($nama_file[$ia], PATHINFO_EXTENSION);

					if(!in_array(strtolower($extension), $allowed)){
						echo '{"status":"error"}';
						exit;
					}
					
					if (!empty($lokasi_file[$ia])){
						//direktori gambar
						$vfile_upload[$ia] = $direktori[$ia] . $nama_file_unik[$ia];

						//Simpan gambar dalam ukuran sebenarnya
						move_uploaded_file($lokasi_file[$ia], $vfile_upload[$ia]);
						
						// INSERT IMAGE Room
						$insAttach 	= $this->db->query("INSERT INTO sbrattach (no_request,file) values ('$norequest','$nama_file_unik[$ia]')");
					}
				}
			}

			// UPDATE PENGAJUAN
			$rows 			= $this->db->query("
							UPDATE sbrdoc set 
								nipnas				= '$pelanggan',
								nama_project		= '$namaproject',
								subject				= '$subject',
								latar_belakang		= '$latarbelakang',
								aspek_strategis		= '$aspekstrategis',
								aspek_finansial		= '$aspekfinansial',
								aspek_kompetisi		= '$aspekkompetisi',
								konfigurasi_teknis	= '$konfigurasiteknis',
								$uptstatus
								updated_by			= '$userid',
								updated_at			= '$createdat'::TIMESTAMP,
								$upcurrent
								service_id			= '$serviceid',
								no_order_ncx 		= '$no_order_ncx'
							WHERE id='$id'
							");
			
			$url 			= "Pengajuan SBR";
			$activity 		= "UPDATE";

			if($rows) {
				// DELETE FIRST LEVEL FLOW
				$delFlow 	= $this->db->query("DELETE FROM sbrdoc_flow where no_request='$norequest' and level not in ('1')");

				// INSERT LEVEL FLOW CREATED
				// $insAFC 	= $this->db->query("
				// 			INSERT INTO sbrdoc_flow (no_request, userid, level, is_final) VALUES
				// 			('$norequest', '$userid', '1', '0')
				// 			");

				// INSERT LEVEL FLOW AUTO SELECTED
				$getLevA 	= $this->db->query("
							SELECT a.*,
							(select userid from mi.user where level_user=a.id_level limit 1) userid
							from mi.level_user a
							where is_select=0 and level not in ('0','1') 
							order by level
							")->result_array();
				foreach ($getLevA as $levA) {
					$useridAS 	= $levA['userid'];
					$levelAS 	= $levA['level'];
					$isfinalAS	= $levA['is_final'];
					$rolenameAS	= $levA['name'];

					$insAFAS 	= $this->db->query("
								INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
								('$norequest', '$useridAS', '$levelAS', '$isfinalAS', '$rolenameAS')
								");
				}


				// INSERT LEVEL FLOW SELECTED
				$jmlAppr	= count($appflow);
				for($ja=0;$ja<$jmlAppr;$ja++) {
					$useridlev 	= $appflow[$ja];

					$getLev 	= $this->db->query("
								SELECT a.userid, b.level, b.is_final, b.name
								from mi.user a 
								left join level_user b 
								on a.level_user=b.id_level 
								where userid='$useridlev'
								order by b.level
								")->result_array();
					$dLev 		= array_shift($getLev);
					$levelFS 	= $dLev['level'];
					$isfinal 	= $dLev['is_final'];
					$rolename 	= $dLev['name'];

					$insAF 		= $this->db->query("
								INSERT INTO sbrdoc_flow (no_request, userid, level, is_final, role_name) VALUES
								('$norequest', '$useridlev', '$levelFS', '$isfinal', '$rolename')
								");

					$getFS 		= $this->db->query("
								SELECT a.userid, b.level, b.is_final 
								from mi.user a 
								left join level_user b 
								on a.level_user=b.id_level 
								where  level=2 and userid='$useridlev'
								order by b.level
								")->result_array();
					foreach ($getFS as $dFS) { $firststep 	= $dFS['userid']; }

					if ($statusbefore!=0) {
						$getNS 		= $this->db->query("
									SELECT a.userid, b.level, b.is_final, b.name
									from mi.user a 
									left join level_user b 
									on a.level_user=b.id_level 
									where  level='$curlevel' and userid='$useridlev'
									order by b.level
									")->result_array();
						foreach ($getNS as $dNS) { $nextstep 	= $dNS['userid']; }
					}
				}

				// INSERT UPDATE HISTORY AND NOTIFICATIONS
				if ($type!='draft') {
					$ReadNotif 		= $this->db->query("UPDATE sbrhistory set is_read='1' where no_request='$norequest' and send_to='$userid'");
					if ($statusbefore==2) {
						$this->sendMailNotif($norequest,$userid,$nextstep,'republish');
						$insHis 	= $this->db->query("
									INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
									('$norequest', 'Perbaikan Pengajuan SBR', '$userid', '$createdat'::TIMESTAMP, 'republish', '$nextstep' ,'0')
									");
					} else {
						if ($statusbefore==0) {
							$this->sendMailNotif($norequest,$userid,$firststep,'new');
							$insHis 	= $this->db->query("
										INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
										('$norequest', 'Pengajuan SBR Baru', '$userid', '$createdat'::TIMESTAMP, 'new', '$firststep' ,'0')
										");
						} else {
							$this->sendMailNotif($norequest,$userid,$nextstep,'update');
							$insHis 	= $this->db->query("
										INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
										('$norequest', 'Perbaikan dan Eskalasi SBR', '$userid', '$createdat'::TIMESTAMP, 'update', '$nextstep' ,'0')
										");
						}
					}
				}
				// BEGIN INSERT ANSWER IN PENGAJUAN
				$getAns 		= $this->db->query("SELECT * from sbranswer where no_request='".$norequest."'")->result_array();
				$rowAns	  		= array_shift($getAns);
				$noanswer 		= $rowAns['no_answer'];
				$layanan		= $_POST['ed_layanan'];
				$volume			= $_POST['ed_volume'];
				$tarif			= $_POST['ed_tarif']; 
				$price			= $_POST['ed_price']; 
				$analisateknis 	= str_replace("'","`",$_POST['ed_analisateknis']);
				$updateAns 		= $this->db->query("UPDATE sbranswer SET analisa_teknis='".$analisateknis."' WHERE no_request='".$norequest."'"); 
				if($updateAns){
					$delDetail 	= $this->db->query("DELETE FROM sbranswer_detail where no_answer='$noanswer'");
					$jmLayanan = count($layanan);
					for($x=0;$x<$jmLayanan;$x++){
							$insAnsDet 	= $this->db->query("
								INSERT INTO sbranswer_detail (no_answer, layanan,volume, tarif,price) VALUES
								('$noanswer', '".$layanan[$x]."', '".$volume[$x]."', '".$tarif[$x]."' ,'".$price[$x]."')
								"); 						
					}
				}
				//END INSERT ANSWER IN PENGAJUAN
				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
			$log = $this->query->insertlog($activity,$url,$id);
		} else {
			redirect('/panel');
		}
	}	

	public function delete(){
		if(checkingsessionpwt()){
			$url 		= "Pengajuan";
			$activity 	= "DELETE";
			
			$this->load->model('query');
			
			$cond		= trim(strip_tags(stripslashes($this->input->post('iddel',true))));

			$getnoreq 	= $this->db->query("SELECT * FROM sbrdoc where id='$cond'")->result_array();
			$noreq 		= array_shift($getnoreq);
			$norequest 	= $noreq['no_request'];

			$gAttach 	= $this->db->query("SELECT * FROM sbrattach where no_request='$norequest'")->result_array();
			foreach ($gAttach as $attach) {
				$dataexis = 'attachment/'.$attach['file'];
				@unlink($dataexis);
			}
			
			$rows 		= $this->db->query("DELETE FROM sbrdoc where id='$cond'");
			
			if(isset($rows)) {
				$del 	= $this->db->query("DELETE FROM sbrhistory where no_request='$norequest'");
				$del2 	= $this->db->query("DELETE FROM sbrattach where no_request='$norequest'");
				$del3 	= $this->db->query("DELETE FROM sbrdoc_flow where no_request='$norequest'");
				$log 	= $this->query->insertlog($activity,$url,$cond);
				print json_encode(array('success'=>true,'rows'=>$rows, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function actionsubmit(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$createdat 	= date('Y-m-d H:i:s');

			$id			= trim(strip_tags(stripslashes($this->input->post('idsubmitact',true))));
			$comment	= $_POST['comment'];
			$gtype		= trim(strip_tags(stripslashes($this->input->post('typesubmitact',true))));
			$type 		= strtolower($gtype);

			$url 		= "Pengajuan";
			$activity 	= $type;

			$getdoc 	= $this->db->query("SELECT * FROM sbrdoc where id='$id'")->result_array();
			$doc 		= array_shift($getdoc);
			$norequest 	= $doc['no_request'];
			$curlevel 	= $doc['current'];
			$nextlevel 	= $doc['current']+1;
			$prevlevel 	= $doc['current']-1;

			$getUser 	= $this->db->query("
						SELECT a.*, 
							(SELECT level from level_user where id_level=a.level_user) step,
							(SELECT action from level_user where id_level=a.level_user) actionbtn
						FROM mi.user a where userid='$userid'
						")->result_array();
			$dUser 		= array_shift($getUser);
			$level		= $dUser['step'];

			if ($type=='submit') {
				$getST 		= $this->db->query("
							SELECT distinct(a.userid) userid
							from mi.user a 
							left join level_user b 
							on a.level_user=b.id_level 
							where  level='2' and 
							userid in (SELECT userid from sbrdoc_flow where no_request='$norequest')
							order by b.level
							")->result_array();
				foreach ($getST as $dST) { $sendto 	= $dST['userid']; }

				$curr 	= 2;
			} else if ($type=='resubmit') {
				$getST 		= $this->db->query("
							SELECT distinct(a.userid) userid
							from mi.user a 
							left join level_user b 
							on a.level_user=b.id_level 
							where  level='$nextlevel' and 
							userid in (SELECT userid from sbrdoc_flow where no_request='$norequest')
							")->result_array();
				foreach ($getST as $dST) { $sendto 	= $dST['userid']; }
				$curr 	= $nextlevel;
			} else if ($type=='escalation') {
				$getST 		= $this->db->query("
							SELECT distinct(a.userid) userid
							from mi.user a 
							left join level_user b 
							on a.level_user=b.id_level 
							where  level='$nextlevel' and 
							userid in (SELECT userid from sbrdoc_flow where no_request='$norequest')
							")->result_array();
				foreach ($getST as $dST) { $sendto 	= $dST['userid']; }
				$curr 	= $nextlevel;
			} else if ($type=='return') {
				$getST 		= $this->db->query("
							SELECT distinct(a.userid) userid
							from mi.user a 
							left join level_user b 
							on a.level_user=b.id_level 
							where  level='$prevlevel' and 
							userid in (SELECT userid from sbrdoc_flow where no_request='$norequest')
							")->result_array();
				foreach ($getST as $dST) { $sendto 	= $dST['userid']; }
				$curr 	= $prevlevel;
			} else if ($type=='reject') {
				$sendto = $doc['created_by'];
				$curr 	= 99;
			} else if ($type=='approve') {
				$sendto = $doc['created_by'];
				$curr 	= 99;
			} else {
				$sendto = 0;
			}

			if ($type=='return') {
				$newstatus = 2;
			} else if ($type=='escalation') {
				$newstatus = 3;
			} else if ($type=='reject') {
				$newstatus = 5;
			} else if ($type=='approve') {
				$newstatus = 4;
			} else if ($type=='submit') {
				$newstatus = 1;
			} else {
				$newstatus = 99;
			}
			
			$ReadNotif 		= $this->db->query("UPDATE sbrhistory set is_read='1' where no_request='$norequest' and send_to='$userid'");
			
			$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', '$comment', '$userid', '$createdat'::TIMESTAMP, '$type', '$sendto' ,'0')
								");
			
			if(isset($insHis)) {
				// UPDATE DOC
				$insHis 	= $this->db->query("
								UPDATE sbrdoc SET
									status 	= '$newstatus',
									current = '$curr'
								where id='$id'
								");
				// SEND EMAIL NOTIF
				$e_noreq 	= str_replace('/', '-', $norequest);
				$this->sendMailNotif($e_noreq,$userid,$sendto,$type);

				$log 	= $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'rows'=>$insHis, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function actionapprove(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$createdat 	= date('Y-m-d H:i:s');
			$now 		= date('ymd');

			$id 			= trim(strip_tags(stripslashes($this->input->post('id',true))));
			$id_no_answer	= trim(strip_tags(stripslashes($this->input->post('id_no_answer',true))));
			$comment		= 'Approve SBR';
			$layanan		= $_POST['layanan'];
			$volume			= $_POST['volume'];
			$tarif			= $_POST['tarif']; 
			$price			= $_POST['price']; 
			$analisateknis 	= str_replace("'","`",$_POST['analisateknis']);
			$type 			= 'approve';
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('app_nopelanggan',true))));
			$app_userid 	= trim(strip_tags(stripslashes($this->input->post('app_userid',true))));	
					
			$url 		= "Pengajuan";
			$activity 	= $type;

			$getdoc 	= $this->db->query("SELECT * FROM sbrdoc where id='$id'")->result_array();
			$doc 		= array_shift($getdoc);
			$norequest 	= $doc['no_request'];

			$getUser 	= $this->db->query("
						SELECT a.*, 
							(SELECT level from mi.level_user where id_level=a.level_user) step,
							(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
						FROM mi.user a where userid='$userid'
						")->result_array();
			$dUser 		= array_shift($getUser);
			$level		= $dUser['step'];

			$sendto = $doc['created_by'];
			$curr 	= 99;

			$newstatus  = 4;
			$ReadNotif 	= $this->db->query("UPDATE sbrhistory set is_read='1' where no_request='$norequest' and send_to='$userid'");
			$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', '$comment', '$userid', '$createdat'::TIMESTAMP, '$type', '$sendto' ,'0')
								");
			
			if(isset($insHis)) {
				// SEND EMAIL NOTIF
				$e_noreq 	= str_replace('/', '-', $norequest);
				$this->sendMailNotif($e_noreq,$userid,$sendto,$type);

				// UPDATE DOC
				$insHis 	= $this->db->query("
								UPDATE sbrdoc SET
									status 	= '$newstatus',
									current = '$curr'
								where no_request='$norequest'
								");
				$cekMax 		= $this->db->query("SELECT MAX(id)+1 maxid from sbranswer")->result_array();
				$gMax 			= array_shift($cekMax);
				if ($gMax['maxid']==0) {
					$lastid 	= '1';
				} else {
					$lastid 	= $gMax['maxid'];
				}

				$code 			= sprintf("%06d", $lastid);
				$maxid 			= $code;

				$noanswer 		= 'SBR/A/'.$now.'/'.$pelanggan.'/'.$maxid;

				$insAns 	= $this->db->query("
								INSERT INTO sbranswer (no_answer, no_request,userid, analisa_teknis,created_by, created_at, updated_at,updated_by) VALUES
								('$noanswer', '$norequest', '$app_userid', '$analisateknis' ,'$userid', '$createdat'::TIMESTAMP,null,null)
								"); 
				if($insAns){
					$jmLayanan = count($layanan);
					for($x=0;$x<$jmLayanan;$x++){
							$insAnsDet 	= $this->db->query("
								INSERT INTO sbranswer_detail (no_answer, layanan,volume, tarif,price) VALUES
								('$noanswer', '".$layanan[$x]."', '".$volume[$x]."', '".$tarif[$x]."' ,'".$price[$x]."')
								"); 						
					}
				}

				$log 	= $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'rows'=>$insHis, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function listcustomer(){
		if(checkingsessionpwt()){
			$key 		= $_GET['q'];
			$getData 	= $this->db->query("
						SELECT * FROM customer where nipnas like '%$key%' or upper(nama_pelanggan) like upper('%$key%') order by nama_pelanggan asc
						")->result_array();
			$count 		= $this->db->query("SELECT * FROM customer where nipnas like '%$key%' or upper(nama_pelanggan) like upper('%$key%')")->num_rows();

			header('Content-type: application/json; charset=UTF-8');
			$json['total_count'] 		= $count;
			$json['incomplete_results'] = true;
			foreach($getData as $row) {
				$json['items'][] 	= array(
					'id'			=> $row['nipnas'],
					'nipnas'		=> $row['nipnas'],
					'nama_pelanggan'=> $row['nama_pelanggan'],
					'text'			=> $row['nama_pelanggan'],
				);
			}
			echo json_encode($json);
		}else{
            redirect('/login');
        }
	}

	public function listapproval($level){
		if(checkingsessionpwt()){
			$key 		= @$_GET['q'];
			$getData 	= $this->db->query("
						SELECT * FROM (
							SELECT a.* ,
								(SELECT level from mi.level_user where id_level=a.level_user) step
							FROM mi.user a
						) as base
						where step='$level' and (upper(username) like upper('%$key%') or upper(name) like upper('%$key%'))
						order by name asc
						")->result_array();
			$count 		= $this->db->query("
						SELECT * FROM (
							SELECT a.* ,
								(SELECT level from mi.level_user where id_level=a.level_user) step
							FROM mi.user a
						) as base
						where step='$level' and (upper(username) like upper('%$key%') or upper(name) like upper('%$key%'))
						")->num_rows();

			header('Content-type: application/json; charset=UTF-8');
			$json['total_count'] 		= $count;
			$json['incomplete_results'] = true;
			foreach($getData as $row) {
				$json['items'][] 	= array(
					'id'			=> $row['userid'],
					'username'		=> $row['username'],
					'name'			=> $row['name'],
					'text'			=> $row['username'].' - '.$row['name'],
				);
			}
			echo json_encode($json);
		}else{
            redirect('/login');
        }
	}

	public function sendMailNotif($id,$sendby,$sendto,$type) {
		$norequest 	= str_replace('-','/',$id);
		$subject 	= 'SBR Online - '.$norequest.'';

		$getSendby 	= $this->db->query("SELECT * FROM mi.user where userid='$sendby'")->result_array();
		$dataSendby	= array_shift($getSendby);
		$nameby		= $dataSendby['name'];
		$emailby	= $dataSendby['email'];

		$getSendto 	= $this->db->query("SELECT * FROM mi.user where userid='$sendto'")->result_array();
		$dataSendto	= array_shift($getSendto);
		$nameto		= $dataSendto['name'];
		$emailto	= $dataSendto['email'];

		if ($type=='new') {
			$notiftext 	= 'Pengajuan baru dari';
		} else if ($type=='republish') {
			$notiftext 	= 'Perbaikan Pengajuan dari';
		} else if ($type=='escalation') {
			$notiftext 	= 'Eskalasi Pengajuan dari';
		} else if ($type=='reject') {
			$notiftext 	= 'Pengajuan tidak disetujui oleh';
		} else if ($type=='approve') {
			$notiftext 	= 'Pengajuan disetujui oleh';
		} else if ($type=='return') {
			$notiftext 	= 'Pengajuan dikembalikan oleh';
		}

		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'blast.telkom.co.id',
		'smtp_port' => 25,
		'smtp_user' => '402868', // change it to yours
		'mailtype'  => 'html',
		'charset'   => 'iso-8859-1'
		);

		//Email content
		$htmlContent = '';
		$htmlContent .= '
			<!DOCTYPE html>
			<html>
			<head>
			  <meta charset="utf-8" />
			  <title>No Reply</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
			  <style>
			  </style>
			</head>
			<body style="font-family: verdana; font-size: 14px;">
				<div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
					<div id="logo" style="background: #FFF;"><img src="'.base_url().'images/logotel.png" style="max-height: 70px; margin-top: 20px;"></div>
					<div id="confirmation-message">
						<div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
							<div class="title" style="color: #1e1e1e; font-size: 24px;">
								<span style="text-transform:capitalize;">Dear, '.$nameto.'</span>
							</div>
						</div>
						<div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
							<div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
								'.$notiftext.' <b>'.$nameby.'</b>  dengan No Request : <b>'.$norequest.'</b>.<br>
								Klik <a href="'.base_url().'docdetail/'.$id.'" target="_blank" style="color: #5d78ff;">disini</a> untuk melihat detail pengajuan.
								<br><br>

								Hubungi Admin jika Anda butuh bantuan lebih lanjut.<br><br>
							</div>
						</div>
					</div>
				</div>
			</body>
			</html>
		';

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('mi@telkom.co.id'); // change it to yours
		$this->email->to($emailto);// change it to yours
		$this->email->subject($subject);
		$this->email->set_mailtype("html");
		$this->email->message($htmlContent);
		
		if($this->email->send()) {
			echo '';
		} else {
			show_error($this->email->print_debugger());
		}
		// echo $htmlContent;
	}

	public function download_old($id) { 
		if(checkingsessionpwt()){
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];
			$username 		= $userdata['username'];
			$gnoreq 		= str_replace('-','/',$id);
			$norequest 		= str_replace('_','-',$gnoreq);

			$getUser 		= $this->db->query("
								SELECT a.*, 
									(SELECT level from mi.level_user where id_level=a.level_user) step,
									(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
								FROM mi.user a where userid='$userid'
							")->result_array();
			$dUser 			= array_shift($getUser);
			$level			= $dUser['step'];
			$aksesCreate 	= $dUser['actionbtn'];

			$getDoc 		= $this->db->query("
							SELECT a.* ,
								(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
								(SELECT picture from mi.user where userid=a.created_by) pictcreated,
								(SELECT name from mi.user where userid=a.created_by) namecreated,
								(SELECT username from mi.user where userid=a.created_by) uncreated,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) poscreated,
								(SELECT picture from mi.user where userid=a.reviewer1) pictreview,
								(SELECT name from mi.user where userid=a.reviewer1) namereview,
								(SELECT username from mi.user where userid=a.reviewer1) unreview,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.reviewer1) posreview,
								(SELECT picture from mi.user where userid=a.approval1) pictappr1,
								(SELECT name from mi.user where userid=a.approval1) nameappr1,
								(SELECT username from mi.user where userid=a.approval1) unappr1,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval1) posappr1,
								(SELECT picture from mi.user where userid=a.approval2) pictappr2,
								(SELECT name from mi.user where userid=a.approval2) nameappr2,
								(SELECT username from mi.user where userid=a.approval2) unappr2,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval2) posappr2,
								(SELECT picture from mi.user where userid=a.approval3) pictappr3,
								(SELECT name from mi.user where userid=a.approval3) nameappr3,
								(SELECT username from mi.user where userid=a.approval3) unappr3,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval3) posappr3,
								(SELECT picture from mi.user where userid=a.timtarif1) picttrf1,
								(SELECT name from mi.user where userid=a.timtarif1) nametrf1,
								(SELECT username from mi.user where userid=a.timtarif1) untrf1,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.timtarif1) postrf1,
								(SELECT picture from mi.user where userid=a.timtarif2) picttrf2,
								(SELECT name from mi.user where userid=a.timtarif2) nametrf2,
								(SELECT username from mi.user where userid=a.timtarif2) untrf2,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.timtarif2) postrf2
							FROM sbrdoc a where no_request='$norequest'
							")->result_array();
			$doc 			= array_shift($getDoc);
			$getAns = $this->db->query("
					SELECT a.* ,					
						(SELECT picture from mi.user where userid=a.userid) pictcreated,
						(SELECT name from mi.user where userid=a.userid) namecreated,
						(SELECT username from mi.user where userid=a.userid) uncreated,
						(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.userid) poscreated,
						(SELECT picture from mi.user where userid=a.created_by) pictappr,
						(SELECT name from mi.user where userid=a.created_by) nameappr,
						(SELECT username from mi.user where userid=a.created_by) unappr,
						(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) posappr
					FROM sbranswer a where no_request='$norequest'
					")->result_array();
			$ans 		= array_shift($getAns);
			$no_answer	= $ans['no_answer'];
			$html 		='';
			$table 		= '';

			$qApprover 		= "
							SELECT * FROM (
								SELECT approver.*,
								(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
								FROM (
									SELECT DISTINCT(a.created_by) approver,
									(SELECT name from mi.user where userid=a.created_by) nameapp,
									(SELECT username from mi.user where userid=a.created_by) unapp,
									(SELECT level_user from mi.user where userid=a.created_by) levelapp,
									(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid
									where no_request='$norequest' and
									xb.userid=a.created_by) posisi
									from mi.sbrhistory a
									where no_request='$norequest' and action in ('escalation','approve','update')
								) as approver
							) as final
							where actionapp like '%approve%'
							order by levelapp
							";
			$cekApprover 	= $this->db->query($qApprover)->num_rows();
			$getApprover 	= $this->db->query($qApprover)->result_array();
			$cekColspan 	= $cekApprover+1;

			$qLastApprove 	= "
							SELECT * FROM (
								SELECT approver.*,
								(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
								FROM (
									SELECT DISTINCT(a.created_by) approver,
									(SELECT name from mi.user where userid=a.created_by) nameapp,
									(SELECT username from mi.user where userid=a.created_by) unapp,
									(SELECT level_user from mi.user where userid=a.created_by) levelapp,
									(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid
									where no_request='$norequest' and
									xb.userid=a.created_by) posisi
									from mi.sbrhistory a
									where no_request='$norequest' and action in ('escalation','approve','update')
								) as approver
							) as final
							where actionapp like '%approve%'
							order by levelapp desc
							limit 1
							";
			$gLastApprove	= $this->db->query($qLastApprove)->result_array();
			$dLastApprove 	= array_shift($gLastApprove);
			$lastapproval 	= $dLastApprove['unapp'];

			$html 		.='
			<style>
			#watermark {
			    position: absolute;
			    z-index: 2;
			    font-size: 14em;
			    opacity: 0.1;
			    width: 100%;
			    text-align: center;
			    top: 10%;
			    transform: rotate(-45deg);
			}
			</style>
			<div id="watermark">'.$lastapproval.'</div>
			<body style="font-size: 13px!important;">
			<span><strong><u><center>SPESIAL BUSINESS REQUEST (SBR)</center></u></strong></span><br>
			<center>NOMOR : TEL :  <b>'.$norequest.'</b></center><br><br>

			<table class="table table-bordered" border="1px" cellspacing="0px" cellpadding="3px" width="100%"  style="font-size:14px;">
				<tr>
					<td style="width: 30%">NAMA PELANGGAN</td>
					<td><b>'.$doc['nama_pelanggan'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">NIPNAS</td>
					<td><b>'.$doc['nipnas'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">SERVICE ID</td>
					<td><b>'.$doc['service_id'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">NAMA PROJECT</td>
					<td><b>'.$doc['nama_project'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">SUBJECT</td>
					<td><b>'.$doc['subject'].'</b></td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>LATAR BELAKANG</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>ASPEK STRATEGIS</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>ASPEK FINANSIAL</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>ASPEK KOMPETISI</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>KONFIGURASI TEKNIS</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<div style="page-break-inside:avoid;">
			<center><h4>PERSETUJUAN</h4></center>
			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
					<td colspan="'.$cekColspan.'"><center>'.$this->formula->TanggalIndo($doc['created_at']).'</center></td>
				</tr>
				<tr>';

					$html .= '<td width="25%"><center>Diusulkan Oleh,<br>'.$doc['poscreated'].'</center></td>';

					foreach ($getApprover as $apr) {
						$html .= '<td width="25%"><center>Disetujui Oleh,<br>'.$apr['posisi'].'</center></td>';
					}

				$html .= '</tr>
				<tr>
					<td height="100px"></td>';
					foreach ($getApprover as $apr) {
						$html .= '<td height="100px"></td>';
					}

				$html .= '</tr>
				<tr>';

					$html .= '<td><b><u>'.$doc['namecreated'].'</u><br>
						'.$doc['uncreated'].'</b><br></td>';

					foreach ($getApprover as $apr) {
						$html .= '<td><b><u>'.$apr['nameapp'].'</u><br>
						'.$apr['unapp'].'</b><br></td>'; 
					}

				$html .= '</tr>
			</table><br>
			<i style="font-size: 12px!important;">* Dokumen SBR Bisnis ini sudah melalui proses persetujuan online.</i>
			</body>';
			// echo $html;
			$this->load->library('PdfGenerator');
			$this->pdfgenerator->generate($html,'DOK-PENGAJUAN','A4','potrait'); 
		} else {
			echo "";
		}
	}
	public function download($id) {
		if(checkingsessionpwt()){
			$this->load->library('Tcpdf');
			$data['id'] = $id;
			// LOAD PDF
			$this->load->view('/panel/pengajuan/downloadpengajuan',$data);
		}else{

		}
	}
	public function uploaddok() { 
		if(checkingsessionpwt()){
			$id 			= trim(strip_tags(stripslashes($this->input->post('idreq',true))));
			$getDoc 		= $this->db->query("SELECT * FROM mi.sbrdoc where id ='$id'")->result_array();
			$doc 			= array_shift($getDoc);
			if($doc['dok_pengajuan'] !=''){
				@unlink('./upload/'.$doc['dok_pengajuan']);
			}

			$filename 		= $_FILES['dokpengajuan']['name'];
			$direktori 		= './upload/';
			$lokasi_file  	= $_FILES['dokpengajuan']['tmp_name'];
			$tipe_file    	= $_FILES['dokpengajuan']['type'];
			$nama_file   	= $_FILES['dokpengajuan']['name'];
			$nama_file_unik	= str_replace(' ','_',date('Ymd').'_'.$nama_file);

	        $allowed = array('pdf');
			
			$extension = pathinfo($nama_file, PATHINFO_EXTENSION);

			if(!in_array(strtolower($extension), $allowed)){
				echo '{"status":"error"}';
				exit;
			}
			
			if (!empty($lokasi_file)){
				//direktori gambar
				$vfile_upload = $direktori . $nama_file_unik;
				//Simpan gambar dalam ukuran sebenarnya
				move_uploaded_file($lokasi_file, $vfile_upload);
				$update 	= $this->db->query("update mi.sbrdoc set dok_pengajuan='".$nama_file_unik."' where id='".$id."'");
				echo "uploaded";
			}
		} else {
			redirect('/login');
		}
	}

	public function atteksis($id) { 
		if(checkingsessionpwt()){
			$getDoc 		= $this->db->query("SELECT * FROM mi.sbrdoc where id ='$id'")->result_array();
			$doc 			= array_shift($getDoc);
			$norequest 		= $doc['no_request'];

			$getAtt 		= $this->db->query("SELECT * FROM sbrattach where no_request='$norequest'")->result_array();

			echo '
				<div class="row">
	                <label class="col-form-label col-lg-4 col-sm-12">Uploaded Attachment</label>
	                <div class="col-lg-8 col-md-8 col-sm-12">
			';
			foreach ($getAtt as $att) {
				echo '
                    <div class="">
                        <a href="'.base_url().'attachment/'.$att['file'].'" class="kt-link kt-link-primary" target="_blank">
                        	<i class="la la-file-pdf-o"></i> '.$att['file'].'
                        </a>
                        <a href="#" class="col-lg-1 text-danger btnDeleteAttach" data-id="'.$att['id'].'" title="Delete Attachment" data-toggle="modal" data-target="#deleteAtt">
                        	<i class="fa fa-times"></i>
                        </a>
                    </div>
				';
			}
			echo '
				</div>
          	</div>
          	<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
          	';
		} else {
			redirect('/login');
		}
	}

	public function getappflow($id) { 
		if(checkingsessionpwt()){
			$getDoc 		= $this->db->query("SELECT * FROM mi.sbrdoc where id ='$id'")->result_array();
			$doc 			= array_shift($getDoc);
			$norequest 		= $doc['no_request'];
			$curlevel 		= $doc['current'];

			$getAtt 		= $this->db->query("
							SELECT * FROM (
								SELECT a.*,
								(SELECT username from mi.user where userid=a.userid) usernamelev,
								(SELECT name from mi.user where userid=a.userid) nameuserlev,
								(SELECT name from mi.level_user where level=a.level) namelev,
								(SELECT action from mi.level_user where level=a.level) actionlev,
								(SELECT is_select from level_user where level=a.level) is_select
								from sbrdoc_flow a
							) as data
							where no_request='$norequest' and is_select=1
							order by no_request asc, level asc
							")->result_array();

			foreach ($getAtt as $data) {
				$level 		= $data['level'];
				$useridlev 	= $data['userid'];
				$usernamelev= $data['usernamelev'];
				$namelev 	= $data['nameuserlev'];

				if ($level<=$curlevel) {
					$readonly 	= 'readonly';
				} else {
					$readonly 	= '';
				}

				if(strpos(strtolower($data['actionlev']),strtolower('approve')) !== false){
					$labeldis = 'Disetujui';
				} else {
					$labeldis = 'Di-Review';
				}

				if ($level>$curlevel) {
					echo '
	                    <div class="col-lg-3">
							<label class="col-form-label col-sm-12">'.$labeldis.' Oleh *<br>('.$data['namelev'].')</label>
							<div class="col-sm-12">
								<div class="input-group">
									<select name="ed_appflow[]" class="form-control ed_getAF'.$level.'" id="ed_appflow'.$level.'" data-placeholder="Pilih..." style="width: 100%;">
									</select>
								</div>
							</div>
						</div>
					';

					echo '
					<script>
			        $(".ed_getAF'.$level.'").select2({
			            placeholder: "Pilih...",
			            allowClear: true,
			            ajax: {
			                url: "'.base_url().'pengajuan/listapproval/'.$level.'",
			                dataType: "json",
			                delay: 250,
			                data: function(params) {
			                    return {
			                        q: params.term, // search term
			                        page: params.page
			                    };
			                },
			                processResults: function(data, params) {
			                    params.page = params.page || 1;

			                    return {
			                        results: data.items,
			                        pagination: {
			                            more: (params.page * 30) < data.total_count
			                        }
			                    };
			                },
			                cache: true
			            },
			            escapeMarkup: function(markup) {
			                return markup;
			            }, 
			            minimumInputLength: 0,
			            templateResult: formatRepo2ed, // omitted for brevity, see the source of this page
			            templateSelection: formatRepoSelection2ed // omitted for brevity, see the source of this page
			        });

			        function formatRepo2ed (repo) {
			          if (repo.loading) {
			            return repo.text;
			          }

			          var markup = "<div>" + repo.text + "</div>";

			          return markup;
			        }

			        function formatRepoSelection2ed (repo) {
			          return repo.text;
			        }

			        var revSelect'.$level.' = $("#ed_appflow'.$level.'");
			        revSelect'.$level.'.append(`<option value="'.$useridlev.'" selected="selected">'.$usernamelev.' - '.$namelev.'</option>`);
			        revSelect'.$level.'.val('.$useridlev.').trigger("change");
					</script>
					';
				} else {
					echo '
	                    <div class="col-lg-3">
							<label class="col-form-label col-sm-12">Disetujui Oleh *<br>('.$data['namelev'].')</label>
							<div class="col-sm-12">
								<div class="input-group">
									<select name="ed_appflow[]" class="form-control" id="" data-placeholder="Pilih..." style="width: 100%;" readonly>
										<option value="'.$useridlev.'" selected="selected">'.$usernamelev.' - '.$namelev.'</option>
									</select>
								</div>
							</div>
						</div>
					';
				}
			}
		} else {
			redirect('/login');
		}
	}

	public function modalatt(){
		if(checkingsessionpwt()){
			
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			
			$dataUsr			= $this->db->query("
								SELECT * FROM sbrattach where id='$id'
								")->result_array();
			
			header('Content-type: application/json; charset=UTF-8');
			
			if (isset($id) && !empty($id)) {
				foreach($dataUsr as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	

	public function deleteAtt(){
		if(checkingsessionpwt()){
			$url 		= "Attachment Pengajuan";
			$activity 	= "DELETE";
			
			$this->load->model('query');
			
			$cond		= trim(strip_tags(stripslashes($this->input->post('iddelatt',true))));

			$gAttach 	= $this->db->query("SELECT * FROM sbrattach where id='$cond'")->result_array();
			$attach 	= array_shift($gAttach);
			$no_req 	= $attach['no_request'];
			
			$dataexis = 'attachment/'.$attach['file'];
			@unlink($dataexis);
			
			$rows 		= $this->db->query("DELETE FROM sbrattach where id='$cond'");
			
			if(isset($rows)) {
				$getnoreq 	= $this->db->query("SELECT * FROM sbrdoc where no_request='$no_req'")->result_array();
				$noreq 		= array_shift($getnoreq);
				$idsbrdoc 	= $noreq['id'];

				$log 	= $this->query->insertlog($activity,$url,$cond);
				print json_encode(array('success'=>true,'rows'=>$rows, 'id'=>$idsbrdoc, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function testemail() {
		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'blast.telkom.co.id',
		'smtp_port' => 25,
		'smtp_user' => '402868', // change it to yours
		'mailtype'  => 'html',
		'charset'   => 'iso-8859-1'
		);

		//Email content
		$htmlContent 	= "cek email";

		$this->load->library('email');
		// $this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('mi@telkom.co.id'); // change it to yours
		$this->email->to('aspsyahputra@gmail.com');// change it to yours
		$this->email->subject('test');
		$this->email->set_mailtype("html");
		$this->email->message($htmlContent);
		
		// $result = $this->email
		// 		->set_newline("\r\n")
		// 		->from('survey@telkom.co.id' ,'noreply')
		// 		->to('aspsyahputra@gmail.com')
		// 		->subject('test')
		// 		->message($htmlContent)
		// 		->set_mailtype('html')
		// 		->send();

		// if($result){
		// 		echo "1";
		// 	}else{
		// 		echo "0";
		// 	}
		if($this->email->send()) {
			echo '';
		} else {
			show_error($this->email->print_debugger());
		}
		// echo $htmlContent;
	}
}
