<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workflow extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $profile;
	private $divisiUAM;
	private $segmenUAM;
	private $tregUAM;
	private $witelUAM;
	private $amUAM;
	
	public function __construct(){
		date_default_timezone_set("Asia/Bangkok");
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('query'); 
		$this->load->model('formula'); 
		$this->load->model('datatable'); 
		$this->load->model('roles_handler');
		$this->load->model('workflow_handler');
		
		ini_set('max_execution_time', 123456);
		ini_set("memory_limit","1256M");
			
		// $session = checkingsessionpwt();
		$session	 = $this->session->userdata('sesspwt'); 
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function getdata(){
		if(checkingsessionpwt()){

			$columnsDefault = [
				'name'		=> true,
				'level'		=> true,
				'final'		=> true,
				'actions'	=> true,
			];
			$arraynya	= $columnsDefault;
			// $jsonfile	= base_url().'roles/data';
			$jsonfile	= $this->workflow_handler->data();

			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}

	public function insert(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt');
			$level_name		= trim(strip_tags(stripslashes($this->input->post('level_name',true))));
			$level			= trim(strip_tags(stripslashes($this->input->post('level',true))));
			$isselect		= trim(strip_tags(stripslashes($this->input->post('is_select',true))))?:0;
			$action			= $_POST['action'];
			$naction 		= implode(',',$action);
			$query 			= $this->query->getData('level_user','max(id_level)+1 as id_level','');

			$getID = array_shift($query);
			if ($getID['id_level']=='') {
				$id = '1';
			} else {
				$id = $getID['id_level'];
			}
			
			$rows = $this->query->insertData('level_user', "id_level,name,level,action,is_final,is_select", "'$id','$level_name','$level','$naction','0','$isselect'");
			//$id			= $this->db->insert_id();
			$url 		= "Manage Level";
			$activity 	= "INSERT";
			$log = $this->query->insertlog($activity,$url,$id);
			if($rows) {
				//update rows level all if exist
				$queryt = $this->query->getData('level_user','*',"where level >= '".$level."' and id_level !='".$id."' ");
				$qcek 	= count($queryt);
				if($qcek < 0){

				}else{
					foreach($queryt as $qinc){
						$idl = $qinc['id_level']; 
						$increment = $qinc['level']+1;
						$updatelevel = $this->query->updateData('level_user',"level='$increment'","WHERE id_level='$idl'");	 
					}
				}		
				//update to final if max
				$querym = $this->query->getData('level_user','id_level','where level = (select max(level) from level_user lu ) order by id_level desc limit 1');
				$getM = array_shift($querym);
				$updateisfinal = $this->query->updateData('level_user',"is_final='0'","");
				$updateisfinal = $this->query->updateData('level_user',"is_final='1'","where id_level = '".$getM['id_level']."'");

				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
		} else {
			redirect('/panel');
		}
	}	

	public function modal(){
		if(checkingsessionpwt()){
			
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			
			$datarole			= $this->query->getData('level_user','*',"WHERE id_level='".$id."' ORDER BY id_level DESC");
			
			header('Content-type: application/json; charset=UTF-8');
			
			if (isset($id) && !empty($id)) {
				foreach($datarole as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	

	public function update(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$id				= trim(strip_tags(stripslashes($this->input->post('ed_id_level',true))));
			$name			= trim(strip_tags(stripslashes($this->input->post('ed_level_name',true))));
			$level			= trim(strip_tags(stripslashes($this->input->post('ed_level',true))));
			$select			= trim(strip_tags(stripslashes($this->input->post('ed_is_select',true))))?:0;
			$action			= $_POST['ed_action']; 
			$naction 		= implode(',',$action);
				
			$updaterole 	= $this->query->updateData('level_user',"name='$name',level='$level',is_select='$select',action='$naction'","WHERE id_level='$id'");
			$url 			= "Manage Level";
			$activity 		= "UPDATE";
			if($updaterole) {
				//update rows level all if exist
				// $queryt = $this->query->getData('level_user','*',"where level >= '".$level."' and id_level !='".$id."' ");
				// $qcek 	= count($queryt);
				// if($qcek < 0){

				// }else{
				// 	foreach($queryt as $qinc){
				// 		$idl = $qinc['id_level']; 
				// 		$increment = $qinc['level']+1;							
				// 		$updatelevel = $this->query->updateData('level_user',"level='$increment'","WHERE id_level='$idl'");	 
				// 	}
				// }		
				//update to final if max
				// $querym = $this->query->getData('level_user','id_level','where level = (select max(level) from level_user lu ) order by id_level desc limit 1');
				// $getM = array_shift($querym);
				// $updateisfinal = $this->query->updateData('level_user',"is_final='0'","");
				// $updateisfinal = $this->query->updateData('level_user',"is_final='1'","where id_level = '".$getM['id_level']."'");
				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
		} else {
			redirect('/panel');
		}
	}	

	public function delete(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt');
			$cond		= trim(strip_tags(stripslashes($this->input->post('iddelroles',true))));
			//update rows level all if exist
			$queryx = $this->query->getData('level_user','*',"where id_level ='".$cond."' ");
			$levelx = array_shift($queryx);

			$queryt = $this->query->getData('level_user','*',"where level > '".$levelx['level']."'");
			$qcek 	= count($queryt);
			if($qcek < 0){

			}else{
				foreach($queryt as $qinc){
					$idl = $qinc['id_level']; 
					$increment = $qinc['level']-1;
					$updatelevel = $this->query->updateData('level_user',"level='$increment'","WHERE id_level='$idl'");	 
				}
			}		
			//update to final if max
			$querym = $this->query->getData('level_user','id_level','where level = (select max(level) from level_user lu ) order by id_level desc limit 1');
			$getM = array_shift($querym);
			$updateisfinal = $this->query->updateData('level_user',"is_final='0'","");
			$updateisfinal = $this->query->updateData('level_user',"is_final='1'","where id_level = '".$getM['id_level']."'");

			$rows = $this->query->deleteData('level_user','id_level',$cond);
			$url 		= "Manage Level";
			$activity 	= "DELETE";
			
			$log = $this->query->insertlog($activity,$url,$cond);

			
			if(isset($rows)) {
				$log = $this->query->insertlog($activity,$url,$cond);
				print json_encode(array('success'=>true,'rows'=>$rows, 'id'=>$cond ,'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}
	public function getdataisselect($value){
		if($value==1){
				$check = '<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
					<input value="1" id="ed_is_checked" type="checkbox" name="ed_is_select" checked="checked">
					<span></span>
				</label>';
		}else{
				$check = '<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
					<input value="1" id="ed_is_checked" type="checkbox" name="ed_is_select">
					<span></span>
				</label>';
		}
		echo $check;

	}
	public function getdataselect2($id){
		if(checkingsessionpwt()){
			$datalevel		= $this->query->getData('level_user','*',"where id_level='$id'");

			$datal 			= array_shift($datalevel);
			$expmod			= explode(',',$datal['action']);
			if(in_array("approve", $expmod)){$appv = "checked='checked'";}else{$appv="";}
			if(in_array("eskalasi", $expmod)){$eskl = "checked='checked'";}else{$eskl="";}
			if(in_array("reject", $expmod)){$rej = "checked='checked'";}else{$rej="";}
			if(in_array("return", $expmod)){$ret = "checked='checked'";}else{$ret="";}
			if(in_array("update", $expmod)){$upt = "checked='checked'";}else{$upt="";}

			$ds = '
			<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
				<input value="approve" id="checkbox" type="checkbox" name="ed_action[]" '.$appv.'> Approve
				<span></span>
			</label> &nbsp;&nbsp;
			<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
				<input value="eskalasi" id="checkbox" type="checkbox" name="ed_action[]" '.$eskl.'> Eskalasi
				<span></span>
			</label> &nbsp;&nbsp;
			<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
				<input value="reject" id="checkbox" type="checkbox" name="ed_action[]" '.$rej.'> Reject
				<span></span>
			</label> &nbsp;&nbsp;
			<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
				<input value="return" id="checkbox" type="checkbox" name="ed_action[]" '.$ret.'> Return
				<span></span>
			</label> &nbsp;&nbsp;
			<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
				<input value="update" id="checkbox" type="checkbox" name="ed_action[]" '.$upt.'> Update
				<span></span>
			</label> &nbsp;&nbsp;	
			';
			echo $ds;
		} else {
			redirect('/panel');
		}
	} 
}
