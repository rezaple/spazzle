<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viewpanel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $akses;
	private $userid;
	// private $divisiUAM;
	// private $segmenUAM;
	// private $tregUAM;
	// private $witelUAM;
	// private $amUAM;
	
	public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->load->model('auth'); 
		$this->load->model('query');
		$this->load->model('formula');
		$this->load->model('datatable');
		// if(checkingsessionpwt()){
			// $session = checkingsessionpwt();
		$session	 	= $this->session->userdata('sessSpazzle'); 
		@$userid 	 	= $session['userid'];
		@$profile 	 	= $session['id_role'];
		$menu 		 	= uri_string();
		$data_akses  	= $this->query->getAkses($profile,$menu);
		$shift 		 	= array_shift($data_akses);
		@$this->akses 	= $shift['akses'];
		$this->userid 	= $userid;

		$now 			= date('Y-m-d');
		$yesterday 		= date("Y-m-d",strtotime("-1 day"));
	
		// $getAksesUAM		= $this->query->getData('user_organization',"*","WHERE userid='$userid'");
		// $dataAksesUAM		= array_shift($getAksesUAM);
		// $this->divisiUAM 	= $dataAksesUAM['akses_divisi'];
		// $this->segmenUAM 	= $dataAksesUAM['akses_segmen'];
		// $this->tregUAM 		= $dataAksesUAM['akses_treg'];
		// $this->witelUAM 	= $dataAksesUAM['akses_witel'];
		// $this->amUAM 		= $dataAksesUAM['akses_am'];
		// } else {
			
		// }
    }
	public function index(){
		if(checkingsessionpwt()){

			// $data['divisiUAM'] 	= str_replace(",","','",$this->divisiUAM);
			// $data['segmenUAM'] 	= str_replace(",","','",$this->segmenUAM);
			// $data['tregUAM'] 	= str_replace(",","','",$this->tregUAM);
			// $data['witelUAM'] 	= str_replace(",","','",$this->witelUAM);
			// $data['amUAM'] 		= str_replace(",","','",$this->amUAM);
			$session	 		= $this->session->userdata('sessSpazzle'); 
			$userid 	 		= $session['userid'];
			$qCUAMM				= "
								SELECT a.userid,a.username,b.nama_role,c.id_menu,d.menu,d.url,d.sort FROM mi.user a
								left join role b
								on a.id_role=b.id_role
								left join role_menu c
								on b.id_role=c.id_role
								left join menu d
								on c.id_menu=d.id_menu
								where userid='$userid' and url!='#'
								order by id_role_menu
								limit 1
								";
			$qcekUAMMENU		= $this->query->getDataByQ($qCUAMM);
			$cekUAMMENU			= array_shift($qcekUAMMENU);

			$data['akses']		= $this->akses;
			$data['userid']		= $this->userid; 
			if ($cekUAMMENU['id_menu']=='1') {
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/dashboard/index');

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			} else {
				$url			= $cekUAMMENU['url'];
				// echo "urlnya".$url;
				redirect(''.$url.'');
			}
		} else {
			// $this->load->view('panel/login');
		}
	}
	// public function index(){
	// 	if(checkingsessionpwt()){
	// 		$cekAkses			= $this->akses;
	// 		// if ($cekAkses=='') {
	// 		// 	redirect('panel/error');
	// 		// } else {
	// 			$data['akses']			= $this->akses;
	// 			$data['userid']			= $this->userid;
				
	// 			// CORE 1
	// 			$this->load->view('/theme/metronic/base1');

	// 			// CONTENT
	// 			$this->load->view('panel/pengajuan/inovasi', $data);

	// 			// CORE2
	// 			$this->load->view('/theme/metronic/base2');

	// 			// PLUGIN JS
	// 			$this->load->view('/theme/metronic/pluginjs');
	// 		// } 
	// 	} else {
	// 		// $this->load->view('panel/login');
	// 	}
	// }

	public function setpassword(){
		if(checkingsessionpwt()){
			redirect('/panel/userprofile');
		} else {
			$this->load->view('panel/user/setpassword');
		}
	}

	public function log(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/log/log', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}
	
	public function roles(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']		= $this->akses;
				$data['userid']		= $this->userid;
				$data['getMenus'] 	= $this->query->getData('menu','*',"where parent='0' ORDER BY sort ASC");

				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/user/roles', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2');

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}
	
	public function data_application(){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/app/index', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function assign_application(){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			$q_exclude = "select string_agg(DISTINCT cast(id_application as char), ',')as listapp  from mi.data_application_assign"; 
			$exclude = $this->query->getDatabyQ($q_exclude); 
			$row_exclude = array_shift($exclude); 
			if($row_exclude['listapp']!=NULL or $row_exclude['listapp']!=''){
				$q_application = "
					select * from mi.data_application where id not in (".$row_exclude['listapp'].")
				"; 				
			}else{
				$q_application = "
					select * from mi.data_application
				";  
			} 
			$data['application'] = $this->query->getDatabyQ($q_application);  
			
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/app/assign', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}

	public function workflow(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']		= $this->akses;
				$data['userid']		= $this->userid;
				$data['getMenus'] 	= $this->query->getData('level_user','*',"ORDER BY level ASC");
				$valcrud 			= $this->query->getData('sbrdoc','count(*)as valcrud',"where status not in(0,4,5)");
				$data['validation'] 	= array_shift($valcrud);
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/workflow/workflow', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2');

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}
	
	public function user(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				$data['getDataRole'] 	= $this->query->getData('role','*',"ORDER BY nama_role ASC");
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/user/user', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}
	public function type_upti(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				$data['getDataRole'] 	= $this->query->getData('role','*',"ORDER BY nama_role ASC");
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/type_upti/type_upti', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}
	public function customer(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				$data['getDataRole'] 	= $this->query->getData('role','*',"ORDER BY nama_role ASC");
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/customer/customer', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}
	
	public function error(){
		$this->load->view('panel/error');
	}

	public function userprofile(){
		if(checkingsessionpwt()){
			$sess	 		= $this->session->userdata('sessSpazzle'); 
			$uid 	 		= $sess['userid'];
			$data['uid'] 	= $uid;

			$data['akses']	= 'ada';
			$data['userid']	= 'ada';

			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/user/userprofile', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2', $data);

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}

	public function config(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;

			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$sess	 		= $this->session->userdata('sessSpazzle'); 
				$uid 	 		= $sess['userid'];
				$data['uid'] 	= $uid;

				$data['akses']	= 'ada';
				$data['userid']	= 'ada';

				$data['getDataSite'] 	= $this->query->getData('configsite','*',"where id_site='1'");
				$data['getDataEmail'] 	= $this->query->getData('mail_site','*',"ORDER BY email ASC");
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/config/config', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}

	public function configparam(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;

			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$sess	 		= $this->session->userdata('sessSpazzle'); 
				$uid 	 		= $sess['userid'];
				$data['uid'] 	= $uid;

				$data['akses']	= 'ada';
				$data['userid']	= 'ada';

				//$data['getDataSite'] 	= $this->query->getData('configsite','*',"where id_site='1'");
				$data['getParam'] 	= $this->query->getData('param','*',"ORDER BY id ASC");
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/configparam/configparam', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2', $data);

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}

	public function pengajuan(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/pengajuan/pengajuan', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2');

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}

	public function inovasi(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/pengajuan/inovasi', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2');

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}

	public function input_inovasi(){
		if(checkingsessionpwt()){
			$sess	 		= $this->session->userdata('sessSpazzle'); 
			$uid 	 		= $sess['userid'];
			$data['uid'] 	= $uid;
			$data['akses']	= 'ada';
			$data['userid']	= 'ada';
			$data['getdataframework'] = $this->query->getData('param_framework','*',"");
			$data['getdatadatabase'] = $this->query->getData('param_database','*',"");
			$data['getcfu'] = $this->query->getData('param_cfu','*',"order by id_cfu ASC");
			$data['getdataspasi'] = $this->query->getData('param_sdlc','*',"where type_tab='SPASI' order by id_sdlc ASC");
			$data['getdatasecurity'] = $this->query->getData('param_sdlc','*',"where type_tab='SECURITY SYSTEM' order by id_sdlc ASC");
			$data['getdatainodes'] = $this->query->getData('param_sdlc','*',"where type_tab='INNOVATION DESCRIPTION' order by id_sdlc ASC");
			$data['getdatapiidoc'] = $this->query->getData('param_sdlc','*',"where type_tab='PERSONAL IDENTIFIABLE INFORMATION' order by id_sdlc ASC");
			$data['getdatastgdoc'] = $this->query->getData('param_sdlc','*',"where type_tab='DATA SHARING TELKOM GROUP' order by id_sdlc ASC");


			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/pengajuan/input_inovasi', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2', $data);

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}

	public function docdetail($id){
		if(checkingsessionpwt()){
			error_reporting(0);
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['id']				= $id;
			$userdata				= $this->session->userdata('sessSpazzle'); 
			$data['userid']			= $userdata['userid'];
			$req 					= str_replace('-','/',$id);
			$data['norequest']      = str_replace('_','-',$req);
			$noreqq 				= str_replace('_','-',$req);

			$getUser 		= $this->db->query("
								SELECT a.*, 
									(SELECT name from mi.level_user where id_level=a.level_user) namelevel,
									(SELECT level from mi.level_user where id_level=a.level_user) step,
									(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
								FROM mi.user a where userid='".$userdata['userid']."'
							")->result_array();
			$dUser 			= array_shift($getUser);
			$data['namelevel']	= $dUser['namelevel'];
			$data['level']	= $dUser['step'];
			$data['aksesCreate'] = $dUser['actionbtn'];
			$getstatus = $this->db->query("
							SELECT a.status
							FROM data_innovasi a
							where 1=1 and noinnovasi ='".$noreqq."'")->result_array();
			$row 			= array_shift($getstatus);

	

			// CORE 1
			$this->load->view('/theme/metronic/base1');
			if($row['status']==3){
				// CONTENT  
				$data['getdataframework'] 	= $this->query->getData('param_framework','*',"");
				$data['getdatadatabase'] 	= $this->query->getData('param_database','*',"");
				$data['getcfu'] = $this->query->getData('param_cfu','*',"order by id_cfu ASC");
				$data['getdataspasi'] 	= $this->query->getData('param_sdlc','*',"where type_tab='SPASI' order by id_sdlc asc");
				$data['getdatasecurity'] 	= $this->query->getData('param_sdlc','*',"where type_tab='SECURITY SYSTEM' order by id_sdlc asc");
				$data['getdatainodes'] = $this->query->getData('param_sdlc','*',"where type_tab='INNOVATION DESCRIPTION' order by id_sdlc ASC");
				$data['getdatapiidoc'] = $this->query->getData('param_sdlc','*',"where type_tab='PERSONAL IDENTIFIABLE INFORMATION' order by id_sdlc ASC");
				$data['getdatastgdoc'] = $this->query->getData('param_sdlc','*',"where type_tab='DATA SHARING TELKOM GROUP' order by id_sdlc ASC");

				$this->load->view('panel/pengajuan/draft', $data);
			}else{
				// CONTENT
				if($dUser['step']==2 or $dUser['step']==3){
					$this->load->view('panel/pengajuan/detail_reviewer', $data);
				}else{
					$data['getdataframework'] 	= $this->query->getData('param_framework','*',"");
					$data['getdatadatabase'] 	= $this->query->getData('param_database','*',"");
					$data['getcfu'] = $this->query->getData('param_cfu','*',"order by id_cfu ASC");
					$data['getdataspasi'] 	= $this->query->getData('param_sdlc','*',"where type_tab='SPASI' order by id_sdlc asc");
					$data['getdatasecurity'] 	= $this->query->getData('param_sdlc','*',"where type_tab='SECURITY SYSTEM' order by id_sdlc asc");
					$data['getdatainodes'] = $this->query->getData('param_sdlc','*',"where type_tab='INNOVATION DESCRIPTION' order by id_sdlc ASC");
					$data['getdatapiidoc'] = $this->query->getData('param_sdlc','*',"where type_tab='PERSONAL IDENTIFIABLE INFORMATION' order by id_sdlc ASC");
					$data['getdatastgdoc'] = $this->query->getData('param_sdlc','*',"where type_tab='DATA SHARING TELKOM GROUP' order by id_sdlc ASC");

					$this->load->view('panel/pengajuan/detail', $data);
				}	
			}
			

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs',$data);
		} else {
			
		}
	}
	public function docanswerdetail($id){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['id']				= $id;
			
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/jawaban/detail', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function help(){
		if(checkingsessionpwt()){
			$cekAkses = $this->akses;
			$data['akses'] = $this->akses;
			$data['userid']	= $this->userid;
			$data['getsdlc'] = $this->query->getData('param_sdlc','*',"WHERE is_show_template='1' ORDER BY id_sdlc");
			$data['getsop'] = $this->query->getData('configsite','*',"WHERE config_name='dok_sop'");
			$data['gettata'] = $this->query->getData('configsite','*',"WHERE config_name='dok_tata_kelola'");

			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/help/help', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function faq(){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			$data['getfaq'] 	= $this->query->getData('data_faq','*',"order by question_faq asc");
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/faq/faq', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function data_faq(){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/faq/data_faq', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function type_sdlc(){
		if(checkingsessionpwt()){
			$cekAkses = $this->akses;
			$data['akses'] = $this->akses;
			$data['userid'] = $this->userid;
			$data['getsop'] = $this->query->getData('configsite','*',"WHERE config_name='dok_sop'");
			$data['gettata'] = $this->query->getData('configsite','*',"WHERE config_name='dok_tata_kelola'");

			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/type_sdlc/type_sdlc', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function data_cfu(){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/cfu/cfu', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function jawaban(){
		if(checkingsessionpwt()){
			$cekAkses			= $this->akses;
			if ($cekAkses=='') {
				redirect('panel/error');
			} else {
				$data['akses']			= $this->akses;
				$data['userid']			= $this->userid;
				
				// CORE 1
				$this->load->view('/theme/metronic/base1');

				// CONTENT
				$this->load->view('panel/jawaban/jawaban', $data);

				// CORE2
				$this->load->view('/theme/metronic/base2');

				// PLUGIN JS
				$this->load->view('/theme/metronic/pluginjs');
			}
		} else {
			
		}
	}

	public function jawabandetail($id){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['id']				= $id;
			
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/pengajuan/jawabandetail', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function laporan(){
		if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			//$data['id']				= $id;
			
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/laporan/index', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		} else {
			
		}
	}
	public function domaincapability(){
		//if(checkingsessionpwt()){
			$cekAkses				= $this->akses;
			$data['akses']			= $this->akses;
			$data['userid']			= $this->userid;
			//$data['id']				= $id;
			
			// CORE 1
			$this->load->view('/theme/metronic/base1');

			// CONTENT
			$this->load->view('panel/dashboard/domaincapability', $data);

			// CORE2
			$this->load->view('/theme/metronic/base2');

			// PLUGIN JS
			$this->load->view('/theme/metronic/pluginjs');
		//} else {
			
		//}
	}
	public function widget($type,$pengaju,$noreq,$status){
		$data['pengaju'] = $pengaju;
		$data['noreq'] = $noreq;
		$data['status'] = $status;

		if($type=='dashboardStatus'){
			// CONTENT Dashboard Status
			$this->load->view('panel/dashboard/widget/dashboardStatus',$data);
		}else if($type=='dashboardDonut'){
			// CONTENT Dashboard Donut
			$this->load->view('panel/dashboard/widget/dashboardDonut',$data);
		}else if($type=='dashboardAm'){
			$this->load->view('panel/dashboard/widget/dashboardAM',$data);
		}
	}
	public function cekkk(){
		$this->load->view('cek');
	}
}
