<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawaban extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $profile;
	private $divisiUAM;
	private $segmenUAM;
	private $tregUAM;
	private $witelUAM;
	private $amUAM;
	
	public function __construct(){
		date_default_timezone_set("Asia/Bangkok");
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('query'); 
		$this->load->model('formula'); 
		$this->load->model('datatable'); 
		$this->load->model('jawaban_handler');
		
		ini_set('max_execution_time', 123456);
		ini_set("memory_limit","1256M");
			
		// $session = checkingsessionpwt();
		$session	 = $this->session->userdata('sesspwt'); 
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function getdatajawaban(){
		if(checkingsessionpwt()){

			$columnsDefault = [
				'no_answer'			=> true,
				'no_request'		=> true,
				'analisa_teknis'	=> true,
				'created_at'		=> true, 
				'actions'			=> true,
			];
			$arraynya	= $columnsDefault;

			// $jsonfile	= base_url().'user/data';
			$jsonfile	= $this->jawaban_handler->data();

			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}

	public function insert($type){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$now 		= date('ymd');

			$namaproject 	= trim(strip_tags(stripslashes($this->input->post('namaproject',true))));
			$subject 		= trim(strip_tags(stripslashes($this->input->post('subject',true))));
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('pelanggan',true))));
			$serviceid 		= trim(strip_tags(stripslashes($this->input->post('serviceid',true))));
			$reviewer1 		= trim(strip_tags(stripslashes($this->input->post('reviewer1',true))));
			$approval1 		= trim(strip_tags(stripslashes($this->input->post('approval1',true))));
			$approval2 		= trim(strip_tags(stripslashes($this->input->post('approval2',true))));
			$latarbelakang 	= $_POST['latarbelakang'];
			$aspekstrategis = $_POST['aspekstrategis'];
			$aspekfinansial = $_POST['aspekfinansial'];
			$aspekkompetisi = $_POST['aspekkompetisi'];
			$konfigurasiteknis = $_POST['konfigurasiteknis'];

			if ($type=='draft') {
				$status 	= '0';
				$current 	= '1';
			} else {
				$status 	= '1';
				$current 	= '2';
			}

			$cekMax 		= $this->db->query("SELECT MAX(id)+1 maxid from sbrdoc")->result_array();
			$gMax 			= array_shift($cekMax);
			if ($gMax['maxid']==0) {
				$lastid 	= '1';
			} else {
				$lastid 	= $gMax['maxid'];
			}

			$code 			= sprintf("%06d", $lastid);
			$maxid 			= $code;

			$norequest 		= 'SBR/'.$now.'/'.$pelanggan.'/'.$maxid;
			$createdat 		= date('Y-m-d H:i:s');

			// INSERT NEW PENGAJUAN
			$rows 			= $this->db->query("
							INSERT INTO sbrdoc (no_request, nipnas, nama_project, subject, latar_belakang, aspek_strategis, aspek_finansial, aspek_kompetisi, konfigurasi_teknis, status, created_by, created_at, reviewer1, approval1, approval2, service_id, current)
							VALUES
							('$norequest', '$pelanggan', '$namaproject', '$subject', '$latarbelakang', '$aspekstrategis', '$aspekfinansial', '$aspekkompetisi', '$konfigurasiteknis', '$status', '$userid', '$createdat'::TIMESTAMP, '$reviewer1', '$approval1', '$approval2', '$serviceid', '$current')
							");
			
			$id				= $this->db->insert_id();
			$url 			= "Pengajuan SBR";
			$activity 		= "INSERT";

			if($rows) {
				// INSERT HISTORY AND NOTIFICATIONS
				if ($type!='draft') {
					$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', 'Pengajuan SBR Baru', '$userid', '$createdat'::TIMESTAMP, 'new', '$reviewer1' ,'0')
								");
				}

				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
		} else {
			redirect('/panel');
		}
	}	

	public function jmlsbr($status){
		if(checkingsessionpwt()){
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];

			$getUser 		= $this->db->query("
								SELECT a.*, 
									(SELECT level from mi.level_user where id_level=a.level_user) step,
									(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
								FROM mi.user a where userid='$userid'
							")->result_array();
			$dUser 			= array_shift($getUser);
			$level			= $dUser['step'];

			if ($level==2) {
				$cond 		= "and reviewer1='$userid'";
			} else if ($level==3) {
				$cond 		= "and approval1='$userid'";
			} else if ($level==4) {
				$cond 		= "and approval2='$userid'";
			} else if ($level==0) {
				$cond 		= "";
			} else {
				$cond 		= "and created_by='$userid'";
			}
			$getPending 	= $this->db->query("SELECT * from sbrdoc where status not in (4,5) $cond")->num_rows();

			echo $getPending.' <span>Record</span>';
		} else {
			redirect('/panel');
		}
	}	

	public function modal(){
		if(checkingsessionpwt()){
			
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			
			$dataUsr			= $this->db->query("
								SELECT a.* ,
									(SELECT username from mi.user where userid=a.reviewer1) usernamerev,
									(SELECT name from mi.user where userid=a.reviewer1) namerev1,
									(SELECT username from mi.user where userid=a.approval1) usernameappr1,
									(SELECT name from mi.user where userid=a.approval1) nameappr1,
									(SELECT username from mi.user where userid=a.approval2) usernameappr2,
									(SELECT name from mi.user where userid=a.approval2) nameappr2,
									(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan
								FROM sbrdoc a where id='$id'
								")->result_array();
			
			header('Content-type: application/json; charset=UTF-8');
			
			if (isset($id) && !empty($id)) {
				foreach($dataUsr as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	

	public function update($type){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$now 		= date('ymd');

			$id 			= trim(strip_tags(stripslashes($this->input->post('ed_id',true))));
			$norequest 		= trim(strip_tags(stripslashes($this->input->post('ed_norequest',true))));
			$statusbefore 	= trim(strip_tags(stripslashes($this->input->post('statusbefore',true))));
			$namaproject 	= trim(strip_tags(stripslashes($this->input->post('ed_namaproject',true))));
			$subject 		= trim(strip_tags(stripslashes($this->input->post('ed_subject',true))));
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('ed_pelanggan',true))));
			$serviceid 		= trim(strip_tags(stripslashes($this->input->post('ed_serviceid',true))));
			$reviewer1 		= trim(strip_tags(stripslashes($this->input->post('ed_reviewer1',true))));
			$approval1 		= trim(strip_tags(stripslashes($this->input->post('ed_approval1',true))));
			$approval2 		= trim(strip_tags(stripslashes($this->input->post('ed_approval2',true))));
			$latarbelakang 	= $_POST['ed_latarbelakang'];
			$aspekstrategis = $_POST['ed_aspekstrategis'];
			$aspekfinansial = $_POST['ed_aspekfinansial'];
			$aspekkompetisi = $_POST['ed_aspekkompetisi'];
			$konfigurasiteknis = $_POST['ed_konfigurasiteknis'];

			if ($type=='draft') {
				$status 	= '0';
				$current 	= '1';
			} else {
				$status 	= '1';
				$current 	= '2';
			}

			$createdat 		= date('Y-m-d H:i:s');

			// UPDATE PENGAJUAN
			$rows 			= $this->db->query("
							UPDATE sbrdoc set 
								nipnas				= '$pelanggan',
								nama_project		= '$namaproject',
								subject				= '$subject',
								latar_belakang		= '$latarbelakang',
								aspek_strategis		= '$aspekstrategis',
								aspek_finansial		= '$aspekfinansial',
								aspek_kompetisi		= '$aspekkompetisi',
								konfigurasi_teknis	= '$konfigurasiteknis',
								status				= '$status',
								updated_by			= '$userid',
								updated_at			= '$createdat'::TIMESTAMP,
								reviewer1			= '$reviewer1',
								approval1			= '$approval1',
								approval2			= '$approval2',
								service_id			= '$serviceid',
								current				= '$current'
							WHERE id='$id'
							");
			
			$url 			= "Pengajuan SBR";
			$activity 		= "UPDATE";

			if($rows) {
				// INSERT UPDATE HISTORY AND NOTIFICATIONS
				if ($type!='draft') {
					if ($statusbefore==2) {
						$insHis 	= $this->db->query("
									INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
									('$norequest', 'Perbaikan Pengajuan SBR', '$userid', '$createdat'::TIMESTAMP, 'republish', '$reviewer1' ,'0')
									");
					} else {
						$insHis 	= $this->db->query("
									INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
									('$norequest', 'Pengajuan SBR Baru', '$userid', '$createdat'::TIMESTAMP, 'new', '$reviewer1' ,'0')
									");
					}
				}

				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
			$log = $this->query->insertlog($activity,$url,$id);
		} else {
			redirect('/panel');
		}
	}	

	public function delete(){
		if(checkingsessionpwt()){
			$url 		= "Pengajuan";
			$activity 	= "DELETE";
			
			$this->load->model('query');
			
			$cond		= trim(strip_tags(stripslashes($this->input->post('iddel',true))));

			$getnoreq 	= $this->db->query("SELECT * FROM sbrdoc where id='$cond'")->result_array();
			$noreq 		= array_shift($getnoreq);
			$norequest 	= $noreq['no_request'];
			
			$rows 		= $this->db->query("DELETE FROM sbrdoc where id='$cond'");
			
			if(isset($rows)) {
				$del 	= $this->db->query("DELETE FROM sbrhistory where no_request='$norequest'");
				$log 	= $this->query->insertlog($activity,$url,$cond);
				print json_encode(array('success'=>true,'rows'=>$rows, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function actionsubmit(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$createdat 	= date('Y-m-d H:i:s');

			$id			= trim(strip_tags(stripslashes($this->input->post('idsubmitact',true))));
			$comment	= $_POST['comment'];
			$gtype		= trim(strip_tags(stripslashes($this->input->post('typesubmitact',true))));
			$type 		= strtolower($gtype);

			$url 		= "Pengajuan";
			$activity 	= $type;

			$getdoc 	= $this->db->query("SELECT * FROM sbrdoc where id='$id'")->result_array();
			$doc 		= array_shift($getdoc);
			$norequest 	= $doc['no_request'];

			$getUser 	= $this->db->query("
						SELECT a.*, 
							(SELECT level from mi.level_user where id_level=a.level_user) step,
							(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
						FROM mi.user a where userid='$userid'
						")->result_array();
			$dUser 		= array_shift($getUser);
			$level		= $dUser['step'];

			if ($type=='submit' or $type=='resubmit') {
				$sendto = $doc['reviewer1'];
				$curr 	= 2;
			} else if ($type=='escalation') {
				if ($level==2) {
					$sendto = $doc['approval1'];
					$curr 	= 3;
				} else if ($level==3) {
					$sendto = $doc['approval2'];
					$curr 	= 4;
				}
			} else if ($type=='return') {
				$sendto = $doc['created_by'];
				$curr 	= 1;
			} else {
				$sendto = 0;
			}
			
			$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', '$comment', '$userid', '$createdat'::TIMESTAMP, '$type', '$sendto' ,'0')
								");
			
			if(isset($insHis)) {
				// UPDATE DOC
				$insHis 	= $this->db->query("
								UPDATE sbrdoc SET
									current = '$curr'
								where id='$id'
								");
				
				$log 	= $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'rows'=>$insHis, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function listcustomer(){
		if(checkingsessionpwt()){
			$key 		= $_GET['q'];
			$getData 	= $this->db->query("
						SELECT * FROM customer where nipnas like '%$key%' or upper(nama_pelanggan) like upper('%$key%') order by nama_pelanggan asc
						")->result_array();
			$count 		= $this->db->query("SELECT * FROM customer where nipnas like '%$key%' or upper(nama_pelanggan) like upper('%$key%')")->num_rows();

			header('Content-type: application/json; charset=UTF-8');
			$json['total_count'] 		= $count;
			$json['incomplete_results'] = true;
			foreach($getData as $row) {
				$json['items'][] 	= array(
					'id'			=> $row['nipnas'],
					'nipnas'		=> $row['nipnas'],
					'nama_pelanggan'=> $row['nama_pelanggan'],
					'text'			=> $row['nama_pelanggan'],
				);
			}
			echo json_encode($json);
		}else{
            redirect('/login');
        }
	}

	public function listapproval($level){
		if(checkingsessionpwt()){
			$key 		= @$_GET['q'];
			$getData 	= $this->db->query("
						SELECT * FROM (
							SELECT a.* ,
								(SELECT level from mi.level_user where id_level=a.level_user) step
							FROM mi.user a
						) as base
						where step='$level' and (upper(username) like upper('%$key%') or upper(name) like upper('%$key%'))
						order by name asc
						")->result_array();
			$count 		= $this->db->query("
						SELECT * FROM (
							SELECT a.* ,
								(SELECT level from mi.level_user where id_level=a.level_user) step
							FROM mi.user a
						) as base
						where step='$level' and (upper(username) like upper('%$key%') or upper(name) like upper('%$key%'))
						")->num_rows();

			header('Content-type: application/json; charset=UTF-8');
			$json['total_count'] 		= $count;
			$json['incomplete_results'] = true;
			foreach($getData as $row) {
				$json['items'][] 	= array(
					'id'			=> $row['userid'],
					'username'		=> $row['username'],
					'name'			=> $row['name'],
					'text'			=> $row['username'].' - '.$row['name'],
				);
			}
			echo json_encode($json);
		}else{
            redirect('/login');
        }
	}

	public function sendMail($name,$username,$pass,$email,$role,$clientid,$ldap) {
		$subject 	= 'ISR-M2M Application';

		$getSiteData 	= $this->query->getData('configsite','*',"");
		$datasite		= array_shift($getSiteData);
		$token 			= md5($username.$pass);
		$pbs 			= $pass;
		
		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'smtp.hostinger.co.id',
		'smtp_port' => 587,
		'smtp_user' => 'isrm2m@parwatha.com', // change it to yours
		'smtp_pass' => 'b1sm1llah', // change it to yours
		'mailtype'  => 'html',
		'charset'   => 'iso-8859-1'
		);
		
		if ($ldap!=1) {
			$fornonldap = '
				<div style="border-bottom: 1px dashed #efefef; padding-bottom:10px;">
					<div style="text-align: center;">Please set your new password <a href="'.base_url().'setpassword?token='.$token.'&pbs='.$pbs.'" target="_blank" style="color: #5d78ff;">here</a>:</div>
					<center>
						<a href="'.base_url().'setpassword?token='.$token.'&pbs='.$pbs.'" target="_blank">
							<button style="cursor: pointer; box-shadow:0px 4px 16px 0px rgba(93, 120, 255, 0.15); color: #fff; background-color: #5d78ff; border-color: #5d78ff; border-radius: 3px; padding: 8px 15px; margin-top: 10px; font-size: 16px; font-weight: bold; margin-bottom: 5px;">
								Set Password
							</button>
						</a>
					</center>
					<div><center>Thanks!</center></div>
				</div>
			';
		} else {
			$fornonldap = '';
		}

		//Email content
		$htmlContent = '';
		$htmlContent .= '
			<!DOCTYPE html>
			<html>
			<head>
			  <meta charset="utf-8" />
			  <title>No Reply</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
			  <style>
			  </style>
			</head>
			<body style="font-family: verdana; font-size: 14px;">
				<div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
					<div id="logo" style="background: #FFF;"><img src="'.base_url().'images/logotel.png" style="max-height: 70px; margin-top: 20px;"></div>
					<div id="confirmation-message">
						<div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
							<div class="title" style="color: #1e1e1e; font-size: 24px;"><span>Dear, '.$name.'</span></div>
						</div>
						<div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
							<div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
								Congratulations! You have been registered as M2M-ISR application User. Please find below your account information:
							</div>
							
							<div style="margin-top: 20px; border-bottom: 1px dashed #efefef; margin-bottom: 10px; padding-bottom: 10px;">
								<table style="font-size: 14px;">
								 <tbody>
								 	<tr>
								 		<td>Username</td>
								 		<td>:</td>
								 		<td><b>'.$username.'</b></td>
								 	</tr>
								 	<tr>
								 		<td>Email</td>
								 		<td>:</td>
								 		<td><b>'.$email.'</b></td>
								 	</tr>
								 	<tr>
								 		<td>Role</td>
								 		<td>:</td>
								 		<td><b>'.$role.'</b></td>
								 	</tr>
								 	<tr>
								 		<td>Client ID</td>
								 		<td>:</td>
								 		<td><b>'.$clientid.'</b></td>
								 	</tr>
								 </tbody>
								</table>
							</div>

							'.$fornonldap.'

							<div>
								<div style="padding-top: 20px; padding-bottom: 10px;">Best Regards,</div>

								<div>
									<b style="font-size: 16px; padding-bottom:0px; margin-bottom: 0px;">
										ISR-<span style="color: #b63127;">M2M</style>
									</b>
								</div>
								<div><b>Solid, Speed, Smart!<b></div>
							</div>
						</div>
					</div>
				</div>
			</body>
			</html>
		';

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('isrm2m@parwatha.com'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->subject($subject);
		$this->email->set_mailtype("html");
		$this->email->message($htmlContent);
		
		if($this->email->send()) {
			echo '';
		} else {
			show_error($this->email->print_debugger());
		}
		// echo $htmlContent;
	}

	public function sendMailSetPass($name,$username,$pass,$email) {
		$subject 	= 'ISR-M2M Application - Set Password Success';

		$getSiteData 	= $this->query->getData('configsite','*',"");
		$datasite		= array_shift($getSiteData);
		$token 			= md5($username.$pass);
		$pbs 			= $pass;
		
		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'smtp.hostinger.co.id',
		'smtp_port' => 587,
		'smtp_user' => 'isrm2m@parwatha.com', // change it to yours
		'smtp_pass' => 'b1sm1llah', // change it to yours
		'mailtype'  => 'html',
		'charset'   => 'iso-8859-1'
		);

		//Email content
		$htmlContent = '';
		$htmlContent .= '
			<!DOCTYPE html>
			<html>
			<head>
			  <meta charset="utf-8" />
			  <title>No Reply</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
			  <style>
			  </style>
			</head>
			<body style="font-family: verdana; font-size: 14px;">
				<div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
					<div id="logo" style="background: #FFF;"><img src="'.base_url().'images/logotel.png" style="max-height: 70px; margin-top: 20px;"></div>
					<div id="confirmation-message">
						<div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
							<div class="title" style="color: #1e1e1e; font-size: 24px;"><span>Dear, '.$name.'</span></div>
						</div>
						<div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
							<div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
								The password for your <b>ISR-<span style="color: #b63127;">M2M</span></b> application user (<b>'.$username.'</b>) has been successfully reset.
								If you did not make this change or you believe an unauthorised person has accessed your account, click <a href="'.base_url().'setpassword?token='.$token.'&pbs='.$pbs.'" target="_blank" style="color: #5d78ff;">here</a> to reset your password without delay. 
								<br><br>

								If you need additional help, please contact Admin.<br><br>
							</div>
							
							<div>
								<div style="padding-top: 20px; padding-bottom: 10px;">Sincerely,</div>
								<div>
									<b style="font-size: 16px; padding-bottom:0px; margin-bottom: 0px;">
										ISR-<span style="color: #b63127;">M2M</style>
									</b>
								</div>
								<div><b>Solid, Speed, Smart!<b></div>
							</div>
						</div>
					</div>
				</div>
			</body>
			</html>
		';

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('isrm2m@parwatha.com'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->subject($subject);
		$this->email->set_mailtype("html");
		$this->email->message($htmlContent);
		
		if($this->email->send()) {
			echo '';
		} else {
			show_error($this->email->print_debugger());
		}
		// echo $htmlContent;
	}
	public function uploaddok() { 
		$id 			= trim(strip_tags(stripslashes($this->input->post('idreq',true))));
		$getDoc 		= $this->db->query("SELECT * FROM mi.sbrdoc where id ='$id'")->result_array();
		$doc 			= array_shift($getDoc);
		if($doc['dok_jawaban'] !=''){
			@unlink('./upload/'.$doc['dok_jawaban']);
		}

		$filename 		= $_FILES['dokjawaban']['name'];
		$direktori 		= './upload/';
		$lokasi_file  	= $_FILES['dokjawaban']['tmp_name'];
		$tipe_file    	= $_FILES['dokjawaban']['type'];
		$nama_file   	= $_FILES['dokjawaban']['name'];
		$nama_file_unik	= str_replace(' ','_',date('Ymd').'_'.$nama_file);

        $allowed = array('pdf');
		
		$extension = pathinfo($nama_file, PATHINFO_EXTENSION);

		if(!in_array(strtolower($extension), $allowed)){
			echo '{"status":"error"}';
			exit;
		}
		
		if (!empty($lokasi_file)){
			//direktori gambar
			$vfile_upload = $direktori . $nama_file_unik;
			//Simpan gambar dalam ukuran sebenarnya
			move_uploaded_file($lokasi_file, $vfile_upload);
			$update 	= $this->db->query("update mi.sbrdoc set dok_jawaban='".$nama_file_unik."' where id='".$id."'");
			echo "uploaded";
		}
	}
	public function download_old($id) { 

		$userdata		= $this->session->userdata('sesspwt'); 
		$userid 		= $userdata['userid'];$userdata		= $this->session->userdata('sesspwt'); 
		$userid 		= $userdata['userid'];
		$username 		= $userdata['username'];
		$gnoreq 		= str_replace('-','/',$id);
		$norequest 		= str_replace('_','-',$gnoreq);

		$getUser 		= $this->db->query("
							SELECT a.*, 
								(SELECT level from mi.level_user where id_level=a.level_user) step,
								(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
							FROM mi.user a where userid='$userid'
						")->result_array();
		$dUser 			= array_shift($getUser);
		$level			= $dUser['step'];
		$aksesCreate 	= $dUser['actionbtn'];

		$getDoc 		= $this->db->query("
						SELECT a.* ,
							(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
							(SELECT picture from mi.user where userid=a.created_by) pictcreated,
							(SELECT name from mi.user where userid=a.created_by) namecreated,
							(SELECT username from mi.user where userid=a.created_by) uncreated,
							(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) poscreated,
							(SELECT picture from mi.user where userid=a.reviewer1) pictreview,
							(SELECT name from mi.user where userid=a.reviewer1) namereview,
							(SELECT username from mi.user where userid=a.reviewer1) unreview,
							(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.reviewer1) posreview,
							(SELECT picture from mi.user where userid=a.approval1) pictappr1,
							(SELECT name from mi.user where userid=a.approval1) nameappr1,
							(SELECT username from mi.user where userid=a.approval1) unappr1,
							(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval1) posappr1,
							(SELECT picture from mi.user where userid=a.approval2) pictappr2,
							(SELECT name from mi.user where userid=a.approval2) nameappr2,
							(SELECT username from mi.user where userid=a.approval2) unappr2,
							(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval2) posappr2
						FROM sbrdoc a where no_request='$norequest'
						")->result_array();
		$doc 			= array_shift($getDoc);
		$getAns = $this->db->query("
				SELECT a.* ,					
					(SELECT picture from mi.user where userid=a.userid) pictcreated,
					(SELECT name from mi.user where userid=a.userid) namecreated,
					(SELECT username from mi.user where userid=a.userid) uncreated,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.userid) poscreated,
					(SELECT picture from mi.user where userid=a.created_by) pictappr,
					(SELECT name from mi.user where userid=a.created_by) nameappr,
					(SELECT username from mi.user where userid=a.created_by) unappr,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) posappr
				FROM sbranswer a where no_request='$norequest'
				")->result_array();
		$ans 		= array_shift($getAns);
		$no_answer	= $ans['no_answer'];
		$html 		='';
		$table 		= '';

		$qApprover 		= "
						SELECT * FROM (
							SELECT approver.*,
							(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
							FROM (
								SELECT DISTINCT(a.created_by) approver,
								(SELECT name from mi.user where userid=a.created_by) nameapp,
								(SELECT username from mi.user where userid=a.created_by) unapp,
								(SELECT level_user from mi.user where userid=a.created_by) levelapp,
								(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid
									where no_request='$norequest' and
									xb.userid=a.created_by) posisi
								from mi.sbrhistory a
								where no_request='$norequest' and action in ('escalation','approve','update')
							) as approver
						) as final
						where actionapp like '%approve%'
						order by levelapp
						";
		$cekApprover 	= $this->db->query($qApprover)->num_rows();
		$getApprover 	= $this->db->query($qApprover)->result_array();
		$cekColspan 	= $cekApprover;

		$qLastApprove 	= "
							SELECT * FROM (
								SELECT approver.*,
								(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
								FROM (
									SELECT DISTINCT(a.created_by) approver,
									(SELECT name from mi.user where userid=a.created_by) nameapp,
									(SELECT username from mi.user where userid=a.created_by) unapp,
									(SELECT level_user from mi.user where userid=a.created_by) levelapp,
									(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid
									where no_request='$norequest' and
									xb.userid=a.created_by) posisi
									from mi.sbrhistory a
									where no_request='$norequest' and action in ('escalation','approve','update')
								) as approver
							) as final
							where actionapp like '%approve%'
							order by levelapp desc
							limit 1
							";
			$gLastApprove	= $this->db->query($qLastApprove)->result_array();
			$dLastApprove 	= array_shift($gLastApprove);
			$lastapproval 	= $dLastApprove['unapp'];

		$html 		.='
		<style>
		#watermark {
		    position: absolute;
		    z-index: 2;
		    font-size: 14em;
		    opacity: 0.1;
		    width: 100%;
		    text-align: center;
		    top: 10%;
		    transform: rotate(-45deg);

		}
		</style>
		<div id="watermark">'.$lastapproval.'</div>
		<span><strong><u>LEMBAR JAWABAN SPESIAL BUSINESS REQUEST</u></strong></span><br>
		NOMOR : TEL :  <b>'.$no_answer.'</b><br><br>

		<br>
		Menjawab SBR <b>'.$doc['namecreated'].' / '.$doc['uncreated'].'</b><br>
		NOMOR : TEL :  <b>'.$doc['no_request'].'</b><br><br>
		<table>
			<tr>
				<td>Pelanggan</td>
				<td>:</td>
				<td><b>'.$doc['nipnas'].' - '.$doc['nama_pelanggan'].'</b></td>
			</tr>
			<tr>
				<td>AM</td>
				<td>:</td>
				<td><b>'.$doc['namecreated'].' / '.$doc['uncreated'].'</b></td>
			</tr>
			<tr>
				<td>Permintaan SBR</td>
				<td>:</td>
				<td><b>'.$doc['nama_project'].'</b></td>
			</tr>
		</table>
		<table class="table table-bordered" border="1px" cellspacing="0px" cellpadding="3px" width="100%"  style="font-size:14px;">
			<thead>
				<tr>
					<th rowspan="2" class="align-middle"><center>NO</center></th>
					<th rowspan="2" class="align-middle">LAYANAN</th>
					<th rowspan="2" class="align-middle">VOLUME</th>
					<th colspan="2" class="align-middle"><center>PEMAKAIAN BULANAN</center></th>
				</tr>
				<tr>
					<th  class="align-middle">TARIF</th>
					<th  class="align-middle">PRICING</th>
				</tr>
			</thead>
			<tbody>';
				$getAnsDet = $this->db->query("SELECT * FROM mi.sbranswer_detail WHERE no_answer ='".$no_answer."'")->result_array();
				$x=0;
				foreach($getAnsDet as $dataAD){
					$x++;
					$table .="<tr>
						<td><center>".$x."</center></td>
						<td>".$dataAD['layanan']."</td>
						<td>".$dataAD['volume']."</td>
						<td><center>".rupiah($dataAD['tarif'])."</center></td>
						<td><center>".rupiah($dataAD['price'])."</center></td>
					</tr>";
				}
				$html.=$table;
		$html .='
			</tbody>
		</table>
		<br>
		<br>
		<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
			<tr>
			<td>
			<b>Analisa Teknis</b><br>
			'.$ans['analisa_teknis'].'
			</td>
			</tr>
		</table>
		<div style="page-break-inside:avoid;">
		<center><h4>PERSETUJUAN</h4></center>
			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
					<td colspan="'.$cekColspan.'"><center>'.$this->formula->TanggalIndo($doc['created_at']).'</center></td>
				</tr>
				<tr>';

					foreach ($getApprover as $apr) {
						$html .= '<td width="25%"><center>Disetujui Oleh,<br>'.$apr['posisi'].'</center></td>';
					}

				$html .= '</tr>
				<tr>';

					foreach ($getApprover as $apr) {
						$html .= '<td height="100px"></td>';
					}

				$html .= '</tr>
				<tr>';

					foreach ($getApprover as $apr) {
						$html .= '<td><b><u>'.$apr['nameapp'].'</u><br>
						'.$apr['unapp'].'</b><br></td>'; 
					}

				$html .= '</tr>
			</table><br>
			<i style="font-size: 12px!important;">* Dokumen SBR Bisnis ini sudah melalui proses persetujuan online.</i>';
			// echo $html;
		$this->load->library('PdfGenerator');
		$this->pdfgenerator->generate($html,'DOKJAWABAN','A4','potrait'); 
	}
	public function download($id) {
		if(checkingsessionpwt()){
			$this->load->library('Tcpdf');
			$data['id'] = $id;
			// LOAD PDF
			$this->load->view('/panel/pengajuan/downloadjawaban',$data);
		}else{

		}
	}
}
