<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		echo date('Y');
		//$this->load->view('welcome_message');
	}
	
	public function dok(){
		$html= '
		<style>
		 	#tblArticles{
				table-layout:fixed;
                font-size: 12px !important;
                font-family: verdana, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }
            #tblArticles tr{
                page-break-before: always;
            }
            #tblArticles td{
                border: 1px solid black;
                text-align: left;
                padding: 8px;
                /*padding: 0;*/
                margin: 0;
               
            }
            #tblArticles table.inner{
                width: 100%;
                margin: 0;
            }
            #tblArticles table.inner td{
                padding: 0;
                width: 50%;
                margin: 0;
                padding: 0;
                border: 0;
            }
             #tblArticles table.inner tr td{
                border-bottom: 1px solid black;
            }
            #tblArticles table.inner tr:last-child td{
                border-bottom: none;
            }
            #tblArticles th {
                border: 1px solid black;
                text-align: center;
                padding: 8px;
                background-color: #dddddd;
            } 
        </style>
		<table id="tblArticles" width="100%" border="1px" cellspacing="0px" cellpadding="3px">
  <tbody>
    <tr>
      <td style="width: 5%">1</td>
      <td style="width: 10%;font-size: 11.1px;"><b>Latar Belakang Kebutuhan Investasi</b></td>
      <td style="width:85%;word-break:break-all; word-wrap:break-word;">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ante felis, pulvinar pulvinar eros a, auctor vestibulum purus. Proin pulvinar, justo tincidunt eleifend vestibulum, mi elit bibendum ipsum, vel consequat justo risus laoreet eros. Integer ullamcorper magna urna, eu fringilla ex elementum eu. Duis malesuada et urna malesuada pretium. Nullam nulla ligula, iaculis sed dui nec, imperdiet suscipit mi. Donec rutrum, purus condimentum fermentum aliquet, nisi ligula tincidunt lorem, non pretium ante risus in magna. Curabitur eget imperdiet enim. Pellentesque id pretium metus. Nulla sed ante urna. Sed augue ante, laoreet a urna tristique, bibendum maximus tellus. Nam quis consectetur lectus. Pellentesque vehicula fringilla velit sed congue. Nunc volutpat odio odio, id dignissim elit maximus sit amet. In venenatis, velit sit amet vehicula viverra, magna augue volutpat justo, eget suscipit mauris tortor sed leo. Donec urna ligula, condimentum a mi ut, scelerisque blandit est. Integer nunc ligula, varius id ex a, congue volutpat neque.
        Nullam tincidunt scelerisque ultrices. Aenean nisi sem, facilisis eu elit a, vulputate gravida diam. In suscipit turpis vel magna finibus congue. Phasellus consectetur urna vel nisl pulvinar, ac bibendum lectus vestibulum. Aenean non enim erat. Sed volutpat quis nisi sed sollicitudin. Phasellus efficitur magna rutrum lectus rhoncus elementum.
        Curabitur est ligula, finibus nec mi et, lacinia congue purus. Nunc et metus tellus. In accumsan molestie mi ac lobortis. Integer eu ligula a velit tristique mattis. Etiam a tortor commodo nibh faucibus facilisis. Duis eu dictum turpis. Donec ac augue lobortis, aliquet turpis ut, hendrerit justo. Aliquam sodales hendrerit efficitur. Phasellus scelerisque dictum fringilla.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur magna libero, pellentesque ut nisi quis, sodales vestibulum diam. Curabitur lacinia sodales cursus. Mauris id felis ut ex gravida iaculis. Maecenas eget dapibus purus. Fusce scelerisque mattis est sed commodo. Donec et dictum leo. Fusce lacus justo, accumsan sed ligula et, ullamcorper molestie neque.
        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus feugiat aliquet nulla, sodales ultricies mi ultricies nec. Nullam arcu quam, tincidunt eleifend justo elementum, vestibulum egestas nisl. Duis egestas quam et neque interdum fringilla. Pellentesque in mauris fermentum augue placerat tincidunt vitae sed ex. Vivamus fermentum massa ante, a fermentum elit dapibus vel. Proin iaculis tincidunt tellus, quis ultrices libero porta non. Duis pulvinar elementum est, ut aliquam neque sollicitudin ut. Vivamus non lorem ac est tristique pretium. Fusce est tortor, egestas sed metus vitae, vehicula ultrices nunc.
        Sed eget tristique eros, sed sagittis turpis. Cras augue turpis, consequat a sodales rhoncus, viverra nec tortor. Donec tellus augue, auctor non viverra maximus, vestibulum sed erat. Integer rhoncus tincidunt lectus. Maecenas orci metus, efficitur sit amet tortor vitae, pellentesque suscipit mi. Curabitur et libero aliquam, porttitor libero vitae, pharetra ipsum. Sed in iaculis augue. Ut venenatis faucibus ultrices. Donec gravida vitae mauris id convallis. Aliquam erat volutpat. Integer tristique, neque quis placerat vehicula, ipsum turpis pharetra ligula, in fermentum risus dui ut urna. Nam vestibulum odio id velit auctor laoreet. Nunc rutrum malesuada nisi, sed ultricies neque finibus id. Proin id augue eget nunc vehicula consequat.
        Ut non mauris scelerisque, lacinia enim ac, vestibulum magna. Nulla eget diam mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis feugiat porta magna, et ullamcorper urna vehicula ut. Sed fringilla euismod felis eu egestas. Ut consectetur, metus et vulputate cursus, augue dolor pulvinar mauris, non mollis lacus nunc ut enim. Suspendisse potenti. Nullam quis suscipit nisi. Donec ut finibus ligula. Pellentesque gravida velit mi, euismod gravida metus lobortis porttitor.
        Donec tristique nulla ac egestas sagittis. Fusce lacinia at turpis elementum congue. Curabitur et mollis arcu, et consequat ante. Integer nisl ipsum, accumsan porta erat id, pretium condimentum nisi. Donec nec metus sit amet enim porta pretium. Nulla dui sem, dignissim ut magna quis, commodo vehicula diam. Nam nec posuere orci. Maecenas vel augue eget eros consectetur suscipit quis eu nisi. Aliquam mollis eros magna, at euismod arcu mattis nec. Proin pretium at mauris eget congue. Cras eget tempor nisi. Donec congue rutrum augue. Nam felis augue, scelerisque efficitur porta sit amet, luctus et metus. Suspendisse ut nisl in tellus vehicula rhoncus quis id mi. Pellentesque vel risus at ipsum viverra pretium.
        Integer mollis vel purus vel congue. Pellentesque tortor nulla, rutrum id pulvinar et, consequat vitae leo. Duis arcu nulla, egestas non est in, ultricies bibendum metus. Integer cursus diam sit amet mollis convallis. Integer rhoncus metus eget dui eleifend pharetra. Nullam pellentesque ac orci ut molestie. In hac habitasse platea dictumst.
        Nam ullamcorper mi eu massa vestibulum, quis volutpat diam placerat. Suspendisse sapien neque, volutpat ut ipsum nec, tristique ullamcorper libero. Phasellus luctus mauris enim, id sagittis lectus dictum ac. Nam et nisl in risus faucibus rhoncus. Vestibulum convallis vulputate mi nec iaculis. Quisque porta sodales tincidunt. Donec ac fermentum ex. Aenean faucibus cursus auctor. Donec eleifend, mauris id ultricies lacinia, diam tellus semper lectus, eget placerat lacus tellus nec diam. Donec iaculis urna vitae mattis pretium. Praesent vehicula ultrices odio rutrum ullamcorper. Sed a nunc ut orci pellentesque aliquet. Praesent hendrerit, orci ut iaculis dictum, erat dolor consectetur nulla, commodo accumsan nunc felis quis nulla. Suspendisse venenatis posuere massa, in tincidunt nibh sodales eu.<br><br>
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ante felis, pulvinar pulvinar eros a, auctor vestibulum purus. Proin pulvinar, justo tincidunt eleifend vestibulum, mi elit bibendum ipsum, vel consequat justo risus laoreet eros. Integer ullamcorper magna urna, eu fringilla ex elementum eu. Duis malesuada et urna malesuada pretium. Nullam nulla ligula, iaculis sed dui nec, imperdiet suscipit mi. Donec rutrum, purus condimentum fermentum aliquet, nisi ligula tincidunt lorem, non pretium ante risus in magna. Curabitur eget imperdiet enim. Pellentesque id pretium metus. Nulla sed ante urna. Sed augue ante, laoreet a urna tristique, bibendum maximus tellus. Nam quis consectetur lectus. Pellentesque vehicula fringilla velit sed congue. Nunc volutpat odio odio, id dignissim elit maximus sit amet. In venenatis, velit sit amet vehicula viverra, magna augue volutpat justo, eget suscipit mauris tortor sed leo. Donec urna ligula, condimentum a mi ut, scelerisque blandit est. Integer nunc ligula, varius id ex a, congue volutpat neque.
        Nullam tincidunt scelerisque ultrices. Aenean nisi sem, facilisis eu elit a, vulputate gravida diam. In suscipit turpis vel magna finibus congue. Phasellus consectetur urna vel nisl pulvinar, ac bibendum lectus vestibulum. Aenean non enim erat. Sed volutpat quis nisi sed sollicitudin. Phasellus efficitur magna rutrum lectus rhoncus elementum.
        Curabitur est ligula, finibus nec mi et, lacinia congue purus. Nunc et metus tellus. In accumsan molestie mi ac lobortis. Integer eu ligula a velit tristique mattis. Etiam a tortor commodo nibh faucibus facilisis. Duis eu dictum turpis. Donec ac augue lobortis, aliquet turpis ut, hendrerit justo. Aliquam sodales hendrerit efficitur. Phasellus scelerisque dictum fringilla.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur magna libero, pellentesque ut nisi quis, sodales vestibulum diam. Curabitur lacinia sodales cursus. Mauris id felis ut ex gravida iaculis. Maecenas eget dapibus purus. Fusce scelerisque mattis est sed commodo. Donec et dictum leo. Fusce lacus justo, accumsan sed ligula et, ullamcorper molestie neque.
        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus feugiat aliquet nulla, sodales ultricies mi ultricies nec. Nullam arcu quam, tincidunt eleifend justo elementum, vestibulum egestas nisl. Duis egestas quam et neque interdum fringilla. Pellentesque in mauris fermentum augue placerat tincidunt vitae sed ex. Vivamus fermentum massa ante, a fermentum elit dapibus vel. Proin iaculis tincidunt tellus, quis ultrices libero porta non. Duis pulvinar elementum est, ut aliquam neque sollicitudin ut. Vivamus non lorem ac est tristique pretium. Fusce est tortor, egestas sed metus vitae, vehicula ultrices nunc.
        Sed eget tristique eros, sed sagittis turpis. Cras augue turpis, consequat a sodales rhoncus, viverra nec tortor. Donec tellus augue, auctor non viverra maximus, vestibulum sed erat. Integer rhoncus tincidunt lectus. Maecenas orci metus, efficitur sit amet tortor vitae, pellentesque suscipit mi. Curabitur et libero aliquam, porttitor libero vitae, pharetra ipsum. Sed in iaculis augue. Ut venenatis faucibus ultrices. Donec gravida vitae mauris id convallis. Aliquam erat volutpat. Integer tristique, neque quis placerat vehicula, ipsum turpis pharetra ligula, in fermentum risus dui ut urna. Nam vestibulum odio id velit auctor laoreet. Nunc rutrum malesuada nisi, sed ultricies neque finibus id. Proin id augue eget nunc vehicula consequat.
        Ut non mauris scelerisque, lacinia enim ac, vestibulum magna. Nulla eget diam mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis feugiat porta magna, et ullamcorper urna vehicula ut. Sed fringilla euismod felis eu egestas. Ut consectetur, metus et vulputate cursus, augue dolor pulvinar mauris, non mollis lacus nunc ut enim. Suspendisse potenti. Nullam quis suscipit nisi. Donec ut finibus ligula. Pellentesque gravida velit mi, euismod gravida metus lobortis porttitor.
        Donec tristique nulla ac egestas sagittis. Fusce lacinia at turpis elementum congue. Curabitur et mollis arcu, et consequat ante. Integer nisl ipsum, accumsan porta erat id, pretium condimentum nisi. Donec nec metus sit amet enim porta pretium. Nulla dui sem, dignissim ut magna quis, commodo vehicula diam. Nam nec posuere orci. Maecenas vel augue eget eros consectetur suscipit quis eu nisi. Aliquam mollis eros magna, at euismod arcu mattis nec. Proin pretium at mauris eget congue. Cras eget tempor nisi. Donec congue rutrum augue. Nam felis augue, scelerisque efficitur porta sit amet, luctus et metus. Suspendisse ut nisl in tellus vehicula rhoncus quis id mi. Pellentesque vel risus at ipsum viverra pretium.
        Integer mollis vel purus vel congue. Pellentesque tortor nulla, rutrum id pulvinar et, consequat vitae leo. Duis arcu nulla, egestas non est in, ultricies bibendum metus. Integer cursus diam sit amet mollis convallis. Integer rhoncus metus eget dui eleifend pharetra. Nullam pellentesque ac orci ut molestie. In hac habitasse platea dictumst.
        Nam ullamcorper mi eu massa vestibulum, quis volutpat diam placerat. Suspendisse sapien neque, volutpat ut ipsum nec, tristique ullamcorper libero. Phasellus luctus mauris enim, id sagittis lectus dictum ac. Nam et nisl in risus faucibus rhoncus. Vestibulum convallis vulputate mi nec iaculis. Quisque porta sodales tincidunt. Donec ac fermentum ex. Aenean faucibus cursus auctor. Donec eleifend, mauris id ultricies lacinia, diam tellus semper lectus, eget placerat lacus tellus nec diam. Donec iaculis urna vitae mattis pretium. Praesent vehicula ultrices odio rutrum ullamcorper. Sed a nunc ut orci pellentesque aliquet. Praesent hendrerit, orci ut iaculis dictum, erat dolor consectetur nulla, commodo accumsan nunc felis quis nulla. Suspendisse venenatis posuere massa, in tincidunt nibh sodales eu.
      </td>
    </tr>

    <tr>
      <td style="width: 5%">2</td>
      <td style="width: 10%;font-size: 11.1px;"><b>Ruang Lingkup</b></td>
      <td style="width:85%;word-break:break-all; word-wrap:break-word;">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ante felis, pulvinar pulvinar eros a, auctor vestibulum purus. Proin pulvinar, justo tincidunt eleifend vestibulum, mi elit bibendum ipsum, vel consequat justo risus laoreet eros. Integer ullamcorper magna urna, eu fringilla ex elementum eu. Duis malesuada et urna malesuada pretium. Nullam nulla ligula, iaculis sed dui nec, imperdiet suscipit mi. Donec rutrum, purus condimentum fermentum aliquet, nisi ligula tincidunt lorem, non pretium ante risus in magna. Curabitur eget imperdiet enim. Pellentesque id pretium metus. Nulla sed ante urna. Sed augue ante, laoreet a urna tristique, bibendum maximus tellus. Nam quis consectetur lectus. Pellentesque vehicula fringilla velit sed congue. Nunc volutpat odio odio, id dignissim elit maximus sit amet. In venenatis, velit sit amet vehicula viverra, magna augue volutpat justo, eget suscipit mauris tortor sed leo. Donec urna ligula, condimentum a mi ut, scelerisque blandit est. Integer nunc ligula, varius id ex a, congue volutpat neque.
        Nullam tincidunt scelerisque ultrices. Aenean nisi sem, facilisis eu elit a, vulputate gravida diam. In suscipit turpis vel magna finibus congue. Phasellus consectetur urna vel nisl pulvinar, ac bibendum lectus vestibulum. Aenean non enim erat. Sed volutpat quis nisi sed sollicitudin. Phasellus efficitur magna rutrum lectus rhoncus elementum.
        Curabitur est ligula, finibus nec mi et, lacinia congue purus. Nunc et metus tellus. In accumsan molestie mi ac lobortis. Integer eu ligula a velit tristique mattis. Etiam a tortor commodo nibh faucibus facilisis. Duis eu dictum turpis. Donec ac augue lobortis, aliquet turpis ut, hendrerit justo. Aliquam sodales hendrerit efficitur. Phasellus scelerisque dictum fringilla.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur magna libero, pellentesque ut nisi quis, sodales vestibulum diam. Curabitur lacinia sodales cursus. Mauris id felis ut ex gravida iaculis. Maecenas eget dapibus purus. Fusce scelerisque mattis est sed commodo. Donec et dictum leo. Fusce lacus justo, accumsan sed ligula et, ullamcorper molestie neque.
        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus feugiat aliquet nulla, sodales ultricies mi ultricies nec. Nullam arcu quam, tincidunt eleifend justo elementum, vestibulum egestas nisl. Duis egestas quam et neque interdum fringilla. Pellentesque in mauris fermentum augue placerat tincidunt vitae sed ex. Vivamus fermentum massa ante, a fermentum elit dapibus vel. Proin iaculis tincidunt tellus, quis ultrices libero porta non. Duis pulvinar elementum est, ut aliquam neque sollicitudin ut. Vivamus non lorem ac est tristique pretium. Fusce est tortor, egestas sed metus vitae, vehicula ultrices nunc.
        Sed eget tristique eros, sed sagittis turpis. Cras augue turpis, consequat a sodales rhoncus, viverra nec tortor. Donec tellus augue, auctor non viverra maximus, vestibulum sed erat. Integer rhoncus tincidunt lectus. Maecenas orci metus, efficitur sit amet tortor vitae, pellentesque suscipit mi. Curabitur et libero aliquam, porttitor libero vitae, pharetra ipsum. Sed in iaculis augue. Ut venenatis faucibus ultrices. Donec gravida vitae mauris id convallis. Aliquam erat volutpat. Integer tristique, neque quis placerat vehicula, ipsum turpis pharetra ligula, in fermentum risus dui ut urna. Nam vestibulum odio id velit auctor laoreet. Nunc rutrum malesuada nisi, sed ultricies neque finibus id. Proin id augue eget nunc vehicula consequat.
        Ut non mauris scelerisque, lacinia enim ac, vestibulum magna. Nulla eget diam mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis feugiat porta magna, et ullamcorper urna vehicula ut. Sed fringilla euismod felis eu egestas. Ut consectetur, metus et vulputate cursus, augue dolor pulvinar mauris, non mollis lacus nunc ut enim. Suspendisse potenti. Nullam quis suscipit nisi. Donec ut finibus ligula. Pellentesque gravida velit mi, euismod gravida metus lobortis porttitor.
        Donec tristique nulla ac egestas sagittis. Fusce lacinia at turpis elementum congue. Curabitur et mollis arcu, et consequat ante. Integer nisl ipsum, accumsan porta erat id, pretium condimentum nisi. Donec nec metus sit amet enim porta pretium. Nulla dui sem, dignissim ut magna quis, commodo vehicula diam. Nam nec posuere orci. Maecenas vel augue eget eros consectetur suscipit quis eu nisi. Aliquam mollis eros magna, at euismod arcu mattis nec. Proin pretium at mauris eget congue. Cras eget tempor nisi. Donec congue rutrum augue. Nam felis augue, scelerisque efficitur porta sit amet, luctus et metus. Suspendisse ut nisl in tellus vehicula rhoncus quis id mi. Pellentesque vel risus at ipsum viverra pretium.
        Integer mollis vel purus vel congue. Pellentesque tortor nulla, rutrum id pulvinar et, consequat vitae leo. Duis arcu nulla, egestas non est in, ultricies bibendum metus. Integer cursus diam sit amet mollis convallis. Integer rhoncus metus eget dui eleifend pharetra. Nullam pellentesque ac orci ut molestie. In hac habitasse platea dictumst.
        Nam ullamcorper mi eu massa vestibulum, quis volutpat diam placerat. Suspendisse sapien neque, volutpat ut ipsum nec, tristique ullamcorper libero. Phasellus luctus mauris enim, id sagittis lectus dictum ac. Nam et nisl in risus faucibus rhoncus. Vestibulum convallis vulputate mi nec iaculis. Quisque porta sodales tincidunt. Donec ac fermentum ex. Aenean faucibus cursus auctor. Donec eleifend, mauris id ultricies lacinia, diam tellus semper lectus, eget placerat lacus tellus nec diam. Donec iaculis urna vitae mattis pretium. Praesent vehicula ultrices odio rutrum ullamcorper. Sed a nunc ut orci pellentesque aliquet. Praesent hendrerit, orci ut iaculis dictum, erat dolor consectetur nulla, commodo accumsan nunc felis quis nulla. Suspendisse venenatis posuere massa, in tincidunt nibh sodales eu.
        	<br><br>
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ante felis, pulvinar pulvinar eros a, auctor vestibulum purus. Proin pulvinar, justo tincidunt eleifend vestibulum, mi elit bibendum ipsum, vel consequat justo risus laoreet eros. Integer ullamcorper magna urna, eu fringilla ex elementum eu. Duis malesuada et urna malesuada pretium. Nullam nulla ligula, iaculis sed dui nec, imperdiet suscipit mi. Donec rutrum, purus condimentum fermentum aliquet, nisi ligula tincidunt lorem, non pretium ante risus in magna. Curabitur eget imperdiet enim. Pellentesque id pretium metus. Nulla sed ante urna. Sed augue ante, laoreet a urna tristique, bibendum maximus tellus. Nam quis consectetur lectus. Pellentesque vehicula fringilla velit sed congue. Nunc volutpat odio odio, id dignissim elit maximus sit amet. In venenatis, velit sit amet vehicula viverra, magna augue volutpat justo, eget suscipit mauris tortor sed leo. Donec urna ligula, condimentum a mi ut, scelerisque blandit est. Integer nunc ligula, varius id ex a, congue volutpat neque.
        Nullam tincidunt scelerisque ultrices. Aenean nisi sem, facilisis eu elit a, vulputate gravida diam. In suscipit turpis vel magna finibus congue. Phasellus consectetur urna vel nisl pulvinar, ac bibendum lectus vestibulum. Aenean non enim erat. Sed volutpat quis nisi sed sollicitudin. Phasellus efficitur magna rutrum lectus rhoncus elementum.
        Curabitur est ligula, finibus nec mi et, lacinia congue purus. Nunc et metus tellus. In accumsan molestie mi ac lobortis. Integer eu ligula a velit tristique mattis. Etiam a tortor commodo nibh faucibus facilisis. Duis eu dictum turpis. Donec ac augue lobortis, aliquet turpis ut, hendrerit justo. Aliquam sodales hendrerit efficitur. Phasellus scelerisque dictum fringilla.
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur magna libero, pellentesque ut nisi quis, sodales vestibulum diam. Curabitur lacinia sodales cursus. Mauris id felis ut ex gravida iaculis. Maecenas eget dapibus purus. Fusce scelerisque mattis est sed commodo. Donec et dictum leo. Fusce lacus justo, accumsan sed ligula et, ullamcorper molestie neque.
        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Phasellus feugiat aliquet nulla, sodales ultricies mi ultricies nec. Nullam arcu quam, tincidunt eleifend justo elementum, vestibulum egestas nisl. Duis egestas quam et neque interdum fringilla. Pellentesque in mauris fermentum augue placerat tincidunt vitae sed ex. Vivamus fermentum massa ante, a fermentum elit dapibus vel. Proin iaculis tincidunt tellus, quis ultrices libero porta non. Duis pulvinar elementum est, ut aliquam neque sollicitudin ut. Vivamus non lorem ac est tristique pretium. Fusce est tortor, egestas sed metus vitae, vehicula ultrices nunc.
        Sed eget tristique eros, sed sagittis turpis. Cras augue turpis, consequat a sodales rhoncus, viverra nec tortor. Donec tellus augue, auctor non viverra maximus, vestibulum sed erat. Integer rhoncus tincidunt lectus. Maecenas orci metus, efficitur sit amet tortor vitae, pellentesque suscipit mi. Curabitur et libero aliquam, porttitor libero vitae, pharetra ipsum. Sed in iaculis augue. Ut venenatis faucibus ultrices. Donec gravida vitae mauris id convallis. Aliquam erat volutpat. Integer tristique, neque quis placerat vehicula, ipsum turpis pharetra ligula, in fermentum risus dui ut urna. Nam vestibulum odio id velit auctor laoreet. Nunc rutrum malesuada nisi, sed ultricies neque finibus id. Proin id augue eget nunc vehicula consequat.
        Ut non mauris scelerisque, lacinia enim ac, vestibulum magna. Nulla eget diam mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis feugiat porta magna, et ullamcorper urna vehicula ut. Sed fringilla euismod felis eu egestas. Ut consectetur, metus et vulputate cursus, augue dolor pulvinar mauris, non mollis lacus nunc ut enim. Suspendisse potenti. Nullam quis suscipit nisi. Donec ut finibus ligula. Pellentesque gravida velit mi, euismod gravida metus lobortis porttitor.
        Donec tristique nulla ac egestas sagittis. Fusce lacinia at turpis elementum congue. Curabitur et mollis arcu, et consequat ante. Integer nisl ipsum, accumsan porta erat id, pretium condimentum nisi. Donec nec metus sit amet enim porta pretium. Nulla dui sem, dignissim ut magna quis, commodo vehicula diam. Nam nec posuere orci. Maecenas vel augue eget eros consectetur suscipit quis eu nisi. Aliquam mollis eros magna, at euismod arcu mattis nec. Proin pretium at mauris eget congue. Cras eget tempor nisi. Donec congue rutrum augue. Nam felis augue, scelerisque efficitur porta sit amet, luctus et metus. Suspendisse ut nisl in tellus vehicula rhoncus quis id mi. Pellentesque vel risus at ipsum viverra pretium.
        Integer mollis vel purus vel congue. Pellentesque tortor nulla, rutrum id pulvinar et, consequat vitae leo. Duis arcu nulla, egestas non est in, ultricies bibendum metus. Integer cursus diam sit amet mollis convallis. Integer rhoncus metus eget dui eleifend pharetra. Nullam pellentesque ac orci ut molestie. In hac habitasse platea dictumst.
        Nam ullamcorper mi eu massa vestibulum, quis volutpat diam placerat. Suspendisse sapien neque, volutpat ut ipsum nec, tristique ullamcorper libero. Phasellus luctus mauris enim, id sagittis lectus dictum ac. Nam et nisl in risus faucibus rhoncus. Vestibulum convallis vulputate mi nec iaculis. Quisque porta sodales tincidunt. Donec ac fermentum ex. Aenean faucibus cursus auctor. Donec eleifend, mauris id ultricies lacinia, diam tellus semper lectus, eget placerat lacus tellus nec diam. Donec iaculis urna vitae mattis pretium. Praesent vehicula ultrices odio rutrum ullamcorper. Sed a nunc ut orci pellentesque aliquet. Praesent hendrerit, orci ut iaculis dictum, erat dolor consectetur nulla, commodo accumsan nunc felis quis nulla. Suspendisse venenatis posuere massa, in tincidunt nibh sodales eu.
      </td>
    </tr>


  </tbody>
</table>';
		$this->load->library('PdfGenerator');
		$this->pdfgenerator->generate($html,'DOK-TESTER-NJKI','A4','potrait'); 
	}
	public function cekkk(){
		$this->load->view('cek');
	}
}
