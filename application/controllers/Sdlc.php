<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sdlc extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $profile;
	private $divisiUAM;
	private $segmenUAM;
	private $tregUAM;
	private $witelUAM;
	private $amUAM;
	
	public function __construct(){
		date_default_timezone_set("Asia/Bangkok");
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('query'); 
		$this->load->model('formula'); 
		$this->load->model('datatable'); 
		$this->load->model('sdlc_handler');
		
		ini_set('max_execution_time', 123456);
		ini_set("memory_limit","1256M");
			
		// $session = checkingsessionpwt();
		$session	 = $this->session->userdata('sessSpazzle'); 
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function getdata(){
		if(checkingsessionpwt()){

			$columnsDefault = [
				'id_sdlc' => true,
				'type_tab' => true,
				'nama_sdlc' => true,
				'mandatory' => true,
				'template_sdlc' => true,
				'type_sdlc' => true,
				'type_upload' => true,
				'actions' => true
			];
			$arraynya	= $columnsDefault;
			$jsonfile	= $this->sdlc_handler->data();
			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}

	public function insert(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sessSpazzle');
			$userid 	= $userdata['userid'];
			$now 		= date('Y-m-d h:i:s'); 

			$nama_sdlc	= trim(strip_tags(stripslashes($this->input->post('nama_sdlc',true))));
			$mandatory	= trim(strip_tags(stripslashes($this->input->post('mandatory',true))));
			$type		= trim(strip_tags(stripslashes($this->input->post('type',true))));
			$kategori	= trim(strip_tags(stripslashes($this->input->post('kategori',true))));
			$type_upload = trim(strip_tags(stripslashes($this->input->post('type_upload',true))));
			$is_show_template	= trim(strip_tags(stripslashes($this->input->post('is_show_template',true))))?:0;

			$template		= $_FILES['doctemplate'];
			$query = $this->query->getData('param_sdlc','max(id_sdlc)+1 as id_sdlc','');
			$getID = array_shift($query);
			if ($getID['id_sdlc']=='') {
				$id = '1';
			} else {
				$id = $getID['id_sdlc'];
			}

			$direktori_stg 		= './attachment/helpdoc/';
			$lokasi_file_stg  	= $_FILES['doctemplate']['tmp_name'];
			$tipe_file_stg     	= $_FILES['doctemplate']['type'];
			$nama_file_stg     	= $_FILES['doctemplate']['name'];
			$nama_file_unik_stg	= str_replace(' ','_', $nama_file_stg);
			$extension_stg = pathinfo($nama_file_stg, PATHINFO_EXTENSION);			
			if (!empty($lokasi_file_stg)){
				$vfile_upload_stg = $direktori_stg . $nama_file_unik_stg;
				move_uploaded_file($lokasi_file_stg, $vfile_upload_stg);
			}

			$data = array(
				"id_sdlc" => $id,
				"nama_sdlc"=>$nama_sdlc,
				"mandatory"=>$mandatory,
				"template_sdlc"=>$nama_file_unik_stg,
				"type_sdlc"=> $type,
				"type_tab"=> $kategori,
				"type_upload"=> $type_upload,
				"is_show_template" => $is_show_template				 
			);
			$row 	= $this->db->insert('param_sdlc', $data);
			if($row){
				$url 	= "Manage SDLC";
				$activity 	= "INSERT";
				$log = $this->query->insertlog($activity,$url,$id);				
				print json_encode(array('success'=>true,'total'=>1));				

			}

		} else {
			redirect('/panel');
		}
	}	

	public function modal(){
		if(checkingsessionpwt()){
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			$datarole			= $this->query->getData('param_sdlc','*',"WHERE id_sdlc='".$id."' ORDER BY id_sdlc DESC");			
			header('Content-type: application/json; charset=UTF-8');			
			if (isset($id) && !empty($id)) {
				foreach($datarole as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	

	public function update(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sessSpazzle'); 
			$userid 	= $userdata['userid'];
			$now 		= date('Y-m-d h:i:s'); 
			$id			= trim(strip_tags(stripslashes($this->input->post('ed_id_sdlc',true))));				
			$nama_sdlc	= trim(strip_tags(stripslashes($this->input->post('ed_nama_sdlc',true))));
			$mandatory	= trim(strip_tags(stripslashes($this->input->post('ed_mandatory',true))));
			$type		= trim(strip_tags(stripslashes($this->input->post('ed_type',true))));
			$kategori 	= trim(strip_tags(stripslashes($this->input->post('ed_kategori',true))));
			$type_upload = trim(strip_tags(stripslashes($this->input->post('ed_type_upload',true))));
			$is_show_template	= trim(strip_tags(stripslashes($this->input->post('ed_is_show_template',true))))?:0;

			$cekfile = empty($_FILES["ed_doctemplate"]["name"]);
			if ($cekfile == 1){
				$data = array(
				    "nama_sdlc"=>$nama_sdlc,
					"mandatory"=>$mandatory,
					"type_sdlc"=>$type,
					"type_tab"=>$kategori,
					"type_upload"=>$type_upload,
					"is_show_template" => $is_show_template				 
				);
			}else{
				$getdatadoc 	= $this->db->query("SELECT * FROM param_sdlc WHERE id_sdlc='".$id."'")->result_array();
				$rowdoc	= array_shift($getdatadoc);
				$dirr = './attachment/helpdoc/';
				@unlink($dirr.$rowdoc['template_sdlc']);			
				$direktori_stg 		= './attachment/helpdoc/';
				$lokasi_file_stg  	= $_FILES['ed_doctemplate']['tmp_name'];
				$tipe_file_stg     	= $_FILES['ed_doctemplate']['type'];
				$nama_file_stg     	= $_FILES['ed_doctemplate']['name'];
				$nama_file_unik_stg	= str_replace(' ','_', $nama_file_stg);
				$extension_stg = pathinfo($nama_file_stg, PATHINFO_EXTENSION);			
				if (!empty($lokasi_file_stg)){
					$vfile_upload_stg = $direktori_stg . $nama_file_unik_stg;
					move_uploaded_file($lokasi_file_stg, $vfile_upload_stg);
				}
				$data = array(
				    "nama_sdlc"=>$nama_sdlc,
					"mandatory"=>$mandatory,
					"template_sdlc"=>$nama_file_unik_stg,
					"type_sdlc"=>$type,
					"type_tab"=>$kategori,
					"type_upload" => $type_upload,
					"is_show_template" => $is_show_template
				);
			}
			
			$this->db->where('id_sdlc', $id);
			$updatesdlc = $this->db->update('param_sdlc', $data);
			if($updatesdlc){
				$url 			= "Manage SDLC";
				$activity 		= "UPDATE";
				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));				
			}

		} else {
			redirect('/panel');
		}
	}	
	public function updatesop(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sessSpazzle'); 
			$userid 	= $userdata['userid'];
			$now 		= date('Y-m-d h:i:s'); 
			$fileold	= trim(strip_tags(stripslashes($this->input->post('doksop_ex',true))));				
			$dirr = './attachment/helpdoc/';
			@unlink($dirr.$fileold);			
			$direktori_stg 		= './attachment/helpdoc/';
			$lokasi_file_stg  	= $_FILES['docsop']['tmp_name'];
			$tipe_file_stg     	= $_FILES['docsop']['type'];
			$nama_file_stg     	= $_FILES['docsop']['name'];
			$nama_file_unik_stg	= str_replace(' ','_', $nama_file_stg);
			$extension_stg = pathinfo($nama_file_stg, PATHINFO_EXTENSION);			
			if (!empty($lokasi_file_stg)){
				$vfile_upload_stg = $direktori_stg . $nama_file_unik_stg;
				move_uploaded_file($lokasi_file_stg, $vfile_upload_stg);
			}
			$data = array(
			    "config_value"=>$nama_file_unik_stg 
			);
			$this->db->where('config_name', 'dok_sop');
			$updatesdlc = $this->db->update('configsite', $data);
			if($updatesdlc){
				$url 			= "Manage Config Site";
				$activity 		= "UPDATE";
				$log = $this->query->insertlog($activity,$url,'2');
				print json_encode(array('success'=>true,'total'=>1));				
			}

		} else {
			redirect('/panel');
		}
	}
	public function updatetata(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sessSpazzle'); 
			$userid 	= $userdata['userid'];
			$now 		= date('Y-m-d h:i:s'); 
			$fileold	= trim(strip_tags(stripslashes($this->input->post('doktata_ex',true))));				
			$dirr = './attachment/helpdoc/';
			@unlink($dirr.$fileold);			
			$direktori_stg 		= './attachment/helpdoc/';
			$lokasi_file_stg  	= $_FILES['doctata']['tmp_name'];
			$tipe_file_stg     	= $_FILES['doctata']['type'];
			$nama_file_stg     	= $_FILES['doctata']['name'];
			$nama_file_unik_stg	= str_replace(' ','_', $nama_file_stg);
			$extension_stg = pathinfo($nama_file_stg, PATHINFO_EXTENSION);			
			if (!empty($lokasi_file_stg)){
				$vfile_upload_stg = $direktori_stg . $nama_file_unik_stg;
				move_uploaded_file($lokasi_file_stg, $vfile_upload_stg);
			}
			$data = array(
			    "config_value"=>$nama_file_unik_stg 
			);
			$this->db->where('config_name', 'dok_tata_kelola');
			$updatesdlc = $this->db->update('configsite', $data);
			if($updatesdlc){
				$url 			= "Manage Config Site";
				$activity 		= "UPDATE";
				$log = $this->query->insertlog($activity,$url,'1');
				print json_encode(array('success'=>true,'total'=>1));				
			}

		} else {
			redirect('/panel');
		}
	}
	

	public function delete(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sessSpazzle');
			$userid 	= $userdata['userid'];
			$cond		= trim(strip_tags(stripslashes($this->input->post('iddelsdlc',true))));
			//unlink file
			$getdatadoc 	= $this->db->query("SELECT * FROM param_sdlc WHERE id_sdlc=".$cond."")->result_array();
			$rowdoc	= array_shift($getdatadoc);
			$dirr = './attachment/helpdoc/';
			@unlink($dirr.$rowdoc['template_sdlc']);			
			//delete data
			$rows = $this->db->delete('param_sdlc', array('id_sdlc' => $cond));
			if(isset($rows)) {
				$url 		= "Manage SDLC";
				$activity 	= "DELETE";						
				$log = $this->query->insertlog($activity,$url,$cond);
				print json_encode(array('success'=>true,'rows'=>$rows, 'id'=>$id ,'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function getdataEksMod($id){
		if(checkingsessionpwt()){
			error_reporting(0);
			$dataroleAll		= $this->query->getData('menu','*',"where parent='0' ORDER BY sort ASC");
			
			// header('Content-type: application/json; charset=UTF-8');
			
			
				echo '
					<style>
					.checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {z-index:2;}
					</style>
					<div class="form-group row">
					<label for="menu_fitur" class="col-sm-2 col-form-label">Module</label>
					<div class="col-sm-12" style="padding-top:10px;">
						<table class="smalltabl nowrap table" width=100%>
						<thead class="bg-gray-dark">
							<th>
								<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
									<input id="ed_selectAll" type="checkbox"> <b>Menu Name</b>
									<span></span>
								</label>
							</th>
							<th class="text-right"><b>Fitur</b></th>
						</thead>
						<tbody>';
						foreach($dataroleAll as $data) {
						$getdataEksrole	= $this->query->getData('role_menu','*',"WHERE id_role='$id' and id_menu='".$data['id_menu']."'");
						$dataEksrole		= array_shift($getdataEksrole);
						if ($dataEksrole!='') { $ceked = "checked"; } else { $ceked = ""; }
						echo "
						<tr>
							<td>
								<label class='kt-checkbox kt-checkbox--bold kt-checkbox--brand'>
									<input value='".$data['id_menu']."' id='ed_checkbox".$data['id_menu']."' type='checkbox' name='ed_menu[]' ".$ceked.">
									".$data['menu']."
									<span></span>
								</label>
							</td>
							<td class='text-right kt-checkbox-inline'>
						";
							$data_fitur = explode_fitur($data['fitur']);
							for($x=0;$x<count($data_fitur);$x++)
							{
								$dataEksFitur = explode_fitur($dataEksrole['akses']);
								if(in_array($data_fitur[$x],$dataEksFitur)){ $ceked_fitur[$x] = 'checked'; } else {   $ceked_fitur[$x] = ''; }
								
								$CekSub	= $this->query->getNumRows('menu','*',"WHERE parent='".$data['id_menu']."'")->num_rows();
								// if ($CekSub>0) { echo ''; } else {
								echo "
									<label class='kt-checkbox kt-checkbox--bold kt-checkbox--brand'>
										<input value='".$data_fitur[$x]."' id='ed_checkbox".$data['id_menu'].$data_fitur[$x]."' type='checkbox' name='ed_fitur[".$data['id_menu']."][]' ".$ceked_fitur[$x].">
										".$data_fitur[$x]."
										<span></span>
									</label>
								";
								echo "
									<script>
										$('#ed_checkbox".$data['id_menu']."').change(function() {
											if($('#ed_checkbox".$data['id_menu']."').prop( 'checked' )){
												$('#ed_checkbox".$data['id_menu'].$data_fitur[$x]."').prop('checked', true);	
											}else{
												$('#ed_checkbox".$data['id_menu'].$data_fitur[$x]."').prop('checked', false);	
											}
										});
									</script>";
								// }
							}
							echo '<script>
									$("#ed_selectAll").change(function() {
										if($("#ed_selectAll").prop( "checked" )){
											$("#ed_checkbox'.$data['id_menu'].'").prop("checked", true);
											$( "#ed_checkbox'.$data['id_menu'].'" ).trigger( "change" );
										}else{
											$("#ed_checkbox'.$data['id_menu'].'").prop("checked", false);
											$( "#ed_checkbox'.$data['id_menu'].'" ).trigger( "change" );
										}
									});
								</script>';
							
							// GET SUB MENU
							$getSubMenu 	= $this->query->getData('menu','*',"WHERE parent='".$data['id_menu']."' order by sort asc");
							foreach ($getSubMenu as $data) {
								$getdataEksrole	= $this->query->getData('role_menu','*',"WHERE id_role='$id' and id_menu='".$data['id_menu']."'");
								$dataEksrole		= array_shift($getdataEksrole);
								if ($dataEksrole!='') { $ceked = "checked"; } else { $ceked = ""; }
								echo "
								<tr style='background: rgba(0,0,0,.04)!important;'>
									<td style='padding-left: 30px!important;'>
										<label class='kt-checkbox kt-checkbox--bold kt-checkbox--brand'>
											<input value='".$data['id_menu']."' id='ed_checkbox".$data['id_menu']."' type='checkbox' name='ed_menu[]' ".$ceked.">
											".$data['menu']."
											<span></span>
										</label>
									</td>
									<td class='text-right'>
								";
									$data_fitur = explode_fitur($data['fitur']);
									for($x=0;$x<count($data_fitur);$x++)
									{
										$dataEksFitur = explode_fitur($dataEksrole['akses']);
										if(in_array($data_fitur[$x],$dataEksFitur)){ $ceked_fitur[$x] = 'checked'; } else {   $ceked_fitur[$x] = ''; }
										echo "
											<label class='kt-checkbox kt-checkbox--bold kt-checkbox--brand'>
												<input value='".$data_fitur[$x]."' id='ed_checkbox".$data['id_menu'].$data_fitur[$x]."' type='checkbox' name='ed_fitur[".$data['id_menu']."][]' ".$ceked_fitur[$x]."> ".$data_fitur[$x]."
												<span></span>
											</label>
											<script>
												$('#ed_checkbox".$data['id_menu']."').change(function() {
													if($('#ed_checkbox".$data['id_menu']."').prop( 'checked' )){
														$('#ed_checkbox".$data['id_menu'].$data_fitur[$x]."').prop('checked', true);	
													}else{
														$('#ed_checkbox".$data['id_menu'].$data_fitur[$x]."').prop('checked', false);	
													}
												});
											</script>";
									}
									echo '<script>
											$("#ed_selectAll").change(function() {
												if($("#ed_selectAll").prop( "checked" )){
													$("#ed_checkbox'.$data['id_menu'].'").prop("checked", true);
													$( "#ed_checkbox'.$data['id_menu'].'" ).trigger( "change" );
												}else{
													$("#ed_checkbox'.$data['id_menu'].'").prop("checked", false);
													$( "#ed_checkbox'.$data['id_menu'].'" ).trigger( "change" );
												}
											});
										</script>';
							}
						}
						
						echo '
						
						</tbody>
						</table>													
					</div>
				</div>						
				';
			
		} else {
			redirect('/panel');
		}
	}
}
