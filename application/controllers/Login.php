<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $akses;
	private $userid;
	// private $divisiUAM;
	// private $segmenUAM;
	// private $tregUAM;
	// private $witelUAM;
	// private $amUAM;
	
	public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->load->model('auth'); 
		$this->load->model('query');
		$this->load->model('formula');
		$this->load->model('datatable');
		
		// if(checkingsessionpwt()){
			// $session = checkingsessionpwt();
		$session	 	= $this->session->userdata('sessSpazzle'); 
		@$userid 	 	= $session['userid'];
		@$profile 	 	= $session['id_role'];
		$menu 		 	= uri_string();
		$data_akses  	= $this->query->getAkses($profile,$menu);
		@$shift 		 	= array_shift($data_akses);
		@$this->akses 	= $shift['akses'];
		@$this->userid 	= $userid;

		$now 			= date('Y-m-d');
		$yesterday 		= date("Y-m-d",strtotime("-1 day"));
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function ceklogin() {
		// ambil cookie

		$username	= trim(strip_tags(stripslashes($this->input->post('username',true))));
		$password	= trim(strip_tags(stripslashes($this->input->post('password',true))));
		$epassword	= md5(trim(strip_tags(stripslashes($this->input->post('password',true)))));
		$remember	= trim(strip_tags(stripslashes($this->input->post('remember',true))));

		$row 		= $this->auth->getuser($username)->row_array();
		@$id 		= $row['userid'];

		if(isset($row['userid'])) {
			if($row['ldap']!=1){
				if ($row['id_role']==0) {
					echo "";
				} else {
					if ($remember=='on') {
						if($row['password']==$epassword){
							$rows = array('data'=>$row);
							$this->session->set_userdata('sessSpazzle', $row);
							$coba = $this->session->userdata('sessSpazzle');
							print $row['name'];
						}else{
							echo "";
						}
					} else {
						if($row['password']==$epassword){
							$rows = array('data'=>$row);
							$this->session->set_userdata('sessSpazzle', $row);
							$coba = $this->session->userdata('sessSpazzle');
							print $row['name'];
						}else{
							echo "";
						}
					}
				}
			} else {
				if ($row['id_role']==0) {
					echo "";
				} else {
					$CGCauth = $this->auth_ldap($username, $password);
					if($CGCauth!='null'){
						$rows = array('data'=>$row);
						$this->session->set_userdata('sessSpazzle', $row);
						$coba = $this->session->userdata('sessSpazzle');

						print $row['name'];
					}else{
						echo "";
					}
				}
			}
		}else{
			$CGCauth = $this->auth_ldap($username, $password);
			if($CGCauth!='null'){
				$datauser = json_decode($CGCauth,true);
				$uname = $datauser['username'];
				$cek 	= $this->auth->getuser($uname)->result_array();
				@$uid 		= $cek['userid'];
				if(isset($cek['userid'])) {
					echo "";
				}else{
					$ss = $this->register_new_user($uname);
					$this->session->set_userdata('sessSpazzle', $ss);
					$coba = $this->session->userdata('sessSpazzle');
					echo $datauser['name'];	
				}
			}else{
				echo "";
			}
		}
	}
	public function register_new_user($username){
		$getdatauser = $this->getprofileLDAP($username);
		$datauser = json_decode($getdatauser,true);
		@$username = $datauser['username'];
		@$name =$datauser['name'];
		@$email =$datauser['email'];
		@$phone =$datauser['phone'];
		$getMaxUID	= $this->db->query("SELECT max(userid)as max_id_user FROM mi.user")->result_array();
		$maxUID		= array_shift($getMaxUID);
		$id			= $maxUID['max_id_user']+1;
		$data_return = array(
			"userid" => $id,
			"username"=>$username,
			"name"=>$name,
			"picture"=>$username.'.jpg',
			"id_role"=>3,
			"level_user"=>3
		);
		$data_register = array(
			"userid" => $id,
			"username"=>$username,
			"name"=>$name,
			"id_role"=>3,
			"picture"=>$username.'.jpg',
			"email"=>$email,
			"phone" => $phone,
			"ldap"=>1,
			"level_user"=>1
		); 
		$rows = $this->query->insertData_Secure('mi.user', $data_register);
		return $data_return;
	}
	public function getprofileLDAP($username){
		$url2 		= 'https://auth.telkom.co.id/api/call/'.$username.'';
		$JSON2 		= file_get_contents($url2);
		
		$gdata2 	= json_decode(json_encode($JSON2), True);
		$data2		= json_decode($gdata2);
		
		$user		= $data2->username;
		$name		= $data2->name;
		$email		= $data2->email;
		$phone		= $data2->phone;

		if (!empty($user)) {
			// $url 		= 'https://myworkbook.telkom.co.id/mwb/api/index.php?r=api/photo&nik='.$user;
			$url 		= 'https://pwb.telkom.co.id/index.php?r=pwbPhoto/profilePhoto&nik='.$user;
			/* Extract the filename */
			$gfilename 	= substr($url, strrpos($url, '/') + 1);
			$filename 	= str_replace('profilePhoto&nik=','',$gfilename);
			/* Save file wherever you want */
			file_put_contents('./images/user/'.$filename.'.jpg', file_get_contents($url));

			$ret = array(
				"name" 		=> $name,
				"username" 	=> $user,
				"email" 	=> $email,
				"phone"		=> $phone
			);
		} else {
			echo "";
		}
		//header('Content-Type: application/json');
		return json_encode($ret);
	}
	public function auth_ldap($username, $password){
		// function auth_ldap2(){
		// $username 	= '400624';
		// $password 	= '30SolCrea';
		$url 		= 'https://auth.telkom.co.id/services/auth?username='.$username.'&password='.$password.'';
		$JSON 		= file_get_contents($url);
		
		$gdata 		= json_decode(json_encode($JSON), True);
		$data		= json_decode($gdata);
		$sukses		= $data->login;
		
		if ($sukses=='1') {
			$url2 = 'https://auth.telkom.co.id/api/call/'.$username.'';
			$JSON2 = file_get_contents($url2);
			
			$gdata2 	= json_decode(json_encode($JSON2), True);
			$data2		= json_decode($gdata2);
			$user		= $data2->username;
			$name		= $data2->name;
			$email		= $data2->email;
			
			// echo $JSON2;
			$ret = array(
				"name" 		=> $name,
				"username" 	=> $username,
				"email" 	=> $email
			);
		} else {
			$ret = null;
		}
		return json_encode($ret);
    }

    public function logout(){
		$this->session->sess_destroy();
        redirect('./panel');
	}
}
