<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $profile;
	private $divisiUAM;
	private $segmenUAM;
	private $tregUAM;
	private $witelUAM;
	private $amUAM;
	
	public function __construct(){
		date_default_timezone_set("Asia/Bangkok");
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('query'); 
		$this->load->model('formula'); 
		$this->load->model('datatable'); 
		$this->load->model('pengajuan_handler');
		$this->load->model('laporan_handler');
		
		ini_set('max_execution_time', 123456);
		ini_set("memory_limit","1256M");
			
		// $session = checkingsessionpwt();
		$session	 = $this->session->userdata('sesspwt'); 
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function getdatapengajuan($filter){
		if(checkingsessionpwt()){
			$columnsDefault = [
				'no_request'		=> true,
				'nama_pengaju'		=> true,
				'nama_pelanggan'	=> true,
				'nipnas'			=> true,
				'service_id'		=> true,
				'nama_project'		=> true,
				'subject'			=> true,
				'creadet_at'		=> true,
				'status'			=> true,
				'docpengajuan'	    => true,
				'docjawaban'	    => true,
				'docpengajuanas'	=> true,
				'docjawabanas'	    => true,
			];
			$arraynya	= $columnsDefault;

			$jsonfile	= $this->laporan_handler->data($filter);

			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}

	public function insert($type){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$now 		= date('ymd');

			$namaproject 	= trim(strip_tags(stripslashes($this->input->post('namaproject',true))));
			$subject 		= trim(strip_tags(stripslashes($this->input->post('subject',true))));
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('pelanggan',true))));
			$serviceid 		= trim(strip_tags(stripslashes($this->input->post('serviceid',true))));
			$reviewer1 		= trim(strip_tags(stripslashes($this->input->post('reviewer1',true))));
			$approval1 		= trim(strip_tags(stripslashes($this->input->post('approval1',true))));
			$approval2 		= trim(strip_tags(stripslashes($this->input->post('approval2',true))));
			$approval3 		= trim(strip_tags(stripslashes($this->input->post('approval3',true))));
			$timtarif1 		= trim(strip_tags(stripslashes($this->input->post('timtarif1',true))));
			$timtarif2 		= trim(strip_tags(stripslashes($this->input->post('timtarif2',true))));
			$latarbelakang 	= $_POST['latarbelakang'];
			$aspekstrategis = $_POST['aspekstrategis'];
			$aspekfinansial = $_POST['aspekfinansial'];
			$aspekkompetisi = $_POST['aspekkompetisi'];
			$konfigurasiteknis = $_POST['konfigurasiteknis'];

			$filename 		= $_FILES['attach'];

			$jmlfile 		= count($filename);
			for($ia=0;$ia<$jmlfile;$ia++) {
				$fileNames 					= str_replace(' ','_',$_FILES['attach'][$ia]['name']);
				$config['upload_path'] 		= './attachment/'; //buat folder dengan nama assets di root folder
		        $config['file_name'] 		= $fileNames;
		        $config['allowed_types'] 	= 'pdf';
		        $config['max_size'] 		= 100000000;

		        $this->load->library('upload');
				$this->upload->initialize($config);
				if(! $this->upload->do_upload('attach') )
				$this->upload->display_errors();
		        $media			 	= $this->upload->data('attach');

		        $insAttach 	= $this->db->query("INSERT INTO sbrattach (no_request,file) values ('$norequest','$fileNames')");
			}

			if ($type=='draft') {
				$status 	= '0';
				$current 	= '1';
			} else {
				$status 	= '1';
				$current 	= '2';
			}

			$cekMax 		= $this->db->query("SELECT MAX(id)+1 maxid from sbrdoc")->result_array();
			$gMax 			= array_shift($cekMax);
			if ($gMax['maxid']==0) {
				$lastid 	= '1';
			} else {
				$lastid 	= $gMax['maxid'];
			}

			$code 			= sprintf("%06d", $lastid);
			$maxid 			= $code;

			$norequest 		= 'SBR/'.$now.'/'.$pelanggan.'/'.$maxid;
			$createdat 		= date('Y-m-d H:i:s');

			// INSERT NEW PENGAJUAN
			$rows 			= $this->db->query("
							INSERT INTO sbrdoc (no_request, nipnas, nama_project, subject, latar_belakang, aspek_strategis, aspek_finansial, aspek_kompetisi, konfigurasi_teknis, status, created_by, created_at, reviewer1, approval1, approval2, service_id, current, timtarif1, timtarif2, approval3)
							VALUES
							('$norequest', '$pelanggan', '$namaproject', '$subject', '$latarbelakang', '$aspekstrategis', '$aspekfinansial', '$aspekkompetisi', '$konfigurasiteknis', '$status', '$userid', '$createdat'::TIMESTAMP, '$reviewer1', '$approval1', '$approval2', '$serviceid', '$current', '$timtarif1', '$timtarif2', '$approval3')
							");
			
			$id				= $this->db->insert_id();
			$url 			= "Pengajuan SBR";
			$activity 		= "INSERT";

			if($rows) {
				// INSERT HISTORY AND NOTIFICATIONS
				if ($type!='draft') {
					$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', 'Pengajuan SBR Baru', '$userid', '$createdat'::TIMESTAMP, 'new', '$reviewer1' ,'0')
								");
				}

				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
		} else {
			redirect('/panel');
		}
	}	

	public function jmlsbr($status){
		if(checkingsessionpwt()){
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];

			$getUser 		= $this->db->query("
								SELECT a.*, 
									(SELECT level from mi.level_user where id_level=a.level_user) step,
									(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
								FROM mi.user a where userid='$userid'
							")->result_array();
			$dUser 			= array_shift($getUser);
			$level			= $dUser['step'];

			if ($level==2) {
				$cond 		= "and reviewer1='$userid'";
			} else if ($level==3) {
				$cond 		= "and approval1='$userid'";
			} else if ($level==4) {
				$cond 		= "and approval2='$userid'";
			} else if ($level==5) {
				$cond 		= "and timtarif1='$userid'";
			} else if ($level==6) {
				$cond 		= "and timtarif2='$userid'";
			} else if ($level==7) {
				$cond 		= "and approval3='$userid'";
			} else if ($level==0) {
				$cond 		= "";
			} else {
				$cond 		= "and created_by='$userid'";
			}
			$getPending 	= $this->db->query("SELECT * from sbrdoc where status not in (4,5) $cond")->num_rows();

			echo $getPending.' <span>Record</span>';
		} else {
			redirect('/panel');
		}
	}	

	public function modal(){
		if(checkingsessionpwt()){
			
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			
			$dataUsr			= $this->db->query("
								SELECT a.* ,
									(SELECT name from mi.user where userid=a.created_by) name_am,
									(SELECT username from mi.user where userid=a.reviewer1) usernamerev,
									(SELECT name from mi.user where userid=a.reviewer1) namerev1,
									(SELECT username from mi.user where userid=a.approval1) usernameappr1,
									(SELECT name from mi.user where userid=a.approval1) nameappr1,
									(SELECT username from mi.user where userid=a.approval2) usernameappr2,
									(SELECT name from mi.user where userid=a.approval2) nameappr3,
									(SELECT username from mi.user where userid=a.approval3) usernameappr3,
									(SELECT name from mi.user where userid=a.approval3) nameappr3,
									(SELECT username from mi.user where userid=a.timtarif1) usernametrf1,
									(SELECT name from mi.user where userid=a.timtarif1) nametrf1,
									(SELECT username from mi.user where userid=a.timtarif2) usernametrf2,
									(SELECT name from mi.user where userid=a.timtarif2) nametrf2,
									(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan
								FROM sbrdoc a where id='$id'
								")->result_array();
			
			header('Content-type: application/json; charset=UTF-8');
			
			if (isset($id) && !empty($id)) {
				foreach($dataUsr as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	

	public function update($type){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$now 		= date('ymd');

			$id 			= trim(strip_tags(stripslashes($this->input->post('ed_id',true))));
			$norequest 		= trim(strip_tags(stripslashes($this->input->post('ed_norequest',true))));
			$statusbefore 	= trim(strip_tags(stripslashes($this->input->post('statusbefore',true))));
			$namaproject 	= trim(strip_tags(stripslashes($this->input->post('ed_namaproject',true))));
			$subject 		= trim(strip_tags(stripslashes($this->input->post('ed_subject',true))));
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('ed_pelanggan',true))));
			$serviceid 		= trim(strip_tags(stripslashes($this->input->post('ed_serviceid',true))));
			$reviewer1 		= trim(strip_tags(stripslashes($this->input->post('ed_reviewer1',true))));
			$approval1 		= trim(strip_tags(stripslashes($this->input->post('ed_approval1',true))));
			$approval2 		= trim(strip_tags(stripslashes($this->input->post('ed_approval2',true))));
			$timtarif1 		= trim(strip_tags(stripslashes($this->input->post('ed_timtarif1',true))));
			$timtarif2 		= trim(strip_tags(stripslashes($this->input->post('ed_timtarif2',true))));
			$approval3 		= trim(strip_tags(stripslashes($this->input->post('ed_approval3',true))));
			$latarbelakang 	= $_POST['ed_latarbelakang'];
			$aspekstrategis = $_POST['ed_aspekstrategis'];
			$aspekfinansial = $_POST['ed_aspekfinansial'];
			$aspekkompetisi = $_POST['ed_aspekkompetisi'];
			$konfigurasiteknis = $_POST['ed_konfigurasiteknis'];

			if ($type=='draft') {
				$status 	= '0';
				$current 	= '1';
				$uptstatus 	= '';
			} else {
				$status 	= '1';
				$current 	= '2';
				$uptstatus 	= "status = '".$status."',";
			}

			$createdat 		= date('Y-m-d H:i:s');

			// UPDATE PENGAJUAN
			$rows 			= $this->db->query("
							UPDATE sbrdoc set 
								nipnas				= '$pelanggan',
								nama_project		= '$namaproject',
								subject				= '$subject',
								latar_belakang		= '$latarbelakang',
								aspek_strategis		= '$aspekstrategis',
								aspek_finansial		= '$aspekfinansial',
								aspek_kompetisi		= '$aspekkompetisi',
								konfigurasi_teknis	= '$konfigurasiteknis',
								$uptstatus
								updated_by			= '$userid',
								updated_at			= '$createdat'::TIMESTAMP,
								reviewer1			= '$reviewer1',
								approval1			= '$approval1',
								approval2			= '$approval2',
								approval3			= '$approval3',
								timtarif1			= '$timtarif1',
								timtarif2			= '$timtarif2',
								service_id			= '$serviceid',
								current				= '$current'
							WHERE id='$id'
							");
			
			$url 			= "Pengajuan SBR";
			$activity 		= "UPDATE";

			if($rows) {
				// INSERT UPDATE HISTORY AND NOTIFICATIONS
				if ($type!='draft') {
					if ($statusbefore==2) {
						$insHis 	= $this->db->query("
									INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
									('$norequest', 'Perbaikan Pengajuan SBR', '$userid', '$createdat'::TIMESTAMP, 'republish', '$reviewer1' ,'0')
									");
					} else {
						$insHis 	= $this->db->query("
									INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
									('$norequest', 'Pengajuan SBR Baru', '$userid', '$createdat'::TIMESTAMP, 'new', '$reviewer1' ,'0')
									");
					}
				}

				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));
			} else {
				echo "";
			}
			$log = $this->query->insertlog($activity,$url,$id);
		} else {
			redirect('/panel');
		}
	}	

	public function delete(){
		if(checkingsessionpwt()){
			$url 		= "Pengajuan";
			$activity 	= "DELETE";
			
			$this->load->model('query');
			
			$cond		= trim(strip_tags(stripslashes($this->input->post('iddel',true))));

			$getnoreq 	= $this->db->query("SELECT * FROM sbrdoc where id='$cond'")->result_array();
			$noreq 		= array_shift($getnoreq);
			$norequest 	= $noreq['no_request'];
			
			$rows 		= $this->db->query("DELETE FROM sbrdoc where id='$cond'");
			
			if(isset($rows)) {
				$del 	= $this->db->query("DELETE FROM sbrhistory where no_request='$norequest'");
				$log 	= $this->query->insertlog($activity,$url,$cond);
				print json_encode(array('success'=>true,'rows'=>$rows, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function actionsubmit(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$createdat 	= date('Y-m-d H:i:s');

			$id			= trim(strip_tags(stripslashes($this->input->post('idsubmitact',true))));
			$comment	= $_POST['comment'];
			$gtype		= trim(strip_tags(stripslashes($this->input->post('typesubmitact',true))));
			$type 		= strtolower($gtype);

			$url 		= "Pengajuan";
			$activity 	= $type;

			$getdoc 	= $this->db->query("SELECT * FROM sbrdoc where id='$id'")->result_array();
			$doc 		= array_shift($getdoc);
			$norequest 	= $doc['no_request'];

			$getUser 	= $this->db->query("
						SELECT a.*, 
							(SELECT level from mi.level_user where id_level=a.level_user) step,
							(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
						FROM mi.user a where userid='$userid'
						")->result_array();
			$dUser 		= array_shift($getUser);
			$level		= $dUser['step'];

			if ($type=='submit' or $type=='resubmit') {
				$sendto = $doc['reviewer1'];
				$curr 	= 2;
			} else if ($type=='escalation') {
				if ($level==2) {
					$sendto = $doc['approval1'];
					$curr 	= 3;
				} else if ($level==3) {
					$sendto = $doc['approval2'];
					$curr 	= 4;
				} else if ($level==4) {
					$sendto = $doc['approval2'];
					$curr 	= 5;
				} else if ($level==5) {
					$sendto = $doc['approval2'];
					$curr 	= 6;
				} else if ($level==6) {
					$sendto = $doc['approval2'];
					$curr 	= 7;
				}
			} else if ($type=='return') {
				$sendto = $doc['created_by'];
				$curr 	= 1;
			} else if ($type=='reject') {
				$sendto = $doc['created_by'];
				$curr 	= 7;
			} else {
				$sendto = 0;
			}

			if ($type=='return') {
				$newstatus = 2;
			} else if ($type=='escalation') {
				$newstatus = 3;
			} else if ($type=='reject') {
				$newstatus = 5;
			} else if ($type=='submit') {
				$newstatus = 1;
			} else {
				$newstatus = 99;
			}
			
			$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', '$comment', '$userid', '$createdat'::TIMESTAMP, '$type', '$sendto' ,'0')
								");
			
			if(isset($insHis)) {
				// UPDATE DOC
				$insHis 	= $this->db->query("
								UPDATE sbrdoc SET
									status 	= '$newstatus',
									current = '$curr'
								where id='$id'
								");
				// SEND EMAIL NOTIF
				$e_noreq 	= str_replace('/', '-', $norequest);
				$this->sendMailNotif($e_noreq,$userid,$sendto,$type);

				$log 	= $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'rows'=>$insHis, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function actionapprove(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sesspwt'); 
			$userid 	= $userdata['userid'];
			$createdat 	= date('Y-m-d H:i:s');
			$now 		= date('ymd');

			$id 			= trim(strip_tags(stripslashes($this->input->post('app_id',true))));
			$norequest		= trim(strip_tags(stripslashes($this->input->post('app_norequest',true))));
			$comment		= 'Approve SBR';
			$layanan		= $_POST['layanan'];
			$volume			= $_POST['volume'];
			$tarif			= $_POST['tarif']; 
			$price			= $_POST['price']; 
			$analisateknis 	= $_POST['analisateknis'];
			$type 			= 'approve';
			$pelanggan 		= trim(strip_tags(stripslashes($this->input->post('app_nopelanggan',true))));
			$app_userid 	= trim(strip_tags(stripslashes($this->input->post('app_userid',true))));	
					
			$url 		= "Pengajuan";
			$activity 	= $type;

			$getdoc 	= $this->db->query("SELECT * FROM sbrdoc where id='$id'")->result_array();
			$doc 		= array_shift($getdoc);
			$norequest 	= $doc['no_request'];

			$getUser 	= $this->db->query("
						SELECT a.*, 
							(SELECT level from mi.level_user where id_level=a.level_user) step,
							(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
						FROM mi.user a where userid='$userid'
						")->result_array();
			$dUser 		= array_shift($getUser);
			$level		= $dUser['step'];

			$sendto = $doc['created_by'];
			$curr 	= 7;

			// if ($type=='submit' or $type=='resubmit') {
			// 	$sendto = $doc['reviewer1'];
			// 	$curr 	= 2;
			// } else if ($type=='escalation') {
			// 	if ($level==2) {
			// 		$sendto = $doc['approval1'];
			// 		$curr 	= 3;
			// 	} else if ($level==3) {
			// 		$sendto = $doc['approval2'];
			// 		$curr 	= 4;
			// 	}
			// } else if ($type=='return') {
			// 	$sendto = $doc['created_by'];
			// 	$curr 	= 1;
			// } else if ($type=='reject') {
			// 	$sendto = $doc['created_by'];
			// 	$curr 	= 4;
			// } else {
			// 	$sendto = 0;
			// }

			// if ($type=='return') {
			// 	$newstatus = 2;
			// } else if ($type=='escalation') {
			// 	$newstatus = 3;
			// } else if ($type=='reject') {
			// 	$newstatus = 5;
			// } else if ($type=='submit') {
			// 	$newstatus = 1;
			// } else {
			// 	$newstatus = 99;
			// }
			$newstatus  = 4;
			$insHis 	= $this->db->query("
								INSERT INTO sbrhistory (no_request, comment, created_by, created_at, action, send_to, is_read) VALUES
								('$norequest', '$comment', '$userid', '$createdat'::TIMESTAMP, '$type', '$sendto' ,'0')
								");
			
			if(isset($insHis)) {
				// SEND EMAIL NOTIF
				$e_noreq 	= str_replace('/', '-', $norequest);
				$this->sendMailNotif($e_noreq,$userid,$sendto,$type);

				// UPDATE DOC
				$insHis 	= $this->db->query("
								UPDATE sbrdoc SET
									status 	= '$newstatus',
									current = '$curr'
								where no_request='$norequest'
								");
				$cekMax 		= $this->db->query("SELECT MAX(id)+1 maxid from sbranswer")->result_array();
				$gMax 			= array_shift($cekMax);
				if ($gMax['maxid']==0) {
					$lastid 	= '1';
				} else {
					$lastid 	= $gMax['maxid'];
				}

				$code 			= sprintf("%06d", $lastid);
				$maxid 			= $code;

				$noanswer 		= 'SBR/A/'.$now.'/'.$pelanggan.'/'.$maxid;

				$insAns 	= $this->db->query("
								INSERT INTO sbranswer (no_answer, no_request,userid, analisa_teknis,created_by, created_at, updated_at,updated_by) VALUES
								('$noanswer', '$norequest', '$app_userid', '$analisateknis' ,'$userid', '$createdat'::TIMESTAMP,null,null)
								"); 
				if($insAns){
					$jmLayanan = count($layanan);
					for($x=0;$x<$jmLayanan;$x++){
							$insAnsDet 	= $this->db->query("
								INSERT INTO sbranswer_detail (no_answer, layanan,volume, tarif,price) VALUES
								('$noanswer', '".$layanan[$x]."', '".$volume[$x]."', '".$tarif[$x]."' ,'".$price[$x]."')
								"); 						
					}
				}

				$log 	= $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'rows'=>$insHis, 'total'=>1));
			} else {
				echo "";
			}
		}else{
            redirect('/login');
        }
	}

	public function listcustomer(){
		if(checkingsessionpwt()){
			$key 		= $_GET['q'];
			$getData 	= $this->db->query("
						SELECT * FROM customer where nipnas like '%$key%' or upper(nama_pelanggan) like upper('%$key%') order by nama_pelanggan asc
						")->result_array();
			$count 		= $this->db->query("SELECT * FROM customer where nipnas like '%$key%' or upper(nama_pelanggan) like upper('%$key%')")->num_rows();

			header('Content-type: application/json; charset=UTF-8');
			$json['total_count'] 		= $count;
			$json['incomplete_results'] = true;
			foreach($getData as $row) {
				$json['items'][] 	= array(
					'id'			=> $row['nipnas'],
					'nipnas'		=> $row['nipnas'],
					'nama_pelanggan'=> $row['nama_pelanggan'],
					'text'			=> $row['nama_pelanggan'],
				);
			}
			echo json_encode($json);
		}else{
            redirect('/login');
        }
	}

	public function listapproval($level){
		if(checkingsessionpwt()){
			$key 		= @$_GET['q'];
			$getData 	= $this->db->query("
						SELECT * FROM (
							SELECT a.* ,
								(SELECT level from mi.level_user where id_level=a.level_user) step
							FROM mi.user a
						) as base
						where step='$level' and (upper(username) like upper('%$key%') or upper(name) like upper('%$key%'))
						order by name asc
						")->result_array();
			$count 		= $this->db->query("
						SELECT * FROM (
							SELECT a.* ,
								(SELECT level from mi.level_user where id_level=a.level_user) step
							FROM mi.user a
						) as base
						where step='$level' and (upper(username) like upper('%$key%') or upper(name) like upper('%$key%'))
						")->num_rows();

			header('Content-type: application/json; charset=UTF-8');
			$json['total_count'] 		= $count;
			$json['incomplete_results'] = true;
			foreach($getData as $row) {
				$json['items'][] 	= array(
					'id'			=> $row['userid'],
					'username'		=> $row['username'],
					'name'			=> $row['name'],
					'text'			=> $row['username'].' - '.$row['name'],
				);
			}
			echo json_encode($json);
		}else{
            redirect('/login');
        }
	}

	public function sendMailNotif($id,$sendby,$sendto,$type) {
		$norequest 	= str_replace('-','/',$id);
		$subject 	= 'SBR Online - '.$norequest.'';

		$getSendby 	= $this->db->query("SELECT * FROM mi.user where userid='$sendby'")->result_array();
		$dataSendby	= array_shift($getSendby);
		$nameby		= $dataSendby['name'];
		$emailby	= $dataSendby['email'];

		$getSendto 	= $this->db->query("SELECT * FROM mi.user where userid='$sendto'")->result_array();
		$dataSendto	= array_shift($getSendto);
		$nameto		= $dataSendto['name'];
		$emailto	= $dataSendto['email'];

		if ($type=='new') {
			$notiftext 	= 'Pengajuan baru dari';
		} else if ($type=='republish') {
			$notiftext 	= 'Perbaikan Pengajuan dari';
		} else if ($type=='escalation') {
			$notiftext 	= 'Eskalasi Pengajuan dari';
		} else if ($type=='reject') {
			$notiftext 	= 'Pengajuan tidak disetujui oleh';
		} else if ($type=='approve') {
			$notiftext 	= 'Pengajuan disetujui oleh';
		} else if ($type=='return') {
			$notiftext 	= 'Pengajuan dikembalikan oleh';
		}

		$config = Array(
		'protocol' => 'smtp',
		'smtp_host' => 'smtp.hostinger.co.id',
		'smtp_port' => 587,
		'smtp_user' => 'mi@parwatha.com', // change it to yours
		'smtp_pass' => 'b1sm1llah', // change it to yours
		'mailtype'  => 'html',
		'charset'   => 'iso-8859-1'
		);

		//Email content
		$htmlContent = '';
		$htmlContent .= '
			<!DOCTYPE html>
			<html>
			<head>
			  <meta charset="utf-8" />
			  <title>No Reply</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
			  <style>
			  </style>
			</head>
			<body style="font-family: verdana; font-size: 14px;">
				<div class="bg" style="background: #FFF; width: 70%; margin: 0 auto;">
					<div id="logo" style="background: #FFF;"><img src="'.base_url().'images/logotel.png" style="max-height: 70px; margin-top: 20px;"></div>
					<div id="confirmation-message">
						<div class="ravis-title-t-2" style="text-align: left; margin-top: 20px;">
							<div class="title" style="color: #1e1e1e; font-size: 24px;">
								<span style="text-transform:capitalize;">Dear, '.$nameto.'</span>
							</div>
						</div>
						<div class="desc" style="color: #1e1e1e; margin-top:20px; font-siz: 14px;">
							<div style="border-bottom: 1px dashed #efefef; padding-bottom: 10px;">
								'.$notiftext.' <b>'.$nameby.'</b>  dengan No Request : <b>'.$norequest.'</b>.<br>
								Klik <a href="'.base_url().'docdetail/'.$id.'" target="_blank" style="color: #5d78ff;">disini</a> untuk melihat detail pengajuan.
								<br><br>

								Hubungi Admin jika Anda butuh bantuan lebih lanjut.<br><br>
							</div>
						</div>
					</div>
				</div>
			</body>
			</html>
		';

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('mi@parwatha.com'); // change it to yours
		$this->email->to($emailto);// change it to yours
		$this->email->subject($subject);
		$this->email->set_mailtype("html");
		$this->email->message($htmlContent);
		
		if($this->email->send()) {
			echo '';
		} else {
			show_error($this->email->print_debugger());
		}
		// echo $htmlContent;
	}

	public function download($id) { 
		if(checkingsessionpwt()){
			$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];$userdata		= $this->session->userdata('sesspwt'); 
			$userid 		= $userdata['userid'];
			$norequest 		= str_replace('-','/',$id);

			$getUser 		= $this->db->query("
								SELECT a.*, 
									(SELECT level from mi.level_user where id_level=a.level_user) step,
									(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
								FROM mi.user a where userid='$userid'
							")->result_array();
			$dUser 			= array_shift($getUser);
			$level			= $dUser['step'];
			$aksesCreate 	= $dUser['actionbtn'];

			$getDoc 		= $this->db->query("
							SELECT a.* ,
								(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
								(SELECT picture from mi.user where userid=a.created_by) pictcreated,
								(SELECT name from mi.user where userid=a.created_by) namecreated,
								(SELECT username from mi.user where userid=a.created_by) uncreated,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) poscreated,
								(SELECT picture from mi.user where userid=a.reviewer1) pictreview,
								(SELECT name from mi.user where userid=a.reviewer1) namereview,
								(SELECT username from mi.user where userid=a.reviewer1) unreview,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.reviewer1) posreview,
								(SELECT picture from mi.user where userid=a.approval1) pictappr1,
								(SELECT name from mi.user where userid=a.approval1) nameappr1,
								(SELECT username from mi.user where userid=a.approval1) unappr1,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval1) posappr1,
								(SELECT picture from mi.user where userid=a.approval2) pictappr2,
								(SELECT name from mi.user where userid=a.approval2) nameappr2,
								(SELECT username from mi.user where userid=a.approval2) unappr2,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval2) posappr2,
								(SELECT picture from mi.user where userid=a.approval3) pictappr3,
								(SELECT name from mi.user where userid=a.approval3) nameappr3,
								(SELECT username from mi.user where userid=a.approval3) unappr3,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval3) posappr3,
								(SELECT picture from mi.user where userid=a.timtarif1) picttrf1,
								(SELECT name from mi.user where userid=a.timtarif1) nametrf1,
								(SELECT username from mi.user where userid=a.timtarif1) untrf1,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.timtarif1) postrf1,
								(SELECT picture from mi.user where userid=a.timtarif2) picttrf2,
								(SELECT name from mi.user where userid=a.timtarif2) nametrf2,
								(SELECT username from mi.user where userid=a.timtarif2) untrf2,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.timtarif2) postrf2
							FROM sbrdoc a where no_request='$norequest'
							")->result_array();
			$doc 			= array_shift($getDoc);
			$getAns = $this->db->query("
					SELECT a.* ,					
						(SELECT picture from mi.user where userid=a.userid) pictcreated,
						(SELECT name from mi.user where userid=a.userid) namecreated,
						(SELECT username from mi.user where userid=a.userid) uncreated,
						(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.userid) poscreated,
						(SELECT picture from mi.user where userid=a.created_by) pictappr,
						(SELECT name from mi.user where userid=a.created_by) nameappr,
						(SELECT username from mi.user where userid=a.created_by) unappr,
						(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) posappr
					FROM sbranswer a where no_request='$norequest'
					")->result_array();
			$ans 		= array_shift($getAns);
			$no_answer	= $ans['no_answer'];
			$html 		='';
			$table 		= '';

			$gcekColspan= $this->db->query("SELECT distinct(created_by) cek from sbrhistory where no_request='".$norequest."' and action in ('escalation','approve')")->num_rows();
			$cekColspan = $gcekColspan+1;

			$html 		.='
			<body style="font-size: 13px!important;">
			<span><strong><u><center>SPESIAL BUSINESS REQUEST (SBR)</center></u></strong></span><br>
			<center>NOMOR : TEL :  <b>'.$norequest.'</b></center><br><br>

			<table class="table table-bordered" border="1px" cellspacing="0px" cellpadding="3px" width="100%"  style="font-size:14px;">
				<tr>
					<td style="width: 30%">NAMA PELANGGAN</td>
					<td><b>'.$doc['nama_pelanggan'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">NIPNAS</td>
					<td><b>'.$doc['nipnas'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">SERVICE ID</td>
					<td><b>'.$doc['service_id'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">NAMA PROJECT</td>
					<td><b>'.$doc['nama_project'].'</b></td>
				</tr>
				<tr>
					<td style="width: 30%">SUBJECT</td>
					<td><b>'.$doc['subject'].'</b></td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>LATAR BELAKANG</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>ASPEK STRATEGIS</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>ASPEK FINANSIAL</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>ASPEK KOMPETISI</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
				<td>
				<b>KONFIGURASI TEKNIS</b><br>
				'.$doc['latar_belakang'].'
				</td>
				</tr>
			</table><br><br>

			<center><h4>PERSETUJUAN</h4></center>
			<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
				<tr>
					<td colspan="'.$cekColspan.'"><center>'.$this->formula->TanggalIndo($doc['created_at']).'</center></td>
				</tr>
				<tr>';

					$cekRev		= $this->db->query("
								SELECT * from sbrhistory 
								where no_request='".$norequest."' and created_by='".$doc['reviewer1']."' and action in ('escalation','approve')
								")->num_rows();

					$cekAppr1	= $this->db->query("
								SELECT * from sbrhistory 
								where no_request='".$norequest."' and created_by='".$doc['approval1']."' and action in ('escalation','approve')
								")->num_rows();
					$cekAppr2	= $this->db->query("
								SELECT * from sbrhistory 
								where no_request='".$norequest."' and created_by='".$doc['approval2']."' and action in ('escalation','approve')
								")->num_rows();
					$cekAppr3	= $this->db->query("
								SELECT * from sbrhistory 
								where no_request='".$norequest."' and created_by='".$doc['approval3']."' and action in ('escalation','approve')
								")->num_rows();

					$html .= '<td width="25%"><center>Diusulkan Oleh,<br>'.$doc['poscreated'].'</center></td>';
					if ($cekRev>0) { $html .= '<td width="25%"><center>Disetujui Oleh,<br>'.$doc['posreview'].'</center></td>'; }
					if ($cekAppr1>0) { $html .= '<td width="25%"><center>Disetujui Oleh,<br>'.$doc['posappr1'].'</center></td>'; }
					if ($cekAppr2>0) { $html .= '<td width="25%"><center>*)Disetujui Oleh,<br>'.$doc['posappr2'].'</center></td>'; }
					if ($cekAppr3>0) { $html .= '<td width="25%"><center>*)Disetujui Oleh,<br>'.$doc['posappr3'].'</center></td>'; }

				$html .= '</tr>
				<tr>
					<td height="100px"></td>';

					if ($cekRev>0) { $html .= '<td height="100px"></td>'; }
					if ($cekAppr1>0) { $html .= '<td height="100px"></td>'; }
					if ($cekAppr2>0) { $html .= '<td height="100px"></td>'; }
					if ($cekAppr3>0) { $html .= '<td height="100px"></td>'; }

				$html .= '</tr>
				<tr>';

					$html .= '<td><b><u>'.$doc['namecreated'].'</u><br>
						'.$doc['uncreated'].'</b><br></td>';

					if ($cekRev>0) { $html .= '<td><b><u>'.$doc['namereview'].'</u><br>
						'.$doc['unreview'].'</b><br></td>'; }

					if ($cekAppr1>0) { $html .= '<td><b><u>'.$doc['nameappr1'].'</u><br>
						'.$doc['unappr1'].'</b><br></td>'; }

					if ($cekAppr2>0) { $html .= '<td><b><u>'.$doc['nameappr2'].'</u><br>
						'.$doc['unappr2'].'</b><br></td>'; }

					if ($cekAppr3>0) { $html .= '<td><b><u>'.$doc['nameappr3'].'</u><br>
						'.$doc['unappr3'].'</b><br></td>'; }

				$html .= '</tr>
			</table>
			</body>';
			// echo $html;
			$this->load->library('PdfGenerator');
			$this->pdfgenerator->generate($html,'DOK-PENGAJUAN','A4','potrait'); 
		} else {
			echo "";
		}
	}
}
