<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $akses;
	private $userid;
	// private $divisiUAM;
	// private $segmenUAM;
	// private $tregUAM;
	// private $witelUAM;
	// private $amUAM;
	
	public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->load->model('auth'); 
		$this->load->model('query');
		$this->load->model('formula');
		$this->load->model('datatable');
		
		// if(checkingsessionpwt()){
			// $session = checkingsessionpwt();
		$session	 	= $this->session->userdata('sesspwt'); 
		$userid 	 	= $session['userid'];
		$profile 	 	= $session['id_role'];
		$menu 		 	= uri_string();
		$data_akses  	= $this->query->getAkses($profile,$menu);
		$shift 		 	= array_shift($data_akses);
		$this->akses 	= $shift['akses'];
		$this->userid 	= $userid;

		$now 			= date('Y-m-d');
		$yesterday 		= date("Y-m-d",strtotime("-1 day"));
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function ceklogin() {
		// ambil cookie
		$ip   		= $_SERVER['REMOTE_ADDR'];
		$browser	= $this->getbrowser();

		$username	= trim(strip_tags(stripslashes($this->input->post('username',true))));
		$password	= trim(strip_tags(stripslashes($this->input->post('password',true))));
		$epassword	= md5(trim(strip_tags(stripslashes($this->input->post('password',true)))));
		$remember	= trim(strip_tags(stripslashes($this->input->post('remember',true))));

		$arr = array(
			'username' => $username
		);
		$row 		= $this->auth->getuser_Secure($arr)->row_array();
		$id 		= $row['userid'];

		if(isset($row['userid'])) {
			$this->db2 	= $this->load->database('dbtfa', TRUE);

			// $cekUserTFA	= $this->db2->query("SELECT * FROM users where username='$username'")->result_array();
			$sqlTFA =" SELECT * from users where username= ?";
            $cekUserTFA = $this->db2->query($sqlTFA, $arr)->result_array();
			$dataUTFA	= array_shift($cekUserTFA);
			$teleid		= $dataUTFA['telegram_id'];

			if (!empty($teleid) or $teleid!='') {
				if($row['ldap']!=1){
					if ($row['id_role']==0) {
						echo "";
					} else {
						if ($remember=='on') {
							if($row['password']==$epassword){
								$checkDevice	= $this->checkDevice($username,7,$ip,$browser);
								if ($checkDevice=="200" or $checkDevice==200) {
									$rows = array('data'=>$row);
									$this->session->set_userdata('sesspwt', $row);
									$coba = $this->session->userdata('sesspwt');
									print $row['name'];
								} else {
									$this->addDeviceTFA($username,7,$ip,$browser);
									$this->requestOTP($username,7);

									echo 'needotp';
								}
							}else{
								echo "";
							}
						} else {
							if($row['password']==$epassword){
								$checkDevice	= $this->checkDevice($username,7,$ip,$browser);
								if ($checkDevice=="200" or $checkDevice==200) {
									$rows = array('data'=>$row);
									$this->session->set_userdata('sesspwt', $row);
									$coba = $this->session->userdata('sesspwt');
									print $row['name'];
								} else {
									$this->addDeviceTFA($username,7,$ip,$browser);
									$this->requestOTP($username,7);

									echo 'needotp';
								}
							}else{
								echo "";
							}
						}
					}
				} else {
					if ($row['id_role']==0) {
						echo "";
					} else {
						$CGCauth = $this->auth_ldap($username, $password);
						if($CGCauth!='null'){
							$checkDevice	= $this->checkDevice($username,7,$ip,$browser);
							if ($checkDevice=="200" or $checkDevice==200) {
								$rows = array('data'=>$row);
								$this->session->set_userdata('sesspwt', $row);
								$coba = $this->session->userdata('sesspwt');

								print $row['name'];
							} else {
								$this->addDeviceTFA($username,7,$ip,$browser);
								$this->requestOTP($username,7);

								echo 'needotp';
							}
						}else{
							echo "";
						}
					}
				}
			} else {
				echo "";
			}
		}else{
			echo "";
		}
	}

	public function loginotp() {
		// ambil cookie
		$browser	= $this->getbrowser();

		$username	= trim(strip_tags(stripslashes($this->input->post('username',true))));
		$password	= trim(strip_tags(stripslashes($this->input->post('password',true))));
		$epassword	= md5(trim(strip_tags(stripslashes($this->input->post('password',true)))));
		$otp		= trim(strip_tags(stripslashes($this->input->post('otp',true))));
		$remember	= trim(strip_tags(stripslashes($this->input->post('remember',true))));
		$arr = array(
			'username' => $username
		);
		
		$row 		= $this->auth->getuser_Secure($username)->row_array();
		$id 		= $row['userid'];

		if(isset($row['userid'])) {
			if($row['ldap']!=1){
				if ($row['id_role']==0) {
					echo "";
				} else {
					if ($remember=='on') {
						if($row['password']==$epassword){
							$validateOTP	= $this->validateOTP($username,$otp);
							if ($validateOTP=="200" or $validateOTP==200) {
								$rows = array('data'=>$row);
								$this->session->set_userdata('sesspwt', $row);
								$coba = $this->session->userdata('sesspwt');
								print $row['name'];
							} else {
								echo "";
							}
						}else{
							echo "";
						}
					} else {
						if($row['password']==$epassword){
							$validateOTP	= $this->validateOTP($username,$otp);
							if ($validateOTP=="200" or $validateOTP==200) {
								$rows = array('data'=>$row);
								$this->session->set_userdata('sesspwt', $row);
								$coba = $this->session->userdata('sesspwt');
								print $row['name'];
							} else {
								echo "";
							}
						}else{
							echo "";
						}
					}
				}
			} else {
				if ($row['id_role']==0) {
					echo "";
				} else {
					$CGCauth = $this->auth_ldap($username, $password);
					if($CGCauth!='null'){
						$validateOTP	= $this->validateOTP($username,$otp);
						if ($validateOTP=="200" or $validateOTP==200) {
							$rows = array('data'=>$row);
							$this->session->set_userdata('sesspwt', $row);
							$coba = $this->session->userdata('sesspwt');

							print $row['name'];
						} else {
							echo "";
						}
					}else{
						echo "";
					}
				}
			}
		}else{
			echo "";
		}
	}

	public function auth_ldap($username, $password){
		// function auth_ldap2(){
		// $username 	= '400624';
		// $password 	= '30SolCrea';
		$url 		= 'https://auth.telkom.co.id/services/auth?username='.$username.'&password='.$password.'';
		$JSON 		= file_get_contents($url);
		
		$gdata 		= json_decode(json_encode($JSON), True);
		$data		= json_decode($gdata);
		$sukses		= $data->login;
		
		if ($sukses=='1') {
			$url2 = 'https://auth.telkom.co.id/api/call/'.$username.'';
			$JSON2 = file_get_contents($url2);
			
			$gdata2 	= json_decode(json_encode($JSON2), True);
			$data2		= json_decode($gdata2);
			$user		= $data2->username;
			$name		= $data2->name;
			$email		= $data2->email;
			
			// echo $JSON2;
			$ret = array(
				"name" 		=> $name,
				"username" 	=> $username,
				"email" 	=> $email
			);
		} else {
			$ret = null;
		}
		return json_encode($ret);
    }

    public function getTokenTFA(){
		$ch2 			= curl_init();
		$data2 			= array(
							'app_id'		=> 7,
							'secret_key'	=> "dOLGj08ukd"
						);

		if ($ch2) {
			curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/auth");
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_ENCODING, "");
			curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			// curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
			curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
			curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json"));
			
			$result2 	= @curl_exec($ch2);
			$resObj2[] 	= json_decode($result2, true);
			@curl_close($ch2);
			foreach($resObj2 as $val) {
				return $val['data']['token'];
			}
			// print json_encode($resObj2,true);
		} 
    }

    public function checkDevice($username,$app,$ip,$browser){
    	$token 			= $this->getTokenTFA();
		$ch2 			= curl_init();

		if ($ch2) {
			curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/devices/check?username=$username&app=$app&ip=$ip&browser=$browser");
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_ENCODING, "");
			curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
			//curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
			// curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
			curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json", "Authorization: Bearer $token"));
			
			$result2 	= @curl_exec($ch2);
			$resObj2[] 	= json_decode($result2, true);
			@curl_close($ch2);
			// print json_encode($resObj2,true);
			foreach($resObj2 as $val) {
				return $val['status']['code'];
			}
		} 
    }

    public function addDeviceTFA($username,$app,$ip,$browser){
    	$token 			= $this->getTokenTFA();
		$ch2 			= curl_init();
		$data2 			= array(
							'username'		=> $username,
							'app'			=> 7,
							'ip'			=> $ip,
							'browser'		=> $browser
						);

		if ($ch2) {
			curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/devices");
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_ENCODING, "");
			curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			// curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
			curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
			curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json", "Authorization: Bearer $token"));
			
			$result2 	= @curl_exec($ch2);
			$resObj2[] 	= json_decode($result2, true);
			@curl_close($ch2);
			foreach($resObj2 as $val) {
				return $val['status']['code'];
			}
			// print json_encode($resObj2,true);
		} 
    }

    public function resendOTP(){
    	$username		= trim(strip_tags(stripslashes($this->input->post('username',true))));
		$app			= '7';
    	$token 			= $this->getTokenTFA();
		$ch2 			= curl_init();

		if ($ch2) {
			curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/otp?username=$username&app=$app");
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_ENCODING, "");
			curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
			//curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
			// curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
			curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json", "Authorization: Bearer $token"));
			
			$result2 	= @curl_exec($ch2);
			$resObj2[] 	= json_decode($result2, true);
			@curl_close($ch2);
			// print json_encode($resObj2,true);
			foreach($resObj2 as $val) {
				print $val['status']['code'];
			}
		} 
    }

    public function requestOTP($username,$app){
    	$token 			= $this->getTokenTFA();
		$ch2 			= curl_init();

		if ($ch2) {
			curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/otp?username=$username&app=$app");
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_ENCODING, "");
			curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
			//curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
			// curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
			curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json", "Authorization: Bearer $token"));
			
			$result2 	= @curl_exec($ch2);
			$resObj2[] 	= json_decode($result2, true);
			@curl_close($ch2);
			// print json_encode($resObj2,true);
			foreach($resObj2 as $val) {
				return $val['status']['code'];
			}
		} 
    }

    public function validateOTP($username,$otp){
    	$token 			= $this->getTokenTFA();
		$ch2 			= curl_init();
		$data2 			= array(
							'username'		=> $username,
							'app'			=> 7,
							'otp_code'		=> $otp,
						);

		if ($ch2) {
			curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/otp/validate");
			curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch2, CURLOPT_ENCODING, "");
			curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
			curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
			curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			// curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
			curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
			curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json", "Authorization: Bearer $token"));
			
			$result2 	= @curl_exec($ch2);
			$resObj2[] 	= json_decode($result2, true);
			@curl_close($ch2);
			foreach($resObj2 as $val) {
				return $val['status']['code'];
			}
			// print json_encode($resObj2,true);
		} 
    }

    public function logout(){
		$this->session->sess_destroy();
        redirect('./panel');
	}

	public function getbrowser(){
		$this->load->helper('url');
		$this->load->library('user_agent');

		$browser = $this->agent->browser();
		$browserVersion = $this->agent->version();
		$platform = $this->agent->platform();
		$full_user_agent_string = $_SERVER['HTTP_USER_AGENT'];

		return $browser.' '.$browserVersion;
	}
}
