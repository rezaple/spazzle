<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listapplication extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $profile;
	private $divisiUAM;
	private $segmenUAM;
	private $tregUAM;
	private $witelUAM;
	private $amUAM;
	
	public function __construct(){
		date_default_timezone_set("Asia/Bangkok");
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->model('query'); 
		$this->load->model('formula'); 
		$this->load->model('datatable'); 
		$this->load->model('faq_handler');
		
		ini_set('max_execution_time', 123456);
		ini_set("memory_limit","1256M");
			
		// $session = checkingsessionpwt();
		$session	 = $this->session->userdata('sessSpazzle'); 
    }
	
	public function index(){
		if(checkingsessionpwt()){
			$this->load->view('panel/dashboard');
		} else {
			// redirect('/panel');
		}
	}

	public function getdata(){
		if(checkingsessionpwt()){

			$columnsDefault = [
				'no'					=> true,
				'nama_aplikasi'			=> true,
				'deskripsi'				=> true,
				'unit'					=> true,
				'tahun_terbit'			=> true,
				'kategori'				=> true,
				'inovator_developer'	=> true,
				'pic'					=> true,
				'tipe_inovasi'			=> true,
				'url'					=> true,
				'scope_implementasi'	=> true,
				'status'				=> true,
				'user'					=> true,
				'action'				=> true,
			];
			$arraynya	= $columnsDefault; 
			$jsonfile	= $this->dataapplication(); 
			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}
	public function getuser(){
		if(checkingsessionpwt()){

			$columnsDefault = [
				'profile_pic'	=> true,
				'nik'			=> true,
				'nama'			=> true, 
				'status'		=> true,
				'actions'		=> true,
			];
			$arraynya	= $columnsDefault; 
			$jsonfile	= $this->datauserassign(); 
			$this->datatable->generateDatatable($arraynya,$jsonfile);
		} else {
			redirect('/panel');
		}
	}
	function datauserassign(){
		$data_aksess = $this->query->getAkses($this->profile,'panel/roles');
		$shift = array_shift($data_aksess);
		@$akses = $shift['akses'];
		$qRole 	= " select 
					a.*,
					(select count(*)as jm from mi.data_application_assign where userid=a.userid)as status_assign,
					(select id_application from mi.data_application_assign where userid=a.userid)as id_application
					from mi.user a where a.divisi='ITSG'"; 
		$datarole			= $this->query->getDatabyQ($qRole); 
		$no=0;
		header('Content-type: application/json; charset=UTF-8'); 
		$cek 	= $this->query->getNumRowsbyQ($qRole)->num_rows(); 
		if ($cek>0) { 
			foreach($datarole as $data) {
				$no++;
				$id = $data['userid']; 
				$status_assign = $data['status_assign'];
				$pic = '
				<div class="kt-user-card-v2">
					<div class="kt-user-card-v2__pic" style="margin: 0 auto;">
						<center><img src="'.base_url().'images/user/'.$data['picture'].'" class="m-img-rounded kt-marginless" alt="photo"></center>
					</div>
				</div> '; 
				if($status_assign==NULL or $status_assign=='' or $status_assign=='0'){
					$st = 'Belum Assign';
				}else{
					$st = 'Sudah Assign';
				}
				$row = array(
					'profile_pic' => $pic,
					'nik' => $data['username'],
					'nama'	=> $data['name'],
					'status' => $st,
					'actions' => $id
				);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	} 
	function dataapplication(){
		$data_aksess = $this->query->getAkses($this->profile,'panel/roles');
		$shift = array_shift($data_aksess);
		@$akses = $shift['akses'];
		$qRole 	= "
					select * from data_application
				"; 
		$datarole			= $this->query->getDatabyQ($qRole); 
		$no=0;
		header('Content-type: application/json; charset=UTF-8'); 
		$cek 	= $this->query->getNumRowsbyQ($qRole)->num_rows(); 
		if ($cek>0) { 
			foreach($datarole as $data) {
				$no++;
				$id = $data['id']; 
				$row = array(
					'no'				=> $no,
					'nama_aplikasi'		=> $data['nama_aplikasi'],
					'deskripsi'			=> $data['deskripsi'],
					'benefit_value' 	=> $data['benefit_value'],
					'unit'				=> $data['unit'],
					'tahun_terbit'		=> $data['tahun_terbit'],
					'kategori'			=> $data['kategori'],
					'inovator_developer'=> $data['inovator_developer'],
					'pic'			=> $data['pic'],
					'tipe_inovasi'	=> $data['tipe_inovasi'],
					'url'			=> $data['url'],
					'scope_implementasi'=> $data['scope_implementasi'],
					'status'			=> $data['status'],
					'user'			=> $data['user'],
					'action' => $id
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}
	public function modal(){
		if(checkingsessionpwt()){
			
			$id					= trim(strip_tags(stripslashes($this->input->post('id',true))));
			
			$qRole 	= " select 
					a.*,
					(select count(*)as jm from mi.data_application_assign where userid=a.userid)as status_assign,
					(select id_application from mi.data_application_assign where userid=a.userid)as id_application
					from mi.user a where a.divisi='ITSG' and a.userid='".$id."'"; 
		$datarole	= $this->query->getDatabyQ($qRole); 
			
			header('Content-type: application/json; charset=UTF-8');
			
			if (isset($id) && !empty($id)) {
				foreach($datarole as $row) {
					echo json_encode($row);
					exit;
				}
			}
		} else {
			redirect('/panel');
		}
	}	
	public function update(){
		if(checkingsessionpwt()){
			$userdata	= $this->session->userdata('sessSpazzle'); 
			$userid 	= $userdata['userid'];
			$now 		= date('Y-m-d h:i:s'); 

			$id	= trim(strip_tags(stripslashes($this->input->post('userid',true))));
			$application = $_POST['list_application'];
			$j_app = count($application);

			$this->db->where('userid', $id);
			$this->db->delete('mi.data_application_assign');
			for($i=0;$i<$j_app;$i++){	
				$maxid = $this->query->getMaxID('mi.data_application_assign','id');
				$arr_max = $maxid->result_array();
				$data = array(
					"question_faq"=>$question,
					"answer_faq"=>$answer,
					"update_by"=>$userid,
					"update_date"=> $now
				);
				$this->db->where('id_faq', $id);
				$updaterole = $this->db->update('data_faq', $data);				
			}
			if($updaterole){
				$url 			= "Manage FAQ";
				$activity 		= "UPDATE";
				$log = $this->query->insertlog($activity,$url,$id);
				print json_encode(array('success'=>true,'total'=>1));				
			}
		} else {
			redirect('/panel');
		}
	}	
 
}
