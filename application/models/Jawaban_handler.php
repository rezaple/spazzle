<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jawaban_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$userdata		= $this->session->userdata('sesspwt'); 
		$userid 		= $userdata['userid'];

		$getUser 		= $this->db->query("
							SELECT a.*, 
								(SELECT level from mi.level_user where id_level=a.level_user) step,
								(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
							FROM mi.user a where userid='$userid'
						")->result_array();
		$dUser 			= array_shift($getUser);
		$level			= $dUser['step'];
		$aksesCreate 	= $dUser['actionbtn'];

		if ($level==2) {
			$cond 		= "and b.reviewer1='$userid' and b.current>=$level";
		} else if ($level==3) {
			$cond 		= "and b.approval1='$userid' and b.current>=$level";
		} else if ($level==4) {
			$cond 		= "and b.approval2='$userid' and b.current>=$level";
		} else if ($level==0) {
			$cond 		= "";
		} else {
			$cond 		= "and a.userid='$userid' and b.current>=$level";
		}

		$qUser				= "
							SELECT a.*,
							b.id as idreq,
							(SELECT nama_pelanggan from customer where nipnas=b.nipnas) nama_pelanggan
							FROM sbranswer a
							left join sbrdoc b on a.no_request = b.no_request
							where 1=1 and a.userid='$userid'
							";
		$dataUsr			= $this->db->query($qUser)->result_array();
		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->db->query($qUser)->num_rows();

		if ($cek>0) {
			foreach($dataUsr as $data) { 
				$no++;

				$id 		= $data['id'];
				$noanswer 	= str_replace('/','-',$data['no_answer']);
				$noreq 		= str_replace('/','-',$data['no_request']);

				// if ($data['status']==1) {
				// 	$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting Approval</button>';
				// } else if ($data['status']==2) {
				// 	$status = '<button class="btn btn-sm btn-label-warning btn-pill">Return</button>';
				// } else if ($data['status']==3) {
				// 	$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting Approval</button>';
				// } else if ($data['status']==4) {
				// 	$status = '<button class="btn btn-sm btn-label-success btn-pill">Approved</button>';
				// } else if ($data['status']==5) {
				// 	$status = '<button class="btn btn-sm btn-label-danger btn-pill">Rejected</button>';
				// } else {
				// 	$status = '<button class="btn btn-sm btn-secondary btn-pill">Draft</button>';
				// }

				// if ($data['status']==0 or $data['status']==2) {
				// 	$button = '
				// 			<div class="btn-group dropleft">
				// 				<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				//                              <i class="fa fa-bars"></i>
				//                          </button>
				//                             <div class="dropdown-menu">
				//                             	'.$this->getBtnAction($aksesCreate,'update','Update','update','btn-sm btnupdateM','la la-pencil-square',$id).'
				//                                 '.$this->getBtnAction($aksesCreate,'delete','Delete','delete','btn-sm btndeleteMenu','la la-trash',$id).'
				//                             </div>
				//                         </div>
				// 			';
				// } else {
				// 	if ($level!='1') {
				// 		$button = '
				// 				<div class="btn-group dropleft">
				// 					<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				//                               <i class="fa fa-bars"></i>
				//                           </button>
				//                              <div class="dropdown-menu">
				//                              	'.$this->getBtnAction($aksesCreate,'modalapprove','Approve','approve','btn-sm text-success btnApprove','la la-check-circle',$noreq).'
				//                              	'.$this->getBtnAction($aksesCreate,'modalreject','Reject','reject','btn-sm text-danger btnReject','la la-times-circle',$noreq).'
				//                              	'.$this->getBtnAction($aksesCreate,'modaleskalasi','Eskalasi','eskalasi','btn-sm text-primary btnEskalasi','la la-step-forward',$noreq).'
				//                              	'.$this->getBtnAction($aksesCreate,'modalreturn','Return','return','btn-sm text-warning btnReturn','la la-step-backward',$noreq).'
				//                              </div>
				//                          </div>
				// 				';
				// 	} else {
				// 		$button = '
				// 				<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
				// 				';
				// 	}
				// }
				$button = '
								<button class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon btnUploadDok" title="Upload Dokumen Jawaban TTD" data-id="'.$data['idreq'].'" data-toggle="modal" data-target="#modaluploaddok"><i class="la la-cloud-upload"></i></button>
								';
				$row = array(
					"no_answer"			=> '<a href="'.base_url().'docdetail/'.$noreq.'">'.$data['no_answer'].'</a>',
					"no_request"		=> $data['no_request'],
					"analisa_teknis"	=> substr(strip_tags($data['analisa_teknis']),0,20).'....',
					"created_at"		=> $data['created_at'], 
					"actions"			=> $button
				);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}

	function getBtnAction($akses,$modal_name,$name_button,$actbtn,$classbtn,$iconbtn,$id){
		$a_array = explode(",",trim($akses));
		if(in_array($actbtn,$a_array))
		{
			$button = '
					<button class="btn '.$classbtn.' btn-icon-sm" data-toggle="modal" data-target="#'.$modal_name.'" data-id="'.$id.'">
						<i class="'.$iconbtn.'"></i>
						'.$name_button.'
					</button>
					';
		} else {
			$button =  '';	
		}
		
		//return $clearbuton.$button;
		return $button;
	}
}
