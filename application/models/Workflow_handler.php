<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workflow_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$data_aksess = $this->query->getAkses($this->profile,'panel/workflow');
		$shift = array_shift($data_aksess);
		$akses = $shift['akses'];



		$qRole 	= "
					select * from mi.level_user where level not in ('0','1') order by level asc
				";
				// echo $qRole;
		$datarole			= $this->query->getDatabyQ($qRole);
		
		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->query->getNumRowsbyQ($qRole)->num_rows();

		if ($cek>0) {

			foreach($datarole as $data) {
				$no++;
				$id = $data['id_level'];

				$buttonupdate = getRoleUpdate($akses,'update',$id);
				$buttondelete = getRoleDelete($akses,'delete',$id);					

				

				$row = array(
					"name"		=> $data['name'],
					"level"		=> $data['level'],
					"final"		=> $data['is_final'],
					"actions"	=> $id
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}
}
