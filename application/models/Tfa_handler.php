<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tfa_handler extends CI_Model{
	function _construct(){
    parent::_construct();
    }
	
	function getTokenTFA(){
        $ch2            = curl_init();
        $data2          = array(
                            'app_id'        => 7,
                            'secret_key'    => "dOLGj08ukd"
                        );

        if ($ch2) {
            curl_setopt($ch2, CURLOPT_URL, "http://10.60.161.82:9001/tfa/auth");
            curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch2, CURLOPT_ENCODING, "");
            curl_setopt($ch2, CURLOPT_MAXREDIRS, 7);
            curl_setopt($ch2, CURLOPT_TIMEOUT, 0);
            curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch2, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            // curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch2, CURLOPT_POST, 1); //POST methode
            curl_setopt($ch2, CURLOPT_POSTFIELDS,json_encode($data2));
            curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Accept: */*', "content-type: application/json"));
            
            $result2    = @curl_exec($ch2);
            $resObj2[]  = json_decode($result2, true);
            @curl_close($ch2);
            foreach($resObj2 as $val) {
                return $val['data']['token'];
            } 
        } 
    }

    function createuserTFA($username,$name,$email,$hp){ 
        $token          = $this->getTokenTFA();
        $data2          = array( 
                            "username" => $username,
                            "name" => $name,
                            "email" => $email,
                            "hp" => $hp
                        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://10.60.176.89:9001/tfa/users",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>json_encode($data2),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$token.""
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    } 

    function updateuserTFA($username,$name,$email,$hp){ 
        $token          = $this->getTokenTFA();
        $data2          = array( 
                            "username" => $username,
                            "name" => $name,
                            "email" => $email,
                            "hp" => $hp,
                            "app" => 7
                        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://10.60.176.89:9001/tfa/users/$username",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS =>json_encode($data2),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$token.""
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    } 

    function deleteuserTFA($username){ 
        $token          = $this->getTokenTFA();
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://10.60.176.89:9001/tfa/users/$username",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "DELETE",
          // CURLOPT_POSTFIELDS =>json_encode($data2),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$token.""
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    } 
}