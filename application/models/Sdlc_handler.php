<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sdlc_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$data_aksess = $this->query->getAkses($this->profile,'panel/roles');
		$shift = array_shift($data_aksess);
		@$akses = $shift['akses'];
		$qRole 	= "
					select
						a.* 
					from
					mi.param_sdlc a
					ORDER BY a.id_sdlc ASC
				";
				// echo $qRole;
		$datarole			= $this->query->getDatabyQ($qRole);
		
		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->query->getNumRowsbyQ($qRole)->num_rows();

		if ($cek>0) {

			foreach($datarole as $data) {
				$no++;
				$id = $data['id_sdlc'];
				
				$buttonupdate = getRoleUpdate($akses,'update',$id);
				$buttondelete = getRoleDelete($akses,'delete',$id);

				if($data['mandatory']==1){
					$icon = '<button type="button" class="btn btn-success  btn-sm btn-icon btn-circle"><i class="la la-check"></i></button>
					';
				}else{
					$icon = '<button type="button" class="btn btn-danger  btn-sm btn-icon btn-circle"><i class="la la-close"></i></button>';
				}
				$link = '<a href="'.base_url().'attachment/helpdoc/'.$data['template_sdlc'].'" target="_blank">'.$data['template_sdlc'].'</a>';
				$row = array(
					"id_sdlc" => $data['id_sdlc'],
					"type_tab" => '<strong>'.$data['type_tab'].'</strong>',
					"nama_sdlc" => $data['nama_sdlc'],
					"mandatory" => '<center>'.$icon.'</center>',
					"template_sdlc" => $link,
					"type_sdlc" => $data['type_sdlc'],
					"type_upload" => $data['type_upload'],
					"actions" => $id
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}
}
