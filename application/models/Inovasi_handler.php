<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inovasi_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$userdata		= $this->session->userdata('sessSpazzle'); 
		$userid 		= $userdata['userid'];
		$getUser 		= $this->db->query("
							SELECT a.*, 
								(SELECT level from mi.level_user where id_level=a.level_user) step,
								(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
							FROM mi.user a where userid='$userid'
						")->result_array();
		$dUser 			= array_shift($getUser);
		$level			= $dUser['step'];
		$aksesCreate 	= $dUser['actionbtn'];
		$cond 			= "
						and (noinnovasi in (
							SELECT noinnovasi from mi.data_innovasi_history where created_by='$userid' or send_to='$userid'
						) or created_by='$userid')
						";
		if($level == 0){
			$cond = '';
		}else if($level == 1){
			$cond = "";
			//$cond = "and created_by='$userid'";
		}else{
			$cond = "";
		}
		$qUser				= "
							SELECT a.*,
							(select name from mi.user where userid=a.created_by)as usercreate
							FROM data_innovasi a
							where 1=1 $cond
							";
		$dataUsr			= $this->db->query($qUser)->result_array();
		$no=0;
		header('Content-type: application/json; charset=UTF-8');
		$cek 	= $this->db->query($qUser)->num_rows();
		if ($cek>0) {
			foreach($dataUsr as $data) { 
				$no++;
				$id 		= $data['id'];
				$req  		= str_replace('-','_',$data['noinnovasi']);
				$noreq 		= str_replace('/','-',$req);

				// $cekstatus ="select 
							// noinnovasi ,status_data_innovasi,status_upti,status_pii,status_stg,
							// case when status_security =jumlah_security then 1 else 0 end as final_status_security,
							// case when status_spasi =jumlah_spasi then 1 else 0 end as final_status_spasi
							// from(
								// select 
								// *, 
								// (select COUNT(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on cast(sd.id_sdlc as char) = dis.type 
									// where noinnovasi =master.noinnovasi and type_tab = 'SPASI') as jumlah_spasi,
								// (select COUNT(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on cast(sd.id_sdlc as char) = dis.type
									// where noinnovasi =master.noinnovasi and type_tab = 'SECURITY SYSTEM')as jumlah_security
								// from (
								// select 
									// a.noinnovasi ,
									// a.status_data_innovasi ,
									// a.status_upti,
									// (select dis.status from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on cast(sd.id_sdlc as char) = dis.type 
									// where noinnovasi =a.noinnovasi and type_tab = 'PERSONAL IDENTIFIABLE INFORMATION')as status_pii,
									// (select dis.status from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on cast(sd.id_sdlc as char) = dis.type 
									// where noinnovasi =a.noinnovasi and type_tab = 'DATA SHARING TELKOM GROUP')as status_stg,
									// (select SUM(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on cast(sd.id_sdlc as char) = dis.type
									// where noinnovasi =a.noinnovasi and type_tab = 'SECURITY SYSTEM'
									// )as status_security,
									// (select SUM(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on cast(sd.id_sdlc as char) = dis.type 
									// where noinnovasi =a.noinnovasi and type_tab = 'SPASI') as status_spasi
								// from mi.data_innovasi a
								// where a.noinnovasi ='".$data['noinnovasi']."'
								// )as master
							// )as master2";
				$cekstatus ="select 
							noinnovasi ,status_data_innovasi,status_upti,
							case when status_security =jumlah_security then 1 else 0 end as final_status_security,
							case when status_spasi =jumlah_spasi then 1 else 0 end as final_status_spasi
							from(
								select 
								*, 
								(select count(*) from mi.param_sdlc where type_tab='SPASI') as jumlah_spasi,
								(select count(*) from mi.param_sdlc where type_tab='SECURITY SYSTEM')as jumlah_security
								from (
								select 
									a.noinnovasi ,
									a.status_data_innovasi ,
									a.status_upti, 
									(select SUM(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on sd.id_sdlc = cast(dis.type as integer)
									where noinnovasi =a.noinnovasi and type_tab = 'SECURITY SYSTEM' and status='1'
									)as status_security,
									(select SUM(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on sd.id_sdlc = cast(dis.type as integer) 
									where noinnovasi =a.noinnovasi and type_tab = 'SPASI' and status='1') as status_spasi
								from mi.data_innovasi a
								where a.noinnovasi ='".$data['noinnovasi']."' 
								)as master
							)as master2 "; 
				$arr_status = $this->db->query($cekstatus)->result_array();
				$rs = array_shift($arr_status);
				if($data['status']==3){
					$status ='<button class="btn btn-sm btn-label-warning btn-pill">DRAFT</button>';
				}else{
					if($rs['status_data_innovasi']=='1' 
						and $rs['status_upti']=='1'  
						and $rs['final_status_security']=='1' 
						and $rs['final_status_spasi']=='1' 
					){
						$status = '<button class="btn btn-sm btn-label-success btn-pill">COMPLY</button>';
					}else if(
						$rs['status_data_innovasi']==NULL
						and $rs['status_upti']==NULL  
						and $rs['final_status_security']==0 
						and $rs['final_status_spasi']==0 ){
						$getPending++;				
						$status = '<button class="btn btn-sm btn-secondary btn-pill">PENDING</button>';	
					}else{
						$status = '<button class="btn btn-sm btn-label-danger btn-pill">NOT COMPLY</button>'; 
					}
				}
				if($data['status']==3){
					if($data['created_by']==$userid){
						$row = array(
							"judul"			=> '<a href="'.base_url().'docdetail/'.$noreq.'">'.$data['judul'].'</a>',
							"cfu_fu"		=> $data['cfu_fu'],
							"url_aplikasi" 	=> '<a href="'.$data['url_aplikasi'].'" target="_blank">'.$data['url_aplikasi'].'</a>',
							"stackholder"	=> $data['stakholder'],
							"created_by"	=> $data['usercreate'],
							"created_date"	=> $data['created_at'],
							"status"		=> $status,
							"actions"		=> $button
						);
					}
				}else{
					$row = array(
						"judul"			=> '<a href="'.base_url().'docdetail/'.$noreq.'">'.$data['judul'].'</a>',
						"cfu_fu"		=> $data['cfu_fu'],
						"url_aplikasi" 	=> '<a href="'.$data['url_aplikasi'].'" target="_blank">'.$data['url_aplikasi'].'</a>',
						"stackholder"	=> $data['stakholder'],
						"created_by"	=> $data['usercreate'],
						"created_date"	=> $data['created_at'],
						"status"		=> $status,
						"actions"		=> $button
					);
				}

				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
		  
	}

	function getBtnAction($akses,$modal_name,$name_button,$actbtn,$classbtn,$iconbtn,$type,$id){
		$a_array = explode(",",trim($akses));
		if(in_array($actbtn,$a_array))
		{
			$button = '
					<button class="btn '.$classbtn.' btn-icon-sm" data-toggle="modal" data-target="#'.$modal_name.'" data-type="'.$type.'" data-id="'.$id.'">
						<i class="'.$iconbtn.'"></i>
						'.$name_button.'
					</button>
					';
		} else {
			$button =  '';	
		}
		
		//return $clearbuton.$button;
		return $button;
	}
}
