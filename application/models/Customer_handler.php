<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$qCustomer	= "
							SELECT * FROM customer
							";
		$dataCustomer= $this->db->query($qCustomer)->result_array();

		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->db->query($qCustomer)->num_rows();

		if ($cek>0) {
			foreach($dataCustomer as $data) { 
				$no++;

				$id 		= $data['id'];
				$row = array(
					"nipnas"		=> $data['nipnas'],
					"nama_pelanggan"=> $data['nama_pelanggan'],
					"actions"		=> $id
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}
}
