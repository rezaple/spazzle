<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cfu_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$data_aksess = $this->query->getAkses($this->profile,'panel/roles');
		$shift = array_shift($data_aksess);
		@$akses = $shift['akses'];
		$qRole 	= "
					select
						a.* 
					from
					mi.param_cfu a
					ORDER BY a.id_cfu ASC
				";
				// echo $qRole;
		$datarole			= $this->query->getDatabyQ($qRole);
		
		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->query->getNumRowsbyQ($qRole)->num_rows();

		if ($cek>0) {

			foreach($datarole as $data) {
				$no++;
				$id = $data['id_cfu'];
				
				$buttonupdate = getRoleUpdate($akses,'update',$id);
				$buttondelete = getRoleDelete($akses,'delete',$id);

				$row = array(
					"id_cfu" => $data['id_cfu'],
					"cfu" => $data['cfu'],
					"actions" => $id
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}
}
