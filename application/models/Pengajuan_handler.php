<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$userdata		= $this->session->userdata('sesspwt'); 
		$userid 		= $userdata['userid'];

		$getUser 		= $this->db->query("
							SELECT a.*, 
								(SELECT level from mi.level_user where id_level=a.level_user) step,
								(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
							FROM mi.user a where userid='$userid'
						")->result_array();
		$dUser 			= array_shift($getUser);
		$level			= $dUser['step'];
		$aksesCreate 	= $dUser['actionbtn'];

		$cond 			= "
						and (no_request in (
							SELECT no_request from mi.sbrhistory where created_by='$userid' or send_to='$userid'
						) or created_by='$userid')
						";
		// if ($level==2) {
		// 	$cond 		= "and reviewer1='$userid' and current>=$level";
		// } else if ($level==3) {
		// 	$cond 		= "and approval1='$userid' and current>=$level";
		// } else if ($level==4) {
		// 	$cond 		= "and approval2='$userid' and current>=$level";
		// } else if ($level==5) {
		// 	$cond 		= "and timtarif1='$userid' and current>=$level";
		// } else if ($level==6) {
		// 	$cond 		= "and timtarif2='$userid' and current>=$level";
		// } else if ($level==7) {
		// 	$cond 		= "and approval3='$userid' and current>=$level";
		// } else if ($level==0) {
		// 	$cond 		= "";
		// } else {
		// 	$cond 		= "and created_by='$userid' and current>=$level";
		// }

		$qUser				= "
							SELECT a.*,
							(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
							(select role_name from mi.sbrdoc_flow where no_request=a.no_request and level=a.current) lastp
							FROM sbrdoc a
							where 1=1 $cond
							";
		$dataUsr			= $this->db->query($qUser)->result_array();

		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->db->query($qUser)->num_rows();

		if ($cek>0) {
			foreach($dataUsr as $data) { 
				$no++;

				$id 		= $data['id'];
				$req  		= str_replace('-','_',$data['no_request']);
				$noreq 		= str_replace('/','-',$req);
				$lastp 		= $data['lastp'];

				if ($data['status']==1) {
					$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting Approval by '.$lastp.'</button>';
				} else if ($data['status']==2) {
					$status = '<button class="btn btn-sm btn-label-warning btn-pill">Return</button>';
				} else if ($data['status']==3) {
					$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting Approval by '.$lastp.'</button>';
				} else if ($data['status']==4) {
					$status = '<button class="btn btn-sm btn-label-success btn-pill">Approved</button>';
				} else if ($data['status']==5) {
					$status = '<button class="btn btn-sm btn-label-danger btn-pill">Rejected</button>';
				} else {
					$status = '<button class="btn btn-sm btn-secondary btn-pill">Draft</button>';
				}

				if ($data['status']==0) {
					$button = '
							<div class="btn-group dropleft">
								<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                                <i class="fa fa-bars"></i>
	                            </button>
                                <div class="dropdown-menu">
                                	'.$this->getBtnAction($aksesCreate,'update','Update','update','btn-sm btnupdateM','la la-pencil-square','update',$id).'
                                    '.$this->getBtnAction($aksesCreate,'delete','Delete','delete','btn-sm btndeleteMenu','la la-trash','delete',$id).'
                                </div>
                            </div>
							';
				} else if ($data['status']==2) {
					if($data['current']==$level) {
						$button = '
								<div class="btn-group dropleft">
									<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                                <i class="fa fa-bars"></i>
		                            </button>
	                                <div class="dropdown-menu">
	                                	'.$this->getBtnAction($aksesCreate,'update','Update','update','btn-sm btnupdateM','la la-pencil-square','update',$id).'
	                                </div>
	                            </div>
								';
					} else {
						$button = '
								<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
								';
					}
				} else {
					if ($level!='1') {
						if($data['current']==$level) {
							if ($data['status']!='4' or $data['status']!='5') {
							$button = '
									<div class="btn-group dropleft">
										<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			                                <i class="fa fa-bars"></i>
			                            </button>
		                                <div class="dropdown-menu">
		                                	'.$this->getBtnAction($aksesCreate,'update','Update','update','btn-sm btnupdateM','la la-pencil-square','update',$id).'
		                                	'.$this->getBtnAction($aksesCreate,'modalaction','Approve','approve','btn-sm text-left kt-link kt-link--success col-sm-12 btnActDoc','la la-check-circle','approve',$id).'
		                                	'.$this->getBtnAction($aksesCreate,'modalaction','Reject','reject','btn-sm text-left kt-link kt-link--danger col-sm-12 btnActDoc','la la-times-circle','reject',$id).'
		                                	'.$this->getBtnAction($aksesCreate,'modalaction','Escalation','eskalasi','btn-sm text-left kt-link kt-link--brand col-sm-12 btnActDoc','la la-rocket','escalation',$id).'
		                                	'.$this->getBtnAction($aksesCreate,'modalaction','Return','return','btn-sm text-left kt-link kt-link--warning col-sm-12 btnActDoc','la la-step-backward','return',$id).'
		                                </div>
		                            </div>
									';
							} else {
								if ($data['status']=='4') {
									$button = '
										<button class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon btnUploadDok" title="Upload Dokumen Jawaban TTD" data-id="'.$id.'" data-toggle="modal" data-target="#modaluploaddok">
											<i class="la la-cloud-upload"></i>
										</button>
										<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
									';
								} else {
									$button = '
									<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
									';
								}
							}
						} else {
							$button = '
							<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
							';
						}
					} else {
						if ($data['status']=='4') {
							$button = '
								<button class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon btnUploadDok" title="Upload Dokumen Pengajuan TTD" data-id="'.$id.'" data-toggle="modal" data-target="#modaluploaddok">
									<i class="la la-cloud-upload"></i>
								</button>
								<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
							';
						} else {
							$button = '
							<a href="'.base_url().'docdetail/'.$noreq.'" class="btn btn-sm btn-outline-brand btn-elevate btn-circle btn-icon"><i class="la la-search"></i></a>
							';
						}
					}
				}

				$row = array(
					"no_request"		=> '<a href="'.base_url().'docdetail/'.$noreq.'">'.$data['no_request'].'</a>',
					"nama_pelanggan"	=> $data['nama_pelanggan'],
					"nipnas"			=> $data['nipnas'],
					"service_id"		=> $data['service_id'],
					"nama_project"		=> $data['nama_project'],
					"subject"			=> $data['subject'],
					"creadet_at"		=> $data['created_at'],
					"status"			=> $status,
					"actions"			=> $button
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}

	function getBtnAction($akses,$modal_name,$name_button,$actbtn,$classbtn,$iconbtn,$type,$id){
		$a_array = explode(",",trim($akses));
		if(in_array($actbtn,$a_array))
		{
			$button = '
					<button class="btn '.$classbtn.' btn-icon-sm" data-toggle="modal" data-target="#'.$modal_name.'" data-type="'.$type.'" data-id="'.$id.'">
						<i class="'.$iconbtn.'"></i>
						'.$name_button.'
					</button>
					';
		} else {
			$button =  '';	
		}
		
		//return $clearbuton.$button;
		return $button;
	}
}
