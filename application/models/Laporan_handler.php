<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data($filter){
		$userdata		= $this->session->userdata('sesspwt'); 
		$userid 		= $userdata['userid'];
		if($filter =='all'){
			$condfilter = '';
		}else{
			$change = explode('_',$filter);
			$condfilter = "and created_at between DATE('".$change[0]."') and DATE('".$change[1]."')";
		}
 
		$cond 			= "and status='4'";
		$qUser			= "
							SELECT a.*,
							(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
							(SELECT name from mi.user where userid=a.created_by) nama_am
							FROM sbrdoc a
							where 1=1 $cond $condfilter
						";
		$dataUsr		= $this->db->query($qUser)->result_array();

		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->db->query($qUser)->num_rows();

		if ($cek>0) {
			foreach($dataUsr as $data) { 
				$no++;

				$id 		= $data['id'];
				$noreq 		= str_replace('/','-',$data['no_request']);

				if ($data['status']==1) {
					$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting Approval</button>';
				} else if ($data['status']==2) {
					$status = '<button class="btn btn-sm btn-label-warning btn-pill">Return</button>';
				} else if ($data['status']==3) {
					$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting Approval</button>';
				} else if ($data['status']==4) {
					$status = '<button class="btn btn-sm btn-label-success btn-pill">Approved</button>';
				} else if ($data['status']==5) {
					$status = '<button class="btn btn-sm btn-label-danger btn-pill">Rejected</button>';
				} else {
					$status = '<button class="btn btn-sm btn-secondary btn-pill">Draft</button>';
				}
				$button_pengajuanas = '-';
				$button_jawabanas ='-';
				$button_pengajuan = '<a href="'.base_url().'pengajuan/download/'.$noreq.'" class="btn btn-sm btn-secondary btn-pill">Download</a>';
				$button_jawaban   = '<a href="'.base_url().'jawaban/download/'.$noreq.'" class="btn btn-sm btn-secondary btn-pill">Download</a>';
				if($data['dok_pengajuan']!='' or $data['dok_pengajuan'] !=null){
					$button_pengajuanas = '<a href="'.base_url().'upload/'.$data['dok_pengajuan'].'" class="btn btn-sm btn-primary btn-pill" target="_blank">Download</a>';					
				}
				if($data['dok_jawaban']!='' or $data['dok_jawaban'] !=null){
					$button_jawabanas   = '<a href="'.base_url().'upload/'.$data['dok_jawaban'].'" class="btn btn-sm btn-primary btn-pill" target="_blank">Download</a>';
				}
				$row = array(
					"no_request"		=> $data['no_request'],
					"nama_pengaju"		=> $data['nama_am'],
					"nama_pelanggan"	=> $data['nama_pelanggan'],
					"nipnas"			=> $data['nipnas'],
					"service_id"		=> $data['service_id'],
					"nama_project"		=> $data['nama_project'],
					"subject"			=> $data['subject'],
					"creadet_at"		=> $data['created_at'],
					"status"			=> $status,
					"docpengajuan"		=> $button_pengajuan,
					"docjawaban"		=> $button_jawaban,	
					"docpengajuanas"	=> $button_pengajuanas,
					"docjawabanas"		=> $button_jawabanas,				
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}

	function getBtnAction($akses,$modal_name,$name_button,$actbtn,$classbtn,$iconbtn,$type,$id){
		$a_array = explode(",",trim($akses));
		if(in_array($actbtn,$a_array))
		{
			$button = '
					<button class="btn '.$classbtn.' btn-icon-sm" data-toggle="modal" data-target="#'.$modal_name.'" data-type="'.$type.'" data-id="'.$id.'">
						<i class="'.$iconbtn.'"></i>
						'.$name_button.'
					</button>
					';
		} else {
			$button =  '';	
		}
		
		//return $clearbuton.$button;
		return $button;
	}
}
