<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_handler extends CI_Model {
	private $profile;

	public function __construct(){
		parent::__construct();
		// $this->db2 = $this->load->database('dblicense', TRUE);
		// $this->db2->get('siswa');
	} 
	function data(){
		$data_aksess = $this->query->getAkses($this->profile,'panel/roles');
		$shift = array_shift($data_aksess);
		@$akses = $shift['akses'];
		$qRole 	= "
					select
						a.*,
						(SELECT xb.name as  update_by FROM mi.user xb WHERE xb.userid=a.update_by)as update_by
					from
					mi.data_faq a
					ORDER BY a.id_faq ASC
				";
				// echo $qRole;
		$datarole			= $this->query->getDatabyQ($qRole);
		
		$no=0;
		header('Content-type: application/json; charset=UTF-8');

		$cek 	= $this->query->getNumRowsbyQ($qRole)->num_rows();

		if ($cek>0) {

			foreach($datarole as $data) {
				$no++;
				$id = $data['id_faq'];
				
				$buttonupdate = getRoleUpdate($akses,'update',$id);
				$buttondelete = getRoleDelete($akses,'delete',$id);

				$row = array(
					"question_faq"		=> $data['question_faq'],
					"answer_faq"		=> $data['answer_faq'],
					"update_by"			=> $data['update_by'],
					"last_update"		=> $data['update_date'],
					"actions"			=> $id
					);
				$json[] = $row;
			}
			return json_encode($json);
		} else {
			$json ='';
			return json_encode($json);
		}
	}
}
