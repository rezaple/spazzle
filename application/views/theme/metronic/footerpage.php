<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" style="
background: -webkit-linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
background: -o-linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
background: -moz-linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
background: linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
border-bottom:none !important;">
	<div class="kt-footer__copyright">
		<?PHP echo date('Y'); ?>&nbsp;&copy;&nbsp;<a href="http://telkom.co.id/" target="_blank" class="kt-link text-white">PT Telekomunikasi Indonesia Tbk.</a>
	</div>
	<!--div class="kt-footer__menu">
		<a href="#" target="_blank" class="kt-footer__menu-link kt-link">About</a>
		<a href="#" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
		<a href="#" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
	</div-->
</div>