<style>
	@media (max-width: 1024px) {
		.kt-header-mobile .kt-header-mobile__toolbar .kt-header-mobile__topbar-toggler i {
		    color: rgba(47, 47, 47)!important;
		}
	}
</style>
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
	<div class="kt-header-mobile__logo">
		<a href="index.html">
			<img alt="Logo" src="<?PHP echo base_url(); ?>images/logotel.png" style="max-height: 2rem;" />
		</a>
	</div>
	<div class="kt-header-mobile__toolbar">
		<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		<!-- <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button> -->
		<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-grid-menu"></i></button>
	</div>
</div>