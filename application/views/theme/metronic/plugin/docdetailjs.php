<?PHP
$noinnovasi      = str_replace('-','/',$id);
$getDoc         = $this->db->query("SELECT * FROM data_innovasi where noinnovasi='$noinnovasi'")->result_array();
$doc            = array_shift($getDoc);
$iddoc          = $doc['id'];
if($level ==2 or $level ==3){
    if($level == 3){
        $start = 3;
    }else{
        $start = 1;
    }
?>
<script>
"use strict";

$(document).on('click', '.btnpreview', function(e){
    e.preventDefault();

    var file = $(this).data('file'); ; // get id of clicked row
    var path = '<?PHP echo base_url();?>attachment/';
    document.getElementById("embedsrc").src = path+file;

});

// Class definition
var KTWizard3 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: <?PHP echo $start;?>,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function(wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function(wizard) {
            KTUtil.scrollTop(); 
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                //= Step 1
                address1: {
                    required: true 
                },
                postcode: {
                    required: true
                },     
                city: {
                    required: true
                },   
                state: {
                    required: true
                },   
                country: {
                    required: true
                },   

                //= Step 2
                package: {
                    required: true
                },
                weight: {
                    required: true
                },  
                width: {
                    required: true
                },
                height: {
                    required: true
                },  
                length: {
                    required: true
                },             

                //= Step 3
                delivery: {
                    required: true
                },
                packaging: {
                    required: true
                },  
                preferreddelivery: {
                    required: true
                },  

                //= Step 4
                locaddress1: {
                    required: true 
                },
                locpostcode: {
                    required: true
                },     
                loccity: {
                    required: true
                },   
                locstate: {
                    required: true
                },   
                loccountry: {
                    required: true
                },
            },
            
            // Display error  
            invalidHandler: function(event, validator) {     
                KTUtil.scrollTop();

                swal.fire({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });

                setTimeout(function() {
                    $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                }, 2000);
            },

            // Submit valid form
            submitHandler: function (form) {
                
            }
        });   
    }

    var initSubmit = function() {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function() {
                        KTApp.unprogress(btn);
                        //KTApp.unblock(formEl);

                        swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                           // $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    }
                });
            }
        });
    }
    
    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#kt_form');

            initWizard(); 
            initValidation();
            initSubmit();
        }
    };
}();
var KTSummernoteDemo={
    init:function(){
        $(".summernote").summernote({
            airMode:false,
            height:50,
            toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
            ]
        })
    }
};
jQuery(document).ready(function() { 
    KTWizard3.init();
    jQuery(document).ready(function(){KTSummernoteDemo.init()});

});
</script>
<?PHP
}else{
?>
<script>
"use strict";
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#bgframework"); //Fields wrapper
    var add_button      = $(".btnframework"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row"><div class="col-lg-9"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label></label><div class="input-group"><input type="text" name="otherframe[]" class="form-control" id="otherframe" placeholder=""></div></div></div> </div><a href="#" class="remove_framework col-lg-1"><br><button class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="Search"><i class="la la-close"></i></button></a></div>'); //add input box
        }
    });
    $(wrapper).on("click",".remove_framework", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });


    var max_fields_x      = 10; //maximum input boxes allowed
    var wrapper_x         = $("#bgdatabase"); //Fields wrapper
    var add_button_x      = $(".btndatabase"); //Add button ID
    
    var xs = 1; //initlal text box count
    $(add_button_x).click(function(e){ //on add input button click
        e.preventDefault();
        if(xs < max_fields_x){ //max input box allowed
            xs++; //text box increment
            $(wrapper_x).append('<div class="row"><div class="col-lg-9"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label></label><div class="input-group"><input type="text" name="otherframe[]" class="form-control" id="otherframe" placeholder=""></div></div></div> </div><a href="#" class="remove_database col-lg-1"><br><button class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="Search"><i class="la la-close"></i></button></a></div>'); //add input box
        }
    });
    $(wrapper_x).on("click",".remove_database", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); xs--;
    })
});
$(document).on('click', '#other_framework', function(e){
    if($(this).is(":checked")){   
        $("#fr_wrapper").show();
    }else{
        $("#fr_wrapper").hide();
    }
});
$(document).on('click', '#other_database', function(e){  
 if($(this).is(":checked")){   
        $("#db_wrapper").show();
    }else{
        $("#db_wrapper").hide();
    }  
});
$(document).on('click', '.btnApprove', function(e){
     e.preventDefault();

    var uid = $(this).data('id'); // get id of clicked row

    $('#dynamic-content').hide(); // hide dive for loader
    $('#modal-loader').show();  // load ajax loader

    $('#ed_pelanggan').empty();
    $('#ed_pelanggan').val('');
    
    $.ajax({
        url: '<?PHP echo base_url(); ?>pengajuan/modal',
        type: 'POST',
        data: 'id='+uid,
        dataType: 'json'
    })
    .done(function(data){
        $('#dynamic-content').hide(); // hide dynamic div
        $('#dynamic-content').show(); // show dynamic div 
        $('#app_norequest').val(data.no_request);
        $('#app_pelanggan').val(data.nama_pelanggan);
        $('#app_subject').val(data.subject);
        $('#app_serviceid').val(data.service_id);
        $('#app_am').val(data.name_am);
        $('#app_nopelanggan').val(data.nipnas);
        $('#app_userid').val(data.created_by);
        $('#app_id').val(data.id);
        $('#modal-loader').hide();    // hide ajax loader
    })
    .fail(function(){
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please refresh page...');
    });
});
$(window).bind("load", function() {
    document.title = "<?PHP echo $norequest; ?> | SBR Online";
});
$(document).on('click', '.btnpreview', function(e){
    e.preventDefault();
    var file = $(this).data('file'); ; // get id of clicked row
    var path = '<?PHP echo base_url();?>attachment/';
    document.getElementById("embedsrc").src = path+file;

});
$(document).on('click', '.btnActDoc', function(e){
    e.preventDefault();

    var uid = $(this).data('id'); ; // get id of clicked row
    var act = $(this).data('type'); // get id of clicked row

    $('#dynamic-content').hide(); // hide dive for loader
    $('#modal-loader').show();  // load ajax loader

    //$( ".labaction" ).html(act);
    
    $.ajax({
        url: '<?PHP echo base_url(); ?>pengajuan/modal',
        type: 'POST',
        data: 'id='+uid,
        dataType: 'json'
    })
    .done(function(data){
        $('#dynamic-content').hide(); // hide dynamic div
        $('#dynamic-content').show(); // show dynamic div

        $('#idsubmitact').val(data.id);
        $('#typesubmitact').val(act);

        $('#modal-loader').hide();    // hide ajax loader
    })
    .fail(function(){
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please refresh page...');
    });
});

$(document).on('click', '.btndeleteMenu', function(e){
    e.preventDefault();

    var id = $(this).data('id'); // get id of clicked row

    $('#dynamic-content').hide(); // hide dive for loader
    $('#modal-loader').show();  // load ajax loader

    $.ajax({
        url: '<?PHP echo base_url(); ?>pengajuan/modal',
        type: 'POST',
        data: 'id='+id,
        dataType: 'json'
    })
    .done(function(data){
        // console.log(data);
        $('#dynamic-content').hide(); // hide dynamic div
        $('#dynamic-content').show(); // show dynamic div
        
        $('#iddel').val(data.id);
        $('#modal-loader').hide();    // hide ajax loader
    })
    .fail(function(){
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
    });
});

// Class definition

var KTFormWidgets = function () {
    // Private functions
    var validator;

    var initWidgets = function() {
        // datepicker
        $('#kt_datepicker').datepicker({
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        // datetimepicker
        $('#kt_datetimepicker').datetimepicker({
            pickerPosition: 'bottom-left',
            todayHighlight: true,
            autoclose: true,
            format: 'yyyy.mm.dd hh:ii'
        });

        $('#kt_datetimepicker').change(function() {
            validator.element($(this));
        });

        // timepicker
        $('#kt_timepicker').timepicker({
            minuteStep: 1,
            showSeconds: true,
            showMeridian: true
        });

        // daterangepicker
        $('#kt_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            var input = $('#kt_daterangepicker').find('.form-control');
            
            input.val( start.format('YYYY/MM/DD') + ' / ' + end.format('YYYY/MM/DD'));
            validator.element(input); // validate element
        });

        // bootstrap switch
        $('[data-switch=true]').bootstrapSwitch();
        $('[data-switch=true]').on('switchChange.bootstrapSwitch', function() {
            validator.element($(this)); // validate element
        });

        // bootstrap select
        $('#kt_bootstrap_select').selectpicker();
        $('#kt_bootstrap_select').on('changed.bs.select', function() {
            validator.element($(this)); // validate element
        });

        // select2
        $('.kt_select2norm').select2({
            placeholder: "Pilih...",
        });
        $('#kt_select2').select2({
            placeholder: "Select a state",
        });
        $('#kt_select2').on('select2:change', function(){
            validator.element($(this)); // validate element
        });

        $(".getCustomer").select2({
            closeOnSelect:true,
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listcustomer",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        function formatRepo (repo) {
          if (repo.loading) {
            return repo.text;
          }

          var markup = "<div>"+ repo.id +" - " + repo.text + "</div>";

          return markup;
        }

        function formatRepoSelection (repo) {
          return repo.id +' - '+ repo.text;
        }

        $(".getRev").select2({
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listapproval/2",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo2, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection2 // omitted for brevity, see the source of this page
        });

        $(".getAppr1").select2({
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listapproval/3",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo2, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection2 // omitted for brevity, see the source of this page
        });

        $(".getAppr2").select2({
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listapproval/4",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo2, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection2 // omitted for brevity, see the source of this page
        });

        function formatRepo2 (repo) {
          if (repo.loading) {
            return repo.text;
          }

          var markup = "<div>" + repo.text + "</div>";

          return markup;
        }

        function formatRepoSelection2 (repo) {
          return repo.text;
        }
        
    }
    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
            <span></span>\
        </div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }
    var initInsert = function () {
        $('#savedraft').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    namaproject    : { required: true },
                    subject        : { required: true },
                    pelanggan      : { required: true },
                    pelanggan      : { required: true },
                    serviceid      : { required: true },
                    reviewer1      : { required: true },
                    approval1      : { required: true },
                    approval2      : { required: true },
                    latarbelakang  : { required: true },
                    aspekstrategis : { required: true },
                    aspekfinansial : { required: true },
                    aspekkompetisi : { required: true },
                    konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/insert/draft",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#modalinsert .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#modalinsert .modal-content');

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#modalinsert').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            $('#forminsertsingle')[0].reset();
                            $('.getCustomer').val(null).trigger('change');
                            $('.getRev').val(null).trigger('change');
                            $('.getAppr1').val(null).trigger('change');
                            $('.getAppr2').val(null).trigger('change');

                            var alert = $('#suksesinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', '<strong>Data Insert Failed!</strong> Change a few things up and try submitting again.');
                            
                            KTApp.unblock('#modalinsert .modal-content');
                            
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initInsertSubmit = function () {
        $('#savepublish').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    namaproject    : { required: true },
                    subject        : { required: true },
                    pelanggan      : { required: true },
                    pelanggan      : { required: true },
                    serviceid      : { required: true },
                    reviewer1      : { required: true },
                    approval1      : { required: true },
                    approval2      : { required: true },
                    latarbelakang  : { required: true },
                    aspekstrategis : { required: true },
                    aspekfinansial : { required: true },
                    aspekkompetisi : { required: true },
                    konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/insert/publish",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#modalinsert .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#modalinsert .modal-content');                            

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#modalinsert').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            $('#forminsertsingle')[0].reset();
                            $('.getCustomer').val(null).trigger('change');
                            $('.getRev').val(null).trigger('change');
                            $('.getAppr1').val(null).trigger('change');
                            $('.getAppr2').val(null).trigger('change');

                            var alert = $('#suksesinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', '<strong>Data Insert Failed!</strong> Change a few things up and try submitting again.');
                            
                            KTApp.unblock('#modalinsert .modal-content');
                            
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initUpdate = function () {
        $('#saveupdatedraft').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    ed_namaproject    : { required: true },
                    ed_subject        : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_serviceid      : { required: true },
                    ed_reviewer1      : { required: true },
                    ed_approval1      : { required: true },
                    ed_approval2      : { required: true },
                    ed_latarbelakang  : { required: true },
                    ed_aspekstrategis : { required: true },
                    ed_aspekfinansial : { required: true },
                    ed_aspekkompetisi : { required: true },
                    ed_konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/update/draft",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#update .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            //showErrorMsg(form, 'success', '<strong>Data Insert Success!</strong>');
                            KTApp.unblock('#update .modal-content');
                            
                            $('#update').modal('toggle');

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});
                            
                            $('#tabledata').DataTable().ajax.reload();
                            $('#ed_pelanggan').val(null).trigger('change');
                            $('#ed_reviewer1').val(null).trigger('change');
                            $('#ed_approval1').val(null).trigger('change');
                            $('#ed_approval2').val(null).trigger('change');

                            $('#formupdate')[0].reset();

                            var alert = $('#suksesupdate');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#update .modal-content');

                            showErrorMsg(form, 'danger', '<strong>Data Update Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initUpdateSubmit = function () {
        $('#saveupdatepublish').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    ed_namaproject    : { required: true },
                    ed_subject        : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_serviceid      : { required: true },
                    ed_reviewer1      : { required: true },
                    ed_approval1      : { required: true },
                    ed_approval2      : { required: true },
                    ed_latarbelakang  : { required: true },
                    ed_aspekstrategis : { required: true },
                    ed_aspekfinansial : { required: true },
                    ed_aspekkompetisi : { required: true },
                    ed_konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/update/publish",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#update .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            //showErrorMsg(form, 'success', '<strong>Data Insert Success!</strong>');
                            KTApp.unblock('#update .modal-content');
                            
                            $('#update').modal('toggle');
                            
                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#tabledata').DataTable().ajax.reload();
                            $('#ed_pelanggan').val(null).trigger('change');
                            $('#ed_reviewer1').val(null).trigger('change');
                            $('#ed_approval1').val(null).trigger('change');
                            $('#ed_approval2').val(null).trigger('change');
                            $('#formupdate')[0].reset();

                            var alert = $('#suksesupdate');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#update .modal-content');

                            showErrorMsg(form, 'danger', '<strong>Data Update Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initDelete = function () {
        $('#deleteBtn').click(function(e) {
            e.preventDefault();
            var btn     = $(this);
            var form    = $(this).closest('form');           
            var id      = $("#iddel").val();

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/delete",
                type: "POST",
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});
                            
                            $('#delete').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            var alert = $('#suksesdelete');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {

                            $('#delete').modal('toggle');
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            $('#textfailed').html('<strong>Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initAction = function () {
        $('#submitActBtn').click(function(e) {
            e.preventDefault();
            var btn     = $(this);
            var form    = $(this).closest('form');           
            var id      = $("#idsubmitact").val();

            form.validate({
                rules: {
                    idsubmitact    : { required: true },
                    comment        : { required: true },
                    typesubmitact  : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/actionsubmit",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#modalaction .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#modalaction .modal-content');

                            $('#modalaction').modal('toggle');
                            
                            var alert = $('#suksesinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                        setTimeout(function() {
                            location.reload();
                        }, 2500);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {

                            $('#modalaction').modal('toggle');
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            $('#textfailed').html('<strong>Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initApprove = function () {
        $('#saveapproved').click(function(e) {
            e.preventDefault();
            var btn     = $(this);
            var form    = $(this).closest('form');           
            var id      = $("#app_norequest").val();

            form.validate({
                rules: {
                    layanan    : { required: true },
                    volume     : { required: true },
                    tarif       : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/actionapprove",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#modalapprove .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#modalapprove .modal-content');

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#modalapprove').modal('toggle');
                            
                            $('#tabledata').DataTable().ajax.reload();
                            
                            var alert = $('#suksesinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                         setTimeout(function() {
                            location.reload();
                        }, 2500);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {

                            $('#modalapprove').modal('toggle');
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            $('#textfailed').html('<strong>Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initRepublish = function () {
        $('#saverepublish').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    judul : { required: true },
                    cfu_fu : { required: true },
                    url : { required: true },
                    domain : { required: true },
                    deskripsi : { required: true },
                    tujuan : { required: true },
                    jenis_data : { required: true },
                    stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/republish_innovasi",
                type: "POST",
                beforeSend: function(){ 
                   // KTApp.block('#modalinsert .modal-content', {
                   //      overlayColor: '#000000',
                   //      type: 'v2',
                   //      state: 'success',
                   //      message: 'Please wait...'
                   //  });
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    } else {
                        // // similate 2s delay
                        // setTimeout(function() {
                        //     btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        //     showErrorMsg(form, 'danger', '<strong>Data Insert Failed!</strong> Change a few things up and try submitting again.');
                            
                        //     KTApp.unblock('#modalinsert .modal-content');
                            
                        //     var alert = $('#gagalinsert');
                        //     alert.removeClass('kt-hidden').show();
                        // }, 2000);
                    }
                }
            });
        });     
    }
    var initRequirment = function () {
        $('#savenewresubmit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/new_resubmit",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "The application has been successfully re-submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
	var initRequirmentDraft = function () {
        $('#savenewresubmit_draft').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/new_resubmit_draft",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
	var initDraft = function () {
        $('#updatedraft').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/draft_resubmit",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "Update Succesfull !", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initDesign = function () { 
        $('#savedocdesign').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_design",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initSOP = function () { 
        $('#savedocsop').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_sop",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initSOPD2P = function () { 
        $('#savedocsopd2p').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_sopd2p",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initSMP = function () { 
        $('#savedocsmp').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_smp",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initTest = function () { 
        $('#savedoctes').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_tes",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initPII = function () { 
        $('#savedocpii').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_pii",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initTou = function () { 
        $('#savedoctou').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_tou",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initTfa = function () { 
        $('#savedoctfa').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_tfa",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initCustomer = function () { 
        $('#savedoccustomer').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_customer",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initNDE = function () { 
        $('#savedocnde').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_nde",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initMoM = function () { 
        $('#savedocstg').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/upload_stg",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initupti = function () { 
        $('#saveupti').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/update_upti",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
	var initDokGov = function () {
        $('#save_komen_gov').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/submit_dok_gov",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "The application has been successfully re-submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initDokTek = function () {
        $('#save_komen_tek').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/submit_dok_tek",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "The application has been successfully re-submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    var initKomite = function () {
        $('#save_komen_komit').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    // judul : { required: true },
                    // cfu_fu : { required: true },
                    // url : { required: true },
                    // domain : { required: true },
                    // deskripsi : { required: true },
                    // tujuan : { required: true },
                    // jenis_data : { required: true },
                    // stakholder : { required: true }
                }
            });

            if (!form.valid()) {
                return;
            }

            KTApp.progress(btn);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/submit_komite",
                type: "POST",
                beforeSend: function(){ 
                },
                success: function(data) {
                    if(data=='') {
                        KTApp.unprogress(btn);
                        // similate 2s delay
                         swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "The application has been successfully re-submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    } else {
                    }
                }
            });
        });     
    }
    
	return {
        // public functions
        init: function() {
            initWidgets(); 
            initInsert();
            initInsertSubmit();
            initUpdate();
            initUpdateSubmit();
            initDelete();
            initAction();
            initApprove();
            initRepublish();
            initRequirment();
			initRequirmentDraft();
            initDesign();
            initSOP();
            initSOPD2P();
            initSMP();
            initTest();
            initPII();
            initTou();
            initTfa();
            initCustomer();
            initNDE();
            initMoM();
            initupti();
			initDraft();
			initDokTek();
			initDokGov();
			initKomite();
        }
    };
}();

var KTSummernoteDemo={
    init:function(){
        $(".summernote").summernote({height:150});
		$(".summernote_lg").summernote({height:300})
    }
};

jQuery(document).ready(function() {
    KTFormWidgets.init();
    jQuery(document).ready(function(){KTSummernoteDemo.init()});
    //KTFormWidgets.init();
    $(".summernote").summernote({
        airMode:false,
        height:50,
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
          ]
    });
    // multi select
    $('#cfu_fu').select2({
        placeholder: "Pilih CFU/FU",
    });
    $('#domain').select2({
        placeholder: "Pilih Domain",
    });
});

</script>
<?PHP } ?>