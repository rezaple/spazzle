<script>
"use strict";
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#bgframework"); //Fields wrapper
    var add_button      = $(".btnframework"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row"><div class="col-lg-9"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label></label><div class="input-group"><input type="text" name="otherframe[]" class="form-control" id="otherframe" placeholder=""></div></div></div> </div><a href="#" class="remove_framework col-lg-1"><br><button class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="Search"><i class="la la-close"></i></button></a></div>'); //add input box
        }
    });
    $(wrapper).on("click",".remove_framework", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });


    var max_fields_x      = 10; //maximum input boxes allowed
    var wrapper_x         = $("#bgdatabase"); //Fields wrapper
    var add_button_x      = $(".btndatabase"); //Add button ID
    
    var xs = 1; //initlal text box count
    $(add_button_x).click(function(e){ //on add input button click
        e.preventDefault();
        if(xs < max_fields_x){ //max input box allowed
            xs++; //text box increment
            $(wrapper_x).append('<div class="row"><div class="col-lg-9"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label></label><div class="input-group"><input type="text" name="otherframe[]" class="form-control" id="otherframe" placeholder=""></div></div></div> </div><a href="#" class="remove_database col-lg-1"><br><button class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="Search"><i class="la la-close"></i></button></a></div>'); //add input box
        }
    });
    $(wrapper_x).on("click",".remove_database", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); xs--;
    })

});
$(window).bind("load", function() {
    document.title = "INPUT | Management INOVASI";
    $('.kt-subheader__title').html('User Profile');
});
$(document).on('click', '#other_framework', function(e){
    if($(this).is(":checked")){   
        $("#fr_wrapper").show();
    }else{
        $("#fr_wrapper").hide();
    }
});
$(document).on('click', '#other_database', function(e){  
 if($(this).is(":checked")){   
        $("#db_wrapper").show();
    }else{
        $("#db_wrapper").hide();
    }  
});
$(document).on('click', '#pii', function(e){  
 if($(this).is(":checked")){   
        $("#uepo").show();
    }else{
        $("#uepo").hide();
    }  
});
// Class definition
var KTWizard2 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v2', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function(wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function(wizard) {
            KTUtil.scrollTop();    
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                //= Step 1
                judul: {
                    required: true 
                },
                cfu_fu: {
                    required: true
                },    
                url: {
                    required: true
                },   
                deskripsi: {
                    required: true
                },   
                tujuan: {
                    required: true
                },   
                jenis_data: {
                    required: true
                }, 
                stakholder: {
                    required: true
                },
                diagram: {
                    required: true
                },
                diagram2: {
                    required: true
                },

                //= Step 2
                docrequirmen: {
                    required: true 
                },
                docdesign: {
                    required: true
                },     
                doctesreport: {
                    required: true
                },   
                docsmp: {
                    required: true
                },   
                docsop: {
                    required: true
                },   
                docsopd2p: {
                    required: true
                },       

                //= Step 3
                doctou: {
                    required: true
                },
                doctfa: {
                    required: true
                },   
                docss: {
                    required: true 
                }, 
            },
            
            // Display error  
            invalidHandler: function(event, validator) {     
                KTUtil.scrollTop();

                swal.fire({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {
                
            }
        });   
    }

    var initSubmit = function() {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function() {
                        KTApp.unprogress(btn);
                        //KTApp.unblock(formEl);

                        swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    }
                });
            }
        });
    }

    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v2');
            formEl = $('#kt_form');

            initWizard(); 
            initValidation();
            initSubmit();
        }
    };
}();

jQuery(document).ready(function() {    
    KTWizard2.init();
    //KTFormWidgets.init();
    $(".summernote").summernote({height:150})
});
</script>