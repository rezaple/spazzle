<script>
"use strict";
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $("#bgframework"); //Fields wrapper
    var add_button      = $(".btnframework"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row"><div class="col-lg-9"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label></label><div class="input-group"><input type="text" name="otherframe[]" class="form-control" id="otherframe" placeholder=""></div></div></div> </div><a href="#" class="remove_framework col-lg-1"><br><button class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="Search"><i class="la la-close"></i></button></a></div>'); //add input box
        }
    });
    $(wrapper).on("click",".remove_framework", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });


    var max_fields_x      = 10; //maximum input boxes allowed
    var wrapper_x         = $("#bgdatabase"); //Fields wrapper
    var add_button_x      = $(".btndatabase"); //Add button ID
    
    var xs = 1; //initlal text box count
    $(add_button_x).click(function(e){ //on add input button click
        e.preventDefault();
        if(xs < max_fields_x){ //max input box allowed
            xs++; //text box increment
            $(wrapper_x).append('<div class="row"><div class="col-lg-9"> <div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label></label><div class="input-group"><input type="text" name="otherframe[]" class="form-control" id="otherframe" placeholder=""></div></div></div> </div><a href="#" class="remove_database col-lg-1"><br><button class="btn btn-danger btn-sm btn-icon btn-icon-md kt-btn btn-sm" title="Search"><i class="la la-close"></i></button></a></div>'); //add input box
        }
    });
    $(wrapper_x).on("click",".remove_database", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); xs--;
    })

});
$(window).bind("load", function() {
    document.title = "Input | Spazzle Innovation Management";
    $('.kt-subheader__title').html('User Profile');
});
$(document).on('click', '#other_framework', function(e){
    if($(this).is(":checked")){   
        $("#fr_wrapper").show();
    }else{
        $("#fr_wrapper").hide();
    }
});
$(document).on('click', '#other_database', function(e){  
 if($(this).is(":checked")){   
        $("#db_wrapper").show();
    }else{
        $("#db_wrapper").hide();
    }  
});
$(document).on('click', '#pii', function(e){  
 if($(this).is(":checked")){   
        $("#uepo").show();
    }else{
        $("#uepo").hide();
    }  
});
// Class definition
var KTWizard2 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v2', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function(wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function(wizard) {
            KTUtil.scrollTop();    
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                //= Step 1
                judul: {
                    required: true 
                },
                cfu_fu: {
                    required: true
                },    
                domain: {
                    required: true
                },  
                url: {
                    required: true
                },   
                deskripsi: {
                    required: true
                },   
                tujuan: {
                    required: true
                },   
                jenis_data: {
                    required: true
                }, 
                stakholder: {
                    required: true
                },
                diagram: {
                    required: true
                },
                diagram2: {
                    required: true
                },

                // //= Step 2
                // docrequirmen: {
                //     required: true 
                // },
                // docdesign: {
                //     required: true
                // },     
                // doctesreport: {
                //     required: true
                // },   
                // docsmp: {
                //     required: true
                // },   
                // docsop: {
                //     required: true
                // },   
                // docsopd2p: {
                //     required: true
                // },       

                // //= Step 3 
                // doctfa: {
                //     required: true
                // },   
                // docss: {
                //     required: true 
                // }, 
            },
            
            // Display error  
            invalidHandler: function(event, validator) {     
                KTUtil.scrollTop();

                swal.fire({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {
                
            }
        });   
    }

    var initSubmit = function() {
        var btn = formEl.find('[id="btn-submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function() {
                        KTApp.unprogress(btn);
                        //KTApp.unblock(formEl);

                        swal.fire({
                            "title": "", 
                            "text": "The application has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                            $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    }
                });
            }
        });
    }
	var initDraft = function() {
        var btn = formEl.find('[id="btn-draft"]');
		
        btn.on('click', function(e) {
            e.preventDefault();
			alert("Tes");
             
        });
    }

    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v2');
            formEl = $('#kt_form');

            initWizard(); 
            initValidation();
            initSubmit();
			initDraft();
        }
    };
}();
var KTWizard3 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v3', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function(wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function(wizard) {
            KTUtil.scrollTop(); 
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                //= Step 1
                // judul: {
                //     required: true 
                // },
                // cfu_fu: {
                //     required: true
                // },    
                // url: {
                //     required: true
                // },   
                // deskripsi: {
                //     required: true
                // },   
                // tujuan: {
                //     required: true
                // },   
                // jenis_data: {
                //     required: true
                // }, 
                // stakholder: {
                //     required: true
                // },
                // diagram: {
                //     required: true
                // },
                // diagram2: {
                //     required: true
                // },

                //= Step 2
                // docrequirmen: {
                //     required: true,
                //     accept: "application/pdf"
                // },
                // docdesign: {
                //     required: true
                // },     
                // doctesreport: {
                //     required: true
                // },   
                // docsmp: {
                //     required: true
                // },   
                // docsop: {
                //     required: true
                // },   
                // docsopd2p: {
                //     required: true
                // },       

                // //= Step 3
                // doctou: {
                //     required: true
                // },
                // doctfa: {
                //     required: true
                // },   
                // docss: {
                //     required: true 
                // }, 
            },
            
            // Display error  
            invalidHandler: function(event, validator) {     
                KTUtil.scrollTop();

                swal.fire({
                    "title": "", 
                    "text": "There are some errors in your submission. Please correct them.", 
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {
                
            }
        });   
    }

    var initSubmit = function() {
        var btn = formEl.find('[id="btn-submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            if (validator.form()) {
                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function() {
                        KTApp.unprogress(btn);
                        //KTApp.unblock(formEl);

                        swal.fire({
                            "title": "SPAZZLE Innovation", 
                            "text": "The Innovation has been successfully submitted!", 
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        setTimeout(function() {
                           $(location).attr('href', '<?PHP echo base_url();?>inovasi');
                        }, 2000);
                    }
                });
            }
        });
    }
	
	var initDraft = function() {
        var btn = formEl.find('[id="btn-draft"]');
		
        btn.on('click', function(e) {
            e.preventDefault();
			var btn = $(this);
            var formEl = $(this).closest('form');           

            // form.validate({
                // rules: {
                    // namaproject    : { required: true },
                    // subject        : { required: true },
                    // pelanggan      : { required: true },
                    // pelanggan      : { required: true },
                    // serviceid      : { required: true },
                    // appflow        : { required: true },
                    // no_order_ncx   : { required: true },
                    // reviewer1      : { required: true },
                    // approval1      : { required: true },
                    // approval2      : { required: true },
                    // approval3      : { required: true },
                    // timtarif1      : { required: true },
                    // timtarif2      : { required: true },
                    // latarbelakang  : { required: true },
                    // aspekstrategis : { required: true },
                    // aspekfinansial : { required: true },
                    // aspekkompetisi : { required: true },
                    // konfigurasiteknis : { required: true },
                // }
            // });

            // if (!form.valid()) {
                // return;
            // }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            formEl.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>ctrl_innovasi/draft_innovasi",
                type: "POST",
                success: function() {
					KTApp.unprogress(btn);
					//KTApp.unblock(formEl);

					swal.fire({
						"title": "SPAZZLE Innovation", 
						"text": "Succesfull Save Draft !", 
						"type": "success",
						"confirmButtonClass": "btn btn-secondary"
					});
					setTimeout(function() {
					    $(location).attr('href', '<?PHP echo base_url();?>inovasi');
					}, 2000);
				}
            });
             
        });
    }
	
    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v3');
            formEl = $('#kt_form');

            initWizard(); 
            initValidation();
            initSubmit();
			initDraft();
        }
    };
}();
jQuery(document).ready(function() {    
    KTWizard3.init();
    //KTFormWidgets.init();
    $(".summernote").summernote({
        airMode:false,
        height:50,
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
          ]
    });
    $(".summernote_deskripsi").summernote({
        airMode:false,
        height:50,
        placeholder: 'Penjelasan Singkat tentang use case',
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
        ]
    });
    $(".summernote_tujuan").summernote({
        airMode:false,
        height:60,
        placeholder: 'Tujuan Implementasi use case data sharing; Manfaat yang akan didapatkan Perusahaan, revenue projection',
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
          ]
    });
    $(".summernote_jenis_data").summernote({
        airMode:false,
        height:50,
        placeholder: 'Jenis data yang akan dimanfaatkan dalam use case data sharing',
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
          ]
    });
    $(".summernote_stakeholder").summernote({
        airMode:false,
        height:50,
        placeholder: 'Pihak-pihak yang terlibat dalam proyek use case data sharing',
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
          ]
    });
    $(".summernote_penjelasan").summernote({
        airMode:false,
        height:50,
        placeholder: 'Penjelasan singkat tentang diagram alur pemrosesan data',
        toolbar: [
            // [groupName, [list of button]]
            //['style', ['bold', 'italic', 'underline', 'clear']],
            //['font', ['strikethrough', 'superscript', 'subscript']],             
          ]
    });
    // multi select
    $('#cfu_fu').select2({
        placeholder: "CFU / FU Initiator Inovasi",
    });
    $('#domain').select2({
        placeholder: "Pilih Domain",
    });
     $('#jenis_aplikasi').select2({
        placeholder: "Pilih Domain",
    });
});
</script>