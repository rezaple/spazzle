<script>
"use strict";

$(document).on('click', '.btnupdateM', function(e){
    e.preventDefault();

    var uid = $(this).data('id'); // get id of clicked row

    $('#dynamic-content').hide(); // hide dive for loader
    $('#modal-loader').show();  // load ajax loader

    $('#ed_pelanggan').empty();
    $('#ed_pelanggan').val('');
    
    $.ajax({
        url: '<?PHP echo base_url(); ?>pengajuan/modal',
        type: 'POST',
        data: 'id='+uid,
        dataType: 'json'
    })
    .done(function(data){
        $('#dynamic-content').hide(); // hide dynamic div
        $('#dynamic-content').show(); // show dynamic div
        
        $( "#updatename" ).html(data.no_request);

        $( "#ed_id" ).val(data.id);
        $( "#statusbefore" ).val(data.status);
        $( "#ed_namaproject" ).val(data.nama_project);
        $( "#ed_subject" ).val(data.subject);
        $( "#ed_serviceid" ).val(data.service_id);
        $( "#ed_norequest" ).val(data.ed_norequest);

        var pelSelect = $('#ed_pelanggan');
        pelSelect.append('<option value="'+data.nipnas+'" selected="selected">'+ data.nama_pelanggan +'</option>');
        pelSelect.val(data.nipnas).trigger('change');

        var revSelect = $('#ed_reviewer1');
        revSelect.append('<option value="'+data.reviewer1+'" selected="selected">'+ data.usernamerev +' - '+ data.namerev1 +'</option>');
        revSelect.val(data.reviewer1).trigger('change');

        var appr1Select = $('#ed_approval1');
        appr1Select.append('<option value="'+data.approval1+'" selected="selected">'+ data.usernameappr1 +' - '+ data.nameappr1 +'</option>');
        appr1Select.val(data.approval1).trigger('change');

        var appr1Select = $('#ed_approval2');
        appr1Select.append('<option value="'+data.approval2+'" selected="selected">'+ data.usernameappr2 +' - '+ data.nameappr2 +'</option>');
        appr1Select.val(data.approval2).trigger('change');

        $("#ed_latarbelakang").summernote("code", data.latar_belakang);
        $("#ed_aspekstrategis").summernote("code", data.aspek_strategis);
        $("#ed_aspekfinansial").summernote("code", data.aspek_finansial);
        $("#ed_aspekkompetisi").summernote("code", data.aspek_kompetisi);
        $("#ed_konfigurasiteknis").summernote("code", data.konfigurasi_teknis);

        $('#modal-loader').hide();    // hide ajax loader
    })
    .fail(function(){
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please refresh page...');
    });
});

$(document).on('click', '.btndeleteMenu', function(e){
    e.preventDefault();

    var id = $(this).data('id'); // get id of clicked row

    $('#dynamic-content').hide(); // hide dive for loader
    $('#modal-loader').show();  // load ajax loader

    $.ajax({
        url: '<?PHP echo base_url(); ?>pengajuan/modal',
        type: 'POST',
        data: 'id='+id,
        dataType: 'json'
    })
    .done(function(data){
        // console.log(data);
        $('#dynamic-content').hide(); // hide dynamic div
        $('#dynamic-content').show(); // show dynamic div
        
        $('#iddel').val(data.id);
        $('#modal-loader').hide();    // hide ajax loader
    })
    .fail(function(){
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
    });
});

$(document).on('click', '.btnUploadDok', function(e){
    e.preventDefault();

    var id = $(this).data('id'); // get id of clicked row

    $('#dynamic-content').hide(); // hide dive for loader
    $('#modal-loader').show();  // load ajax loader
    $('#dokexist').html('');
    $.ajax({
        url: '<?PHP echo base_url(); ?>pengajuan/modal',
        type: 'POST',
        data: 'id='+id,
        dataType: 'json'
    })
    .done(function(data){
        // console.log(data);
        $('#dynamic-content').hide(); // hide dynamic div
        $('#dynamic-content').show(); // show dynamic div
        if(data.dok_jawaban =='' || data.dok_jawaban == null){
        }else{
            $('#dokexist').html('Dok Jawaban Exist : <a href="<?PHP echo base_url(); ?>upload/'+data.dok_jawaban+'" target="_blank">'+data.dok_jawaban+'</a> <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>');
        }
        $('#idreq').val(data.id);
        $('#uploadname').html(data.no_request);
        $('#modal-loader').hide();    // hide ajax loader
    })
    .fail(function(){
        $('.modal-body').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
    });
});

var KTDatatablesSearchOptionsColumnSearch = function() {

    $.fn.dataTable.Api.register('column().title()', function() {
        return $(this.header()).text().trim();
    });

    var initTable1 = function() {
        //var table = $('#tabledata');

        // DATATABLE
        //table.DataTable({
        var table = $('#tabledata').DataTable({
            responsive: true,
            dom:
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", // read more: https://datatables.net/examples/basic_init/dom.html

            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
                'emptyTable': `
                            <div class="row" style="padding: 20px;">
                                <div class="col-sm-12">
                                    <div><img src="<?PHP echo base_url(); ?>images/icon/notfound.png"></div><br>
                                    <h5 class="text-center">Anda Belum Memiliki Data Tersimpan</h5>
                                    </h6>Silahkan buat data baru</h6><br>
                                </div>
                            </div>`
            },
            searchDelay: 500,
            processing: true,
            serverSide: true,
            order: [[3,'desc'],[0,'asc']],
            ajax: {
                url: '<?PHP echo base_url(); ?>jawaban/getdatajawaban',
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                         'no_answer','no_request', 'analisa_teknis', 'created_at', 'actions',    
                    ],
                },
            },
            columns: [
                {data: 'no_answer'},
                {data: 'no_request'},
                {data: 'analisa_teknis'},
                {data: 'created_at'},
                {data: 'actions'},
            ],
            columnDefs: [
                {
                    type: 'formatted-num',
                    targets: 0,
                },
                {
                    class: 'text-center',
                    targets: -1,
                },
            ],
        });
    };

    return {

        //main function to initiate the module
        init: function() {
            initTable1();
        },

    };

}();

// Class definition

var KTFormWidgets = function () {
    // Private functions
    var validator;

    var initWidgets = function() {
        // datepicker
        $('#kt_datepicker').datepicker({
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        // datetimepicker
        $('#kt_datetimepicker').datetimepicker({
            pickerPosition: 'bottom-left',
            todayHighlight: true,
            autoclose: true,
            format: 'yyyy.mm.dd hh:ii'
        });

        $('#kt_datetimepicker').change(function() {
            validator.element($(this));
        });

        // timepicker
        $('#kt_timepicker').timepicker({
            minuteStep: 1,
            showSeconds: true,
            showMeridian: true
        });

        // daterangepicker
        $('#kt_daterangepicker').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            var input = $('#kt_daterangepicker').find('.form-control');
            
            input.val( start.format('YYYY/MM/DD') + ' / ' + end.format('YYYY/MM/DD'));
            validator.element(input); // validate element
        });

        // bootstrap switch
        $('[data-switch=true]').bootstrapSwitch();
        $('[data-switch=true]').on('switchChange.bootstrapSwitch', function() {
            validator.element($(this)); // validate element
        });

        // bootstrap select
        $('#kt_bootstrap_select').selectpicker();
        $('#kt_bootstrap_select').on('changed.bs.select', function() {
            validator.element($(this)); // validate element
        });

        // select2
        $('.kt_select2norm').select2({
            placeholder: "Pilih...",
        });
        $('#kt_select2').select2({
            placeholder: "Select a state",
        });
        $('#kt_select2').on('select2:change', function(){
            validator.element($(this)); // validate element
        });

        $(".getCustomer").select2({
            closeOnSelect:true,
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listcustomer",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        function formatRepo (repo) {
          if (repo.loading) {
            return repo.text;
          }

          var markup = "<div>"+ repo.id +" - " + repo.text + "</div>";

          return markup;
        }

        function formatRepoSelection (repo) {
          return repo.id +' - '+ repo.text;
        }

        $(".getRev").select2({
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listapproval/2",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo2, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection2 // omitted for brevity, see the source of this page
        });

        $(".getAppr1").select2({
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listapproval/3",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo2, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection2 // omitted for brevity, see the source of this page
        });

        $(".getAppr2").select2({
            placeholder: "Pilih...",
            allowClear: true,
            ajax: {
                url: "<?PHP echo base_url(); ?>pengajuan/listapproval/4",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 0,
            templateResult: formatRepo2, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection2 // omitted for brevity, see the source of this page
        });

        function formatRepo2 (repo) {
          if (repo.loading) {
            return repo.text;
          }

          var markup = "<div>" + repo.text + "</div>";

          return markup;
        }

        function formatRepoSelection2 (repo) {
          return repo.text;
        }
        
    }

    var showErrorMsg = function(form, type, msg) {
        var alert = $('<div class="kt-alert kt-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
            <span></span>\
        </div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        //alert.animateClass('fadeIn animated');
        KTUtil.animateClass(alert[0], 'fadeIn animated');
        alert.find('span').html(msg);
    }

    var initInsert = function () {
        $('#savedraft').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    namaproject    : { required: true },
                    subject        : { required: true },
                    pelanggan      : { required: true },
                    pelanggan      : { required: true },
                    serviceid      : { required: true },
                    reviewer1      : { required: true },
                    approval1      : { required: true },
                    approval2      : { required: true },
                    latarbelakang  : { required: true },
                    aspekstrategis : { required: true },
                    aspekfinansial : { required: true },
                    aspekkompetisi : { required: true },
                    konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/insert/draft",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#modalinsert .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#modalinsert .modal-content');

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#modalinsert').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            $('#forminsertsingle')[0].reset();
                            $('.getCustomer').val(null).trigger('change');
                            $('.getRev').val(null).trigger('change');
                            $('.getAppr1').val(null).trigger('change');
                            $('.getAppr2').val(null).trigger('change');

                            var alert = $('#suksesinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', '<strong>Data Insert Failed!</strong> Change a few things up and try submitting again.');
                            
                            KTApp.unblock('#modalinsert .modal-content');
                            
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }

    var initInsertSubmit = function () {
        $('#savepublish').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    namaproject    : { required: true },
                    subject        : { required: true },
                    pelanggan      : { required: true },
                    pelanggan      : { required: true },
                    serviceid      : { required: true },
                    reviewer1      : { required: true },
                    approval1      : { required: true },
                    approval2      : { required: true },
                    latarbelakang  : { required: true },
                    aspekstrategis : { required: true },
                    aspekfinansial : { required: true },
                    aspekkompetisi : { required: true },
                    konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/insert/publish",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#modalinsert .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#modalinsert .modal-content');                            

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#modalinsert').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            $('#forminsertsingle')[0].reset();
                            $('.getCustomer').val(null).trigger('change');
                            $('.getRev').val(null).trigger('change');
                            $('.getAppr1').val(null).trigger('change');
                            $('.getAppr2').val(null).trigger('change');

                            var alert = $('#suksesinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            showErrorMsg(form, 'danger', '<strong>Data Insert Failed!</strong> Change a few things up and try submitting again.');
                            
                            KTApp.unblock('#modalinsert .modal-content');
                            
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }

    var initUpdate = function () {
        $('#saveupdatedraft').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    ed_namaproject    : { required: true },
                    ed_subject        : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_serviceid      : { required: true },
                    ed_reviewer1      : { required: true },
                    ed_approval1      : { required: true },
                    ed_approval2      : { required: true },
                    ed_latarbelakang  : { required: true },
                    ed_aspekstrategis : { required: true },
                    ed_aspekfinansial : { required: true },
                    ed_aspekkompetisi : { required: true },
                    ed_konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/update/draft",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#update .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            //showErrorMsg(form, 'success', '<strong>Data Insert Success!</strong>');
                            KTApp.unblock('#update .modal-content');
                            
                            $('#update').modal('toggle');

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});
                            
                            $('#tabledata').DataTable().ajax.reload();
                            $('#ed_pelanggan').val(null).trigger('change');
                            $('#ed_reviewer1').val(null).trigger('change');
                            $('#ed_approval1').val(null).trigger('change');
                            $('#ed_approval2').val(null).trigger('change');

                            $('#formupdate')[0].reset();

                            var alert = $('#suksesupdate');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#update .modal-content');

                            showErrorMsg(form, 'danger', '<strong>Data Update Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }

    var initUpdateSubmit = function () {
        $('#saveupdatepublish').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    ed_namaproject    : { required: true },
                    ed_subject        : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_pelanggan      : { required: true },
                    ed_serviceid      : { required: true },
                    ed_reviewer1      : { required: true },
                    ed_approval1      : { required: true },
                    ed_approval2      : { required: true },
                    ed_latarbelakang  : { required: true },
                    ed_aspekstrategis : { required: true },
                    ed_aspekfinansial : { required: true },
                    ed_aspekkompetisi : { required: true },
                    ed_konfigurasiteknis : { required: true },
                }
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/update/publish",
                type: "POST",
                beforeSend: function(){ 
                   KTApp.block('#update .modal-content', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'success',
                        message: 'Please wait...'
                    });
                },
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            //showErrorMsg(form, 'success', '<strong>Data Insert Success!</strong>');
                            KTApp.unblock('#update .modal-content');
                            
                            $('#update').modal('toggle');
                            
                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});

                            $('#tabledata').DataTable().ajax.reload();
                            $('#ed_pelanggan').val(null).trigger('change');
                            $('#ed_reviewer1').val(null).trigger('change');
                            $('#ed_approval1').val(null).trigger('change');
                            $('#ed_approval2').val(null).trigger('change');
                            $('#formupdate')[0].reset();

                            var alert = $('#suksesupdate');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#update .modal-content');

                            showErrorMsg(form, 'danger', '<strong>Data Update Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }

    var initDelete = function () {
        $('#deleteBtn').click(function(e) {
            e.preventDefault();
            var btn     = $(this);
            var form    = $(this).closest('form');           
            var id      = $("#iddel").val();

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>pengajuan/delete",
                type: "POST",
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});
                            
                            $('#delete').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            var alert = $('#suksesdelete');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {

                            $('#delete').modal('toggle');
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            $('#textfailed').html('<strong>Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }
    var initUpload = function () {
        $('#uploadJawaban').click(function(e) {
            e.preventDefault();
            var btn     = $(this);
            var form    = $(this).closest('form');           
            var id      = $("#idreq").val();

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.ajaxSubmit({
                url: "<?PHP echo base_url(); ?>jawaban/uploaddok",
                type: "POST",
                success: function(data) {
                    if(data) {
                        // similate 2s delay
                        setTimeout(function() {
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

//                            $('#jmlPending').load("<?PHP echo base_url(); ?>pengajuan/jmlsbr/pending", function(){});
                            
                            $('#modaluploaddok').modal('toggle');
                            $('#tabledata').DataTable().ajax.reload();
                            var alert = $('#suksesupload');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    } else {
                        // similate 2s delay
                        setTimeout(function() {

                            $('#modaluploaddok').modal('toggle');
                            btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            $('#textfailed').html('<strong>Failed!</strong> Change a few things up and try submitting again.');
                            var alert = $('#gagalinsert');
                            alert.removeClass('kt-hidden').show();
                        }, 2000);
                    }
                }
            });
        });     
    }

    return {
        // public functions
        init: function() {
            initWidgets(); 
            initInsert();
            initInsertSubmit();
            initUpdate();
            initUpdateSubmit();
            initDelete();
            initUpload();
        }
    };
}();

var KTSummernoteDemo={
    init:function(){
        $(".summernote").summernote({height:150})
    }
};

jQuery(document).ready(function() {    
    KTDatatablesSearchOptionsColumnSearch.init();
    KTFormWidgets.init();
    jQuery(document).ready(function(){KTSummernoteDemo.init()});
});

</script>
