<?PHP
$userdata 	= $this->session->userdata('sessSpazzle'); 
$userid 	= $userdata['userid'];
$q 			= "select * from mi.user where userid='$userid'";
$getUser 	= $this->query->getDatabyQ($q);
$dataUser	= array_shift($getUser);

if ($dataUser['picture']=='') {
	$hidden 	= 'kt-hidden';
} else {
	$hidden 	= '';
}
$activepage	= $this->uri->uri_string();
$q 			= "select title_page,icon from menu where url='$activepage'";
$getMenu 	= $this->query->getDatabyQ($q);
$dataMenu	= array_shift($getMenu);
?>
<style>
	.smalltextcard { font-size: 1rem; }
	#labelnotify {
	    position: absolute;
	    right: 9px;
	    top: 5px;
	    background: #e27777;
	    color: #FFF;
	    font-weight: bold;
	    font-size: 7px;
	    border-radius: 100%;
	}
	.kt-header .kt-header__topbar .kt-header__topbar-item .kt-header__topbar-icon svg g [fill] {
	    fill: #969696!important;
	}
	.kt-pulse.kt-pulse--brand .kt-pulse__ring {
	    border-color: rgb(245, 184, 60, .8)!important;
	}
	.kt-header__topbar .kt-header__topbar-item.kt-header__topbar-item--user .kt-header__topbar-user img {
		border-radius:50px !important; 
	}
	.kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img{
		border-radius:50px !important; 		
	}

	.xd{
		background-color:#004874 !important;
	}
/*@media (min-width: 1025px) {
	.kt-header--fixed.kt-subheader--fixed.kt-subheader--enabled .kt-wrapper {
	    padding-top: 59px;
	}
}*/
</style>
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " style="
background: -webkit-linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
background: -o-linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
background: -moz-linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
background: linear-gradient(90deg, #004874 47.3%, #eeeeee 47.3%) !important;
border-bottom:none !important; 
z-index:999 !important;
">

	<!-- begin:: Header Menu -->
	<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
	<div class="kt-header-menu-wrapperd" id="kt_header_menu_wrapper">
		<div id="kt_header_menu xd" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
			<ul class="kt-menu__nav xd">
				<li class="kt-menu__item kt-menu__item--active xd">
					<span class="kt-menu__link-text xd">
						<img src='<?PHP echo base_url();?>images/logo_spazzel.png' width='220em'>
					</span>
				</li>
			</ul>
		</div>
	</div>

	<!-- end:: Header Menu -->

	<!-- begin:: Header Topbar -->
	<div class="kt-header__topbar">
		

		<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" data-toggle="kt-tooltip" title="FAQ" data-placement="bottom">
			<span class="kt-header__topbar-icon">
				<a href='<?PHP echo base_url();?>panel/faq'>
				<!--svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect id="bound" x="0" y="0" width="24" height="24"></rect>
						<path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" id="Combined-Shape" fill="#000000" opacity="0.3"></path>
						<path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" id="Combined-Shape" fill="#000000"></path>
						<rect id="Rectangle-152" fill="#000000" opacity="0.3" x="7" y="10" width="5" height="2" rx="1"></rect>
						<rect id="Rectangle-152-Copy" fill="#000000" opacity="0.3" x="7" y="14" width="9" height="2" rx="1"></rect>
					</g>
				</svg-->
				<strong>FAQ</strong> 
				</a>
			</span>
		</div>
		<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" data-toggle="kt-tooltip" title="Help" data-placement="bottom">
			<span class="kt-header__topbar-icon">
				<a href='<?PHP echo base_url();?>panel/help'>
				<!--svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
				    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <rect id="bound" x="0" y="0" width="24" height="24"/>
				        <circle id="Oval-5" fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
				        <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" id="Combined-Shape" fill="#000000"/>
				    </g>
				</svg-->  
				<strong>Help</strong>
				</a> 
			</span>			
		</div>
		<!--begin: Quick panel toggler -->
		<div class="kt-header__topbar-item kt-header__topbar-item--quick-panel" id="shownotif" data-toggle="kt-tooltip" title="Notifications" data-placement="bottom">
			<span class="kt-header__topbar-icon kt-pulse kt-pulse--brand" id="kt_quick_panel_toggler_btn">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
				    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				        <path d="M17,12 L18.5,12 C19.3284271,12 20,12.6715729 20,13.5 C20,14.3284271 19.3284271,15 18.5,15 L5.5,15 C4.67157288,15 4,14.3284271 4,13.5 C4,12.6715729 4.67157288,12 5.5,12 L7,12 L7.5582739,6.97553494 C7.80974924,4.71225688 9.72279394,3 12,3 C14.2772061,3 16.1902508,4.71225688 16.4417261,6.97553494 L17,12 Z" id="Combined-Shape" fill="#000000"/>
				        <rect id="Rectangle-23" fill="#000000" opacity="0.3" x="10" y="16" width="4" height="4" rx="2"/>
				    </g>
				</svg>
				<div id="labelnotify" class="badge badge-default" style="display: none;"> </div>
				<span class="kt-pulse__ring" id="alertnotif" style="display:none;"></span>
			</span>
		</div>


		<!--end: Quick panel toggler -->


		<!--begin: User Bar -->
		<div class="kt-header__topbar-item kt-header__topbar-item--user">
			<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
				<div class="kt-header__topbar-user">
					<span class="kt-header__topbar-welcome">Hi,</span>
					<?PHP
					$genValue 	= $dataUser['name'];
					$nickname 	= explode(' ',trim($genValue));
					?>
					<span class="kt-header__topbar-username"><?PHP echo $nickname[0]; ?></span>
					<img class="<?PHP echo $hidden; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $dataUser['picture']; ?>" />

					<?PHP if ($dataUser['picture']=='') { ?>
					<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
					<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($dataUser['name'],0,1); ?></span>
					<?PHP } ?>
				</div>
			</div>
			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

				<!--begin: Head -->
				<div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url('<?PHP echo base_url(); ?>images/sidebar.png')">
					<div class="kt-user-card__avatar">
						<img class="<?PHP echo $hidden; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $dataUser['picture']; ?>" />

						<?PHP if ($dataUser['picture']=='') { ?>
						<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
						<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?PHP echo substr($dataUser['name'],0,1); ?></span>
						<?PHP } ?>
					</div>
					<div class="kt-user-card__name">
						<?PHP echo $dataUser['name']; ?><br>
						<span class="smalltextcard"><?PHP echo $dataUser['username']; ?></span>
					</div>
					<div class="kt-user-card__badge">
						
					</div>
				</div>

				<!--end: Head -->

				<!--begin: Navigation -->
				<div class="kt-notification">
					<a href="<?PHP echo base_url(); ?>panel/userprofile" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-calendar-3 kt-font-success"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								My Profile
							</div>
							<div class="kt-notification__item-time">
								Account settings and more
							</div>
						</div>
					</a>
					<!--a href="#" class="kt-notification__item">
						<div class="kt-notification__item-icon">
							<i class="flaticon2-rocket-1 kt-font-danger"></i>
						</div>
						<div class="kt-notification__item-details">
							<div class="kt-notification__item-title kt-font-bold">
								My Activities
							</div>
							<div class="kt-notification__item-time">
								Logs and notifications
							</div>
						</div>
					</a-->
					<div class="kt-notification__custom">
						<a href="<?PHP echo base_url(); ?>panel/logout" class="btn btn-label-danger btn-sm btn-bold">Sign Out</a>
					</div>
				</div>

				<!--end: Navigation -->
			</div>
		</div>

		<!--end: User Bar -->
	</div>

	<!-- end:: Header Topbar -->
</div>