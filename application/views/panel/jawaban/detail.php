<?PHP
$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];
$norequest 		= str_replace('-','/',$id);

$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$level			= $dUser['step'];
$aksesCreate 	= $dUser['actionbtn'];

$getDoc 		= $this->db->query("
				SELECT a.* ,
					(SELECT nama_pelanggan from mi.customer where nipnas=a.nipnas) nama_pelanggan,
					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
					(SELECT name from mi.user where userid=a.created_by) namecreated,
					(SELECT username from mi.user where userid=a.created_by) uncreated,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) poscreated,
					(SELECT picture from mi.user where userid=a.reviewer1) pictreview,
					(SELECT name from mi.user where userid=a.reviewer1) namereview,
					(SELECT username from mi.user where userid=a.reviewer1) unreview,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.reviewer1) posreview,
					(SELECT picture from mi.user where userid=a.approval1) pictappr1,
					(SELECT name from mi.user where userid=a.approval1) nameappr1,
					(SELECT username from mi.user where userid=a.approval1) unappr1,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval1) posappr1,
					(SELECT picture from mi.user where userid=a.approval2) pictappr2,
					(SELECT name from mi.user where userid=a.approval2) nameappr2,
					(SELECT username from mi.user where userid=a.approval2) unappr2,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval2) posappr2,
					b.no_answer,
					b.analisa_teknis
				FROM mi.sbrdoc a 
				left join mi.sbranswer b on a.no_request = b.no_request where b.no_answer='$norequest'
				")->result_array();
$doc 			= array_shift($getDoc);
$no_answer 		= $doc['no_answer'];
if ($level==1) {
	if ($doc['status']!=0 and $doc['status']!=2) {
		$hideact = 'kt-hidden';
	} else {
		$hideact = '';
	}
} else if ($level!=1) {
	if ($doc['current']==$level) {
		$hideact = '';
	} else {
		$hideact = 'kt-hidden';
	}
} else {
	$hideact = 'kt-hidden';
}

if ($doc['pictcreated']=='') {
	$hidden 	= 'kt-hidden';
} else {
	$hidden 	= '';
}

if ($doc['status']==1) {
	$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting approval</button>';
} else if ($doc['status']==2) {
	$status = '<button class="btn btn-sm btn-label-warning btn-pill">Return</button>';
} else if ($doc['status']==3) {
	$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting approval</button>';
} else if ($doc['status']==4) {
	$status = '<button class="btn btn-sm btn-label-success btn-pill">Approved</button>';
} else if ($doc['status']==5) {
	$status = '<button class="btn btn-sm btn-label-danger btn-pill">Rejected</button>';
} else {
	$status = '<button class="btn btn-sm btn-secondary btn-pill">Draft</button>';
}

$ReadNotif 		= $this->db->query("UPDATE sbrhistory set is_read='1' where no_request='$norequest' and send_to='$userid'");
?>
<style>
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.mt15rem { margin-top: 1.5rem; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	#history .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#history .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<!--begin::Portlet-->
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#detail" role="tab">
								<i class="flaticon flaticon-file-2"></i> <b><?PHP echo $norequest; ?></b>
							</a>
						</li>
						<li class="nav-item" style='display:none;'>
							<a class="nav-link" data-toggle="tab" href="#history" role="tab">
								<i class="flaticon flaticon-list-3"></i> History Document
							</a>
						</li>
					</ul>
				</h3>
			</div>
			<div id="bgkonten" class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-toolbar-wrapper">
					<?PHP echo $status; ?>
					<div class="btn-group <?PHP echo $hideact; ?>" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-secondary btn-pill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" style="">
                        	<?PHP if ($doc['status']==0) { ?>
                        	<?PHP echo getBtnAction($aksesCreate,'modalaction','Submit','submit','btn-sm text-left kt-link kt-link--primary col-sm-12 btnActDoc','la la-rocket','Submit');?>
                        	<?PHP } ?>

                        	<?PHP echo getBtnAction($aksesCreate,'modalaction','Return','return','btn-sm text-left kt-link kt-link--warning col-sm-12 btnActDoc','la la-check-circle','Return');?>
                        	<?PHP echo getBtnAction($aksesCreate,'modalaction','Escalation','eskalasi','btn-sm text-left kt-link kt-link--brand col-sm-12 btnActDoc','la la-rocket','Escalation');?>
                            <?PHP echo getRoleBtnAction($aksesCreate,'modalapprove','Approve','approve','btn-sm text-left kt-link kt-link--success col-sm-12','la la-check-circle');?>
                            <?PHP echo getBtnAction($aksesCreate,'modalaction','Reject','reject','btn-sm text-left kt-link kt-link--danger col-sm-12 btnActDoc','la la-times-circle','Reject');?>
                        </div>
                    </div>
				</div>
			</div>
		</div>

		

		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="detail" role="tabpanel">
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">No Request</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['no_request']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Nama Project</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['nama_project']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Subject</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['subject']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Pelanggan</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['nipnas']; ?> - <?PHP echo $doc['nama_pelanggan']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Service ID</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['service_id']; ?></b></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<div class="form-group row">
						<?PHP 
						?>
						<table class='table table-bordered'>
							<thead>
								<tr>
									<th rowspan='2' class="align-middle"><center>NO</center></th>
									<th rowspan='2' class="align-middle">LAYANAN</th>
									<th rowspan='2' class="align-middle">VOLUME</th>
									<th colspan='2' class="align-middle"><center>PEMAKAIAN BULANAN</center></th>
								</tr>
								<tr>
									<th  class="align-middle">TARIF</th>
									<th  class="align-middle">PRICING</th>
								</tr>
							</thead>
							<tbody>
								<?PHP 
								$getAnsDet = $this->db->query("SELECT * FROM mi.sbranswer_detail WHERE no_answer ='".$no_answer."'")->result_array();
								$x=0;
								foreach($getAnsDet as $dataAD){
									$x++;
								?>
									<tr>
										<td><center><?PHP echo $x;?></center></td>
										<td><?PHP echo $dataAD['layanan'];?></td>
										<td><?PHP echo $dataAD['volume'];?></td>
										<td><?PHP echo $dataAD['tarif'];?></td>
										<td><?PHP echo $dataAD['price'];?></td>
									</tr>
								<?PHP 
								}
								?>
							</tbody>
						</table>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Analisa Teknis</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['analisa_teknis']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>


					<div class="row">
						<div class="col-md-3">
							<label class="col-form-label col-lg-12 col-sm-12">Diusulkan Oleh:</label>
							<label class="col-form-label col-lg-12 col-sm-12">
								<b><?PHP echo $doc['namecreated']; ?><br>
								<?PHP echo $doc['uncreated']; ?></b><br>
								(<?PHP echo $doc['poscreated']; ?>)
							</label>
						</div>
						<div class="col-md-3">
							<label class="col-form-label col-lg-12 col-sm-12">Disetujui Oleh:</label>
							<label class="col-form-label col-lg-12 col-sm-12">
								<b><?PHP echo $doc['namereview']; ?><br>
								<?PHP echo $doc['unreview']; ?></b><br>
								(<?PHP echo $doc['posreview']; ?>)
							</label>
						</div>
						<div class="col-md-3">
							<label class="col-form-label col-lg-12 col-sm-12">Disetujui Oleh:</label>
							<label class="col-form-label col-lg-12 col-sm-12">
								<b><?PHP echo $doc['nameappr1']; ?><br>
								<?PHP echo $doc['unappr1']; ?></b><br>
								(<?PHP echo $doc['posappr1']; ?>)
							</label>
						</div>
						<div class="col-md-3">
							<label class="col-form-label col-lg-12 col-sm-12">Disetujui Oleh:</label>
							<label class="col-form-label col-lg-12 col-sm-12">
								<b><?PHP echo $doc['nameappr2']; ?><br>
								<?PHP echo $doc['unappr2']; ?></b><br>
								(<?PHP echo $doc['posappr2']; ?>)
							</label>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="history" role="tabpanel">
					<div class="kt-scroll" data-scroll="true" data-height="380" data-mobile-height="300">
	                    <!--Begin::Timeline 3 -->
	                    <div class="kt-timeline-v2">
	                        <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	                        	<div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $doc['pictcreated']; ?>" />

											<?PHP if ($doc['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($doc['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless kt-font-brand"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    Pembuatan Pengajuan SBR <br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-user"></i>  <?PHP echo $doc['namecreated']; ?><br>
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($doc['created_at']); ?>
		                                </div>
	                                </div>
	                            </div>
	                        	<?PHP
	                        	$getHistory = $this->db->query("
	                        				SELECT a.*,
	                        					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
												(SELECT name from mi.user where userid=a.created_by) namecreated
	                        				FROM sbrhistory a where no_request='$norequest'
	                        				order by 1
	                        				")->result_array();
	                        	foreach($getHistory as $history) {
	                        		if ($history['action']=='reject') {
	                        			$colortl 	= 'kt-font-danger';
	                        		} else if ($history['action']=='return') {
	                        			$colortl 	= 'kt-font-warning';
	                        		} else if ($history['action']=='approve') {
	                        			$colortl 	= 'kt-font-success';
	                        		} else {
	                        			$colortl 	= 'kt-font-brand';
	                        		}

	                        		if ($history['pictcreated']=='') {
										$hidden2 	= 'kt-hidden';
									} else {
										$hidden2 	= '';
									}

									if ($history['action']=='new') {
										$notiftext 	= 'Publish Pengajuan SBR';
									} else if ($history['action']=='republish') {
										$notiftext 	= 'Perbaikan Pengajuan';
									} else if ($history['action']=='eskalasi') {
										$notiftext 	= 'Eskalasi Pengajuan';
									} else if ($history['action']=='reject') {
										$notiftext 	= 'Pengajuan tidak disetujui';
									} else if ($history['action']=='approve') {
										$notiftext 	= 'Pengajuan telah disetujui';
									} else if ($history['action']=='return') {
										$notiftext 	= 'Pengajuan telah dikembalikan';
									}

									$genValue 	= $history['namecreated'];
									$nickname 	= explode(' ',trim($genValue));
	                        	?>
	                            <div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden2; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $history['pictcreated']; ?>" />

											<?PHP if ($history['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($history['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless <?PHP echo $colortl; ?>"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    <?PHP echo $notiftext; ?><br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-user"></i>  <?PHP echo $history['namecreated']; ?><br>
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($history['created_at']); ?>
		                                </div>
	                                </div>
	                            </div>
	                            <?PHP } ?>
	                        </div>
	                    </div>
	                    <!--End::Timeline 3 -->
	                </div>
				</div>
			</div>
		</div>
	</div>

</div>