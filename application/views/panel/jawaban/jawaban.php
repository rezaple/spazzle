<?PHP
$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];

$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$level			= $dUser['step'];

if ($level==2) {
	$cond 		= "and reviewer1='$userid' and current>=$level";
} else if ($level==3) {
	$cond 		= "and approval1='$userid' and current>=$level";
} else if ($level==4) {
	$cond 		= "and approval2='$userid' and current>=$level";
} else if ($level==0) {
	$cond 		= "";
} else {
	$cond 		= "and created_by='$userid' and current>=$level";
}

$getPending 	= $this->db->query("SELECT * from sbrdoc where status not in (4,5) $cond")->num_rows();
$getApproved 	= $this->db->query("SELECT * from sbrdoc where status in (4) $cond")->num_rows();
$getRejected 	= $this->db->query("SELECT * from sbrdoc where status in (5) $cond")->num_rows();

$qGetLabRev 	= $this->db->query("SELECT name from mi.level_user where level='2'")->result_array();
$qGetLabApp1 	= $this->db->query("SELECT name from mi.level_user where level='3'")->result_array();
$qGetLabApp2 	= $this->db->query("SELECT name from mi.level_user where level='4'")->result_array();

$getLabRev 		= array_shift($qGetLabRev);
$getLabApp1 	= array_shift($qGetLabApp1);
$getLabApp2 	= array_shift($qGetLabApp2);

$reviewer1 		= $getLabRev['name'];
$approval1 		= $getLabApp1['name'];
$approval2 		= $getLabApp2['name'];

$aksesCreate 	= $dUser['actionbtn'];
?>
<style>
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.mt15rem { margin-top: 1.5rem; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesupload" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been Upload!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<div class="row" style='display:none;'>
		<div class="col-md-4 col-sm-12">
			<div class="kt-portlet kt-portlet--solid-warning kt-portlet--height-fluid kt-portlet--bordered">
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-sm-12 text-white mt15rem">SBR Pending</div>
						<div class="col-sm-12">
							<h4 style="margin-top: 0.5rem;">
								<a class="text-white" id="jmlPending"><?PHP echo $getPending; ?> <span>Record</span></a>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12">
			<div class="kt-portlet kt-portlet--solid-success kt-portlet--height-fluid kt-portlet--bordered">
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-sm-12 text-white mt15rem">SBR Approved</div>
						<div class="col-sm-12">
							<h4 style="margin-top: 0.5rem;">
								<a class="text-white" id="jmlApproved"><?PHP echo $getApproved; ?> <span>Record</span></a>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12">
			<div class="kt-portlet kt-portlet--solid-danger kt-portlet--height-fluid kt-portlet--bordered">
				<div class="kt-portlet__body">
					<div class="row">
						<div class="col-sm-12 text-white mt15rem">SBR Rejected</div>
						<div class="col-sm-12">
							<h4 style="margin-top: 0.5rem;">
								<a class="text-white" id="jmlRejected"><?PHP echo $getRejected; ?> <span>Record</span></a>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--begin::Portlet-->
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-danger" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#draft" role="tab"><i class="flaticon flaticon-file-2"></i> Data Jawaban</a>
						</li>
					</ul>
				</h3>
			</div>
			<div id="bgkonten" class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-toolbar-wrapper">
					<?PHP //echo getRoleBtnAction($aksesCreate,'modalinsert','Buat Pengajuan','submit','btn-info btn-sm','flaticon flaticon-doc');?>
				</div>
			</div>
		</div>

		<!-- MODAL INSERT -->
		<div class="modal fade" id="modalinsert" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">

					<form class="kt-form kt-form--label-right" id="forminsertsingle" enctype="multipart/form-data">
						<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Form Pengajuan</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class='row'>
									<div class="col-lg-5">
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Nama Project *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="namaproject" class="form-control" id="namaproject" placeholder="Nama Project">
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Subject *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="subject" class="form-control" id="subject" placeholder="Subject">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5"> 
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Pelanggan *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<select name="pelanggan" class="form-control getCustomer" id="pelanggan" data-placeholder="Pelanggan" style="width: 100%;">
														<option value=""></option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Service ID *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="serviceid" class="form-control" id="serviceid" placeholder="Service ID">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								
								<div class="row">
									<div class="col-lg-4">
										<label class="col-form-label col-sm-12">Disetujui Oleh *<br>(<?PHP echo $reviewer1; ?>)</label>
										<div class="col-sm-12">
											<div class='input-group'>
												<select name="reviewer1" class="form-control getRev" id="reviewer1" data-placeholder="Pilih..." style="width: 100%;">
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<label class="col-form-label col-sm-12">Disetujui Oleh *<br>(<?PHP echo $approval1; ?>)</label>
										<div class="col-sm-12">
											<div class='input-group'>
												<select name="approval1" class="form-control getAppr1" id="approval1" data-placeholder="Pilih..." style="width: 100%;">
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<label class="col-form-label col-sm-12">Disetujui Oleh *<br>(<?PHP echo $approval2; ?>)</label>
										<div class="col-sm-12">
											<div class='input-group'>
												<select name="approval2" class="form-control getAppr2" id="approval2" data-placeholder="Pilih..." style="width: 100%;">
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Latar Belakang *</label>
											<div class="col-sm-12">
												<textarea name="latarbelakang" class="summernote" id="latarbelakang"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Aspek Strategis *</label>
											<div class="col-sm-12">
												<textarea name="aspekstrategis" class="summernote" id="aspekstrategis"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Aspek Finansial *</label>
											<div class="col-sm-12">
												<textarea name="aspekfinansial" class="summernote" id="aspekfinansial"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Aspek Kompetisi *</label>
											<div class="col-sm-12">
												<textarea name="aspekkompetisi" class="summernote" id="aspekkompetisi"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Konfigurasi Teknis *</label>
											<div class="col-sm-12">
												<textarea name="konfigurasiteknis" class="summernote" id="konfigurasiteknis"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" id="savedraft" class="btn btn-primary"><i class="la la-save"></i> Save as Draft</button>
							<button type="submit" id="savepublish" class="btn btn-success"><i class="la la-rocket"></i> Save and Publish</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- END MODAL INSERT -->

		<!-- MODAL UPDATE -->
		<div class="modal fade" id="update" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">

					<form class="kt-form kt-form--label-right" id="forminsertsingle" enctype="multipart/form-data">
						<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Update Data : <b id="updatename"></b></h3>
								</div>
								<div class="kt-portlet__head-toolbar">
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class='row'>
									<div class="col-lg-5">
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Nama Project *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" name="ed_id" id="ed_id">
													<input type="hidden" name="ed_norequest" id="ed_norequest">
													<input type="hidden" name="statusbefore" id="statusbefore">
													<input type="text" name="ed_namaproject" class="form-control" id="ed_namaproject" placeholder="Nama Project">
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Subject *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="ed_subject" class="form-control" id="ed_subject" placeholder="Subject">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5"> 
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Pelanggan *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<select name="ed_pelanggan" class="form-control getCustomer" id="ed_pelanggan" data-placeholder="Pelanggan" style="width: 100%;">
														<option value=""></option>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Service ID *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="ed_serviceid" class="form-control" id="ed_serviceid" placeholder="Service ID">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								
								<div class="row">
									<div class="col-lg-4">
										<label class="col-form-label col-sm-12">Disetujui Oleh *<br>(<?PHP echo $reviewer1; ?>)</label>
										<div class="col-sm-12">
											<div class='input-group'>
												<select name="ed_reviewer1" class="form-control getRev" id="ed_reviewer1" data-placeholder="Pilih..." style="width: 100%;">
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<label class="col-form-label col-sm-12">Disetujui Oleh *<br>(<?PHP echo $approval1; ?>)</label>
										<div class="col-sm-12">
											<div class='input-group'>
												<select name="ed_approval1" class="form-control getAppr1" id="ed_approval1" data-placeholder="Pilih..." style="width: 100%;">
												</select>
											</div>
										</div>
									</div>
									<div class="col-lg-4">
										<label class="col-form-label col-sm-12">Disetujui Oleh *<br>(<?PHP echo $approval2; ?>)</label>
										<div class="col-sm-12">
											<div class='input-group'>
												<select name="ed_approval2" class="form-control getAppr2" id="ed_approval2" data-placeholder="Pilih..." style="width: 100%;">
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Latar Belakang *</label>
											<div class="col-sm-12">
												<textarea name="ed_latarbelakang" class="summernote" id="ed_latarbelakang"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Aspek Strategis *</label>
											<div class="col-sm-12">
												<textarea name="ed_aspekstrategis" class="summernote" id="ed_aspekstrategis"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Aspek Finansial *</label>
											<div class="col-sm-12">
												<textarea name="ed_aspekfinansial" class="summernote" id="ed_aspekfinansial"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Aspek Kompetisi *</label>
											<div class="col-sm-12">
												<textarea name="ed_aspekkompetisi" class="summernote" id="ed_aspekkompetisi"></textarea>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Konfigurasi Teknis *</label>
											<div class="col-sm-12">
												<textarea name="ed_konfigurasiteknis" class="summernote" id="ed_konfigurasiteknis"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" id="saveupdatedraft" class="btn btn-primary"><i class="la la-save"></i> Update as Draft</button>
							<button type="submit" id="saveupdatepublish" class="btn btn-success"><i class="la la-rocket"></i> Update and Publish</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- END MODAL UPDATE -->

		<!-- MODAL DELETE -->
		<div class="modal fade" id="modalsubmit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
						<div class="swal2-header">
							<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
							<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure want to submit data?</h2>
						</div>
						<div class="swal2-content">
							<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
						</div>
						<div class="swal2-actions" style="display: flex;">
							<form method="POST">
							<input type="hidden" name="idsubmit" id="idsubmit" value="<?PHP echo $userid; ?>">
							<center>
							<button type="button" id="submitBtn" class="swal2-styled btn btn-success" aria-label="">
								Yes, submit it!
							</button>
							<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
							</center>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END MODAL DELETE -->
		<!-- MODAL UPLOAD DOK -->
		<div class="modal fade" id="modaluploaddok" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">

					<form class="kt-form kt-form--label-right" id="formupload" enctype="multipart/form-data">
						<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Upload Assign Dok Jawaban : <b id="uploadname"></b></h3>
								</div>
								<div class="kt-portlet__head-toolbar">
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class='row'>
									<div class="col-lg-8" id="dokexist">
									</div>
									<div class="col-lg-8">
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Dok Jawaban *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'> 
													<input type="hidden" name="idreq" class="form-control" id="idreq" value="">
													<input type="file" name="dokjawaban" class="form-control" id="dokjawaban" placeholder="FILE">
												</div>
											</div>
										</div> 
									</div> 
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" id="uploadJawaban" class="btn btn-success"><i class="la la-rocket"></i> Update Dok Jawaban</button>
						</div>
					</form>

				</div>
			</div>
		</div> 
		<!-- END MODAL UPLOAD DOK -->

		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="draft" role="tabpanel">
					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="tabledata">
						<thead>
							<tr>
								<th>No Answer</th>
								<th>No Request</th>
								<th>Analisa Teknis</th>
								<th>Created At</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>No Answer</th>
								<th>No Request</th>
								<th>Analisa Teknis</th>
								<th>Created At</th>
								<th>Actions</th>
							</tr>
						</tfoot>
					</table>
					<!--end: Datatable -->
				</div>
			</div>

		</div>

		<!-- MODAL DELETE -->
		<div class="modal fade" id="delete" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
						<div class="swal2-header">
							<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
							<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure?</h2>
						</div>
						<div class="swal2-content">
							<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
						</div>
						<div class="swal2-actions" style="display: flex;">
							<form method="POST">
							<input type="hidden" name="iddel" id="iddel" value="">
							<center>
							<button type="button" id="deleteBtn" class="swal2-styled btn btn-danger" aria-label="">
								Yes, delete it!
							</button>
							<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
							</center>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END MODAL DELETE -->
	</div>
</div>