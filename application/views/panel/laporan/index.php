<style>
.nav-tabs.nav-tabs-line .nav-link {
    padding: 12px 10px;
}
</style>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div class="kt-portlet kt-portlet--tabs">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-2x nav-tabs-line-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#laporansbr" role="tab" data-id="laporansbr">
							<strong>Laporan SBR Online</strong>
						</a>
					</li> 
				</ul>
			</div> 
		</div>

		<div id="contkontak" class="kt-portlet__body">

			<div class="tab-content">
				<div class="tab-pane active" id="laporansbr" role="tabpanel">
					<div class="row">
						<div class="col-lg-12 col-sm-12">
							<div class='row'>
							<div class="col-lg-6">
								<div class="form-group row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class='input-group'>
											<div class='input-group' id='kt_daterangepicker_2'>
												<input type='text' class="form-control" name='daterange'  id='daterange' readonly placeholder="Select date range" />
												<div class="input-group-append">
													<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> 
							<div class="col-lg-6">
								<button type="button" id="filterdata" class="btn btn-label-dark btn-pill">Cari</button>
							</div>
							</div>
							<div class="col-sm-12 kt-separator kt-separator--dashed"></div>
						</div> 
					</div> 
					<div class="kt-portlet__body">
							<div class="tab-content">
								<div class="tab-pane active" id="draft" role="tabpanel">
									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="tabledata" style='font-size:12px;'>
										<thead>
											<tr>
												<th>No Request</th>
												<th>Nama AM</th>
												<th>Nama Pelanggan</th>
												<th>NIPNAS</th>
												<th>Service ID</th>
												<th>Nama Project</th>
												<th>Subject</th>
												<th>Created At</th>
												<th>Status</th>
												<th>Original Dok.Pengajuan</th>
												<th>Original Dok.Jawaban</th>
												<th>Assign Dok.Pengajuan</th>
												<th>Assign Dok.Jawaban</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>No Request</th>
												<th>Nama AM</th>
												<th>Nama Pelanggan</th>
												<th>NIPNAS</th>
												<th>Service ID</th>
												<th>Nama Project</th>
												<th>Subject</th>
												<th>Created At</th>
												<th>Status</th>
												<th>Original Dok.Pengajuan</th>
												<th>Original Dok.Jawaban</th>
												<th>Assign Dok.Pengajuan</th>
												<th>Assign Dok.Jawaban</th>
											</tr>
										</tfoot>
									</table>
									<!--end: Datatable -->
								</div>
							</div>

						</div> 
				</div>
			</div>
		</div>

	<!-- MODAL INSERT -->
	<div class="modal fade" id="addnewfac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<!--div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div-->

				<form class="kt-form kt-form--label-right" id="forminsert" enctype="multipart/form-data">
					<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">Add New Produk</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Gambar</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="file" name="file" class="form-control" id="file" placeholder="Gambar">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nama *</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="text" name="nama" class="form-control" id="nama" placeholder="Nama">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Kode/SKU</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="text" name="kode" class="form-control" id="kode" placeholder="Kode/SKU">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Unit</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class='input-group'>
												<select class="form-control m-select2 select2grup" id="unit" name="unit" style="width: 100%;">
													<option value="1">Buah</option>
												</select>
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Deskripsi</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="text" name="deskripsi" class="form-control" id="deskripsi" placeholder="Deskripsi">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Jenis Barang</label>
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class='input-group'>
												<select class="form-control m-select2 select2grup" id="jenis" name="jenis" style="width: 100%;">
													<option value="single">Single</option>
													<option value="bundle">Bundle</option>
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6 col-sm-12">
									<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-2x" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#single" role="tab" data-id="">
												Harga & Pengaturan
											</a>
										</li>
										
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#bundle" role="tab">
												Produk Bundle
											</a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="single" role="tabpanel">

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" id="saveinsert" class="btn btn-primary">Save</button>
					</div>
				</form>

			</div>
		</div>
	</div>
	<!-- END MODAL INSERT -->

	<!-- MODAL UPDATE -->
	<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<!--div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Update Data : <b id="nameroles"></b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div-->

				<form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data">
					<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">Update Data : <b id="nameroles"></b></h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<ul class="nav nav-tabs nav-tabs-line nav-tabs-line-right" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#ed_kt_portlet_base_demo_1_tab_content" role="tab">
											<i class="flaticon-user"></i> User Data
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#ed_kt_portlet_base_demo_2_tab_content" role="tab">
											<i class="flaticon-interface-1"></i> Roles
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="tab-content">
								<div class="tab-pane active" id="ed_kt_portlet_base_demo_1_tab_content" role="tabpanel">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Name *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="hidden" name="ed_iduser" class="form-control" id="ed_iduser" placeholder="Name">
												<input type="text" name="ed_name" class="form-control" id="ed_name" placeholder="Name">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Photo *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="file" name="upl" class="form-control" id="upl" placeholder="Photo">
											</div>
											<span class="form-text text-muted">Untuk tampilan lebih maksimal, gambar disarankan dengan bentuk kotak (100px x 100px)</span>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Username *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="text" name="ed_user" class="form-control" id="ed_user" placeholder="User">
											</div>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Password *</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="password" name="ed_pass" class="form-control" id="ed_pass" placeholder="Password">
											</div>
											<span class="form-text text-muted">Kosongkan jika tidak akan merubah password</span>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
												<input type="email" name="ed_email" class="form-control" id="ed_email" placeholder="E-mail">
											</div>
										</div>
									</div>
								</div>

								<div class="tab-pane" id="ed_kt_portlet_base_demo_2_tab_content" role="tabpanel">
									<div class="form-group row">
										<label class="col-form-label col-lg-3 col-sm-12">Role</label>
										<div class="col-lg-4 col-md-9 col-sm-12">
											<div class='input-group'>
												<select name="ed_role" class="form-control" id="ed_role" placeholder="Role">
													<option value="">-- Pilih Role --</option>
													<?PHP foreach ($getDataRole as $data) { ?>
													<option value="<?PHP echo $data['id_role']; ?>"><?PHP echo $data['nama_role']; ?></option>
													<?PHP } ?>
												</select>
											</div>
										</div>
									</div>
									<div id='edcontentrole'></div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" id="saveupdate" class="btn btn-primary">Save</button>
					</div>
				</form>

			</div>
		</div>
	</div>
	<!-- END MODAL UPDATE -->

	<!-- MODAL DELETE -->
	<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
					<div class="swal2-header">
						<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
						<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure?</h2>
					</div>
					<div class="swal2-content">
						<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
					</div>
					<div class="swal2-actions" style="display: flex;">
						<form method="POST">
						<input type="hidden" name="iddel" id="iddel" value="">
						<center>
						<button type="button" id="deleteBtn" class="swal2-confirm swal2-styled" aria-label="" style="border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">
							Yes, delete it!
						</button>
						<button type="button" class="swal2-cancel swal2-styled" data-dismiss="modal">Cancel</button>
						</center>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MODAL DELETE -->
</div>

<!-- end:: Content -->