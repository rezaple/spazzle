<?PHP 
$allowedit =  $validation['valcrud'];
if($allowedit > 0){
?>
<style>
.btnaddnewdata {
	display:none;
} 
</style>
<?PHP	
}	
?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
				</span>
				<h3 class="kt-portlet__head-title">
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">
					<div class="kt-portlet__head-actions">
						<!--a href="#" class="btn btn-default btn-icon-sm">
							<i class="la la-download"></i> Export
						</a-->
						&nbsp;
						<?PHP echo getRoleInsert($akses,'addnewfac','Add New Workflow');?>
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL INSERT -->
		<div class="modal fade" id="addnewfac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Add New Level Workflow</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>

					<form class="kt-form kt-form--label-left" id="forminsert" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="kt-form__content">
								<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
									<div class="kt-alert__icon">
										<i class="la la-warning"></i>
									</div>
									<div class="kt-alert__text">
										Oh snap! Change a few things up and try submitting again.
									</div>
									<div class="kt-alert__close">
										<button type="button" class="close" data-close="alert" aria-label="Close">
										</button>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Level Name *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<div class='input-group'>
										<input type="text" name="level_name" class="form-control" placeholder="Level Name" id="level_name" value="">
									</div>
									<span class="form-text text-muted">Type a Level Name</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Level *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<div class='input-group'>
										<input type="number" min='2' name="level" class="form-control" placeholder="Level" id="level" value="">
									</div>
									<span class="form-text text-muted">Type a Level</span>
								</div>
							</div> 
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Select Flow *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<div class='input-group'>
										<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
											<input value="1" id="checkbox" type="checkbox" name="is_select">
											<span></span>
										</label>
									</div> 
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Action *</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
									<input value="approve" id="checkbox" type="checkbox" name="action[]"> Approve
									<span></span>
									</label> &nbsp;&nbsp;
									<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
									<input value="eskalasi" id="checkbox" type="checkbox" name="action[]"> Eskalasi
									<span></span>
									</label> &nbsp;&nbsp;
									<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
									<input value="reject" id="checkbox" type="checkbox" name="action[]"> Reject
									<span></span>
									</label> &nbsp;&nbsp;
									<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
									<input value="return" id="checkbox" type="checkbox" name="action[]"> Return
									<span></span>
									</label> &nbsp;&nbsp;
									<label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
									<input value="update" id="checkbox" type="checkbox" name="action[]"> Update
									<span></span>
									</label> &nbsp;&nbsp;	
								</div>
							</div> 
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary" id="saveinsert">Save</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- END MODAL INSERT -->

		<div class="kt-portlet__body">

			<!--begin: Datatable -->
			<table class="table table-striped- table-bordered table-hover table-checkable" id="tabledata">
				<thead>
					<tr>
						<th>LEVEL NAME</th>
						<th>LEVEL</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>LEVEL NAME</th>
						<th>LEVEL</th>
						<th>ACTIONS</th>
					</tr>
				</tfoot>
			</table>
			<!--end: Datatable -->

			<!-- MODAL UPDATE -->
			<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Update Data : <b id="nameworkflow"></b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>

						<form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="kt-form__content">
									<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
										<div class="kt-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="kt-alert__text">
											Oh snap! Change a few things up and try submitting again.
										</div>
										<div class="kt-alert__close">
											<button type="button" class="close" data-close="alert" aria-label="Close">
											</button>
										</div>
									</div>
								</div>

								<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Level Name *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<div class='input-group'>
										<input type="hidden" name="ed_id_level" id="ed_id_level" value="">
										<input type="text" name="ed_level_name" class="form-control" placeholder="Level Name" id="ed_level_name" value="">
									</div>
									<span class="form-text text-muted">Type a Level Name</span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Level *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<div class='input-group'>
										<input type="number" min='2' name="ed_level" class="form-control" placeholder="Level" id="ed_level" value="">
									</div>
									<span class="form-text text-muted">Type a Level</span>
								</div>
							</div> 
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Select Flow *</label>
								<div class="col-lg-4 col-md-9 col-sm-12">
									<div class='input-group'>
										<div id='exmod1'></div>
									</div> 
								</div>
							</div> 
							<div class="form-group row">
								<label class="col-form-label col-lg-3 col-sm-12">Action *</label>
								<div class="col-lg-9 col-md-9 col-sm-12">
									<div id="exmod2"></div>
								</div>
							</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" id="saveupdate" class="btn btn-primary">Update</button>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- END MODAL UPDATE -->

			<!-- MODAL DELETE -->
			<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
							<div class="swal2-header">
								<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
								<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure?</h2>
							</div>
							<div class="swal2-content">
								<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
							</div>
							<div class="swal2-actions" style="display: flex;">
								<form method="POST">
								<input type="hidden" name="iddelroles" id="iddelroles" value="">
								<center>
								<button type="button" id="deleteRoles" class="swal2-confirm swal2-styled" aria-label="" style="border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">
									Yes, delete it!
								</button>
								<button type="button" class="swal2-cancel swal2-styled" data-dismiss="modal">Cancel</button>
								</center>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MODAL DELETE -->
		</div>
	</div>
</div>

<!-- end:: Content -->