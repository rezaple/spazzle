<?PHP
$data		= array_shift($getParam);
$nameparam 	= $data['name'];
if($nameparam = 'mail_notif'){
	$mail_notif = $data['value'];
}
?>
<style>
.mr1rem { margin-left: -1rem; }
.sizemidle {
	max-width: auto!important;
    max-height: 140px!important;
}
.kt-invoice-1 .kt-invoice__wrapper .kt-invoice__head .kt-invoice__container .kt-invoice__logo {
	padding-top: 5rem!important;
}
#nama_lengkap {
	color:white !important;
}
</style>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<div class="kt-portlet">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
					<i class="flaticon-cogwheel-2"></i>
				</span>
				<h3 class="kt-portlet__head-title">
					Configuration System
				</h3>
			</div>
		</div>
		<form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data">
		<div class="kt-portlet__body">
			<div class="form-group row">
				<label class="col-form-label col-lg-3 col-sm-12">Mail Notification *</label>
				<div class="col-lg-7 col-md-9 col-sm-12">
					<div class='input-group'>
						<input type="text" name="mail_notif" class="form-control" id="mail_notif" placeholder="Mail Notification" value="<?PHP echo $mail_notif; ?>">
						<span class="form-text text-muted">Catatan : <br>Jika dalam <span id='mn'><?PHP echo $mail_notif; ?></span> Hari tidak ada aksi dari Manager Solution / AVP / IMA maka akan dikirim notifikasi melalui Email per Hari nya.</span>
					</div>
				</div>
			</div>
		</div>
		<div class="kt-portlet__foot">
			<div class="row align-items-center">
				<div class="col-lg-6 m--valign-middle">
				</div>
				<div class="col-lg-6 kt-align-right"> 
					<button type="button" id='saveupdate' class="btn btn-brand">Save Configuration</button> 
				</div>
			</div>
		</div>
		</form>
	</div>
	 
</div>
<!-- end:: Content -->