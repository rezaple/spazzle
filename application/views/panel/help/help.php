<?PHP 
$sop = array_shift($getsop);
$tata = array_shift($gettata);
?>

<style>
#btncreate {
    width: 100% !important;
    border-radius: 20px;
    box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
    background-color: #004874;
    font-size:12px !important;
    margin-bottom:10px;
}
</style>
<script>
function resizeIframe(obj) {
	obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<!--Begin::Dashboard 1--> 
	<!--Begin::Section-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
							<div class="kt-portlet kt-faq-v1">
								<div class="kt-portlet__head">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="flaticon-questions-circular-button"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											HELP
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body">
									<div class="row">
										<div class="col-lg-3">
											<div class="row">
												<div class="col-lg-12">
												<h6>Download Template Dokumen <b>SPASI</b></h6>
												<hr>
												</div>
											</div>
											<?PHP 
											foreach($getsdlc as $sdlc){
												echo '
												<div class="row">
													<div class="col-lg-12">
														<a href="'.base_url().'attachment/helpdoc/'.$sdlc['template_sdlc'].'" download>
														<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
															<i class="flaticon flaticon-download"></i>
															'.$sdlc['nama_sdlc'].'
														</button>
														</a>
													</div>
												</div>
												';
											}
											?>
											<div class="row">
													<div class="col-lg-12">
														<a href="<?PHP echo base_url(); ?>attachment/helpdoc/OWASP Application Security Verification Standard 4.0 v3.7.xlsx" download>
														<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
															<i class="flaticon flaticon-download"></i>
															OWASP Application Security Verification Standard 4.0 v3.7
														</button>
														</a>
													</div>
												</div>
											<!--div class="row">
												<div class="col-lg-12">
													<a href="<?PHP echo base_url();?>attachment/helpdoc/Template BRD.docx" download>
													<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
														<i class="flaticon flaticon-download"></i>
														Download Sample Dok BRD
													</button>
													</a>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<a href="<?PHP echo base_url();?>attachment/helpdoc/Template DD.docx" download>
													<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
														<i class="flaticon flaticon-download"></i>
														Download Sample Dok DD
													</button>
													</a>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<a href="<?PHP echo base_url();?>attachment/helpdoc/Template SMP.docx" download>
													<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
														<i class="flaticon flaticon-download"></i>
														Download Sample Dok SMP
													</button>
													</a>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<a href="<?PHP echo base_url();?>attachment/helpdoc/Template SOP Aplikasi.docx" download>			
													<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
														<i class="flaticon flaticon-download"></i>
														Downlaod Sample Dok SOP Aplikasi
													</button>
													</a>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<a href="<?PHP echo base_url();?>attachment/helpdoc/Template SOP D2P.docx" download>
													<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
														<i class="flaticon flaticon-download"></i>
														Download Sample Dok SOP D2P
													</button>
													</a>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<a href="<?PHP echo base_url();?>attachment/helpdoc/Template Test Plan and Test Report.docx" download>
													<button class="btn btn-default btn-wide btn-md btn-icon-md" id="btncreate" style="font-size:10px;">
														<i class="flaticon flaticon-download"></i>
														Download Sample Dok Tes Plan and Test Report 
													</button>
													</a>
												</div>
											</div-->										
										</div>
										<div class="col-lg-9">
											<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-brand" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#kt_tabs_9_1" role="tab"><i class="flaticon-doc"></i> Sosialisasi Pedoman Tata Kelola Inovasi IT Digitisasi</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#kt_tabs_9_3" role="tab"><i class="flaticon-doc"></i> Standard Operasional Procedure</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#kt_tabs_9_4" role="tab"><i class="flaticon-doc"></i> Kebijakan Perusahaan</a>
												</li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="kt_tabs_9_1" role="tabpanel">
													<iframe src="<?PHP echo base_url();?>attachment/helpdoc/<?PHP echo $tata['config_value'];?>" id='embedsrc' frameborder="0" type="application/pdf" width="100%" height='800px'>
													</iframe>

												</div>
												<div class="tab-pane" id="kt_tabs_9_3" role="tabpanel">
													<iframe src="<?PHP echo base_url();?>attachment/helpdoc/<?PHP echo $sop['config_value'];?>" id='embedsrc_sop' type="application/pdf" width="100%" frameborder="0" height='800px'>
													</iframe>
												</div>
												<div class="tab-pane" id="kt_tabs_9_4" role="tabpanel">
													<div class="accordion" id="accordionExample1">
														<div class="card">
															<div class="card-header" id="headingOne">
																<div class="card-title" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
																	Governance Clinic
																</div>
															</div>
															<div id="collapseOne1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample1">
																 <div class="col-xl-4">

																	<!--begin:: Widgets/Download Files-->
																	<div class="kt-portlet kt-portlet--height-fluid">
																		 
																		<div class="kt-portlet__body">

																			<!--begin::k-widget4-->
																			<div class="kt-widget4">
																				<div class="kt-widget4__item">
																					<div class="kt-widget4__pic kt-widget4__pic--icon">
																						<img src="../assets/media/files/pdf.svg" alt="">
																					</div>
																					<a href="<?PHP echo base_url();?>attachment/helpdoc/Penetapan PII (Personally Identifiable Information) dalam Lingkup Internal Telkom.pdf" target='_blank' class="kt-widget4__title">
																						Penetapan PII (Personally Identifiable Information) dalam Lingkup Internal Telkom.pdf
																					</a>
																					<div class="kt-widget4__tools">
																						<a href="#" class="btn btn-clean btn-icon btn-sm">
																							<i class="flaticon2-download-symbol-of-down-arrow-in-a-rectangle"></i>
																						</a>
																					</div>
																				</div> 
																				<div class="kt-widget4__item">
																					<div class="kt-widget4__pic kt-widget4__pic--icon">
																						<img src="../assets/media/files/pdf.svg" alt="">
																					</div>
																					<a href="<?PHP echo base_url();?>attachment/helpdoc/Standar-Data-Sharing-Telkom-Group.pdf" target='_blank' class="kt-widget4__title">
																						Standar-Data-Sharing-Telkom-Group.pdf
																					</a>
																					<div class="kt-widget4__tools">
																						<a href="#" class="btn btn-clean btn-icon btn-sm">
																							<i class="flaticon2-download-symbol-of-down-arrow-in-a-rectangle"></i>
																						</a>
																					</div>
																				</div> 
																				<div class="kt-widget4__item">
																					<div class="kt-widget4__pic kt-widget4__pic--icon">
																						<img src="../assets/media/files/pdf.svg" alt="">
																					</div>
																					<a href="<?PHP echo base_url();?>attachment/helpdoc/STDDIR 2020 - Standar Verifikasi Keamanan Aplikasi Telkom Group.pdf" target='_blank' class="kt-widget4__title">
																						STDDIR 2020 - Standar Verifikasi Keamanan Aplikasi Telkom Group.pdf
																					</a>
																					<div class="kt-widget4__tools">
																						<a href="#" class="btn btn-clean btn-icon btn-sm">
																							<i class="flaticon2-download-symbol-of-down-arrow-in-a-rectangle"></i>
																						</a>
																					</div>
																				</div>  
																			</div>

																			<!--end::Widget 9-->
																		</div>
																	</div>

																	<!--end:: Widgets/Download Files-->
																</div>
															</div>
														</div>
														<div class="card">
															<div class="card-header" id="headingTwo">
																<div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo1">
																	Technology Clinic
																</div>
															</div>
															<div id="collapseTwo1" class="collapse show" aria-labelledby="headingTwo1" data-parent="#accordionExample1">
																 <div class="col-xl-4">

																	<!--begin:: Widgets/Download Files-->
																	<div class="kt-portlet kt-portlet--height-fluid">
																		 
																		<div class="kt-portlet__body">

																			<!--begin::k-widget4-->
																			<div class="kt-widget4">
																				<div class="kt-widget4__item">
																					<div class="kt-widget4__pic kt-widget4__pic--icon">
																						<img src="../assets/media/files/pdf.svg" alt="">
																					</div>
																					<a href="<?PHP echo base_url();?>attachment/helpdoc/200917-Dok-SK-EGM--Dok-STEPI_final.pdf" target="_blank" class="kt-widget4__title">
																						200917-Dok-SK-EGM--Dok-STEPI_final.pdf
																					</a>
																					<div class="kt-widget4__tools">
																						<a href="#" class="btn btn-clean btn-icon btn-sm">
																							<i class="flaticon2-download-symbol-of-down-arrow-in-a-rectangle"></i>
																						</a>
																					</div>
																				</div>  
																			</div>

																			<!--end::Widget 9-->
																		</div>
																	</div>

																	<!--end:: Widgets/Download Files-->
																</div>
															</div>
														</div>
														 
													</div>
													
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!--end::Portlet-->
		</div>
	</div> 
</div>
