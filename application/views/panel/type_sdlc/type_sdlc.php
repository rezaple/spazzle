<?PHP 
$sop = array_shift($getsop);
$tata = array_shift($gettata);
?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
				</span>
				<h3 class="kt-portlet__head-title">
					Master Dokumen
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-wrapper">

					<div class="kt-portlet__head-actions">
						<!--a href="#" class="btn btn-default btn-icon-sm">
							<i class="la la-download"></i> Export
						</a-->
						<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
							<div class="btn-group" role="group">
								<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Other Dokumen
								</button>
								<div class="dropdown-menu" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style='left:-64px !important;'>
									<a class="dropdown-item" data-toggle="modal" data-target="#doktata">Dokumen Tata Kelola</a>
									<a class="dropdown-item" data-toggle="modal" data-target="#doksop">Dokumen SOP</a>
								</div>
							</div>
							<?PHP echo getRoleInsert($akses,'addnewfac','Add New Dokumen');?>

						</div> 
					</div>
				</div>
			</div>
		</div>

		

		<div class="kt-portlet__body">
			<!--begin: Datatable --> 
			<table class="table table-striped- table-bordered table-hover table-checkable" id="tabledata">
				<thead>
					<tr>
						<th>NO</th>
						<th>KATEGORI</th>
						<th>NAMA DOKUMEN</th>
						<th>MANDATORY</th>
						<th>TEMPLATE</th>
						<th>TYPE</th>
						<th>TYPE UPLOAD FE</th>
						<th>ACTIONS</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>NO</th>
						<th>KATEGORI</th>
						<th>NAMA DOKUMEN</th>
						<th>MANDATORY</th>
						<th>TEMPLATE</th>
						<th>TYPE</th>
						<th>TYPE UPLOAD FE</th>
						<th>ACTIONS</th>
					</tr>
				</tfoot>
			</table>
			<!--end: Datatable -->
			<!-- MODAL SOP -->
			<div class="modal fade" id="doksop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">UPDATE DOKUMEN SOP</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>

						<form class="kt-form kt-form--label-right" id="formupdatesop" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="kt-form__content">
									<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
										<div class="kt-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="kt-alert__text">
											Oh snap! Change a few things up and try submitting again.
										</div>
										<div class="kt-alert__close">
											<button type="button" class="close" data-close="alert" aria-label="Close">
											</button>
										</div>
									</div>
								</div> 
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">File SOP Exist *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<input type='hidden' name='doksop_ex' value='<?PHP echo $sop['config_value'];?>'>
											<a href='<?PHP echo base_url();?>attachment/helpdoc/<?PHP echo $sop['config_value'];?>' target='_blank'><?PHP echo $sop['config_value'];?></a>
										</div>
									</div>
								</div> 
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Dokumen SOP *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
						                    <input type="file" class="form-control" name="docsop" id="docsop" accept="application/pdf"">
										</div>
									</div>
								</div> 
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" id="savesop" class="btn btn-primary">Save</button>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- END MODAL SOP -->
			<!-- MODAL TATA KELOLA -->
			<div class="modal fade" id="doktata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">UPDATE DOKUMEN TATA KELOLA</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>

						<form class="kt-form kt-form--label-right" id="formupdatett" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="kt-form__content">
									<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
										<div class="kt-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="kt-alert__text">
											Oh snap! Change a few things up and try submitting again.
										</div>
										<div class="kt-alert__close">
											<button type="button" class="close" data-close="alert" aria-label="Close">
											</button>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">File Tata Kelolas Exist *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<input type='hidden' name='doktata_ex' value='<?PHP echo $tata['config_value'];?>'>
											<a href='<?PHP echo base_url();?>attachment/helpdoc/<?PHP echo $tata['config_value'];?>' target='_blank'><?PHP echo $tata['config_value'];?></a>
										</div>
									</div>
								</div> 
								 
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Template *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
						                    <input type="file" class="form-control" name="doctata" id="doctata" accept="application/pdf"">
										</div>
									</div>
								</div>
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" id="saveupdatett" class="btn btn-primary">Save</button>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- END MODAL TATA KELOLA -->
			<!-- MODAL INSERT -->
			<div class="modal fade" id="addnewfac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Add New SPASI</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>

						<form class="kt-form kt-form--label-right" id="forminsert" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="kt-form__content">
									<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
										<div class="kt-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="kt-alert__text">
											Oh snap! Change a few things up and try submitting again.
										</div>
										<div class="kt-alert__close">
											<button type="button" class="close" data-close="alert" aria-label="Close">
											</button>
										</div>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Nama Dokumen *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<input type="text" name="nama_sdlc" class="form-control" placeholder="Nama Dokumen" id="nama_sdlc" value="">
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Kategori *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											 <select class="form-control" id="kategori" name='kategori'>
											 	<option value='INNOVATION DESCRIPTION'>INNOVATION DESCRIPTION</option> 
												<option value='SPASI'>SPASI</option> 
											 	<option value='PERSONAL IDENTIFIABLE INFORMATION'>PERSONAL IDENTIFIABLE INFORMATION</option> 
												<option value='SECURITY SYSTEM'>SECURITY SYSTEM</option> 
											 	<option value='DATA SHARING TELKOM GROUP'>DATA SHARING TELKOM GROUP</option> 
											</select>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Mandatory *</label>
									<div class="col-9">
										<div class="kt-radio-list">
											<label class="kt-radio">
												<input type="radio" name="mandatory" id='mandatory' value='1'> Yes
												<span></span>
											</label>
											<label class="kt-radio">
												<input type="radio" name="mandatory" id='mandatory' value='0'> No
												<span></span>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Template *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
						                    <input type="file" class="form-control" name="doctemplate" id="doctemplate" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*"">
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Type *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<select class="form-control" id="type" name='type'>
												<option value='Requirment Gathering'>Requirment Gathering</option>
												<option value='Analysis'>Analysis</option>
												<option value='Design'>Design</option>
												<option value='Construction'>Construction</option>
												<option value='Testing'>Testing</option>
												<option value='Implementation'>Implementation</option>
												<option value='Post Implementation'>Post Implementation</option>
												<option value='Other'>Other</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Type Upload FE *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<select class="form-control" id="type_upload" name='type_upload'>
												<option value='PDF'>PDF</option>
												<option value='IMAGES'>IMAGES</option>
												<option value='MS.WORD'>MS.WORD</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Show in Template *</label>
									<div class="col-lg-9 col-md-9 col-sm-12"> 
										<div class="kt-checkbox-list">
											<label class="kt-checkbox">
												<input type="checkbox" value='1' name='is_show_template'> YES
												<span></span>
											</label>
										</div> 
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" id="saveinsert" class="btn btn-primary">Save</button>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- END MODAL INSERT -->
			<!-- MODAL UPDATE -->
			<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Update Data : <b id="nameroles"></b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>

						<form class="kt-form kt-form--label-right" id="formupdate" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="kt-form__content">
									<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
										<div class="kt-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="kt-alert__text">
											Oh snap! Change a few things up and try submitting again.
										</div>
										<div class="kt-alert__close">
											<button type="button" class="close" data-close="alert" aria-label="Close">
											</button>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Nama Dokumen *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<input type="hidden" name="ed_id_sdlc" id="ed_id_sdlc" value="">
											<input type="text" name="ed_nama_sdlc" class="form-control" placeholder="Nama Dokumen" id="ed_nama_sdlc" value="">
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Kategori *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											 <select class="form-control" id="ed_kategori" name='ed_kategori'>
												<option value='INNOVATION DESCRIPTION'>INNOVATION DESCRIPTION</option> 
												<option value='SPASI'>SPASI</option> 
											 	<option value='PERSONAL IDENTIFIABLE INFORMATION'>PERSONAL IDENTIFIABLE INFORMATION</option> 
												<option value='SECURITY SYSTEM'>SECURITY SYSTEM</option> 
											 	<option value='DATA SHARING TELKOM GROUP'>DATA SHARING TELKOM GROUP</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Mandatory *</label>
									<div class="col-9">
										<div class="kt-radio-list">
											<label class="kt-radio">
												<input type="radio" name="ed_mandatory" id='ed_yes' value='1'> Yes
												<span></span>
											</label>
											<label class="kt-radio">
												<input type="radio" name="ed_mandatory" id='ed_no' value='0'> No
												<span></span>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Template *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
						                    <input type="file" class="form-control" name="ed_doctemplate" id="ed_doctemplate" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/*"">
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12">
									</div>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<small>Kosongkan Jika Tidak Merubah File</small>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Type *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<select class="form-control" id="ed_type" name='ed_type'>
												<option value='Requirment Gathering'>Requirment Gathering</option>
												<option value='Analysis'>Analysis</option>
												<option value='Design'>Design</option>
												<option value='Construction'>Construction</option>
												<option value='Testing'>Testing</option>
												<option value='Implementation'>Implementation</option>
												<option value='Post Implementation'>Post Implementation</option>
												<option value='Other'>Other</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Type Upload FE *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<select class="form-control" id="ed_type_upload" name='ed_type_upload'>
												<option value='PDF'>PDF</option>
												<option value='IMAGES'>IMAGES</option>
												<option value='MS.WORD'>MS.WORD</option>
												<option value='MS.EXCEL'>MS.WORD</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Show in Template *</label>
									<div class="col-lg-9 col-md-9 col-sm-12"> 
										<div class="kt-checkbox-list">
											<label class="kt-checkbox">
												<input type="checkbox" value='1' id='ed_is_show_template' name='ed_is_show_template'> YES
												<span></span>
											</label>
										</div> 
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" id="saveupdate" class="btn btn-primary">Update</button>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- END MODAL UPDATE --> 
			<!-- MODAL DELETE -->
			<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
							<div class="swal2-header">
								<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
								<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure?</h2>
							</div>
							<div class="swal2-content">
								<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
							</div>
							<div class="swal2-actions" style="display: flex;">
								<form method="POST">
								<input type="hidden" name="iddelsdlc" id="iddelsdlc" value="">
								<center>
								<button type="button" id="deleteRoles" class="swal2-confirm swal2-styled" aria-label="" style="border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);">
									Yes, delete it!
								</button>
								<button type="button" class="swal2-cancel swal2-styled" data-dismiss="modal">Cancel</button>
								</center>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MODAL DELETE -->
		</div>
	</div>
</div>

<!-- end:: Content -->