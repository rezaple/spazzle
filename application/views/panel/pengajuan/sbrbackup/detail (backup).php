<?PHP
$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];
$req 			= str_replace('-','/',$id);
$norequest 		= str_replace('_','-',$req);

$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$level			= $dUser['step'];
$aksesCreate 	= $dUser['actionbtn'];

$getDoc 		= $this->db->query("
				SELECT a.* ,
					(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
					(SELECT name from mi.user where userid=a.created_by) namecreated,
					(SELECT username from mi.user where userid=a.created_by) uncreated,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) poscreated,
					(SELECT picture from mi.user where userid=a.reviewer1) pictreview,
					(SELECT name from mi.user where userid=a.reviewer1) namereview,
					(SELECT username from mi.user where userid=a.reviewer1) unreview,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.reviewer1) posreview,
					(SELECT picture from mi.user where userid=a.approval1) pictappr1,
					(SELECT name from mi.user where userid=a.approval1) nameappr1,
					(SELECT username from mi.user where userid=a.approval1) unappr1,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval1) posappr1,
					(SELECT picture from mi.user where userid=a.approval2) pictappr2,
					(SELECT name from mi.user where userid=a.approval2) nameappr2,
					(SELECT username from mi.user where userid=a.approval2) unappr2,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval2) posappr2,
					(SELECT picture from mi.user where userid=a.approval3) pictappr3,
					(SELECT name from mi.user where userid=a.approval3) nameappr3,
					(SELECT username from mi.user where userid=a.approval3) unappr3,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval3) posappr3,
					(SELECT picture from mi.user where userid=a.timtarif1) picttrf1,
					(SELECT name from mi.user where userid=a.timtarif1) nametrf1,
					(SELECT username from mi.user where userid=a.timtarif1) untrf1,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.timtarif1) postrf1,
					(SELECT picture from mi.user where userid=a.timtarif2) picttrf2,
					(SELECT name from mi.user where userid=a.timtarif2) nametrf2,
					(SELECT username from mi.user where userid=a.timtarif2) untrf2,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.timtarif2) postrf2,
					(SELECT analisa_teknis from mi.sbranswer where no_request=a.no_request) analisa_teknis,
					(SELECT no_answer from mi.sbranswer where no_request=a.no_request) no_answer
				FROM sbrdoc a where no_request='$norequest'
				")->result_array();
$doc 			= array_shift($getDoc);

if ($level==1) {
	if ($doc['status']!=0 or $doc['status']!=2) {
		$hideact = 'kt-hidden';
	} else {
		$hideact = '';
	}
} else if ($level!=1) {
	if ($doc['current']==$level) {
		$hideact = '';
	} else {
		$hideact = 'kt-hidden';
	}
} else {
	$hideact = 'kt-hidden';
}

if ($doc['pictcreated']=='') {
	$hidden 	= 'kt-hidden';
} else {
	$hidden 	= '';
}

if ($doc['status']==1) {
	$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting approval</button>';
} else if ($doc['status']==2) {
	$status = '<button class="btn btn-sm btn-label-warning btn-pill">Return</button>';
} else if ($doc['status']==3) {
	$status = '<button class="btn btn-sm btn-label-info btn-pill">Waiting approval</button>';
} else if ($doc['status']==4) {
	$status = '<button class="btn btn-sm btn-label-success btn-pill">Approved</button>';
	$ReadNotif 		= $this->db->query("UPDATE sbrhistory set is_read='1' where no_request='$norequest' and send_to='$userid'");
} else if ($doc['status']==5) {
	$status = '<button class="btn btn-sm btn-label-danger btn-pill">Rejected</button>';
} else {
	$status = '<button class="btn btn-sm btn-secondary btn-pill">Draft</button>';
}
?>
<style>
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.mt15rem { margin-top: 1.5rem; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	#jawaban .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	#history .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#history .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
	#wfinfo .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#wfinfo .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<!--begin::Portlet-->
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#detail" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<b><i class="flaticon flaticon-file-2"></i> Pengajuan SBR</b>
							</a>
						</li>
						<?PHP if ($doc['status']==4) { ?>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#jawaban" role="tab">
									<b><i class="flaticon flaticon2-checking"></i> Jawaban SBR</b>
								</a>
							</li>
						<?PHP } ?>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#history" role="tab">
								<i class="flaticon flaticon-list-3"></i> History Document
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#wfinfo" role="tab">
								<i class="flaticon flaticon-map"></i> Workflow Information
							</a>
						</li>
					</ul>
				</h3>
			</div>
			<div id="bgkonten" class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-toolbar-wrapper">
					<?PHP echo $status; ?>

					<?PHP
					$cekAttach 	= $this->db->query("SELECT * FROM sbrattach WHERE no_request='$norequest'")->num_rows();
					if ($cekAttach>0) {
					?>
					<div class="btn-group" role="group">
                        <button id="btnAttachment" type="button" class="btn btn-sm btn-secondary btn-pill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="la la-file-pdf-o"></i> Attachment
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnAttachment" style="">
                        	<?PHP
                        	$getAttach 	= $this->db->query("SELECT * FROM sbrattach WHERE no_request='$norequest'")->result_array();
                        	foreach ($getAttach as $att) {
                        	?>
	                        	<a href="<?PHP echo base_url();?>attachment/<?PHP echo $att['file']; ?>" target="_blank" class="dropdown-item" title=''>
	                        		<i class="la la-file-pdf-o"></i> <?PHP echo $att['file']; ?>
	                        	</a>
							<?PHP } ?>
                        </div>
                    </div>
                    <?PHP } ?>
					
					<?PHP if ($doc['status']==4 and $doc['created_by']==$userid) { ?>
						<div class="btn-group" role="group">
	                        <button id="btnDownloadDoc" type="button" class="btn btn-sm btn-label-brand btn-pill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <i class="la la-download"></i> Download
	                        </button>
	                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnDownloadDoc" style="">
	                        	<a href="<?PHP echo base_url();?>pengajuan/download/<?PHP echo $id; ?>" class="dropdown-item" title='Dokumen Pengajuan'>
	                        		<i class="la la-file-pdf-o"></i> Dokumen Pengajuan
	                        	</a>
								<a href="<?PHP echo base_url();?>jawaban/download/<?PHP echo $id; ?>" class="dropdown-item" title='Dokumen Jawaban'>
									<i class="la la-file-pdf-o"></i> Dokumen Jawaban
								</a>
								<?PHP if($doc['dok_pengajuan']!='' or $doc['dok_jawaban']!='') { ?>
									<div class="dropdown-divider"></div>
								<?PHP } ?>

								<?PHP if($doc['dok_pengajuan']!='') { ?>
								<a href="<?PHP echo base_url();?>upload/<?PHP echo $doc['dok_pengajuan']; ?>" class="dropdown-item" title='Assign Dokumen Pengajuan'>
									<i class="la la-file-pdf-o"></i> Assign Dokumen Pengajuan
								</a>
								<?PHP } ?>
								<?PHP if($doc['dok_jawaban']!='') { ?>
								<a href="<?PHP echo base_url();?>upload/<?PHP echo $doc['dok_jawaban']; ?>" class="dropdown-item" title='Assign Dokumen Jawaban'>
									<i class="la la-file-pdf-o"></i> Assign Dokumen Jawaban
								</a>
								<?PHP } ?>
	                        </div>
	                    </div>
	                <?PHP } ?>

					<div class="btn-group <?PHP echo $hideact; ?>" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-secondary btn-pill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" style="">
                        	<?PHP if ($doc['status']==0) { ?>
                        	<?PHP echo getBtnAction3($aksesCreate,'modalaction','Submit','submit','btn-sm text-left kt-link kt-link--primary col-sm-12 btnActDoc','la la-rocket','submit',''.$doc['id'].'');?>
                        	<?PHP } ?>

                        	<?PHP echo getBtnAction3($aksesCreate,'modalaction','Return','return','btn-sm text-left kt-link kt-link--warning col-sm-12 btnActDoc','la la-check-circle','return',''.$doc['id'].'');?>
                        	<?PHP echo getBtnAction3($aksesCreate,'modalaction','Escalation','eskalasi','btn-sm text-left kt-link kt-link--brand col-sm-12 btnActDoc','la la-rocket','escalation',''.$doc['id'].'');?>
                            <?PHP echo getBtnAction3($aksesCreate,'modalaction','Approve','approve','btn-sm text-left kt-link kt-link--success col-sm-12 btnActDoc','la la-check-circle','approve',''.$doc['id'].'');?>
                            <?PHP echo getBtnAction3($aksesCreate,'modalaction','Reject','reject','btn-sm text-left kt-link kt-link--danger col-sm-12 btnActDoc','la la-times-circle','reject',''.$doc['id'].'');?>
                        </div>
                    </div>
				</div>
			</div>
		</div>

		<!-- MODAL ACTION -->
		<div class="modal fade" id="modalaction" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-md" role="document">
				<form method="POST">
					<div class="modal-content row">
						<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show col-sm-12" style="display: flex;">
							<div class="swal2-header">
								<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
								<h2 class="swal2-title" id="swal2-title">Are you sure want to <b class="labaction"></b> SBR?</h2>
							</div>
							<div class="swal2-content">
								<div id="swal2-content">You won't be able to revert this!</div>
							</div>
							<div class="swal2-actions">
								<input type="hidden" name="idsubmitact" id="idsubmitact">
								<input type="hidden" name="typesubmitact" id="typesubmitact">
								<div class="form-group row">
									<label class="col-form-label text-left col-sm-12">Write a comment *</label>
									<div class="col-sm-12">
										<textarea name="comment" class="form-control" style="width: 100%;" id="comment"></textarea>
									</div>
								</div>
								<center>
								<button type="submit" id="submitActBtn" class="swal2-styled btn btn-brand" aria-label="">
									Yes, <span class="labaction"></span> it!
								</button>
								<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
								</center>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END MODAL ACTION -->

		<!-- MODAL APPROVAL -->
		<div class="modal fade" id="modalapprove" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">

					<form class="kt-form kt-form--label-right" id="forminsertsingle" enctype="multipart/form-data">
						<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Approve SBR | Please Submit Answer</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class='row'>
									<div class="col-lg-5">
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Nomor Request *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" id='app_id' name='app_id'>
													<input type="text" name="app_norequest" class="form-control" id="app_norequest" placeholder="Nomor Request" readonly="readonly">
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Subject *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="app_subject" class="form-control" id="app_subject" placeholder="Subject" readonly="readonly">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5"> 
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Pelanggan *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" name="app_nopelanggan" id="app_nopelanggan" value="">
													<input type="text" name="app_pelanggan" class="form-control" id="app_pelanggan" placeholder="Pelanggan" readonly="readonly">
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Service ID *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="app_serviceid" class="form-control" id="app_serviceid" placeholder="Service ID" readonly="readonly">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5"> 
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">AM *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" name="app_userid" id="app_userid" value="">
													<input type="text" name="app_am" class="form-control" id="app_am" placeholder="AM" readonly="readonly">
												</div>
											</div>
										</div> 
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								<div class="input_fields_wrap">
									<div class="row">
										<div class="col-lg-12"> 
											<button type="button" class="btn btn-label-dark btn-pill add_field_button pull-right">Add More Fields</button>
										</div>
									</div>
									<div class="row">
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Layanan *</label>
														<div class='input-group'>
															<input type="text" name="layanan[]" class="form-control" id="layanan" placeholder="Layanan">
														</div>
													</div>
												</div> 
											</div>
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Volume *</label>
														<div class='input-group'>
															<input type="text" name="volume[]" class="form-control" id="volume" placeholder="Volume">
														</div>
													</div>
												</div> 
											</div>
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Tarif *</label>
														<div class='input-group'>
															<input type="text" name="tarif[]" class="form-control" id="tarif" placeholder="Tarif">
														</div>
													</div>
												</div> 
											</div>
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Price *</label>
														<div class='input-group'>
															<input type="text" name="price[]" class="form-control" id="price" placeholder="Price">
														</div>
													</div>
												</div>
											</div>
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Analisa Teknis /Bisnis *</label>
											<div class="col-sm-12">
												<textarea name="analisateknis" class="summernote" id="analisateknis"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" id="saveapproved" class="btn btn-success"><i class="la la-rocket"></i> Approve</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- END MODAL APPROVAL -->

		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="detail" role="tabpanel">
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">No. Request</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $norequest; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Nama Project</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['nama_project']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Subject</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['subject']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Pelanggan</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['nipnas']; ?> - <?PHP echo $doc['nama_pelanggan']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Service ID</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['service_id']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">No Order (NCX)</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['no_order_ncx']; ?></b></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Latar Belakang</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['latar_belakang']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Aspek Strategis</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['aspek_strategis']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Aspek Finansial</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['aspek_finansial']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Aspek Kompetisi</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['aspek_kompetisi']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Konfigurasi Teknis</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['konfigurasi_teknis']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<table class='table table-bordered'>
						<thead>
							<tr>
								<th rowspan='2' class="align-middle"><center>NO</center></th>
								<th rowspan='2' class="align-middle">LAYANAN</th>
								<th rowspan='2' class="align-middle">VOLUME</th>
								<th colspan='2' class="align-middle"><center>PEMAKAIAN BULANAN</center></th>
							</tr>
							<tr>
								<th  class="align-middle">TARIF</th>
								<th  class="align-middle">PRICING</th>
							</tr>
						</thead>
						<tbody>
							<?PHP 
							$no_answer = $doc['no_answer'];
							$getAns = $this->db->query("SELECT * FROM mi.sbranswer_detail WHERE no_answer ='".$no_answer."'")->result_array();
							$Ans=0;
							foreach($getAns as $dataAns){
								$Ans++;
							?>
								<tr>
									<td><center><?PHP echo $Ans;?></center></td>
									<td><?PHP echo $dataAns['layanan'];?></td>
									<td><?PHP echo $dataAns['volume'];?></td>
									<td><?PHP echo rupiah($dataAns['tarif']);?></td>
									<td><?PHP echo rupiah($dataAns['price']);?></td>
								</tr>
							<?PHP 
							}
							?>
						</tbody>
					</table>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Analisa Bisnis</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $doc['analisa_teknis']; ?></label>
					</div>
					<!-- <div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div> -->
				</div>

				<?PHP
				if ($doc['status']==4) {
					$getAns = $this->db->query("
							SELECT a.* ,					
								(SELECT picture from mi.user where userid=a.userid) pictcreated,
								(SELECT name from mi.user where userid=a.userid) namecreated,
								(SELECT username from mi.user where userid=a.userid) uncreated,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.userid) poscreated,
								(SELECT picture from mi.user where userid=a.created_by) pictappr,
								(SELECT name from mi.user where userid=a.created_by) nameappr,
								(SELECT username from mi.user where userid=a.created_by) unappr,
								(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) posappr
							FROM sbranswer a where no_request='$norequest'
							")->result_array();
					$ans 	= array_shift($getAns);
					$no_answer= $ans['no_answer'];
				?>
				<div class="tab-pane" id="jawaban" role="tabpanel">
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">No. Answer</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $ans['no_answer']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">No. SBR</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['no_request']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Pelanggan</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['nipnas']; ?> - <?PHP echo $doc['nama_pelanggan']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">AM</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['namecreated']; ?> / <?PHP echo $doc['uncreated']; ?></b></label>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-lg-2 col-sm-12">Permintaan SBR</label>
						<label class="col-form-label col-lg-10 col-sm-12">: <b><?PHP echo $doc['nama_project']; ?></b></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<table class='table table-bordered'>
							<thead>
								<tr>
									<th rowspan='2' class="align-middle"><center>NO</center></th>
									<th rowspan='2' class="align-middle">LAYANAN</th>
									<th rowspan='2' class="align-middle">VOLUME</th>
									<th colspan='2' class="align-middle"><center>PEMAKAIAN BULANAN</center></th>
								</tr>
								<tr>
									<th  class="align-middle">TARIF</th>
									<th  class="align-middle">PRICING</th>
								</tr>
							</thead>
							<tbody>
								<?PHP 
								$getAnsDet = $this->db->query("SELECT * FROM mi.sbranswer_detail WHERE no_answer ='".$no_answer."'")->result_array();
								$x=0;
								foreach($getAnsDet as $dataAD){
									$x++;
								?>
									<tr>
										<td><center><?PHP echo $x;?></center></td>
										<td><?PHP echo $dataAD['layanan'];?></td>
										<td><?PHP echo $dataAD['volume'];?></td>
										<td><?PHP echo rupiah($dataAD['tarif']);?></td>
										<td><?PHP echo rupiah($dataAD['price']);?></td>
									</tr>
								<?PHP 
								}
								?>
							</tbody>
						</table>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="form-group row">
						<label class="col-form-label col-lg-12 col-sm-12"><b>Analisa Bisnis</b></label>
						<label class="col-form-label col-lg-12 col-sm-12"><?PHP echo $ans['analisa_teknis']; ?></label>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

					<div class="row">
						<div class="col-md-12 text-center">
							<h4>PERSETUJUAN</h4>
							<b><?PHP echo $this->formula->TanggalIndo($ans['created_at']); ?></b>
						</div>

						<?PHP
						$qApprover 		= "
										SELECT * FROM (
											SELECT approver.*,
											(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
											FROM (
												SELECT DISTINCT(a.created_by) approver,
												(SELECT name from mi.user where userid=a.created_by) nameapp,
												(SELECT username from mi.user where userid=a.created_by) unapp,
												(SELECT level_user from mi.user where userid=a.created_by) levelapp,
												(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid 
												where no_request='$norequest' and
												xb.userid=a.created_by) posisi
												from mi.sbrhistory a
												where no_request='$norequest' and action in ('escalation','approve','update')
											) as approver
										) as final
										where actionapp like '%approve%'
										order by levelapp
										";
						$cekApprover 	= $this->db->query($qApprover)->num_rows();
						$getApprover 	= $this->db->query($qApprover)->result_array();
						foreach ($getApprover as $apr) {
							if ($cekApprover==1) {
								$width 		= '12';
							} else if ($cekApprover==2) {
								$width 		= '6';
							} else if ($cekApprover==3) {
								$width 		= '4';
							} else if ($cekApprover==4) {
								$width 		= '3';
							} else {
								$width 		= '3';
							}
						?>
						<div class="col-md-<?PHP echo $width; ?>">
							<label class="col-form-label col-lg-12 col-sm-12 text-center">Disetujui Oleh:</label>
							<label class="col-form-label col-lg-12 col-sm-12 text-center">
								<b><?PHP echo $apr['nameapp']; ?><br>
								<?PHP echo $apr['unapp']; ?></b><br>
								(<?PHP echo $apr['posisi']; ?>)
							</label>
						</div>
						<?PHP } ?>
					</div>
				</div>
				<?PHP } ?>

				<div class="tab-pane" id="history" role="tabpanel">
					<div class="kt-scroll" data-scroll="true" data-height="380" data-mobile-height="300">
	                    <!--Begin::Timeline 3 -->
	                    <div class="kt-timeline-v2">
	                        <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	                        	<div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $doc['pictcreated']; ?>" />

											<?PHP if ($doc['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($doc['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless kt-font-brand"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    Pembuatan Pengajuan SBR <br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($doc['created_at']); ?> | 
		                                    <i class="la la-user"></i>  <?PHP echo $doc['namecreated']; ?>
		                                </div>
	                                </div>
	                            </div>
	                        	<?PHP
	                        	$getHistory = $this->db->query("
	                        				SELECT a.*,
	                        					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
												(SELECT name from mi.user where userid=a.created_by) namecreated
	                        				FROM sbrhistory a where no_request='$norequest'
	                        				order by 1
	                        				")->result_array();
	                        	foreach($getHistory as $history) {
	                        		if ($history['action']=='reject') {
	                        			$colortl 	= 'kt-font-danger';
	                        		} else if ($history['action']=='return') {
	                        			$colortl 	= 'kt-font-warning';
	                        		} else if ($history['action']=='approve') {
	                        			$colortl 	= 'kt-font-success';
	                        		} else {
	                        			$colortl 	= 'kt-font-brand';
	                        		}

	                        		if ($history['pictcreated']=='') {
										$hidden2 	= 'kt-hidden';
									} else {
										$hidden2 	= '';
									}

									if ($history['action']=='new') {
										$notiftext 	= 'Publish Pengajuan SBR';
									} else if ($history['action']=='republish') {
										$notiftext 	= 'Perbaikan Pengajuan';
									} else if ($history['action']=='escalation') {
										$notiftext 	= 'Eskalasi Pengajuan';
									} else if ($history['action']=='reject') {
										$notiftext 	= 'Pengajuan tidak disetujui';
									} else if ($history['action']=='approve') {
										$notiftext 	= 'Pengajuan telah disetujui';
									} else if ($history['action']=='return') {
										$notiftext 	= 'Pengajuan telah dikembalikan';
									} else if ($history['action']=='update') {
										$notiftext 	= 'Perbaikan dan Eskalasi SBR';
									}

									$genValue 	= $history['namecreated'];
									$nickname 	= explode(' ',trim($genValue));
	                        	?>
	                            <div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden2; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $history['pictcreated']; ?>" />

											<?PHP if ($history['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($history['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless <?PHP echo $colortl; ?>"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    <?PHP echo $notiftext; ?><br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($history['created_at']); ?> | 
		                                    <i class="la la-user"></i>  <?PHP echo $history['namecreated']; ?>
	                                    	
	                                    	<div style="margin-top: 0.5rem;">
		                                    	<i class="la la-comment"></i>  Comment :<br>
		                                    	<?PHP echo $history['comment']; ?>
		                                    </div>
		                                </div>
	                                </div>
	                            </div>
	                            <?PHP } ?>
	                        </div>
	                    </div>
	                    <!--End::Timeline 3 -->
	                </div>
				</div>

				<div class="tab-pane" id="wfinfo" role="tabpanel">
					<div class="kt-scroll" data-scroll="true" data-height="380" data-mobile-height="300">
	                    <!--Begin::Timeline 3 -->
	                    <div class="kt-timeline-v2">
	                        <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	                        	<?PHP
	                        	$getFlow 	= $this->db->query("
	                        				SELECT a.*,
	                        					(SELECT picture from mi.user where userid=a.userid) pictcreated,
												(SELECT name from mi.user where userid=a.userid) namecreated,
												a.role_name levelname,
												case when level = (select current from sbrdoc where no_request='$norequest') then 1 else 0 end active
	                        				FROM sbrdoc_flow a where no_request='$norequest'
	                        				order by level
	                        				")->result_array();
	                        	foreach($getFlow as $flow) {
	                        		if ($doc['status']!='4' and $flow['active']==1 or $doc['status']!='5' and $flow['active']==1) {
	                        			$colortl 	= 'kt-font-warning';
	                        		} else {
	                        			$colortl 	= 'kt-font-brand';
	                        		}

	                        		if ($flow['pictcreated']=='') {
										$hidden2 	= 'kt-hidden';
									} else {
										$hidden2 	= '';
									}

									$genValue 	= $flow['namecreated'];
									$nickname 	= explode(' ',trim($genValue));
	                        	?>
	                            <div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden2; ?>" alt="Pic" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $flow['pictcreated']; ?>" />

											<?PHP if ($flow['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($flow['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless <?PHP echo $colortl; ?>"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    <?PHP echo $flow['namecreated']; ?><br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-user"></i> <?PHP echo $flow['levelname']; ?><br>
		                                    <?PHP
		                                    if ($doc['status']!='4' and $flow['active']==1 or $doc['status']!='5' and $flow['active']==1) {
	                                    	?>
	                                    	<b class="text-warning"><i class="la la-clock-o"></i> Waiting Approval</b><br>
		                                    <?PHP } ?>
		                                </div>
	                                </div>
	                            </div>
	                            <?PHP } ?>
	                        </div>
	                    </div>
	                    <!--End::Timeline 3 -->
	                </div>
				</div>
			</div>
		</div>
	</div>

</div>