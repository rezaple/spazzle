<?php
$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];
$username 		= $userdata['username'];
$gnoreq 		= str_replace('-','/',$id);
$norequest 		= str_replace('_','-',$gnoreq);

$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$level			= $dUser['step'];
$aksesCreate 	= $dUser['actionbtn'];

$getDoc 		= $this->db->query("
				SELECT a.* ,
					(SELECT nama_pelanggan from customer where nipnas=a.nipnas) nama_pelanggan,
					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
					(SELECT name from mi.user where userid=a.created_by) namecreated,
					(SELECT username from mi.user where userid=a.created_by) uncreated,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) poscreated,
					(SELECT picture from mi.user where userid=a.reviewer1) pictreview,
					(SELECT name from mi.user where userid=a.reviewer1) namereview,
					(SELECT username from mi.user where userid=a.reviewer1) unreview,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.reviewer1) posreview,
					(SELECT picture from mi.user where userid=a.approval1) pictappr1,
					(SELECT name from mi.user where userid=a.approval1) nameappr1,
					(SELECT username from mi.user where userid=a.approval1) unappr1,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval1) posappr1,
					(SELECT picture from mi.user where userid=a.approval2) pictappr2,
					(SELECT name from mi.user where userid=a.approval2) nameappr2,
					(SELECT username from mi.user where userid=a.approval2) unappr2,
					(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.approval2) posappr2
				FROM sbrdoc a where no_request='$norequest'
				")->result_array();
$doc 			= array_shift($getDoc);
$getAns = $this->db->query("
		SELECT a.* ,					
			(SELECT picture from mi.user where userid=a.userid) pictcreated,
			(SELECT name from mi.user where userid=a.userid) namecreated,
			(SELECT username from mi.user where userid=a.userid) uncreated,
			(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.userid) poscreated,
			(SELECT picture from mi.user where userid=a.created_by) pictappr,
			(SELECT name from mi.user where userid=a.created_by) nameappr,
			(SELECT username from mi.user where userid=a.created_by) unappr,
			(SELECT xb.name from mi.user xa left join mi.level_user xb on xa.level_user=xb.id_level where userid=a.created_by) posappr
		FROM sbranswer a where no_request='$norequest'
		")->result_array();
$ans 		= array_shift($getAns);
$no_answer	= $ans['no_answer'];
$html 		='';
$table 		= '';

$qApprover 		= "
				SELECT * FROM (
					SELECT approver.*,
					(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
					FROM (
						SELECT DISTINCT(a.created_by) approver,
						(SELECT name from mi.user where userid=a.created_by) nameapp,
						(SELECT username from mi.user where userid=a.created_by) unapp,
						(SELECT level_user from mi.user where userid=a.created_by) levelapp,
						(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid
							where no_request='$norequest' and
							xb.userid=a.created_by) posisi
						from mi.sbrhistory a
						where no_request='$norequest' and action in ('escalation','approve','update')
					) as approver
				) as final
				where actionapp like '%approve%'
				order by levelapp
				";
$cekApprover 	= $this->db->query($qApprover)->num_rows();
$getApprover 	= $this->db->query($qApprover)->result_array();
$cekColspan 	= $cekApprover;

$qLastApprove 	= "
					SELECT * FROM (
						SELECT approver.*,
						(SELECT action from mi.level_user where id_level=approver.levelapp) actionapp
						FROM (
							SELECT DISTINCT(a.created_by) approver,
							(SELECT name from mi.user where userid=a.created_by) nameapp,
							(SELECT username from mi.user where userid=a.created_by) unapp,
							(SELECT level_user from mi.user where userid=a.created_by) levelapp,
							(SELECT xa.role_name from sbrdoc_flow xa left join mi.user xb on xa.userid=xb.userid
							where no_request='$norequest' and
							xb.userid=a.created_by) posisi
							from mi.sbrhistory a
							where no_request='$norequest' and action in ('escalation','approve','update')
						) as approver
					) as final
					where actionapp like '%approve%'
					order by levelapp desc
					limit 1
					";
	$gLastApprove	= $this->db->query($qLastApprove)->result_array();
	$dLastApprove 	= array_shift($gLastApprove);
	$lastapproval 	= $dLastApprove['unapp'];
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */
//echo getcwd();exit();
// Include the main TCPDF library (search for installation path).
//require_once('tcpdf/examples/tcpdf_include.php');
// Extend the TCPDF class to create custom Header and Footer

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Keputusan Direksi Perusahaan Perseroan');
$pdf->SetTitle('DOK-JAWABAN');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 9);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '<h3><u>LEMBAR JAWABAN SPECIAL BUSINESS REQUEST (SBR)</u></h3>
<span>NOMOR TEL : '.$no_answer.'</span>
<br><br><br>
Menjawab SBR <strong>'.$doc['namecreated'].' / '.$doc['uncreated'].'</strong><br>
NOMOR TEL : <strong>'.$doc['no_request'].'</strong><br>
<br><br>
<table width="100%" cellpadding="2px" cellspacing="0px" border="0px">
	<tr>
		<td style="width:20%;">PELANGGAN</td>
		<td style="width:5%">:</td>
		<td style="text-align:left;width:65%;"><strong>'.$doc['nipnas'].' - '.$doc['nama_pelanggan'].'</strong></td>
	</tr>
	<tr>
		<td style="width:20%;">AM</td>
		<td style="width:5%">:</td>
		<td style="text-align:left;width:65%;"><strong>'.$doc['namecreated'].' / '.$doc['uncreated'].'</strong></td>
	</tr>
	<tr>
		<td style="width:20%;">PERMINTAAN SBR</td>
		<td style="width:5%">:</td>
		<td style="text-align:left;width:65%;"><strong>'.$doc['nama_project'].'</strong></td>
	</tr>
</table>
'; 

// output the HTML content
//$pdf->writeHTML($html, true, false, true, false, ''); 

// reset pointer to the last page
//$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
//$pdf->AddPage();


$html .= '<br><br>
<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
  <thead>
		<tr>
			<th rowspan="2" style="vertical-align:middle"><center>NO</center></th>
			<th rowspan="2" style="vertical-align:middle">LAYANAN</th>
			<th rowspan="2" style="vertical-align:middle">VOLUME</th>
			<th colspan="2" style="vertical-align:middle"><center>PEMAKAIAN BULANAN</center></th>
		</tr>
		<tr>
			<th  class="align-middle">TARIF</th>
			<th  class="align-middle">PRICING</th>
		</tr>
  </thead> 
  <tbody>';
	$getAnsDet = $this->db->query("SELECT * FROM mi.sbranswer_detail WHERE no_answer ='".$no_answer."'")->result_array();
	$x=0;
	foreach($getAnsDet as $dataAD){
		$x++;
		$table .="<tr>
			<td><center>".$x."</center></td>
			<td>".$dataAD['layanan']."</td>
			<td>".$dataAD['volume']."</td>
			<td><center>".rupiah($dataAD['tarif'])."</center></td>
			<td><center>".rupiah($dataAD['price'])."</center></td>
		</tr>";
	}
	$html.=$table;
$html.='</tbody>
</table><br><br>
<table width="100%" border="1px" cellspacing="0px" cellpadding="15px">
  <tbody>
    <tr>
      <td style="width:100%">
      	<strong>ANALISA TEKNIS</strong>
      	<p>
		'.$ans['analisa_teknis'].'
		</p>
      </td> 
    </tr>  
  </tbody>
</table><br><br><br>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, ''); 

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table

// add a page
$pdf->AddPage();
$html ='
<h3 style="text-align:center;">PERSETUJUAN</h3>
<table width="100%" border="1px" cellspacing="0px" cellpadding="10px">
<tr>
	<td colspan="'.$cekColspan.'" style="text-align:center;"><center>'.$this->formula->TanggalIndo($doc['created_at']).'</center></td>
</tr>
<tr>';
	if($cekColspan < 2 or $cekColspan==''){
		$dinamic = 100;		
	}else{
		$dinamic = 100/$cekColspan;
	}
	foreach ($getApprover as $apr) {
		$html .= '<td width="'.$dinamic.'%" style="text-align:center;"><center>Disetujui Oleh,<br>'.$apr['posisi'].'</center></td>';
	}

$html .= '</tr>
<tr>';

	foreach ($getApprover as $apr) {
		$html .= '<td height="100px"></td>';
	}

$html .= '</tr>
<tr>';

	foreach ($getApprover as $apr) {
		$html .= '<td><b><u>'.$apr['nameapp'].'</u><br>
		'.$apr['unapp'].'</b><br></td>'; 
	}

$html .= '</tr>
</table><br>
<small><i>* Dokumen SBR Bisnis ini sudah melalui proses persetujuan online.</i></small>
';
 
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// reset pointer to the last page
$pdf->lastPage();


$maxnum = $pdf->getNumPages();
for($i=1;$i<=$maxnum;$i++){
	// Simple watermark
	// This will set it to page one and lay over anything written before it on the first page
	$pdf->setPage( $i );

	// Get the page width/height
	$myPageWidth = $pdf->getPageWidth();
	$myPageHeight = $pdf->getPageHeight();

	// Find the middle of the page and adjust.
	$myX = ( $myPageWidth / 2 ) - 50;
	$myY = ( $myPageHeight / 2 ) + 20;

	// Set the transparency of the text to really light
	$pdf->SetAlpha(0.10);

	// Rotate 45 degrees and write the watermarking text
	$pdf->StartTransform();
	$pdf->Rotate(45, $myX, $myY);
	$pdf->SetFont("dejavusans", "", 80);
	$pdf->Text($myX, $myY,"".$lastapproval."");
	$pdf->StopTransform();

	// Reset the transparency to default
	$pdf->SetAlpha(1);
	// ---------------------------------------------------------
}
//Close and output PDF document
$pdf->Output('DOK-JAWABAN.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
