<script src="<?PHP echo base_url(); ?>assets/zxcvbn.js"></script>
<style>
.sizemidle {
	max-width: 140px!important;
    max-height: 140px!important;
}
.kt-invoice-1 .kt-invoice__wrapper .kt-invoice__head .kt-invoice__container .kt-invoice__logo {
	padding-top: 5rem!important;
}
.bghead {
    background: url('<?PHP echo base_url(); ?>images/sidebar.png') no-repeat!important;
    background-size: 100% auto!important;
    background-position: 0px 30%!important;
}
</style>
<style>
.kt-portlet{
	border-radius:10px !important;
}
meter {
    /* Reset the default appearance */
    -webkit-appearance: none;
       -moz-appearance: none;
            appearance: none;
            
    margin: 0 auto 1em;
    width: 100%;
    height: .5em;
    
    /* Applicable only to Firefox */
    background: none;
    background-color: rgba(0,0,0,0.1);
}

meter::-webkit-meter-bar {
    background: none;
    background-color: rgba(0,0,0,0.1);
}

meter[value="1"]::-webkit-meter-optimum-value { background: red; }
meter[value="2"]::-webkit-meter-optimum-value { background: yellow; }
meter[value="3"]::-webkit-meter-optimum-value { background: orange; }
meter[value="4"]::-webkit-meter-optimum-value { background: green; }

meter[value="1"]::-moz-meter-bar { background: red; }
meter[value="2"]::-moz-meter-bar { background: yellow; }
meter[value="3"]::-moz-meter-bar { background: orange; }
meter[value="4"]::-moz-meter-bar { background: green; }

.feedback {
    color: #9ab;
    font-size: 90%;
    padding: 0 .25em;
    padding-left: 0em;
    margin-top: 1em;
}
 
#btn-prev {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
}
#btn-draft {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
  background-color: #d8494f;
  border-color:#d8494f;;
}
#btn-submit {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
  background-color: #d8494f;
  border-color:#d8494f;;
}
#btn-next {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
  background-color: #d8494f;
  border-color:#d8494f;;

} 

.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items{
	padding:0 2rem !important;
}
.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body{
	padding: 2rem 0.5rem 0rem;
}
.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item{
	flex: auto;
}
.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body .kt-wizard-v3__nav-label{
	font-size:12px;
}
.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body .kt-wizard-v3__nav-label span{
	font-size:13px;
}
.kt-wizard-v3 .kt-wizard-v3__wrapper .kt-form{
	width: 90%;
    padding: 0rem 0 5rem;
}
</style>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<div class="kt-portlet" id='formwizard'>
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="first">
				<div class="kt-grid__item">

					<!--begin: Form Wizard Nav -->
					<div class="kt-wizard-v3__nav">
						<div class="kt-wizard-v3__nav-items">
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>1</span> Innovation Description
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="sdlc">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>2</span> SPASI
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="tech">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>3</span> Technology Platform
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="pii">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>4</span> Personal Identifiable Information
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="security">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>5</span> Security System
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="sharing">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>6</span> Data Sharing Telkom Group
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
						</div>
					</div>

					<!--end: Form Wizard Nav -->
				</div>
				<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

					<!--begin: Form Wizard Form-->
					<form class="kt-form" id="kt_form" novalidate="novalidate" id="kt_form" method='POST' action='<?PHP echo base_url(); ?>/ctrl_innovasi/submit_innovasi_new'>

						<!--begin: Form Wizard Step 1-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
							<!--div class="kt-heading kt-heading--md">Form Innovation Description</div-->
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v3__form">
									<table class='table table-bordered' style='border:solid 1px #013453;'>
										<tbody>
										<tr>
											<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>JUDUL :</b></td>
											<td>
												<input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul INOVASI" value="">
											</td>
											<td rowspan='6' colspan='2' style='padding: 0rem;'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
												<div style='padding:8px;vertical-align: middle'>
													<?PHP 
													foreach($getdatainodes as $inodes){
														if($inodes['mandatory']==1){ $md = '*'; }else{ $md = ''; }
														if($inodes['type_upload']=='PDF'){ $type = 'application/pdf'; }
														if($inodes['type_upload']=='IMAGES'){ $type = 'image/*'; }
														if($inodes['type_upload']=='MS.WORD'){ $type = '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'; }
														if($inodes['type_upload']=='MS.EXCEL'){ $type = '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'; }
													?>
													<small><?PHP echo $inodes['nama_sdlc'];?> <?PHP echo $md;?></small>
													<center>
														 <input type="file" class="form-control" name="docspasi<?PHP echo $inodes['id_sdlc'];?>" id="docspasi<?PHP echo $inodes['id_sdlc'];?>" accept="<?PHP echo $type;?>"> 
													</center>
													<hr>
													<?PHP 
													} 
													?>
												</div>
											</td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU :</b></td>
											<td>
												<select class="form-control m-select2" id="cfu_fu" name="cfu_fu[]" multiple="multiple">
													<?PHP 
														foreach($getcfu as $cfux){
															echo "<option value='".$cfux['cfu']."'>".$cfux['cfu']."</option>";
														}
													?> 
												</select>
											</td>
											
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>JENIS APLIKASI :</b></td>
											<td>
												<!--input type="text" class="form-control" id="jenis_aplikasi" name="jenis_aplikasi" placeholder="Web Application / Mobile / Desktop Application"-->
												<select class="form-control m-select2" id="jenis_aplikasi" name="jenis_aplikasi">
													<option value='Web Application'>Web Application</option>
													<option value='Mobile'>Mobile</option>
													<option value='Desktop Application'>Desktop Application</option>
												</select>	
											</td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL :</b></td>
											<td><input type="text" class="form-control" id="url" name="url" placeholder="URL Aplikasi"></td>
										</tr>
										
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>DOMAIN :</b></td>
											<td>
												<select class="form-control m-select2" id="domain" name="domain">
													<option value='Partner Mgmt'>Partner Mgmt</option>
													<option value='Analytics'>Analytics</option>
													<option value='Security Mgmt'>Security Mgmt</option>
													<option value='Performance Mgmt'>Performance Mgmt</option>
													<option value='Product Mgmt'>Product Mgmt</option>
													<option value='Fullfilment Mgmt'>Fullfilment Mgmt</option>
													<option value='Assurance Mgmt'>Assurance Mgmt</option>
													<option value='Data & Inventory Mgmt'>Data & Inventory Mgmt</option> 
												</select>
											</td>
										</tr>
										<tr>
											<td colspan='2' style='width:50%;padding: 0rem'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
												<div style='padding:8px;'>
													<b>Deskripsi:</b>
													<textarea name="deskripsi" class="summernote_deskripsi" id="deskripsi"></textarea>
													<hr>
													<b>Tujuan:</b>
													<textarea name="tujuan" class="summernote_tujuan" id="tujuan"></textarea>
												</div>
											</td>
										</tr>

	 									<tr>
											<td colspan='2' style='padding: 0rem'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
												<div class="row" style='padding:8px;'>
													<div class="col-md-6">
														<b>Jenis Data:</b>
														<textarea name="jenis_data" class="summernote_jenis_data" id="jenis_data"></textarea>

													</div>
													<div class="col-md-6">
														<b>Stakeholder:</b>
														<textarea name="stakholder" class="summernote_stakeholder" id="stakholder"></textarea>
													</div>
												</div>
											</td>
											<td colspan='2' style='padding: 0rem;'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasan Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
												<div style='padding:8px;'>
													<br>
													 <textarea name="penjelasan" class="summernote_penjelasan" id="penjelasan"></textarea>
												</div>
											</td>
										 
										</tr>
										</tbody>
									</table> 
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 1-->

						<!--begin: Form Wizard Step 2-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="sdlc">
							<div class="kt-heading kt-heading--md">Dokumen SPASI</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v3__form">
									<div class="row">
										<?PHP 
											$n = 0;
											foreach($getdataspasi as $sdlc){ 
												$n++;
												if($sdlc['mandatory']==1){ $md = '*'; }else{ $md = ''; }
												if($sdlc['type_upload']=='PDF'){ $type = 'application/pdf'; }
												if($sdlc['type_upload']=='IMAGES'){ $type = 'image/*'; }
												if($sdlc['type_upload']=='MS.WORD'){ $type = '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'; }
												if($sdlc['type_upload']=='MS.EXCEL'){ $type = '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'; }

										?>
										<div class="col-md-3">
											<!--begin::Portlet-->
											<!--div class="kt-portlet kt-portlet"> 
												<div class="kt-portlet__body"-->
													<div class="col-md-12">
														<div class="form-group">
															<label style='font-size:12px;'><b><?PHP echo $n; ?>. Dokumen <?PHP echo $sdlc['nama_sdlc'];?> <?PHP echo $md;?></b><br>  
															</label>
															<div class="input-group">
																<a href=''>
																	<button type="button" class="btn btn-secondary btn-sm"><i class="flaticon-download"></i> Example</button> 
																</a>
																<a href='<?PHP echo base_url();?>attachment/helpdoc/<?PHP echo $sdlc['template_sdlc'];?>'>
																	<button type="button" class="btn btn-secondary btn-sm"><i class="flaticon-download"></i> Template</button>
																</a> 
															</div>
															<br>
															<div id="bgattach">
																<div class="input-group">
												                    <input type="file" class="form-control" name="docspasi<?PHP echo $sdlc['id_sdlc'];?>" id="docspasi<?PHP echo $sdlc['id_sdlc'];?>" accept="<?PHP echo $type;?>">
												                    <a href="#" class="col-lg-1">
												                    </a>
												                </div>
															</div>
															<br>
																<span class="form-text text-muted">
																Type Document <?PHP echo $sdlc['type_upload']?>
															</span>
														</div>
													</div>
												<!--/div>
											</div>

											<!--end::Portlet--> 
										</div>
										<?PHP } ?>
										<!--div class="col-xl-6">
											<div class="form-group">
												<label>Doc Requirment*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docrequirmen[]" id="docrequirmen" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc Design*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docdesign[]" id="docdesign" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div--> 
									</div>
									<!--div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc Test Report & Plan *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doctesreport[]" id="doctesreport" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc SMP*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docsmp[]" id="docsmp" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc SOP*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docsop[]" id="docsop">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc SOP D2P*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docsopd2p[]" id="docsopd2p" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 

									</div-->
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 2-->

						<!--begin: Form Wizard Step 3-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="tech">
							<div class="kt-heading kt-heading--md">Technology Platform</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v3__form">
									 <div class="row">
										<!--div class="col-12">
											<table class='table table-bordered' width='100%'>
												<tr>
													<td>App Definition & Development</td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td rowspan='2'>Platform</td>
													<td colspan='2'></td>
													<td></td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td colspan='2'></td>
													<td></td> 
												</tr>
												<tr>
													<td>Infrastructure</td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr> 
											</table>
										</div-->
										<div class="col-6">
											<div class="form-group row">
												<label class="col-12 col-form-label"><strong>Language & Framework</strong></label>
												<div class="col-12">

													<div class="kt-checkbox-list">
														<?PHP
														foreach($getdataframework as $gframe){
														?>
														<label class="kt-checkbox">
															<input type="checkbox" name='framework[]' value='<?PHP echo $gframe['framework'];?>'> <?PHP echo $gframe['framework'];?>
															<span></span>
														</label>
														<?PHP														
														} 
														?>
														
														<label class="kt-checkbox">
															<input type="checkbox" id='other_framework'> Other
															<span></span>
														</label>

													</div>
												</div>
											</div>
											<div class="form-group row" id='fr_wrapper' style='display:none;'> 
												<label class="col-form-label col-lg-12 col-sm-12">Other Language & Framework *</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<div id="bgframework">
														<div class="input-group">
										                    <input type="text" class="form-control" name="otherframe[]" id="otherframe"> 
										                </div>
													</div>
													<br>
													<button type="button" class="btn btn-sm btn-default btnframework">
														<i class="flaticon flaticon-plus"></i> Tambah
													</button>
												</div> 
											</div>												
										</div>
										<div class="col-6">
											<div class="form-group row">
												<label class="col-12 col-form-label"><strong>Database</strong></label>
												<div class="col-12">
													<div class="kt-checkbox-list">
														<?PHP
														foreach($getdatadatabase as $gbase){
														?>
														<label class="kt-checkbox">
															<input type="checkbox" name='database[]' value='<?PHP echo $gbase['nama_database'];?>'> <?PHP echo $gbase['nama_database'];?>
															<span></span>
														</label>
														<?PHP														
														} 
														?>
														<label class="kt-checkbox">
															<input type="checkbox" id='other_database'> Other
															<span></span>
														</label>

													</div>
												</div>
											</div>
											<div class="form-group row" id='db_wrapper' style='display:none;'> 
												<label class="col-form-label col-lg-12 col-sm-12">Other Database *</label>
												<div class="col-lg-12 col-md-12 col-sm-12">
													<div id="bgdatabase">
														<div class="input-group">
										                    <input type="text" class="form-control" name="otherdb[]" id="otherdb"> 
										                </div>
													</div>
													<br>
													<button type="button" class="btn btn-sm btn-default btndatabase">
														<i class="flaticon flaticon-plus"></i> Tambah
													</button>
												</div> 
											</div>
										</div>
									</div>  
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 3-->

						<!--begin: Form Wizard Step 4-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="pii">
							<div class="kt-heading kt-heading--md">Personal Identifiable Information</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v3__form">
									 <div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<b>Jika inovasi menggunakan jenis data PII ,maka upload data persetujuan owner (Nota dinas / MoM).</b>
											</div> 
										</div> 
									</div> 
									 <div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label class="kt-checkbox">
													<input type="checkbox" name='pii' id='pii' value='1'>PII
													<span></span>
												</label>
												<span class="form-text text-muted">Pilih ceklist jika inovasi memenuhi ketentuan berikut.</span> 
											</div> 
										</div> 
									</div> 
									<div class="row" id='uepo' style='display:none;'>
										<div class="col-xl-6">
											<?PHP 
											foreach($getdatapiidoc as $piidoc){
												if($piidoc['mandatory']==1){ $md = '*'; }else{ $md = ''; }
												if($piidoc['type_upload']=='PDF'){ $type = 'application/pdf'; }
												if($piidoc['type_upload']=='IMAGES'){ $type = 'image/*'; }
												if($piidoc['type_upload']=='MS.WORD'){ $type = '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'; }
												if($piidoc['type_upload']=='MS.EXCEL'){ $type = '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'; }
											?>
											<div class="form-group">
												<label>Upload <?PHP echo $piidoc['nama_sdlc']?> <?PHP echo $md;?></label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docspasi<?PHP echo $piidoc['id_sdlc'];?>" id="docspasi<?PHP echo $piidoc['id_sdlc'];?>">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document <?PHP echo $piidoc['type_upload'];?></span>
											</div> 
											<?PHP 
											} 
											?> 
										</div> 
									</div>
									
									<div class="row">
										<h6>Persetujuan Data Owner via NDE ditujukan kepada :</h6>
										<div class="col-xl-12">
											<ul>
												<li>Direktorat Customer : VP Planning and Resource Management</li>
												<li>Direktorat Enterprise &  Business Service : VP Enterprise Business Development</li>
												<li>Direktorat Wholesale & International Service : VP Wholsale and Intl.Development</li>
												<li>Direktorat Customer : VP Planning and Resource Management</li>
												<li>Subsidiary : Pejabat yang bertanggung jawab sebagai Data Owner Subsidiary</li>
											</ul>
										</div>
									</div>
									<div class="row">
										<div class="col-xl-12">
											<table class='table table-striped table-bordered' width='100%'>
												<thead>
														<th>No</th>
														<th>PII</th>
														<th>Keterangan / Contoh</th>
												</thead>
												<tbody>
														<tr>
															<td>1</td>
															<td>ID Berlangganan</td>
															<td>Termasuk No.Telepon Telkom, No. Indihome,No. Speedy dll.</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Nama</td>
															<td>Nama Perorangan , Nama Pelanggan Korporat dll.</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Tempat/Tanggal Lahir</td>
															<td>Termasuk No.Telepon Telkom, No. Indihome,No. Speedy dll.</td>
														</tr>
														<tr>
															<td>4</td>
															<td>No.HP</td>
															<td>Termasuk No.HP yang muncul dari sistem apapun (Pengaduan via Twitter, MyIndihome dll)</td>
														</tr>
														<tr>
															<td>5</td>
															<td>E-Mail</td>
															<td>Termasuk E-Mail yang muncul dari sistem manapun</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Alamat Pelanggan</td>
															<td>Alamat rumah / kantor, Koordinasi GPS(long/lat) dll.</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Info Sosial Media</td>
															<td>Akun / User ID Facebook , Instagram , Twitter dll, Termasuk jumlah follower.</td>
														</tr>
														<tr>
															<td>8</td>
															<td>No. Identitas Resmi</td>
															<td>Termasuk No.KTP, nomor Kartu Keluarga, NPWP dll.</td>
														</tr>

												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 4-->

						<!--begin: Form Wizard Step 5-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="security">
							<div class="kt-heading kt-heading--md">Security System</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v3__review">
									<div class="row">
									<?PHP 
										$n = 0;
										foreach($getdatasecurity as $security){ 
											$n++;
											if($security['mandatory']==1){ $md = '*'; }else{ $md = ''; }
											if($security['type_upload']=='PDF'){ $type = 'application/pdf'; }
											if($security['type_upload']=='IMAGES'){ $type = 'image/*'; }
											if($security['type_upload']=='MS.WORD'){ $type = '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'; }
											if($security['type_upload']=='MS.EXCEL'){ $type = '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'; }

									?>
										<div class="col-md-6">
											<div class="form-group">
												<label><b><?PHP echo $n; ?>. Dokumen <?PHP echo $security['nama_sdlc'];?> <?PHP echo $md;?></b>
												</label>
												<div class="input-group">
													<a href=''>
														<button type="button" class="btn btn-secondary btn-sm"><i class="flaticon-download"></i> Example</button> 
													</a>
													<a href='<?PHP echo base_url();?>attachment/helpdoc/<?PHP echo $sdlc['template_sdlc'];?>'>
														<button type="button" class="btn btn-secondary btn-sm"><i class="flaticon-download"></i> Template</button>
													</a> 
												</div>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docspasi<?PHP echo $security['id_sdlc'];?>" id="docspasi<?PHP echo $security['id_sdlc'];?>" accept="<?PHP echo $type;?>">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br> 
												<span class="form-text text-muted">Type Document <?PHP echo $security['type_upload']?></span>
											</div>
										</div>
									<?PHP } ?>
									</div>
									<!--div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence ToU (Terms Of Use)</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doctou[]" id="doctou" accept="image/*">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document Scrennshot PNG/JPG</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence Costumer Consent</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doccustcons[]" id="doccustcons" accept="image/*">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document Scrennshot PNG/JPG</span>
											</div>
										</div> 
									</div> 

									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence Multi Factor Authentication *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doctfa[]" id="doctfa" accept="image/*">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document Scrennshot PNG/JPG</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence NDE Hasil VA Test *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docss[]" id="docss" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div-->
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 5-->

						<!--begin: Form Wizard Step 6-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="sharing">
							<div class="kt-heading kt-heading--md">Data Sharing Telkom Group</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v3__review">
									 <div class="row">
									 	<div class="col-xl-12">
									 		<div class="form-group">
												<label>Upload Penyampaian Hasil DG Council Meeting Evidance jika terjadi data sharing antara telkom group atau pihak external.</label> 
											</div>
									 	</div>
										<div class="col-xl-12">
											<?PHP 
											foreach($getdatastgdoc as $stgdoc){
												if($stgdoc['mandatory']==1){ $md = '*'; }else{ $md = ''; }
												if($stgdoc['type_upload']=='PDF'){ $type = 'application/pdf'; }
												if($stgdoc['type_upload']=='IMAGES'){ $type = 'image/*'; }
												if($stgdoc['type_upload']=='MS.WORD'){ $type = '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'; }
												if($stgdoc['type_upload']=='MS.EXCEL'){ $type = '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'; }
											?>
											<div class="form-group">
												<label>Upload <?PHP echo $stgdoc['nama_sdlc']?> <?PHP echo $md;?></label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docspasi<?PHP echo $stgdoc['id_sdlc'];?>" id="docspasi<?PHP echo $stgdoc['id_sdlc'];?>" accept="<?PHP echo $type;?>">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document <?PHP echo $stgdoc['type_upload'];?></span>
											</div> 
											<?PHP 
											} 
											?> 
											 
										</div> 
									</div>
									 <div class="row">
										
										<div class="col-xl-12">
											<h6>Data Governance Council Meeting</h6>
											<span>
												Pertukaran Data antara Entitas Telkom Group : 
											</span>
											<ul>
												<li>Telkom -> Subsidiary</li>
												<li>Subsidiary -> Subsdiary</li> 
												<li>Pihak Eksternal (Third Party)</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 6-->

						<!--begin: Form Actions -->
						<div class="kt-form__actions">
							<!--div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev" id='btn-prev'>
								Previous
							</div-->
							<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" id="btn-draft">
								Save As Draft
							</div>
							<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" id="btn-submit">
								Submit
							</div>
							<!--div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next" id='btn-next'>
								Next Step
							</div-->
						</div>

						<!--end: Form Actions -->
					</form>

					<!--end: Form Wizard Form-->
				</div>
			</div>
		</div>
	</div>
	 
</div>
 