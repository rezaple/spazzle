<?PHP
error_reporting(0);


$getInnovation = $this->db->query("
				SELECT a.*,
				(SELECT picture from mi.user where userid=a.created_by) pictcreated,
				(select name from mi.user where userid=a.created_by)as usercreate,
				(SELECT name from mi.user where userid=a.created_by)as namecreated,
				(SELECT name from mi.user where userid=a.approve_by)as approve_name	
				FROM data_innovasi a
				where 1=1 and noinnovasi ='".$norequest."'")->result_array();
$row 			= array_shift($getInnovation);
$current = $row['current'];
if ($level==1) {
	if ($row['status']!=0 or $row['status']!=2) {
		$hideact = 'kt-hidden';
	} else {
		$hideact = '';
	}
} else if ($level!=1) {
	if ($row['current']==$level) {
		$hideact = '';
	} else {
		$hideact = 'kt-hidden';
	}
} else {
	$hideact = 'kt-hidden';
}

if ($row['pictcreated']=='') {
	$hidden 	= 'style="display:none;"';
} else {
	$hidden 	= '';
}
if($userid == $row['created_by']){
	$owner 	= true;
}else{
	$owner 	= false;
}
$pathfile 		= base_url().'attachment/';

$judul 			= $row['judul'];
$cfu_fu 		= $row['cfu_fu'];
$url 			= '<a href="'.$row['url_aplikasi'].'">'.$row['url_aplikasi'].'</a>';
$deskripsi 		= $row['deskripsi'];
$tujuan 		= $row['tujuan'];
$jenisdata 		= $row['jenis_data'];
$stakholder  	= $row['stakholder'];
$diagram 		= $pathfile.$row['diagram'];
$diagram2 		= $pathfile.$row['diagram2'];
$penjelasan	 	= $row['penjelasan_singkat'];
$pii_status 	= $row['pii_status'];
$sharing_group_status = $row['sharing_telkom_group'];

$status_innovation_data = $row['status_data_innovasi'];
$status_sdlc = $row['sdlc_status'];
$status_teknologi_platform = $row['upti_status'];
$status_pii = $row['status_pii'];
$status_upti = $row['status_upti'];
$status_security = $row['security_status'];
$status_sharing_group = $row['status_datasharing'];




$sdlc_required= 0;
//GET DATA DOC REQUIRMENT
$docreq = getdatadoc('Lihat Dokumen Requirment',$norequest,'Dok.Requirment');
$docdesign = getdatadoc('Lihat Dokumen Design',$norequest,'Dok.Design');
$docsop = getdatadoc('Lihat Dokumen SOP',$norequest,'Dok.SOP');
$docsopd2p = getdatadoc('Lihat Dokumen SOP D2P',$norequest,'Dok.SOP D2P');
$docsmp = getdatadoc('Lihat Dokumen SMP',$norequest,'Dok.SMP');
$doctes = getdatadoc('Lihat Dokumen Test Report / Test Plan',$norequest,'Dok.Test Plan/Report');
if($docreq=='-'){}else{$sdlc_required++;}
if($docdesign=='-'){}else{$sdlc_required++;}
if($docsop=='-'){}else{$sdlc_required++;}
if($docsopd2p=='-'){}else{$sdlc_required++;}
if($docsmp=='-'){}else{$sdlc_required++;}
if($doctes=='-'){}else{$sdlc_required++;}

if($sdlc_required < 6){
	$sdlc_status =true;
}else{
	$sdlc_status =false;
}

//GET DATA UPTI 
$li_fr='';
$stdstatus_fr = 0;
$notstdstatus_fr = 0;
$getupti = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Framework'")->result_array();	
foreach($getupti as $row_fr){
	$cekupti = $this->db->query("select * from param_framework where framework='".$row_fr['name_upti']."'")->num_rows();
	if($cekupti > 0){
		$stdstatus_fr++;
		$stdfr = 'STANDARD';
	}else{
		$notstdstatus_fr++;
		$stdfr = 'NOT STANDARD';
	}
	$li_fr .= "<li>".$row_fr['name_upti']." <small>(".$stdfr.")</small></li>";
} 
if($stdstatus_fr > 0){
	$showalertfr = false;
}else{
	$showalertfr = true;								
}

$li_db = '';
$stdstatus_db = 0;
$notstdstatus_db = 0;
$getupti2 = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Database'")->result_array();	
foreach($getupti2 as $row_db){
	$cekdb = $this->db->query("select * from param_database where nama_database='".$row_db['name_upti']."'")->num_rows();
	if($cekdb > 0){
		$stdstatus_db++;
		$stddb = 'STANDARD';
	}else{
		$notstdstatus_db++;
		$stddb = 'NOT STANDARD';
	}
	$li_db .= "<li>".$row_db['name_upti']." <small>(".$stddb.")</small></li>";
} 
if($stdstatus_db > 0){
	$showalertdv = false;
}else{
	$showalertdb = true;								
}

//GET DATA SECUITY SYSTEM
$security_count = 0;
$docToU = getdatadoc('Lihat Dokumen Evidance ToU (Terms of Use)',$norequest,'Evidance.ToU');
$docTFA = getdatadoc('Lihat Dokumen Multi Factor Authentication',$norequest,'Evidance.TFA');
$docNDE = getdatadoc('Lihat Dokumen NDA Hasil VA Test',$norequest,'Evidance.NDE VA Test');
$docCustomer = getdatadoc('Lihat Dokumen Customer Consent',$norequest,'Evidance.Customer Consent');

if($docToU=='-'){}else{$security_count++;}
if($docTFA=='-'){}else{$security_count++;}
if($docNDE=='-'){}else{$security_count++;}

if($security_count < 3){
	$security_status =true;
}else{
	$security_status =false;
}
$status = $row['status'];
if($status == 1){
	$label_text = 'success';
	$status_text = 'COMPLY';
}else if($status == 2){
	$label_text = 'danger';
	$status_text = 'NOT COMPLY';
}else{
	$label_text = 'warning';
	$status_text = 'PENDING';
}

//GET STATUS DOC
$st_docreq = getstatusdoc($norequest,'Dok.Requirment');
$st_docdesign = getstatusdoc($norequest,'Dok.Design');
$st_docsop = getstatusdoc($norequest,'Dok.SOP');
$st_docsopd2p = getstatusdoc($norequest,'Dok.SOP D2P');
$st_docsmp = getstatusdoc($norequest,'Dok.SMP');
$st_doctes = getstatusdoc($norequest,'Dok.Test Plan/Report');


$st_docToU = getstatusdoc($norequest,'Evidance.ToU');
$st_docTFA = getstatusdoc($norequest,'Evidance.TFA');
$st_docNDE = getstatusdoc($norequest,'Evidance.NDE VA Test');
$st_docCustomer = getstatusdoc($norequest,'Evidance.Customer Consent');

?>
<style>
	/* 
	  ##Device = Laptops, Desktops
	  ##Screen = B/w 1025px to 1280px
	*/
	@media (min-width: 1025px) and (max-width: 1365px) {
	  	.first-text {
			font-size:18px !important;
		}
		.second-text {
			font-size:12px !important;
		}
		#orange { 
			background-size: 130% 93% !important;
		}
		.btn.btn-wide{
			padding-left: 1.25rem !important;
   			padding-right: 1.25rem !important;
		}
		.mt15rem{
			font-size:8px !important;
		}	  
		.nav-tabs.nav-tabs-line .nav-item{
			margin-right:4px;
		}
	}
	/* 
	  ##Device = Tablets, Ipads (portrait)
	  ##Screen = B/w 768px to 1024px
	*/
	@media (min-width: 768px) and (max-width: 1024px) { 
	}
	/* 
	  ##Device = Tablets, Ipads (landscape)
	  ##Screen = B/w 768px to 1024px
	*/
	@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
	}
	/* 
	  ##Device = Low Resolution Tablets, Mobiles (Landscape)
	  ##Screen = B/w 481px to 767px
	*/
	@media (min-width: 481px) and (max-width: 767px) {
	}
	/* 
	  ##Device = Most of the Smartphones Mobiles (Portrait)
	  ##Screen = B/w 320px to 479px
	*/
	@media (min-width: 320px) and (max-width: 480px) {
	}
	.alert{
		padding :0rem 2rem !important;
	}
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.mt15rem { margin-top: 1.5rem; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	#jawaban .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	#history .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#history .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
	.kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 60px !important;
    	height: 60px !important;
	}
	/*#wfinfo .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#wfinfo .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}*/
	.nav-tabs.nav-tabs-line .nav-link{
		font-size:11px !important;
	}
	.tab-pane{
		font-size:12px !important;
		min-height:400px;
	} 
	.table-bordered th, .table-bordered td{
		border-color:#0b5e90;
	}
	p {
	    margin-top: 0;
	    margin-bottom: 0.2rem !important;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<!--begin::Portlet-->
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#detail" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<b><i class="flaticon flaticon-file-2"></i> Innovation Description</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#sdlc" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> SPASI
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tp" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Technology Platform
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#pii" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Personal Identifiable Information
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#ss" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Security System
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#stg" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Data Sharing Telkom Group
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#history" role="tab">
								<i class="flaticon flaticon-list-3"></i> History Document
							</a>
						</li>
					</ul>
				</h3>
			</div>
			<div id="bgkonten" class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-toolbar-wrapper"> 
					<!--button class="btn btn-sm btn-label-<?PHP echo $label_text;?> btn-pill"><?PHP echo $status_text;?></button>
					<!--div class="btn-group " role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-secondary btn-pill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" style="">
								<button class="btn btn-sm text-left kt-link kt-link--success col-sm-12 btnActDoc btn-icon-sm" data-toggle="modal" data-target="#modalaction" data-type="submit" data-id="">
									<i class="la la-rocket"></i>
									COMPLY
								</button>
								<button class="btn btn-sm text-left kt-link kt-link--danger col-sm-12 btnActDoc btn-icon-sm" data-toggle="modal" data-target="#modalaction" data-type="submit" data-id="">
									<i class="la la-rocket"></i>
									NOT COMPLY
								</button>
                        </div>
                    </div-->
				</div>
			</div>
		</div>

		<!-- MODAL ACTION -->
		<div class="modal fade" id="modalaction" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<form method="POST">
					<div class="modal-content row">
						 
					</div>
				</form>
			</div>
		</div>
		<!-- END MODAL ACTION -->

		<!-- MODAL APPROVAL -->
		<div class="modal fade" id="modalapprove" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">

					<form class="kt-form kt-form--label-right" id="forminsertsingle" enctype="multipart/form-data">
						<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Approve SBR | Please Submit Answer</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class='row'>
									<div class="col-lg-5">
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Nomor Request *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" id='app_id' name='app_id'>
													<input type="text" name="app_norequest" class="form-control" id="app_norequest" placeholder="Nomor Request" readonly="readonly">
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Subject *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="app_subject" class="form-control" id="app_subject" placeholder="Subject" readonly="readonly">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5"> 
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Pelanggan *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" name="app_nopelanggan" id="app_nopelanggan" value="">
													<input type="text" name="app_pelanggan" class="form-control" id="app_pelanggan" placeholder="Pelanggan" readonly="readonly">
												</div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">Service ID *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="text" name="app_serviceid" class="form-control" id="app_serviceid" placeholder="Service ID" readonly="readonly">
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-5"> 
										<div class="form-group row">
											<label class="col-form-label col-lg-4 col-sm-12">AM *</label>
											<div class="col-lg-8 col-md-8 col-sm-12">
												<div class='input-group'>
													<input type="hidden" name="app_userid" id="app_userid" value="">
													<input type="text" name="app_am" class="form-control" id="app_am" placeholder="AM" readonly="readonly">
												</div>
											</div>
										</div> 
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								<div class="input_fields_wrap">
									<div class="row">
										<div class="col-lg-12"> 
											<button type="button" class="btn btn-label-dark btn-pill add_field_button pull-right">Add More Fields</button>
										</div>
									</div>
									<div class="row">
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Layanan *</label>
														<div class='input-group'>
															<input type="text" name="layanan[]" class="form-control" id="layanan" placeholder="Layanan">
														</div>
													</div>
												</div> 
											</div>
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Volume *</label>
														<div class='input-group'>
															<input type="text" name="volume[]" class="form-control" id="volume" placeholder="Volume">
														</div>
													</div>
												</div> 
											</div>
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Tarif *</label>
														<div class='input-group'>
															<input type="text" name="tarif[]" class="form-control" id="tarif" placeholder="Tarif">
														</div>
													</div>
												</div> 
											</div>
											<div class="col-lg-3"> 
												<div class="form-group row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<label>Price *</label>
														<div class='input-group'>
															<input type="text" name="price[]" class="form-control" id="price" placeholder="Price">
														</div>
													</div>
												</div>
											</div>
									</div>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Analisa Teknis /Bisnis *</label>
											<div class="col-sm-12">
												<textarea name="analisateknis" class="summernote" id="analisateknis"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" id="saveapproved" class="btn btn-success"><i class="la la-rocket"></i> Approve</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- END MODAL APPROVAL -->

		<div class="kt-portlet__body">
		<!-- MODAL PREVIEW -->
		<div class="modal fade" id="modalpreview" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
						<!--div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">Dokumen</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
							</div>
						</div-->
						<div class="kt-portlet__body">
							<div class='row'>
								<div class="col-lg-12" style='min-height:500px;'>
									<iframe src="" id='embedsrc' type="application/pdf" width="100%" height="100%">
									</iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END MODAL PREVIEW -->
			<div class="tab-content">
				<div class="tab-pane active" id="detail" role="tabpanel">
					<div class="row">  
							<?PHP 
							if($status_innovation_data ==1 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Inovasi Description' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-success kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div> 
								</div> ';
							}else if($status_innovation_data==2 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Inovasi Description' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-warning kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">NOT COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div>
								</div> ';
							}else{
								echo '<div class="col-md-12"></div>';
							}
							?> 
						<div class="col-lg-12">
							<div>
								<?PHP 
									if($status_innovation_data ==1){
								?>
								<table class='table table-bordered' style='border:solid 1px #013453;'>
									<tbody>
									<tr>
										<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>Judul:</b></td>
										<td><?PHP echo $judul;?></td>
										<td rowspan='4' colspan='2' style='padding: 0rem;'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
											<div style='padding:8px;'>
												<center>
													<a class="example-image-link" href="<?PHP echo $diagram; ?>" data-lightbox="example-set" data-title="Arsitektur Aplikasi">
													<img class="example-image" src="<?PHP echo $diagram; ?>" height='100px' alt=""/>
													</a>
												</center>
												<hr>
												<center>
													<a class="example-image-link" href="<?PHP echo $diagram2; ?>" data-lightbox="example-set" data-title="Flow Proses Aplikasi">
														<img class="example-image" src="<?PHP echo $diagram2; ?>" height='100px' alt=""/>
													</a>
												</center>
											</div>
										</td>
									</tr>
									<tr>
										<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU:</b></td>
										<td><?PHP echo $cfu_fu;?></td>
										
									</tr>
									<tr>
										<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL:</b></td>
										<td><?PHP echo $url;?></td>
									</tr>
									<tr>
										<td colspan='2' style='width:50%;padding: 0rem'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
											<div style='padding:8px;'>
												<b>Deskripsi:</b>
												<?PHP echo str_replace("<div><br></div>", "", $deskripsi);?> 
												<hr>
												<b>Tujuan:</b>
												<?PHP echo str_replace("<div><br></div>", "", $tujuan);?> 
											</div>
										</td>
									</tr>

 									<tr>
										<td colspan='2' style='padding: 0rem'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
											<div class="row" style='padding:8px;'>
												<div class="col-md-6">
													<b>Jenis Data:</b>
													<?PHP echo str_replace("<div><br></div>", "", $jenisdata);?> 
												</div>
												<div class="col-md-6">
													<b>Stakeholder:</b>
													<?PHP echo str_replace("<div><br></div>", "", $stakholder);?> 
												</div>
											</div>
										</td>
										<td colspan='2' style='padding: 0rem;'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasang Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
											<div style='padding:8px;'>
												<?PHP echo $penjelasan?>
											</div>
										</td>
									 
									</tr>
									</tbody>
								</table>
								<?PHP }else if($status_innovation_data==2 and $owner == true){ ?>
								<form class="kt-form kt-form--label-right" id="forminsertsingle" enctype="multipart/form-data">
								<table class='table table-bordered' style='border:solid 1px #013453;'>
										<tbody>
										<tr>
											<input type='hidden' name='noinnovasi' value='<?PHP echo $norequest;?>'>
											<input type='hidden' name='file_diagram_1' value='<?PHP echo $row['diagram'];?>'>
											<input type='hidden' name='file_diagram_2' value='<?PHP echo $row['diagram2'];?>'>

											<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>Judul:</b></td>
											<td>
												<input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul INOVASI" value="<?PHP echo $judul;?>">
											</td>
											<td rowspan='5' colspan='2' style='padding: 0rem;'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
												<div style='padding:8px;vertical-align: middle'>
													<small>Arsitektur Aplikasi</small>
													<center>
														 <input type="file" class="form-control" name="diagram" id="diagram" accept="image/*"> 
														 <small>#Kosongkan jika tidak akan mengupdate file Arsitektur Aplikasi</small>
													</center>
													<hr>
 												 	<small>Flow Proses Aplikasi</small>
													<center>
														 <input type="file" class="form-control" name="diagram2" id="diagram2" accept="image/*">
														 <small>#Kosongkan jika tidak akan mengupdate file Flow Proses Aplikasi</small>
													</center>
												</div>
											</td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU:</b></td>
											<td>
												<select class="form-control m-select2" id="cfu_fu" name="cfu_fu[]" multiple="multiple">
													<option value='TREG-1'>TREG-1</option>
													<option value='TREG-2'>TREG-2</option>
													<option value='TREG-3'>TREG-3</option>
													<option value='TREG-4'>TREG-4</option>
													<option value='TREG-5'>TREG-5</option>
													<option value='TREG-6'>TREG-6</option>
													<option value='TREG-7'>TREG-7</option>
													<option value='AHCI'>AHCI</option>
													<option value='DWS'>DWS</option>
													<option value='DSS'>DSS</option>
													<option value='DSO'>DSO</option>
													<option value='EBIS'>EBIS</option> 
													<option value='PND'>PND</option>
													<option value='NITS'>NITS</option>
													<option value='HCM'>HCM</option>
													<option value='HCIS'>HCIS</option>
													<option value='WINS'>WINS</option>
													<option value='IMA'>IMA</option>
													<option value='DDS'>DDS</option>
													<option value='DIT'>DIT</option>
													<option value='HCB'>HCB</option>
												</select>
											</td>
											
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL:</b></td>
											<td><input type="text" value="<?PHP echo $row['url_aplikasi'];?>" class="form-control" id="url" name="url" placeholder="URL Aplikasi"></td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>DOMAIN:</b></td>
											<td>
												<select class="form-control m-select2" id="domain" name="domain">
													<option value='Partner Mgmt'>Partner Mgmt</option>
													<option value='Analytics'>Analytics</option>
													<option value='Security Mgmt'>Security Mgmt</option>
													<option value='Performance Mgmt'>Performance Mgmt</option>
													<option value='Product Mgmt'>Product Mgmt</option>
													<option value='Fullfilment Mgmt'>Fullfilment Mgmt</option>
													<option value='Assurance Mgmt'>Assurance Mgmt</option>
													<option value='Data & Inventory Mgmt'>Data & Inventory Mgmt</option> 
												</select>
											</td>
										</tr>
										<tr>
											<td colspan='2' style='width:50%;padding: 0rem'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
												<div style='padding:8px;'>
													<b>Deskripsi:</b>
													<textarea name="deskripsi" class="summernote" id="deskripsi"><?PHP echo str_replace("<div><br></div>", "", $deskripsi);?> </textarea>
													<hr>
													<b>Tujuan:</b>
													<textarea name="tujuan" class="summernote" id="tujuan"><?PHP echo str_replace("<div><br></div>", "", $tujuan);?> </textarea>
												</div>
											</td>
										</tr>

	 									<tr>
											<td colspan='2' style='padding: 0rem'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
												<div class="row" style='padding:8px;'>
													<div class="col-md-6">
														<b>Jenis Data:</b>
														<textarea name="jenis_data" class="summernote" id="jenis_data"><?PHP echo str_replace("<div><br></div>", "", $jenisdata);?> </textarea>

													</div>
													<div class="col-md-6">
														<b>Stakeholder:</b>
														<textarea name="stakholder" class="summernote" id="stakholder"><?PHP echo str_replace("<div><br></div>", "", $stakholder);?> </textarea>
													</div>
												</div>
											</td>
											<td colspan='2' style='padding: 0rem;'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasan Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
												<div style='padding:8px;'>
													<br>
													 <textarea name="penjelasan" class="summernote" id="penjelasan"><?PHP echo $penjelasan?></textarea>
												</div>
											</td>
										 
										</tr>
										</tbody>
								</table> 
								<center>
									<button type="submit" id="saverepublish" class="btn btn-success"><i class="la la-rocket"></i> UPDATE INNOVATION DESCRIPTION </button>

								</center>
								<?PHP 
								}else{
								?>
								<table class='table table-bordered' style='border:solid 1px #013453;'>
									<tbody>
									<tr>
										<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>Judul:</b></td>
										<td><?PHP echo $judul;?></td>
										<td rowspan='4' colspan='2' style='padding: 0rem;'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
											<div style='padding:8px;'>
												<center>
													<a class="example-image-link" href="<?PHP echo $diagram; ?>" data-lightbox="example-set" data-title="Arsitektur Aplikasi">
													<img class="example-image" src="<?PHP echo $diagram; ?>" height='100px' alt=""/>
													</a>
												</center>
												<hr>
												<center>
													<a class="example-image-link" href="<?PHP echo $diagram2; ?>" data-lightbox="example-set" data-title="Flow Proses Aplikasi">
														<img class="example-image" src="<?PHP echo $diagram2; ?>" height='100px' alt=""/>
													</a>
												</center>
											</div>
										</td>
									</tr>
									<tr>
										<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU:</b></td>
										<td><?PHP echo $cfu_fu;?></td>
										
									</tr>
									<tr>
										<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL:</b></td>
										<td><?PHP echo $url;?></td>
									</tr>
									<tr>
										<td colspan='2' style='width:50%;padding: 0rem'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
											<div style='padding:8px;'>
												<b>Deskripsi:</b>
												<?PHP echo str_replace("<div><br></div>", "", $deskripsi);?> 
												<hr>
												<b>Tujuan:</b>
												<?PHP echo str_replace("<div><br></div>", "", $tujuan);?> 
											</div>
										</td>
									</tr>

 									<tr>
										<td colspan='2' style='padding: 0rem'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
											<div class="row" style='padding:8px;'>
												<div class="col-md-6">
													<b>Jenis Data:</b>
													<?PHP echo str_replace("<div><br></div>", "", $jenisdata);?> 
												</div>
												<div class="col-md-6">
													<b>Stakeholder:</b>
													<?PHP echo str_replace("<div><br></div>", "", $stakholder);?> 
												</div>
											</div>
										</td>
										<td colspan='2' style='padding: 0rem;'>
											<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasang Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
											<div style='padding:8px;'>
												<?PHP echo $penjelasan?>
											</div>
										</td>
									 
									</tr>
									</tbody>
								</table>
								<?PHP	
								}
								?>
							</div>
						</div>
						

					</div>
					<!--div class="row">
						<div class="col-lg-6">
								<div class="form-group row">
									<label class="col-form-label col-lg-2 col-sm-12">Judul</label>
									<label class="col-form-label col-lg-10 col-sm-12">: Sample Innovation 1</label>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-2 col-sm-12">CFU/FU</label>
									<label class="col-form-label col-lg-10 col-sm-12">: EBIS</label>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-2 col-sm-12">URL</label>
									<label class="col-form-label col-lg-10 col-sm-12">: <a href='#'>https://managemeninnovasi.telkom.co.id</a></label>
								</div>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								<div class="form-group row">
									<label class="col-form-label col-lg-12 col-sm-12"><b>Deskripsi & Tujuan</b></label>
								</div>
								<ul>
									<li>
										<div class="form-group row">
										<label class="col-form-label col-lg-12 col-sm-12">Deskripsi :</label>
										<label class="col-form-label col-lg-12 col-sm-12">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
										</label>
										</div>
									</li>
									<li>
										<div class="form-group row">
											<label class="col-form-label col-lg-12 col-sm-12">Tujuan :</label>
											<label class="col-form-label col-lg-12 col-sm-12">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
											</label>
										</div>
									</li>
								</ul>
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								<div class="form-group row">
									<label class="col-form-label col-lg-12 col-sm-12"><b>Jenis Data & Stakeholder</b></label>
								</div>
								<ul>
									<li>
										<div class="form-group row">
										<label class="col-form-label col-lg-12 col-sm-12">Jenis Data :</label>
										<label class="col-form-label col-lg-12 col-sm-12">
											Lorem Ipsum is simply dummy text of the printing and typesetting industry.  
										</label>
										</div>
									</li>
									<li>
										<div class="form-group row">
											<label class="col-form-label col-lg-12 col-sm-12">Stakeholder :</label>
											<label class="col-form-label col-lg-12 col-sm-12">
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
											</label>
										</div>
									</li>
								</ul>
						</div>
						<div class="col-lg-6">
							<div class="form-group row"> 
								<label class="col-form-label col-lg-12 col-sm-12"><b>Infrastruktur Aplikasi</b></label>
								<label class="col-form-label col-lg-12 col-sm-12">
									<center><img src='<?PHP echo base_url();?>images/samplediagram.png'></center>
								</label>
							</div>

							<div class="form-group row"> 
								<label class="col-form-label col-lg-12 col-sm-12"><b>Penjelasang Singkat Infrastruktur Aplikasi</b></label>
								<label class="col-form-label col-lg-12 col-sm-12">
									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</label>
							</div>
						</div>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->					 
				</div>
				<div class="tab-pane" id="sdlc" role="tabpanel"> 
					<div class="row">
						<div class="col-md-12">
							<h5>DOKUMEN SPASI</h5>							 
						</div>   
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?PHP 
							$n=0;
							foreach($getdataspasi as $spasi){
								$n++;
							?>
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong><?PHP echo $n;?>. Dokumen <?PHP echo $spasi['nama_sdlc'];?> :</strong></label>
											<div class="col-9">
												<?PHP 
													$getDoc = $this->db->query("
																	SELECT a.*	
																	FROM data_innovasi_sdlc a
																	where 1=1 and noinnovasi ='".$norequest."' and type = '".$spasi['id_sdlc']."'")->result_array();
													$doc 			= array_shift($getDoc);
 													echo '<button type="button" class="btn btn-sm btn-secondary btn-pill btnpreview" data-toggle="modal" data-target="#modalpreview" data-file="'.$doc['doc'].'">'.$doc['doc'].'</button>';
												?>
											</div>
										</div> 
									</div> 
								</div>
							</div>
							<?PHP 
							} 
							?>
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong>Dokumen Requirment :</strong></label>
											<div class="col-9">
												<?PHP  echo $docreq; ?> 
											</div>
										</div> 
									</div> 
								</div>
								<?PHP 
								if($owner ==true and $st_docreq !=0){
									$ghistory = "SELECT 
													*,
												(select name from mi.user where userid = a.created_by) create_by
												FROM mi.data_innovasi_history a where a.judul ='Review SDLC | Dok.Requirment' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									$action_req = $rowhistory['action'];
									if($st_docreq==1){
										$colorbtn = 'success';
										$hidden = 'style="display:none;"';
									}else{
										$colorbtn = 'danger';
										$hidden = '';
									}
								?>
								<div class="row">  
									<div class="col-md-6"> 
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label"><strong>STATUS :</strong></label>
													<div class="col-9">
														<div class="kt-radio-inline">
															<button class="btn btn-sm btn-label-<?PHP echo $colorbtn;?> btn-pill"><?PHP echo $action_req; ?></button>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label"><strong>Komentar :</strong></label>
													<div class="col-9">
														 <?PHP echo $rowhistory['comment'];?>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="col-md-6" <?PHP echo $hidden;?>> 
										<div class="row">  
											<form class="kt-form kt-form--label-right" id="formrequirment" enctype="multipart/form-data">
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Update Dok.Requirment :</strong></label>
													<div class="col-8">
														<div id="bgattach">
															<div class="input-group">
											                    <input type="file" class="form-control" name="docrequirmen[]" id="docrequirmen" accept="application/pdf">
											                    <a href="#" class="col-lg-1">
											                    </a>
											                </div>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
													<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>
													<button type="submit" id="savedocreq" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK.REQUIRMENT
													</button>  
											</div>
											</form>
										</div>
									</div>
								</div>
								<?PHP
								}
								?>
							</div>
							<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->

							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong>Dokumen Design :</strong></label>
											<div class="col-9">
												<?PHP  echo $docdesign; ?> 
											</div>
										</div> 
									</div> 
								</div>
								<?PHP 
								if($owner ==true and $st_docdesign !=0){
									$ghistory = "SELECT 
													*,
												(select name from mi.user where userid = a.created_by) create_by
												FROM mi.data_innovasi_history a where a.judul ='Review SDLC | Dok.Design' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									$action_dd = $rowhistory['action'];
									if($st_docdesign==1){
										$colorbtn_dd = 'success';
										$hidden = 'style="display:none;"';

									}else{
										$colorbtn_dd = 'danger';
										$hidden = '';

									}
								?>
								<div class="row">  
									<div class="col-md-6"> 
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
													<div class="col-9">
														<div class="kt-radio-inline">
															<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_dd;?> btn-pill"><?PHP echo $action_dd; ?></button>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
													<div class="col-9">
														 <?PHP echo $rowhistory['comment'];?>
													</div>
												</div> 
											</div> 
										</div>
									</div>
									<div class="col-md-6" <?PHP echo $hidden;?>> 
										<div class="row">  
											<form class="kt-form kt-form--label-right" id="formdesign" enctype="multipart/form-data">

											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Update Dok.Design :</strong></label>
													<div class="col-8">
														<div id="bgattach">
															<div class="input-group">
											                    <input type="file" class="form-control" name="docdesign[]" id="docdesign" accept="application/pdf">
											                    <a href="#" class="col-lg-1">
											                    </a>
											                </div>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
													<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'> 
													<button type="submit" id="savedocdesign" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK.DESIGN
													</button>  
											</div>

											</form>
										</div>
									</div>
								</div>
								<?PHP
								}
								?>
							</div>
							<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->
							
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong>Dokumen SOP :</strong></label>
											<div class="col-9">
												<?PHP  echo $docsop; ?> 
											</div>
										</div> 
									</div> 
								</div>
								<?PHP 
								if($owner ==true and $st_docsop !=0){
									$ghistory = "SELECT 
													*,
												(select name from mi.user where userid = a.created_by) create_by
												FROM mi.data_innovasi_history a where a.judul ='Review SDLC | Dok.SOP' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									$action_sop = $rowhistory['action'];
									if($st_docsop==1){
										$colorbtn_sop = 'success';
										$hidden = 'style="display:none;"';
									}else{
										$colorbtn_sop = 'danger';
										$hidden = '';
									}
								?>
								<div class="row">  
									<div class="col-md-6"> 
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
													<div class="col-9">
														<div class="kt-radio-inline">
															<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_sop;?> btn-pill"><?PHP echo $action_sop; ?></button>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
													<div class="col-9">
														 <?PHP echo $rowhistory['comment'];?>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="col-md-6" <?PHP echo $hidden; ?>> 
										<div class="row">  
											<form class="kt-form kt-form--label-right" id="formsop" enctype="multipart/form-data">

											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Update Dok.SOP :</strong></label>
													<div class="col-8">
														<div id="bgattach">
															<div class="input-group">
											                    <input type="file" class="form-control" name="docsop[]" id="docsop" accept="application/pdf">
											                    <a href="#" class="col-lg-1">
											                    </a>
											                </div>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
													<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'> 
													<button type="submit" id="savedocsop" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK.SOP
													</button>  
											</div>
											</form>
										</div>
									</div>
								</div>
								<?PHP
								}
								?>
							</div>
							<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->

							 

							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong>Dokumen SOP D2P:</strong></label>
											<div class="col-9">
												<?PHP  echo $docsopd2p; ?> 
											</div>
										</div> 
									</div> 
								</div>
								<?PHP 
								if($owner ==true and $st_docsopd2p !=0){
									$ghistory = "SELECT 
													*,
												(select name from mi.user where userid = a.created_by) create_by
												FROM mi.data_innovasi_history a where a.judul ='Review SDLC | Dok.SOP D2P' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									$action_sopd2p = $rowhistory['action'];
									if($st_docsopd2p==1){
										$colorbtn_sopd2p = 'success';
										$hidden = 'style="display:none;"';
									}else{
										$colorbtn_sopd2p = 'danger';
										$hidden = '';
									}
								?>
								<div class="row">  
									<div class="col-md-6"> 
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
													<div class="col-9">
														<div class="kt-radio-inline">
															<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_sopd2p;?> btn-pill"><?PHP echo $action_sopd2p; ?></button>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
													<div class="col-9">
														 <?PHP echo $rowhistory['comment'];?>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="col-md-6" <?PHP echo $hidden;?>> 
										<div class="row">  
											<form class="kt-form kt-form--label-right" id="formsopd2p" enctype="multipart/form-data">

											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Update Dok.SOP D2P:</strong></label>
													<div class="col-8">
														<div id="bgattach">
															<div class="input-group">
											                    <input type="file" class="form-control" name="docsopd2p[]" id="docsopd2p" accept="application/pdf">
											                    <a href="#" class="col-lg-1">
											                    </a>
											                </div>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
													<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'> 
													<button type="submit" id="savedocsopd2p" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK.SOP D2P
													</button>  
											</div>
											</form>
										</div>
									</div>
								</div>
								<?PHP
								}
								?>
							</div>
							<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->

							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong>Dokumen SMP:</strong></label>
											<div class="col-9">
												<?PHP  echo $docsmp; ?> 
											</div>
										</div> 
									</div> 
								</div>
								<?PHP 
								if($owner ==true and $st_docsmp!=0){
									$ghistory = "SELECT 
													*,
												(select name from mi.user where userid = a.created_by) create_by
												FROM mi.data_innovasi_history a where a.judul ='Review SDLC | Dok.SMP' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									$action_smp = $rowhistory['action'];
									if($st_docsmp==1){
										$colorbtn_smp = 'success';
										$hidden = 'style="display:none;"';
									}else{
										$colorbtn_smp = 'danger';
										$hidden = '';
									}
								?>
								<div class="row">  
									<div class="col-md-6"> 
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
													<div class="col-9">
														<div class="kt-radio-inline">
															<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_smp;?> btn-pill"><?PHP echo $action_smp; ?></button>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
													<div class="col-9">
														 <?PHP echo $rowhistory['comment'];?>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="col-md-6" <?PHP echo $hidden;?>> 
										<div class="row"> 
											<form class="kt-form kt-form--label-right" id="formsmp" enctype="multipart/form-data"> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Update Dok.SMP :</strong></label>
													<div class="col-8">
														<div id="bgattach">
															<div class="input-group">
											                    <input type="file" class="form-control" name="docsmp[]" id="docsmp" accept="application/pdf">
											                    <a href="#" class="col-lg-1">
											                    </a>
											                </div>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12">
													<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
													<button type="submit" id="savedocsmp" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK.SMP
													</button>  
											</div>
											</form>
										</div>
									</div>
								</div>
								<?PHP
								}
								?>
							</div>
							<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->
							
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-3 col-form-label"><strong>Dokumen Test Plan & Test Report:</strong></label>
											<div class="col-9">
												<?PHP  echo $doctes; ?> 
											</div>
										</div> 
									</div> 
								</div>
								<?PHP 
								if($owner ==true and $st_doctes !=0){
									$ghistory = "SELECT 
													*,
												(select name from mi.user where userid = a.created_by) create_by
												FROM mi.data_innovasi_history a where a.judul ='Review SDLC | Dok.Test Plan & Test Report' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									$action_tes = $rowhistory['action'];
									if($st_doctes==1){
										$colorbtn_tes = 'success';
										$hidden = 'style="display:none;"';
									}else{
										$colorbtn_tes = 'danger';
										$hidden = '';
									}
								?>
								<div class="row">  
									<div class="col-md-6"> 
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
													<div class="col-9">
														<div class="kt-radio-inline">
															<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_tes;?> btn-pill"><?PHP echo $action_tes; ?></button>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
													<div class="col-9">
														 <?PHP echo $rowhistory['comment'];?>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="col-md-6" <?PHP echo $hidden;?>> 
										<div class="row">  
											<form class="kt-form kt-form--label-right" id="formsmp" enctype="multipart/form-data">
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Update Dok.Test Plan & Test Report :</strong></label>
													<div class="col-8">
														<div id="bgattach">
															<div class="input-group">
											                    <input type="file" class="form-control" name="doctes[]" id="doctes" accept="application/pdf">
											                    <a href="#" class="col-lg-1">
											                    </a>
											                </div>
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
													<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'> 
													<button type="submit" id="savedoctes" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK.TES
													</button>  
											</div>
											</form>
										</div>
									</div>
								</div>
								<?PHP
								}
								?>
							</div>
							<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div--> 						 
						</div>
					</div>
				</div> 
				<div class="tab-pane" id="tp" role="tabpanel"> 
					<div class="row">
						<div class="col-md-6">
							<h5>TEKNOLOGI PLATFORM</h5>							 
						</div>
						<div class="col-md-12">
							<?PHP 
							if($status_upti ==1 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Inovasi Description' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-success kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div> 
								</div> ';
							}else if($status_upti==2 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Inovasi Description' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-warning kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">NOT COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div>
								</div> ';
							}else{
								echo '<div class="col-md-12"></div>';
							}
							?> 
						</div>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<div class="row">
						<div class="col-md-6">
							<h6>LANGUAGE & FRAMEWORK</h6>							 
							<?PHP if($showalertfr == true) { ?>
							<div class="col-md-12">
								<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Not Standard Language & Framework</div>
							</div>						
							</div>
							<?PHP } else { ?>
							<div class="col-md-12">
								<div class="alert alert-success" role="alert">
								<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
								<div class="alert-text">Standard Language & Framework</div>
							</div>						
							</div>
							<?PHP } ?>
							<ul style='color:black'>
							<?PHP
								echo $li_fr;
							?>
							</ul>
						</div>
						<div class="col-md-6">
							<h6>DATABASE</h6>							 
							<?PHP if($showalertdb == true) { ?>
							<div class="col-md-12">
								<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Not Standard Database</div>
							</div>						
							</div>
							<?PHP } else { ?>
							<div class="col-md-12">
								<div class="alert alert-success" role="alert">
								<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
								<div class="alert-text">Standard Database</div>
							</div>						
							</div>
							<?PHP } ?>
							<ul>
							<?PHP
								echo $li_db;
							?>
							</ul> 
						</div>	
					</div>
					<?PHP if($owner ==true and $status_upti==2){ ?>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<form id="formupti" enctype="multipart/form-data"> 
					<div class="row">
						<div class="col-6">
							<div class="form-group row">
								<label class="col-12 col-form-label">Language & Framework</label>
								<div class="col-12">
									<div class="kt-checkbox-list">
										<?PHP
										foreach($getdataframework as $gframe){
										?>
										<label class="kt-checkbox">
											<input type="checkbox" name='framework[]' value='<?PHP echo $gframe['framework'];?>'> <?PHP echo $gframe['framework'];?>
											<span></span>
										</label>
										<?PHP														
										} 
										?>
										
										<label class="kt-checkbox">
											<input type="checkbox" id='other_framework'> Other
											<span></span>
										</label>

									</div>
								</div>
							</div>
							<div class="form-group row" id='fr_wrapper' style='display:none;'> 
								<label class="col-form-label col-lg-12 col-sm-12">Other Language & Framework *</label>
								<div class="col-lg-12 col-md-9 col-sm-12">
									<div id="bgframework">
										<div class="input-group">
						                    <input type="text" class="form-control" name="otherframe[]" id="otherframe"> 
						                </div>
									</div>
									<br>
									<button type="button" class="btn btn-sm btn-default btnframework">
										<i class="flaticon flaticon-plus"></i> Tambah
									</button>
								</div> 
							</div>												
						</div>
						<div class="col-6">
							<div class="form-group row">
								<label class="col-12 col-form-label">Database</label>
								<div class="col-12">
									<div class="kt-checkbox-list">
										<?PHP
										foreach($getdatadatabase as $gbase){
										?>
										<label class="kt-checkbox">
											<input type="checkbox" name='database[]' value='<?PHP echo $gbase['nama_database'];?>'> <?PHP echo $gbase['nama_database'];?>
											<span></span>
										</label>
										<?PHP														
										} 
										?>
										<label class="kt-checkbox">
											<input type="checkbox" id='other_database'> Other
											<span></span>
										</label>

									</div>
								</div>
							</div>
							<div class="form-group row" id='db_wrapper' style='display:none;'> 
								<label class="col-form-label col-lg-12 col-sm-12">Other Database *</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div id="bgdatabase">
										<div class="input-group">
						                    <input type="text" class="form-control" name="otherdb[]" id="otherdb"> 
						                </div>
									</div>
									<br>
									<button type="button" class="btn btn-sm btn-default btndatabase">
										<i class="flaticon flaticon-plus"></i> Tambah
									</button>
								</div> 
							</div>
							
						</div>
					</div> 
					<div class="row">
						<div class="col-12">
							<center>
								<div class="form-group row">
									<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
									<button type="submit" id="saveupti" class="btn btn-success"><i class="la la-rocket"></i> UPDATE UPTI
									</button>
								</div>
							</center>
						</div>
					</div>
					</form>
					<?PHP } ?>
				</div> 
				<div class="tab-pane" id="pii" role="tabpanel"> 
					<div class="row">
						<div class="col-md-12">
							<h5>PERSONAL IDENTIFIABLE INFORMATION</h5>							 
						</div>
						<div class="col-md-12">
							<?PHP 
							if($status_pii ==1 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review  PII (Personal Identifiable Information)' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-success kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div> 
								</div> ';
							}else if($status_pii==2 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review  PII (Personal Identifiable Information)' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-warning kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">NOT COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div>
								</div> ';
							?>
							<div class="row" id='uepox'>
								<div class="col-xl-6">
								</div>
								<div class="col-xl-6">
									<form class="kt-form kt-form--label-right" id="formpii" enctype="multipart/form-data"> 
									<div class="form-group">
										<label>Upload Evidance Persetujuan Owner *</label>
										<div id="bgattach">
											<div class="input-group">
							                    <input type="file" class="form-control" name="docpii" id="docpii">
							                    <a href="#" class="col-lg-1">
							                    </a>
							                </div>
										</div>
										<br>
										<span class="form-text text-muted">Type Document PDF</span>
									</div>
									<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
									<button type="submit" id="savedocpii" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOKUMEN
									</button>
									</form>
								</div> 
							</div>		
							<?PHP
							}else{
								echo '<div class="col-md-12"></div>';
							}
							?>  
						</div>
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
					</div>
					<div class="row">
						<?PHP 
						$file_pii = './attachment/'.$row['pii_evidance'];
						if (file_exists($file_pii)) {
						    $file_pii_exi = true;
						} else {
						    $file_pii_exi = false;
						}
						?>
						<?PHP if($row['pii_evidance'] ='') { ?>
						<?PHP if($file_pii_exi == false) { ?>
						<div class="col-md-12">
							<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Dokumen Persetujuan Owner Tidak ditemukan.</div>
							</div>						
						</div>
						<?PHP } else { ?>
							<div class="col-md-12">
								<div class="alert alert-success" role="alert">
									<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
									<div class="alert-text">Dokumen Persetujuan Owner ditemukan</div>
								</div>						
							</div>
							<?PHP  
							if($file_pii_exi == true){
							?>
							<div class="col-md-12">
								<object data="<?PHP echo base_url();?>attachment/<?PHP echo $row['pii_evidance'];?>" type="application/pdf" width="100%" height="400px">
								</object>
							</div>
							<?PHP 
							}else{
								echo "<center>File Broken Or Not Exist , Please Contact Administrator !!!</center>";
							}
							?>
						<?PHP } ?> 
						<?PHP 
						}else{
						?>
						<div class="col-md-12">
							<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Dokumen Persetujuan Owner Tidak ditemukan.</div>
							</div>						
						</div>
						<?PHP
						}
						?>
					</div>
				</div> 			 
				<div class="tab-pane" id="ss" role="tabpanel"> 
					<div class="row">
						<div class="col-md-6">
							<h5>SECURITY SYSTEM</h5>							 
						</div>
						<div class="col-md-6">
							<?PHP 
							if($status_security ==1){
								echo '
								<div class="kt-timeline-v2__item-text  kt-padding-top-5 pull-right" style="padding:0.35rem 0 0 0rem;">
	                                <i class="la la-tag"></i> <span style="color:green;">COMPLY</span>  | 
	                                <i class="la la-clock-o"></i> '.$this->formula->TanggalIndo($row['approve_at']).'  | 
	                                <i class="la la-user"></i>  '.$row['approve_name'].'		                                
		                       	</div>';
							}else if($status_security==2){
								echo '
								<div class="kt-timeline-v2__item-text  kt-padding-top-5 pull-right" style="padding:0.35rem 0 0 0rem;">
	                                <i class="la la-tag"></i> <span style="color:red">NOT COMPLY</span>  | 
	                                <i class="la la-clock-o"></i> 04 Oktober 2020 | 
	                                <i class="la la-user"></i>  '.$row['approve_name'].'		                                
		                       	</div>';
							}else{
								echo '';
							}
							?>
						</div>
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
						<?PHP if($security_status == true) { ?>
						<div class="col-md-12">
							<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Security System Not Comply</div>
							</div>						
						</div>
						<?PHP } ?>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Evidance ToU (Terms Of Use):</strong></label>
										<div class="col-9">
											<?PHP  echo $docToU; ?>  
										</div>
									</div> 
								</div> 
							</div>
							<?PHP 
							if($owner ==true and $st_docToU!=0){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Security System | Evidance.ToU (Terms Of Use)' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								$action_tou = $rowhistory['action'];
								if($st_docToU==1){
									$colorbtn_tou = 'success';
									$hidden = 'style="display:none;"';
								}else{
									$colorbtn_tou = 'danger';
									$hidden = '';
								}
							?>
							 
							<div class="row">  
								<div class="col-md-6"> 
									<div class="row">  
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
												<div class="col-9">
													<div class="kt-radio-inline">
														<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_tou;?> btn-pill"><?PHP echo $action_tou; ?></button>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
												<div class="col-9">
													 <?PHP echo $rowhistory['comment'];?>
												</div>
											</div> 
										</div>
									</div>
								</div>
								<div class="col-md-6" <?PHP echo $hidden;?>> 
									<div class="row"> 
										<form class="kt-form kt-form--label-right" id="formtou" enctype="multipart/form-data"> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-4 col-form-label"><strong>Update Evidance ToU :</strong></label>
												<div class="col-8">
													<div id="bgattach">
														<div class="input-group">
										                    <input type="file" class="form-control" name="doctou[]" id="doctou" accept="image/*">
										                    <a href="#" class="col-lg-1">
										                    </a>
										                </div>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12">
												<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
												<button type="submit" id="savedoctou" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK
												</button>  
										</div>
										</form>
									</div>
								</div>
							</div>
							<?PHP
							}
							?>
						</div>
						<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Evidance Multifactor Authentication:</strong></label>
										<div class="col-9">
											<?PHP  echo $docTFA; ?> 
										</div>
									</div> 
								</div> 
							</div>
							<?PHP 
							if($owner ==true and $st_docTFA!=0){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Security System | Evidance.TFA' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								$action_tfa = $rowhistory['action'];
								if($st_docTFA==1){
									$colorbtn_tfa = 'success';
									$hidden = 'style="display:none;"';
								}else{
									$colorbtn_tfa = 'danger';
									$hidden = '';
								}
							?>
							 
							<div class="row">  
								<div class="col-md-6"> 
									<div class="row">  
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
												<div class="col-9">
													<div class="kt-radio-inline">
														<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_tfa;?> btn-pill"><?PHP echo $action_tfa; ?></button>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
												<div class="col-9">
													 <?PHP echo $rowhistory['comment'];?>
												</div>
											</div> 
										</div>
									</div>
								</div>
								<div class="col-md-6" <?PHP echo $hidden;?>> 
									<div class="row"> 
										<form class="kt-form kt-form--label-right" id="formtfa" enctype="multipart/form-data"> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-4 col-form-label"><strong>Update Multifactor Authentication :</strong></label>
												<div class="col-8">
													<div id="bgattach">
														<div class="input-group">
										                    <input type="file" class="form-control" name="doctfa[]" id="doctfa" accept="image/*">
										                    <a href="#" class="col-lg-1">
										                    </a>
										                </div>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12">
												<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
												<button type="submit" id="savedoctfa" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK
												</button>  
										</div>
										</form>
									</div>
								</div>
							</div>
							<?PHP
							}
							?>
						</div>
						<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Costumer Consenst:</strong></label>
										<div class="col-9">
											<?PHP  echo $docCustomer; ?> 
										</div>
									</div> 
								</div> 
							</div>
							<?PHP 
							if($owner ==true and $st_docCustomer!=0){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Security System | Evidance.Customer Consent' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								$action_cus = $rowhistory['action'];
								if($st_docCustomer==1){
									$colorbtn_docCustomer = 'success';
									$hidden = 'style="display:none;"';
								}else{
									$colorbtn_docCustomer = 'danger';
									$hidden = '';
								}
							?>
							 
							<div class="row">  
								<div class="col-md-6"> 
									<div class="row">  
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
												<div class="col-9">
													<div class="kt-radio-inline">
														<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_docCustomer;?> btn-pill"><?PHP echo $action_cus; ?></button>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
												<div class="col-9">
													 <?PHP echo $rowhistory['comment'];?>
												</div>
											</div> 
										</div>
									</div>
								</div>
								<div class="col-md-6" <?PHP echo $hidden;?>> 
									<div class="row"> 
										<form class="kt-form kt-form--label-right" id="formcustomer" enctype="multipart/form-data"> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-4 col-form-label"><strong>Update Costumer Consenst :</strong></label>
												<div class="col-8">
													<div id="bgattach">
														<div class="input-group">
										                    <input type="file" class="form-control" name="doccustomer[]" id="doccustomer" accept="image/*">
										                    <a href="#" class="col-lg-1">
										                    </a>
										                </div>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12">
												<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
												<button type="submit" id="savedoccustomer" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK
												</button>  
										</div>
										</form>
									</div>
								</div>
							</div>
							<?PHP
							}
							?>
						</div>
						<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Evidence NDE Hasil VA Test:</strong></label>
										<div class="col-9">
											<?PHP  echo $docNDE; ?> 
										</div>
									</div> 
								</div> 
							</div>
							<?PHP 
							if($owner ==true and $st_docNDE!=0){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Security System | Evidance.NDE VA Test' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								$action_nde = $rowhistory['action'];
								if($st_docNDE==1){
									$colorbtn_nde = 'success';
									$hidden = 'style="display:none;"';
								}else{
									$colorbtn_nde = 'danger';
									$hidden = '';
								}
							?>
							 
							<div class="row">  
								<div class="col-md-6"> 
									<div class="row">  
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">STATUS :</label>
												<div class="col-9">
													<div class="kt-radio-inline">
														<button class="btn btn-sm btn-label-<?PHP echo $colorbtn_nde;?> btn-pill"><?PHP echo $action_nde; ?></button>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-3 col-form-label">Komentar :</label>
												<div class="col-9">
													 <?PHP echo $rowhistory['comment'];?>
												</div>
											</div> 
										</div>
									</div>
								</div>
								<div class="col-md-6" <?PHP echo $hidden;?>> 
									<div class="row"> 
										<form class="kt-form kt-form--label-right" id="formsmp" enctype="multipart/form-data"> 
										<div class="col-md-12"> 
											<div class="form-group row">
												<label for="example-text-input" class="col-4 col-form-label"><strong>Update Evidance NDE Hasil VA Test :</strong></label>
												<div class="col-8">
													<div id="bgattach">
														<div class="input-group">
										                    <input type="file" class="form-control" name="docnde[]" id="docnde" accept="application/pdf">
										                    <a href="#" class="col-lg-1">
										                    </a>
										                </div>
													</div>
												</div>
											</div> 
										</div> 
										<div class="col-md-12">
												<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
												<button type="submit" id="savedocnde" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOK
												</button>  
										</div>
										</form>
									</div>
								</div>
							</div>
							<?PHP
							}
							?>
						</div>
						<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
 
					</div>
				</div> 						
				<div class="tab-pane" id="stg" role="tabpanel"> 
					<div class="row">
						<div class="col-md-6">
							<h5>DATA SHARING TELKOM GROUP</h5>							 
						</div>
						<div class="col-md-12">
							<?PHP  
							if($status_sharing_group ==1 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Data Sharing Telkom Group' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-success kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div> 
								</div> ';
							}else if($status_sharing_group==2 and $owner ==true){
								$ghistory = "SELECT 
												*,
											(select name from mi.user where userid = a.created_by) create_by
											FROM mi.data_innovasi_history a where a.judul ='Review Data Sharing Telkom Group' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
								//echo $ghistory;
								$gethistory = $this->db->query($ghistory)->result_array();
								$rowhistory = array_shift($gethistory);	
								echo '
								<div class="kt-portlet kt-portlet--solid-warning kt-portletk-portlet--height-fluid">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>
											<h3 class="kt-portlet__head-title">NOT COMPLY</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											'.$rowhistory['create_by'].' | '.$this->formula->TanggalIndo($rowhistory['created_at']).'
										</div>
									</div>
									<div class="kt-portlet__body">
										<div class="kt-portlet__content">
											'.$rowhistory['comment'].'
										</div>
									</div>
								</div> ';
							?>
							<div class="row" id='uepoxa'>
								<div class="col-xl-6">
								</div>
								<div class="col-xl-6">
									<form class="kt-form kt-form--label-right" id="formstg" enctype="multipart/form-data"> 
									<div class="form-group">
										<label>Upload MoM DG Council Meeting</label>
										<div id="bgattach">
											<div class="input-group">
							                    <input type="file" class="form-control" name="docstg" id="docstg" accept="application/pdf">
							                    <a href="#" class="col-lg-1">
							                    </a>
							                </div>
										</div>
										<br>
										<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
											<i class="flaticon flaticon-plus"></i> Tambah Document
										</button-->
										<span class="form-text text-muted">Type Document PDF</span>
									</div>
									<input type='hidden' name='noinnovasi' id='noinnovasi' value='<?PHP echo $norequest;?>'>  
									<button type="submit" id="savedocstg" class="btn btn-success"><i class="la la-rocket"></i> UPDATE DOKUMEN
									</button>
									</form>
								</div> 
							</div>		
							<?PHP
							}else{
								echo '<div class="col-md-12"></div>';
							}
							?>  
						</div>
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
					</div>
					<div class="row">						
						<?PHP
						$file_stg = './attachment/'.$row['sharing_telkom_group'];
						if (file_exists($file_stg)) {
						    $sharing_group_status = true;
						} else {
						    $sharing_group_status = false;
						}
						?>
						<?PHP if($sharing_group_status == false) { ?>
						<div class="col-md-12">
							<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Dok MoM DG Council Meeting tidak ditemukan.</div>
							</div>						
						</div>
						<?PHP } else { ?>
						<div class="col-md-12">
							<div class="alert alert-success" role="alert">
								<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
								<div class="alert-text">MoM DG Council Meeting ditemukan.</div>
							</div>						
						</div>
						<div class="col-md-12">
							<object data="<?PHP echo base_url();?>attachment/<?PHP echo $row['sharing_telkom_group'];?>" type="application/pdf" width="100%" height="400px">
							</object>
						</div>
						<?PHP } ?> 
					</div>
				</div> 

				<div class="tab-pane" id="history" role="tabpanel">
					<div class="kt-scroll" data-scroll="true" data-height="380" data-mobile-height="300">
	                    <!--Begin::Timeline 3 -->
	                    <div class="kt-timeline-v2">
	                        <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	                        	<div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden; ?>" alt="Pic" style="width:40px !important;height:40px !important;" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $row['pictcreated']; ?>" />

											<?PHP if ($row['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($row['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless kt-font-brand"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    Pembuatan Data Innovation <br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($row['created_at']); ?> | 
		                                    <i class="la la-user"></i>  <?PHP echo $row['namecreated']; ?>
		                                </div>
	                                </div>
	                            </div>
	                        	<?PHP
	                        	$getHistory = $this->db->query("
	                        				SELECT a.*,
	                        					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
												(SELECT name from mi.user where userid=a.created_by) namecreated
	                        				FROM mi.data_innovasi_history a where noinnovasi='$norequest'
	                        				order by 1
	                        				")->result_array();
	                        				
	                        	foreach($getHistory as $history) {
	                        		if ($history['action']=='COMPLY') {
	                        			$colortl 	= 'kt-font-success';
	                        		} else if ($history['action']=='NOT COMPLY') {
	                        			$colortl 	= 'kt-font-danger';
	                        		} else{
	                        			$colortl 	= 'kt-font-brand';
	                        		}

	                        		if ($history['pictcreated']=='') {
										$hidden2 	= 'kt-hidden';
									} else {
										$hidden2 	= '';
									}

									// if ($history['action']=='new') {
									// 	$notiftext 	= 'Publish Pengajuan Innovation';
									// } else if ($history['action']=='republish') {
									// 	$notiftext 	= 'Perbaikan Pengajuan';
									// } else if ($history['action']=='escalation') {
									// 	$notiftext 	= 'Eskalasi Pengajuan';
									// } else if ($history['action']=='reject') {
									// 	$notiftext 	= 'Pengajuan tidak disetujui';
									// } else if ($history['action']=='approve') {
									// 	$notiftext 	= 'Pengajuan Innovation telah direview';
									// } 
									// else if ($history['action']=='return') {
									// 	$notiftext 	= 'Pengajuan telah dikembalikan';
									// } else if ($history['action']=='update') {
									// 	$notiftext 	= 'Perbaikan dan Eskalasi SBR';
									// }
									$notiftext = $history['judul'];
									$genValue 	= $history['namecreated'];
									$nickname 	= explode(' ',trim($genValue));
	                        	?>
	                            <div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden2; ?>" alt="Pic" style="width:40px !important;height:40px !important;" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $history['pictcreated']; ?>" />

											<?PHP if ($history['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($history['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless <?PHP echo $colortl; ?>"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    <?PHP echo $notiftext; ?><br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($history['created_at']); ?> | 
		                                    <i class="la la-user"></i>  <?PHP echo $history['namecreated']; ?>
	                                    	
	                                    	<div style="margin-top: 0.5rem;">
		                                    	<i class="la la-comment"></i>  Comment :<br>
		                                    	<?PHP echo $history['comment']; ?>
		                                    </div>
		                                </div>
	                                </div>
	                            </div>
	                            <?PHP } ?>
	                        </div>
	                    </div>
	                    <!--End::Timeline 3 -->
	                </div>
				</div> 

			</div>
		</div>
	</div>
</div>
