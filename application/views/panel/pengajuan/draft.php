<?PHP
error_reporting(0);
$getInnovation = $this->db->query("
				SELECT a.*,
				(SELECT picture from mi.user where userid=a.created_by) pictcreated,
				(select name from mi.user where userid=a.created_by)as usercreate,
				(SELECT name from mi.user where userid=a.created_by)as namecreated,
				(SELECT name from mi.user where userid=a.approve_by)as approve_name	
				FROM data_innovasi a
				where 1=1 and noinnovasi ='".$norequest."'")->result_array();
$row 			= array_shift($getInnovation);
$current = $row['current'];
if ($level==1) {
	if ($row['status']!=0 or $row['status']!=2) {
		$hideact = 'kt-hidden';
	} else {
		$hideact = '';
	}
} else if ($level!=1) {
	if ($row['current']==$level) {
		$hideact = '';
	} else {
		$hideact = 'kt-hidden';
	}
} else {
	$hideact = 'kt-hidden';
}

if ($row['pictcreated']=='') {
	$hidden 	= 'style="display:none;"';
} else {
	$hidden 	= '';
}
if($userid == $row['created_by']){
	$owner 	= true;
}else{
	$owner 	= false;
}
$pathfile 		= base_url().'attachment/';

$judul 			= $row['judul'];
$cfu_fu 		= $row['cfu_fu'];
$url 			= '<a href="'.$row['url_aplikasi'].'">'.$row['url_aplikasi'].'</a>';
$jenis_aplikasi = $row['jenis_aplikasi'];
$deskripsi 		= $row['deskripsi'];
$tujuan 		= $row['tujuan'];
$jenisdata 		= $row['jenis_data'];
$stakholder  	= $row['stakholder'];
$diagram 		= $pathfile.$row['diagram'];
$diagram2 		= $pathfile.$row['diagram2'];
$penjelasan	 	= $row['penjelasan_singkat'];
$pii_status 	= $row['status_pii'];
$sharing_group_status = $row['sharing_telkom_group'];
$domain = $row['domain'];

$status_innovation_data = $row['status_data_innovasi'];
$status_sdlc = $row['sdlc_status'];
$status_teknologi_platform = $row['upti_status'];
$status_pii = $row['status_pii'];
$status_upti = $row['status_upti'];
$status_security = $row['security_status'];
$status_sharing_group = $row['status_datasharing'];




$sdlc_required= 0;
//GET DATA DOC REQUIRMENT
$docreq = getdatadoc('Lihat Dokumen Requirment',$norequest,'Dok.Requirment');
$docdesign = getdatadoc('Lihat Dokumen Design',$norequest,'Dok.Design');
$docsop = getdatadoc('Lihat Dokumen SOP',$norequest,'Dok.SOP');
$docsopd2p = getdatadoc('Lihat Dokumen SOP D2P',$norequest,'Dok.SOP D2P');
$docsmp = getdatadoc('Lihat Dokumen SMP',$norequest,'Dok.SMP');
$doctes = getdatadoc('Lihat Dokumen Test Report / Test Plan',$norequest,'Dok.Test Plan/Report');
if($docreq=='-'){}else{$sdlc_required++;}
if($docdesign=='-'){}else{$sdlc_required++;}
if($docsop=='-'){}else{$sdlc_required++;}
if($docsopd2p=='-'){}else{$sdlc_required++;}
if($docsmp=='-'){}else{$sdlc_required++;}
if($doctes=='-'){}else{$sdlc_required++;}

if($sdlc_required < 6){
	$sdlc_status =true;
}else{
	$sdlc_status =false;
}

//GET DATA UPTI 
$li_fr='';
$stdstatus_fr = 0;
$notstdstatus_fr = 0;
$getupti = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Framework'")->result_array();	
foreach($getupti as $row_fr){
	$cekupti = $this->db->query("select * from param_framework where framework='".$row_fr['name_upti']."'")->num_rows();
	if($cekupti > 0){
		$stdstatus_fr++;
		$stdfr = 'STANDARD';
	}else{
		$notstdstatus_fr++;
		$stdfr = 'NOT STANDARD';
	}
	$li_fr .= "<li>".$row_fr['name_upti']." <small>(".$stdfr.")</small></li>";
} 
if($stdstatus_fr > 0){
	$showalertfr = false;
}else{
	$showalertfr = true;								
}

$li_db = '';
$stdstatus_db = 0;
$notstdstatus_db = 0;
$getupti2 = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Database'")->result_array();	
foreach($getupti2 as $row_db){
	$cekdb = $this->db->query("select * from param_database where nama_database='".$row_db['name_upti']."'")->num_rows();
	if($cekdb > 0){
		$stdstatus_db++;
		$stddb = 'STANDARD';
	}else{
		$notstdstatus_db++;
		$stddb = 'NOT STANDARD';
	}
	$li_db .= "<li>".$row_db['name_upti']." <small>(".$stddb.")</small></li>";
} 
if($stdstatus_db > 0){
	$showalertdv = false;
}else{
	$showalertdb = true;								
}

//GET DATA SECUITY SYSTEM
$security_count = 0;
$docToU = getdatadoc('Lihat Dokumen Evidance ToU (Terms of Use)',$norequest,'Evidance.ToU');
$docTFA = getdatadoc('Lihat Dokumen Multi Factor Authentication',$norequest,'Evidance.TFA');
$docNDE = getdatadoc('Lihat Dokumen NDA Hasil VA Test',$norequest,'Evidance.NDE VA Test');
$docCustomer = getdatadoc('Lihat Dokumen Customer Consent',$norequest,'Evidance.Customer Consent');

if($docToU=='-'){}else{$security_count++;}
if($docTFA=='-'){}else{$security_count++;}
if($docNDE=='-'){}else{$security_count++;}

if($security_count < 3){
	$security_status =true;
}else{
	$security_status =false;
}
$status = $row['status'];
if($status == 1){
	$label_text = 'success';
	$status_text = 'COMPLY';
}else if($status == 2){
	$label_text = 'danger';
	$status_text = 'NOT COMPLY';
}else{
	$label_text = 'warning';
	$status_text = 'PENDING';
}

//GET STATUS DOC
$st_docreq = getstatusdoc($norequest,'Dok.Requirment');
$st_docdesign = getstatusdoc($norequest,'Dok.Design');
$st_docsop = getstatusdoc($norequest,'Dok.SOP');
$st_docsopd2p = getstatusdoc($norequest,'Dok.SOP D2P');
$st_docsmp = getstatusdoc($norequest,'Dok.SMP');
$st_doctes = getstatusdoc($norequest,'Dok.Test Plan/Report');


$st_docToU = getstatusdoc($norequest,'Evidance.ToU');
$st_docTFA = getstatusdoc($norequest,'Evidance.TFA');
$st_docNDE = getstatusdoc($norequest,'Evidance.NDE VA Test');
$st_docCustomer = getstatusdoc($norequest,'Evidance.Customer Consent');
$notcomply = 0;

?>
<style>
	/* 
	  ##Device = Laptops, Desktops
	  ##Screen = B/w 1025px to 1280px
	*/
	@media (min-width: 1025px) and (max-width: 1365px) {
	  	.first-text {
			font-size:18px !important;
		}
		.second-text {
			font-size:12px !important;
		}
		#orange { 
			background-size: 130% 93% !important;
		}
		.btn.btn-wide{
			padding-left: 1.25rem !important;
   			padding-right: 1.25rem !important;
		}
		.mt15rem{
			font-size:8px !important;
		}	  
		.nav-tabs.nav-tabs-line .nav-item{
			margin-right:4px;
		}
	}
	/* 
	  ##Device = Tablets, Ipads (portrait)
	  ##Screen = B/w 768px to 1024px
	*/
	@media (min-width: 768px) and (max-width: 1024px) { 
	}
	/* 
	  ##Device = Tablets, Ipads (landscape)
	  ##Screen = B/w 768px to 1024px
	*/
	@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
	}
	/* 
	  ##Device = Low Resolution Tablets, Mobiles (Landscape)
	  ##Screen = B/w 481px to 767px
	*/
	@media (min-width: 481px) and (max-width: 767px) {
	}
	/* 
	  ##Device = Most of the Smartphones Mobiles (Portrait)
	  ##Screen = B/w 320px to 479px
	*/
	@media (min-width: 320px) and (max-width: 480px) {
	}
	.alert{
		padding :0rem 2rem !important;
	}
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.mt15rem { margin-top: 1.5rem; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	#jawaban .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	#history .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#history .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
	.kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 60px !important;
    	height: 60px !important;
	}
	/*#wfinfo .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#wfinfo .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}*/
	.nav-tabs.nav-tabs-line .nav-link{
		font-size:11px !important;
	}
	.tab-pane{
		font-size:12px !important;
		min-height:400px;
	} 
	.table-bordered th, .table-bordered td{
		border-color:#0b5e90;
	}
	p {
	    margin-top: 0;
	    margin-bottom: 0.2rem !important;
	}
	kt-section .kt-section__content.kt-section__content--solid {
	    padding: 1.5rem;
	    border-left: 4px solid #e2e5ec;
	    background-color: #f7f8fa;
	    border-radius-top-left: 4px;
	    border-radius-top-right: 4px;
	}
	#updatedraft {
	  border-radius: 20px;
	  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
	  background-color: #d8494f;
	  border-color:#d8494f;
	  color:white !important;
	  float:left;
	}
	#savenewresubmit_draft {
	  border-radius: 20px;
	  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
	  background-color: #d8494f;
	  border-color:#d8494f;
	   color:white !important;
	   float:right;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<!--begin::Portlet-->
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#detail" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<b><i class="flaticon flaticon-file-2"></i> Innovation Description</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#sdlc" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> SPASI
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tp" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Technology Platform
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#pii" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Personal Identifiable Information
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#ss" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Security System
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#stg" role="tab">
								<!-- <b><i class="flaticon flaticon-file-2"></i> <?PHP echo $norequest; ?></b> -->
								<i class="flaticon flaticon-file-2"></i> Data Sharing Telkom Group
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#history" role="tab">
								<i class="flaticon flaticon-list-3"></i> History Document
							</a>
						</li>
					</ul>
				</h3>
			</div>
			<div id="bgkonten" class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-toolbar-wrapper"> 
					<!--button class="btn btn-sm btn-label-<?PHP echo $label_text;?> btn-pill"><?PHP echo $status_text;?></button>
					<!--div class="btn-group " role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-secondary btn-pill dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" style="">
								<button class="btn btn-sm text-left kt-link kt-link--success col-sm-12 btnActDoc btn-icon-sm" data-toggle="modal" data-target="#modalaction" data-type="submit" data-id="">
									<i class="la la-rocket"></i>
									COMPLY
								</button>
								<button class="btn btn-sm text-left kt-link kt-link--danger col-sm-12 btnActDoc btn-icon-sm" data-toggle="modal" data-target="#modalaction" data-type="submit" data-id="">
									<i class="la la-rocket"></i>
									NOT COMPLY
								</button>
                        </div>
                    </div-->
				</div>
			</div>
		</div>

		<!-- MODAL ACTION -->
		<div class="modal fade" id="modalaction" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<form method="POST">
					<div class="modal-content row">
						 
					</div>
				</form>
			</div>
		</div>
		<!-- END MODAL ACTION --> 

		<div class="kt-portlet__body">
		<!-- MODAL PREVIEW -->
		<div class="modal fade" id="modalpreview" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
						<!--div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">Dokumen</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
							</div>
						</div-->
						<div class="kt-portlet__body">
							<div class='row'>
								<div class="col-lg-12" style='min-height:500px;'>
									<iframe src="" id='embedsrc' type="application/pdf" width="100%" height="100%">
									</iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END MODAL PREVIEW -->
		<form class="kt-form" id="formresubmit" enctype="multipart/form-data">
			<input type='hidden' name='noinnovasi' value='<?PHP echo $norequest;?>'>
			<div class="tab-content">
				<div class="tab-pane active" id="detail" role="tabpanel">
					<div class="row">  
						<div class="col-lg-12">
							<?PHP 
							if(($status_innovation_data==2 or $status_innovation_data==1) and $owner == true){ 	
							?> 
						 	<div class="kt-portlet kt-portlet--mobile"> 
								<div class="kt-portlet__body"> 
										<div class="kt-widget3">
											<?PHP 
											$ghistory = "SELECT *, (select name from mi.user where userid = a.created_by) create_by
														FROM mi.data_innovasi_history a where a.judul ='Review Inovasi Description' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
											$gethistory = $this->db->query($ghistory)->result_array();
											foreach($gethistory as $rowhistory){
												if($rowhistory['action']=='COMPLY'){
													$color ='success';
												}else{
													$color ='danger';
												}
											?>
											<div class="kt-widget3__item">
												<div class="kt-widget3__header">
													<div class="kt-widget3__user-img">
														<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
													</div>
													<div class="kt-widget3__info">
														<a href="#" class="kt-widget3__username">
															<?PHP echo $rowhistory['create_by'];?>
														</a><br>
														<span class="kt-widget3__time">
															<?PHP echo $rowhistory['created_at'];?>
														</span>
													</div>

													<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
														<?PHP echo $rowhistory['action'];?>
													</span>
												</div>
												<div class="kt-widget3__body">
													<?PHP echo $rowhistory['comment'];?>
												</div>
											</div> 
											<?PHP 
											}
											?>
										</div>
								</div>
							</div> 
							<?PHP 
							}
							?>
						</div>
						<div class="col-lg-12">
							<div> 
								<table class='table table-bordered' style='border:solid 1px #013453;'>
										<tbody>
										<tr>
											<input type='hidden' name='file_diagram_1' value='<?PHP echo $row['diagram'];?>'>
											<input type='hidden' name='file_diagram_2' value='<?PHP echo $row['diagram2'];?>'>

											<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>JUDUL:</b></td>
											<td>
												<input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul INOVASI" value="<?PHP echo $judul;?>">
											</td>
											<td rowspan='6' colspan='2' style='padding: 0rem;'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
												<div style='padding:8px;vertical-align: middle'>
													<?PHP 
													foreach($getdatainodes as $inodes){
														if($inodes['mandatory']==1){ $md = '*'; }else{ $md = ''; }
														if($inodes['type_upload']=='PDF'){ $type = 'application/pdf'; }
														if($inodes['type_upload']=='IMAGES'){ $type = 'image/*'; }
														if($inodes['type_upload']=='MS.WORD'){ $type = '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document'; }
														if($inodes['type_upload']=='MS.EXCEL'){ $type = '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'; }
													?>
													<?PHP  											
														$getDoc_inode = $this->db->query("
																		SELECT a.*	
																		FROM data_innovasi_sdlc a
																		where 1=1 and noinnovasi ='".$norequest."' and type = '".$inodes['id_sdlc']."'")->result_array();
														$doc_inode 		= array_shift($getDoc_inode);
													?>
													
													<center>
														<a class="example-image-link" href="<?PHP echo base_url();?>attachment/<?PHP echo $doc_inode['doc'];?>" data-lightbox="example-set" data-title="<?PHP echo $inode['nama_sdlc'];?>">
														<img class="example-image" src="<?PHP echo base_url();?>attachment/<?PHP echo $doc_inode['doc'];?>" height='100px' alt=""/>
														</a>
													</center>
													<hr>  
													<small><?PHP echo $inodes['nama_sdlc'];?> <?PHP echo $md;?></small>
													<center>
														 <input type="file" class="form-control" name="docspasi<?PHP echo $inodes['id_sdlc'];?>" id="docspasi<?PHP echo $inodes['id_sdlc'];?>" accept="<?PHP echo $type;?>"> 
													</center>
													<hr>
													<?PHP 
													} 
													?>
												</div>
											</td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU:</b></td>
											<td>
												<?PHP  $datacfu = explode(',',$cfu_fu); ?>
												<select class="form-control m-select2" id="cfu_fu" name="cfu_fu[]" multiple="multiple">
													<?PHP 
														foreach($getcfu as $cfux){
															if (in_array($cfux['cfu'], $datacfu)){
															   $s = 'selected';
															} else {
															   $s = "";
															}
															echo "<option value='".$cfux['cfu']."' ".$s.">".$cfux['cfu']."</option>";
														}
													?> 
												</select>
											</td>
											
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>JENIS APLIKASI :</b></td>
											<td>
												<!--input type="text" class="form-control" id="jenis_aplikasi" name="jenis_aplikasi" placeholder="Web Application / Mobile / Desktop Application"-->
												<select class="form-control m-select2" id="jenis_aplikasi" name="jenis_aplikasi">
													<option value='Web Application'>Web Application</option>
													<option value='Mobile'>Mobile</option>
													<option value='Desktop Application'>Desktop Application</option>
												</select>	
											</td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL:</b></td>
											<td><input type="text" value="<?PHP echo $row['url_aplikasi'];?>" class="form-control" id="url" name="url" placeholder="URL Aplikasi"></td>
										</tr>
										<tr>
											<td style='text-align:right;background-color:#013453;' class='text-white'><b>DOMAIN:</b></td>
											<td>
											<?PHP if($domain =='Partner Mgmt'){ echo 'selected'; }?>
												<select class="form-control m-select2" id="domain" name="domain">
													<option value='Partner Mgmt' <?PHP if($domain =='Partner Mgmt'){ echo 'selected'; }?>>Partner Mgmt</option>
													<option value='Analytics' <?PHP if($domain =='Analytics'){ echo 'selected'; }?>>Analytics</option>
													<option value='Security Mgmt' <?PHP if($domain =='Security Mgmt'){ echo 'selected'; }?>>Security Mgmt</option>
													<option value='Performance Mgmt' <?PHP if($domain =='Performance Mgmt'){ echo 'selected'; }?>>Performance Mgmt</option>
													<option value='Product Mgmt' <?PHP if($domain =='Product Mgmt'){ echo 'selected'; }?>>Product Mgmt</option>
													<option value='Fullfilment Mgmt' <?PHP if($domain =='Fullfilment Mgmt'){ echo 'selected'; }?>>Fullfilment Mgmt</option>
													<option value='Assurance Mgmt' <?PHP if($domain =='Assurance Mgmt'){ echo 'selected'; }?>>Assurance Mgmt</option>
													<option value='Data & Inventory Mgmt' <?PHP if($domain =='Data & Inventory Mgmt'){ echo 'selected'; }?>>Data & Inventory Mgmt</option> 
												</select>
											</td>
										</tr>
										<tr>
											<td colspan='2' style='width:50%;padding: 0rem'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
												<div style='padding:8px;'>
													<b>Deskripsi:</b>
													<textarea name="deskripsi" class="summernote" id="deskripsi"><?PHP echo str_replace("<div><br></div>", "", $deskripsi);?> </textarea>
													<hr>
													<b>Tujuan:</b>
													<textarea name="tujuan" class="summernote" id="tujuan"><?PHP echo str_replace("<div><br></div>", "", $tujuan);?> </textarea>
												</div>
											</td>
										</tr>

	 									<tr>
											<td colspan='2' style='padding: 0rem'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
												<div class="row" style='padding:8px;'>
													<div class="col-md-6">
														<b>Jenis Data:</b>
														<textarea name="jenis_data" class="summernote" id="jenis_data"><?PHP echo str_replace("<div><br></div>", "", $jenisdata);?> </textarea>

													</div>
													<div class="col-md-6">
														<b>Stakeholder:</b>
														<textarea name="stakholder" class="summernote" id="stakholder"><?PHP echo str_replace("<div><br></div>", "", $stakholder);?> </textarea>
													</div>
												</div>
											</td>
											<td colspan='2' style='padding: 0rem;'>
												<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasan Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
												<div style='padding:8px;'>
													<br>
													 <textarea name="penjelasan" class="summernote" id="penjelasan"><?PHP echo $penjelasan?></textarea>
												</div>
											</td>
										 
										</tr>
										</tbody>
								</table>  
								 
							</div>
						</div> 
					</div> 			 
				</div>
				<div class="tab-pane" id="sdlc" role="tabpanel"> 
					<div class="row">
						<div class="col-md-12">
							<h5>DOKUMEN SPASI</h5>							 
						</div>   
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
					</div>
					<div class="row"> 
						<?PHP 
						$n=0;
						foreach($getdataspasi as $spasi){
							$n++; 
						?>  
						<div class="col-md-4">
							<div class="col-sm-12 col-md-12 col-xl-12"> 
								<div class="form-group row">
									<label for="example-text-input" class="col-9 col-form-label"><strong><?PHP echo $n;?>. Dokumen <?PHP echo $spasi['nama_sdlc'];?> :</strong></label>
									<div class="col-3">
										<div class="input-group">	
										<?PHP 
											$qDoc = $this->db->query("
															SELECT a.*	
															FROM data_innovasi_sdlc a
															where 1=1 and noinnovasi ='".$norequest."' and type = '".$spasi['id_sdlc']."'");
											$getDoc = $qDoc->result_array($qDoc);
											$doc 	= array_shift($getDoc);
											$numDoc = $qDoc->num_rows($qDoc);

											if($numDoc<1){
												echo '-';
											}else{ 
												echo '<button type="button" class="btn btn-sm btn-brand btn-pill btnpreview" data-toggle="modal" data-target="#modalpreview" data-file="'.$doc['doc'].'">Preview</button>';
											}
										?>
										</div>
									</div>
								</div> 
							</div>  	 
							<div class="col-sm-12 col-md-12 col-xl-12">
							</div>
							<div class="col-md-12"> 
								<div class="form-group">
									<label>Update <?PHP echo $spasi['nama_sdlc'];?> *</label>
									<div id="bgattach">
										<div class="input-group">
											<input type="file" class="form-control" name="docspasi<?PHP echo $spasi['id_sdlc'];?>" id="docspasi<?PHP echo $spasi['id_sdlc'];?>">
											<a href="#" class="col-lg-1">
											</a>
										</div>
									</div>
									<br>
									<span class="form-text text-muted">Type Document PDF</span>
								</div>  
							</div> 	 
						</div>
						<?PHP
						} 
						?>  
					</div>
				</div> 
				<div class="tab-pane" id="tp" role="tabpanel"> 
					<div class="row">
						<div class="col-md-6">
							<h5>TEKNOLOGI PLATFORM</h5>							 
						</div>
					</div>
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<div class="row">
						<div class="col-md-6">
							<h6>LANGUAGE & FRAMEWORK</h6>							 
							<?PHP if($showalertfr == true) { ?>
							<div class="col-md-12">
								<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Not Standard Language & Framework</div>
							</div>						
							</div>
							<?PHP } else { ?>
							<div class="col-md-12">
								<div class="alert alert-success" role="alert">
								<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
								<div class="alert-text">Standard Language & Framework</div>
							</div>						
							</div>
							<?PHP } ?>
							<ul style='color:black'>
							<?PHP
								echo $li_fr;
							?>
							</ul>
						</div>
						<div class="col-md-6">
							<h6>DATABASE</h6>							 
							<?PHP if($showalertdb == true) { ?>
							<div class="col-md-12">
								<div class="alert alert-warning" role="alert">
								<div class="alert-icon"><i class="flaticon-warning"></i></div>
								<div class="alert-text">Not Standard Database</div>
							</div>						
							</div>
							<?PHP } else { ?>
							<div class="col-md-12">
								<div class="alert alert-success" role="alert">
								<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
								<div class="alert-text">Standard Database</div>
							</div>						
							</div>
							<?PHP } ?>
							<ul>
							<?PHP
								echo $li_db;
							?>
							</ul> 
						</div>	
					</div> 
					<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
					<div class="row"> 					
						<div class="col-6">
							<div class="form-group row">
								<label class="col-12 col-form-label">Language & Framework</label>
								<div class="col-12">
									<div class="kt-checkbox-list">
										<?PHP  //$datacfu = explode(',',$cfu_fu); ?> 			
										<?PHP
										foreach($getdataframework as $gframe){
											// if (in_array($cfux['cfu'], $datacfu)){
											   // $s = 'selected';
											// } else {
											   // $s = "";
											// }
										?>
										<label class="kt-checkbox">
											<input type="checkbox" name='framework[]' value='<?PHP echo $gframe['framework'];?>'> <?PHP echo $gframe['framework'];?>
											<span></span>
										</label>
										<?PHP														
										} 
										?>
										
										<label class="kt-checkbox">
											<input type="checkbox" id='other_framework'> Other
											<span></span>
										</label>

									</div>
								</div>
							</div>
							<div class="form-group row" id='fr_wrapper' style='display:none;'> 
								<label class="col-form-label col-lg-12 col-sm-12">Other Language & Framework *</label>
								<div class="col-lg-12 col-md-9 col-sm-12">
									<div id="bgframework">
										<div class="input-group">
						                    <input type="text" class="form-control" name="otherframe[]" id="otherframe"> 
						                </div>
									</div>
									<br>
									<button type="button" class="btn btn-sm btn-default btnframework">
										<i class="flaticon flaticon-plus"></i> Tambah
									</button>
								</div> 
							</div>												
						</div>
						<div class="col-6">
							<div class="form-group row">
								<label class="col-12 col-form-label">Database</label>
								<div class="col-12">
									<div class="kt-checkbox-list">
										<?PHP
										foreach($getdatadatabase as $gbase){
										?>
										<label class="kt-checkbox">
											<input type="checkbox" name='database[]' value='<?PHP echo $gbase['nama_database'];?>'> <?PHP echo $gbase['nama_database'];?>
											<span></span>
										</label>
										<?PHP														
										} 
										?>
										<label class="kt-checkbox">
											<input type="checkbox" id='other_database'> Other
											<span></span>
										</label>

									</div>
								</div>
							</div>
							<div class="form-group row" id='db_wrapper' style='display:none;'> 
								<label class="col-form-label col-lg-12 col-sm-12">Other Database *</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div id="bgdatabase">
										<div class="input-group">
						                    <input type="text" class="form-control" name="otherdb[]" id="otherdb"> 
						                </div>
									</div>
									<br>
									<button type="button" class="btn btn-sm btn-default btndatabase">
										<i class="flaticon flaticon-plus"></i> Tambah
									</button>
								</div> 
							</div>
							
						</div>
					</div>   
				</div> 
				<div class="tab-pane" id="pii" role="tabpanel"> 
					<div class="row">
						<div class="col-md-12">
							<h5>PERSONAL IDENTIFIABLE INFORMATION</h5>							 
						</div>
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
					</div>
					<div class="row">
						<?PHP 
						foreach($getdatapiidoc as $piidoc){
							$getDoc_pii = $this->db->query("
											SELECT a.*	
											FROM data_innovasi_sdlc a
											where 1=1 and noinnovasi ='".$norequest."' and type = '".$piidoc['id_sdlc']."'")->result_array();
							$docpii 		= array_shift($getDoc_pii);
							$jmdocpii = $this->db->query("
											SELECT a.*	
											FROM data_innovasi_sdlc a
											where 1=1 and noinnovasi ='".$norequest."' and type = '".$piidoc['id_sdlc']."'")->num_rows();
							  
							?> 
							<div class="row"> 
								<div class="col-sm-12 col-md-12 col-xl-12"> 
									<div class="form-group">
										<label>Update <?PHP echo $piidoc['nama_sdlc'];?> *</label>
										<div id="bgattach">
											<div class="input-group">
												<input type="file" class="form-control" name="docspasi<?PHP echo $piidoc['id_sdlc'];?>" id="docspasi<?PHP echo $piidoc['id_sdlc'];?>">
												<a href="#" class="col-lg-1">
												</a>
											</div>
										</div>
										<br>
										<span class="form-text text-muted">Type Document PDF</span>
									</div>  
								</div> 
							</div>  
							<?PHP
							if($jmdocpii < 1){
								echo '
								<!--div class="col-md-12">
									<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Dokumen Persetujuan Owner Tidak ditemukan.</div>
									</div>						
								</div-->
								';
							}else{
								echo '
								<!--div class="col-md-12">
									<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">Dokumen Persetujuan Owner ditemukan</div>
									</div>						
								</div-->
								<div class="col-md-12">
									<object data="'.base_url().'attachment/'.$docpii['doc'].'" type="application/pdf" width="100%" height="400px">
									</object>
								</div>
								';
							} 
						} 
						?> 
					</div>
				</div> 			 
				<div class="tab-pane" id="ss" role="tabpanel"> 
					<div class="row">
						<div class="col-md-6">
							<h5>SECURITY SYSTEM</h5>							 
						</div> 
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div> 
					</div> 
					<div class="row">
						<?PHP 
						$n=0;
						foreach($getdatasecurity as $security){
							$n++;
						?>
						<div class="col-md-6">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12"> 
										<div class="form-group row">
											<label for="example-text-input" class="col-9 col-form-label"><strong><?PHP echo $n;?>. Dokumen <?PHP echo $security['nama_sdlc'];?> :</strong></label>
											<div class="col-3">
												<div class="input-group">	
												<?PHP 
													$qdocs = $this->db->query("
																	SELECT a.*	
																	FROM data_innovasi_sdlc a
																	where 1=1 and noinnovasi ='".$norequest."' and type = '".$security['id_sdlc']."'");
													$getDocs = $qdocs->result_array();
													$numDocs = $qdocs->num_rows();
													$docs = array_shift($getDocs);
													if($numDocs < 1){
														echo "-";
													}else{ 
														echo '<button type="button" class="btn btn-sm btn-brand btn-pill btnpreview" data-toggle="modal" data-target="#modalpreview" data-file="'.$docs['doc'].'">Preview</button>';													
													}
												?>
												</div>
											</div>
										</div> 
									</div> 
								</div>
							</div> 
							<div class="col-sm-12 col-md-12 col-xl-12"> 
								<div class="form-group">
									<label>Update <?PHP echo $security['nama_sdlc'];?> *</label>
									<div id="bgattach">
										<div class="input-group">
											<input type="file" class="form-control" name="docspasi<?PHP echo $security['id_sdlc'];?>" id="docspasi<?PHP echo $security['id_sdlc'];?>">
											<a href="#" class="col-lg-1">
											</a>
										</div>
									</div>
									<br>
									<span class="form-text text-muted">Type Document PDF</span>
								</div>  
							</div> 
						</div>
						<?PHP
						} 
						?> 						
					</div>
				</div> 						
				<div class="tab-pane" id="stg" role="tabpanel"> 
					<div class="row">
						<div class="col-md-6">
							<h5>DATA SHARING TELKOM GROUP</h5>							 
						</div>
						 
						<div class="col-md-12">
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
						</div>
					</div>
					<div class="row">
						<div class='col-lg-12'>
						<?PHP 
						foreach($getdatastgdoc as $stgdoc){
							$getDoc_stg = $this->db->query("
											SELECT a.*	
											FROM data_innovasi_sdlc a
											where 1=1 and noinnovasi ='".$norequest."' and type = '".$stgdoc['id_sdlc']."'")->result_array();
							$docstg		= array_shift($getDoc_stg);
							$jmdocstg = $this->db->query("
											SELECT a.*	
											FROM data_innovasi_sdlc a
											where 1=1 and noinnovasi ='".$norequest."' and type = '".$stgdoc['id_sdlc']."'")->num_rows(); 
						 
						?> 
						<div class="row"> 
							<div class="col-sm-12 col-md-12 col-xl-12"> 
								<div class="form-group">
									<label>Update <?PHP echo $stgdoc['nama_sdlc'];?> *</label>
									<div id="bgattach">
										<div class="input-group">
											<input type="file" class="form-control" name="docspasi<?PHP echo $stgdoc['id_sdlc'];?>" id="docspasi<?PHP echo $stgdoc['id_sdlc'];?>">
											<a href="#" class="col-lg-1">
											</a>
										</div>
									</div>
									<br>
									<span class="form-text text-muted">Type Document PDF</span>
								</div>  
							</div> 
						</div> 
						<?PHP 
						if($jmdocstg < 1){
							echo '
							<!--div class="col-md-12">
								<div class="alert alert-warning" role="alert">
									<div class="alert-icon"><i class="flaticon-warning"></i></div>
									<div class="alert-text">Dok MoM DG Council Meeting tidak ditemukan.</div>
								</div>						
							</div-->
							';
						}else{
							echo '
							<!--div class="col-md-12">
								<div class="alert alert-success" role="alert">
									<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
									<div class="alert-text">MoM DG Council Meeting ditemukan.</div>
								</div>						
							</div-->
							<div class="col-md-12">
								<object data="'.base_url().'attachment/'.$docstg['doc'].'" type="application/pdf" width="100%" height="400px">
								</object>
							</div>
							';
						} 
						} 
						?> 	
						</div>
					</div>
				</div>  
				<div class="tab-pane" id="history" role="tabpanel">
					<div class="kt-scroll" data-scroll="true" data-height="380" data-mobile-height="300">
	                    <!--Begin::Timeline 3 -->
	                    <div class="kt-timeline-v2">
	                        <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">
	                        	<div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden; ?>" alt="Pic" style="width:40px !important;height:40px !important;" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $row['pictcreated']; ?>" />

											<?PHP if ($row['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($row['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless kt-font-brand"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    Pembuatan Data Innovation <br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($row['created_at']); ?> | 
		                                    <i class="la la-user"></i>  <?PHP echo $row['namecreated']; ?>
		                                </div>
	                                </div>
	                            </div>
	                        	<?PHP
	                        	$getHistory = $this->db->query("
	                        				SELECT a.*,
	                        					(SELECT picture from mi.user where userid=a.created_by) pictcreated,
												(SELECT name from mi.user where userid=a.created_by) namecreated
	                        				FROM mi.data_innovasi_history a where noinnovasi='$norequest'
	                        				order by 1
	                        				")->result_array();
	                        				
	                        	foreach($getHistory as $history) {
	                        		if ($history['action']=='COMPLY') {
	                        			$colortl 	= 'kt-font-success';
	                        		} else if ($history['action']=='NOT COMPLY') {
	                        			$colortl 	= 'kt-font-danger';
	                        		} else{
	                        			$colortl 	= 'kt-font-brand';
	                        		}

	                        		if ($history['pictcreated']=='') {
										$hidden2 	= 'kt-hidden';
									} else {
										$hidden2 	= '';
									}

									// if ($history['action']=='new') {
									// 	$notiftext 	= 'Publish Pengajuan Innovation';
									// } else if ($history['action']=='republish') {
									// 	$notiftext 	= 'Perbaikan Pengajuan';
									// } else if ($history['action']=='escalation') {
									// 	$notiftext 	= 'Eskalasi Pengajuan';
									// } else if ($history['action']=='reject') {
									// 	$notiftext 	= 'Pengajuan tidak disetujui';
									// } else if ($history['action']=='approve') {
									// 	$notiftext 	= 'Pengajuan Innovation telah direview';
									// } 
									// else if ($history['action']=='return') {
									// 	$notiftext 	= 'Pengajuan telah dikembalikan';
									// } else if ($history['action']=='update') {
									// 	$notiftext 	= 'Perbaikan dan Eskalasi SBR';
									// }
									$notiftext = $history['judul'];
									$genValue 	= $history['namecreated'];
									$nickname 	= explode(' ',trim($genValue));
	                        	?>
	                            <div class="kt-timeline-v2__item">
	                                <span class="kt-timeline-v2__item-time kt-user-card">
	                                	<div class="kt-user-card__avatar">
								        	<img class="<?PHP echo $hidden2; ?>" alt="Pic" style="width:40px !important;height:40px !important;" src="<?PHP echo base_url(); ?>images/user/<?PHP echo $history['pictcreated']; ?>" />

											<?PHP if ($history['pictcreated']=='') { ?>
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?PHP echo substr($history['namecreated'],0,1); ?></span>
											<?PHP } ?>
										</div>
	                                </span>
	                                <div class="kt-timeline-v2__item-cricle">
	                                    <i class="fa fa-genderless <?PHP echo $colortl; ?>"></i>
	                                </div>
	                                <div class="kt-timeline-v2__item-text kt-timeline-v2__item-text--bold">
	                                    <?PHP echo $notiftext; ?><br>
	                                    <div class="kt-timeline-v2__item-text  kt-padding-top-5" style="padding:0.35rem 0 0 0rem;">
		                                    <i class="la la-clock-o"></i> <?PHP echo $this->formula->TanggalIndo($history['created_at']); ?> | 
		                                    <i class="la la-user"></i>  <?PHP echo $history['namecreated']; ?>
	                                    	
	                                    	<div style="margin-top: 0.5rem;">
		                                    	<i class="la la-comment"></i>  Comment :<br>
		                                    	<?PHP echo $history['comment']; ?>
		                                    </div>
		                                </div>
	                                </div>
	                            </div>
	                            <?PHP } ?>
	                        </div>
	                    </div>
	                    <!--End::Timeline 3 -->
	                </div>
				</div>  
			</div>
			<div class='col-lg-12'>
				<button type="submit" id="updatedraft" class="btn btn-warning"><i class="la la-save"></i> Save</button>			 
				<button type="submit" id="savenewresubmit_draft" class="btn btn-success "><i class="la la-rocket"></i> Submit</button>

			</div>
		</form>
		</div>
	</div>
</div>
