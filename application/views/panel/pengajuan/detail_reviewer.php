<?PHP
error_reporting(0);
$userdata		= $this->session->userdata('sessSpazzle'); 
$userid 		= $userdata['userid'];
$idrole 		= $userdata['id_role'];

$req 			= str_replace('-','/',$id);
$norequest 		= str_replace('_','-',$req);

$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$level			= $dUser['step'];
$aksesCreate 	= $dUser['actionbtn'];

$getInnovation = $this->db->query("
				SELECT a.*,
				(SELECT picture from mi.user where userid=a.created_by) pictcreated,
				(select name from mi.user where userid=a.created_by)as usercreate,
				(SELECT name from mi.user where userid=a.created_by)as namecreated
				FROM data_innovasi a
				where 1=1 and noinnovasi ='".$norequest."'")->result_array();
$row 			= array_shift($getInnovation);

if ($level==1) {
	if ($row['status']!=0 or $row['status']!=2) {
		$hideact = 'kt-hidden';
	} else {
		$hideact = '';
	}
} else if ($level!=1) {
	if ($row['current']==$level) {
		$hideact = '';
	} else {
		$hideact = 'kt-hidden';
	}
} else {
	$hideact = 'kt-hidden';
}

if ($row['pictcreated']=='') {
	$hidden 	= 'kt-hidden';
} else {
	$hidden 	= '';
}

$pathfile 		= base_url().'attachment/';

$judul 			= $row['judul'];
$cfu_fu 		= $row['cfu_fu'];
$url 			= '<a href="'.$row['url_aplikasi'].'">'.$row['url_aplikasi'].'</a>';
$jenis_aplikasi 		= $row['jenis_aplikasi'];
$deskripsi 		= $row['deskripsi'];
$tujuan 		= $row['tujuan'];
$jenisdata 		= $row['jenis_data'];
$stakholder  	= $row['stakholder'];
$diagram 		= $pathfile.$row['diagram'];
$diagram2 		= $pathfile.$row['diagram2'];
$penjelasan	 	= $row['penjelasan_singkat'];
$pii_status 	= $row['status_pii'];
$status_datasharing = $row['status_datasharing'];
$sharing_group_status = $row['sharing_telkom_group'];
$status 		= $row['status'];
$status_upti 	= $row['status_upti'];
$status_data_innovasi = $row['status_data_innovasi'];
$domain 	= $row['domain'];

$sdlc_required= 0;
//GET DATA DOC REQUIRMENT
$docreq = getdatadoc('Lihat Dokumen Requirment',$norequest,'Dok.Requirment');
$docdesign = getdatadoc('Lihat Dokumen Design',$norequest,'Dok.Design');
$docsop = getdatadoc('Lihat Dokumen SOP',$norequest,'Dok.SOP');
$docsopd2p = getdatadoc('Lihat Dokumen SOP D2P',$norequest,'Dok.SOP D2P');
$docsmp = getdatadoc('Lihat Dokumen SMP',$norequest,'Dok.SMP');
$doctes = getdatadoc('Lihat Dokumen Test Report / Test Plan',$norequest,'Dok.Test Plan/Report');
if($docreq=='-'){}else{$sdlc_required++;}
if($docdesign=='-'){}else{$sdlc_required++;}
if($docsop=='-'){}else{$sdlc_required++;}
if($docsopd2p=='-'){}else{$sdlc_required++;}
if($docsmp=='-'){}else{$sdlc_required++;}
if($doctes=='-'){}else{$sdlc_required++;}

if($sdlc_required < 6){
	$sdlc_status =true;
}else{
	$sdlc_status =false;
}

//GET DATA UPTI 
$li_fr='';
$stdstatus_fr = 0;
$notstdstatus_fr = 0;
$getupti = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Framework'")->result_array();	
foreach($getupti as $row_fr){
	$cekupti = $this->db->query("select * from param_framework where framework='".$row_fr['name_upti']."'")->num_rows();
	if($cekupti > 0){
		$stdstatus_fr++;
		$stdfr = 'STANDARD';
	}else{
		$notstdstatus_fr++;
		$stdfr = 'NOT STANDARD';
	}
	$li_fr .= "<li>".$row_fr['name_upti']." <small>(".$stdfr.")</small></li>";
} 
if($stdstatus_fr > 0){
	$showalertfr = false;
}else{
	$showalertfr = true;								
}

$li_db = '';
$stdstatus_db = 0;
$notstdstatus_db = 0;
$getupti2 = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Database'")->result_array();	
foreach($getupti2 as $row_db){
	$cekdb = $this->db->query("select * from param_database where nama_database='".$row_db['name_upti']."'")->num_rows();
	if($cekdb > 0){
		$stdstatus_db++;
		$stddb = 'STANDARD';
	}else{
		$notstdstatus_db++;
		$stddb = 'NOT STANDARD';
	}
	$li_db .= "<li>".$row_db['name_upti']." <small>(".$stddb.")</small></li>";
} 
if($stdstatus_db > 0){
	$showalertdv = false;
}else{
	$showalertdb = true;								
}

//GET DATA SECUITY SYSTEM
$security_count = 0;
$docToU = getdatadoc('Lihat Dokumen Evidance ToU (Terms of Use)',$norequest,'Evidance.ToU');
$docTFA = getdatadoc('Lihat Dokumen Multi Factor Authentication',$norequest,'Evidance.TFA');
$docNDE = getdatadoc('Lihat Dokumen NDE Hasil VA Test',$norequest,'Evidance.NDE VA Test');
$docCustomer = getdatadoc('Lihat Dokumen Customer Consent',$norequest,'Evidance.Customer Consent');

if($docToU=='-'){}else{$security_count++;}
if($docTFA=='-'){}else{$security_count++;}
if($docNDE=='-'){}else{$security_count++;}

if($security_count < 3){
	$security_status =true;
}else{
	$security_status =false;
}
//GET STATUS DOC
$st_docreq = getstatusdoc($norequest,'Dok.Requirment');
$st_docdesign = getstatusdoc($norequest,'Dok.Design');
$st_docsop = getstatusdoc($norequest,'Dok.SOP');
$st_docsopd2p = getstatusdoc($norequest,'Dok.SOP D2P');
$st_docsmp = getstatusdoc($norequest,'Dok.SMP');
$st_doctes = getstatusdoc($norequest,'Dok.Test Plan/Report');


$st_docToU = getstatusdoc($norequest,'Evidance.ToU');
$st_docTFA = getstatusdoc($norequest,'Evidance.TFA');
$st_docNDE = getstatusdoc($norequest,'Evidance.NDE VA Test');
$st_docCustomer = getstatusdoc($norequest,'Evidance.Customer Consent');

if($level == 3){
	$step1 = '';
	$step3 = 'data-ktwizard-state="current"';
	$post_url = 'ctrl_innovasi/review_technology';
}else{
	$step1 = 'data-ktwizard-state="current"';
	$step3 = '';
	$post_url = 'ctrl_innovasi/review_innovasi_new';
}

$getdataspasi = $this->query->getData('param_sdlc','*',"where type_tab='SPASI' order by id_sdlc asc");
$getdatasecurity = $this->query->getData('param_sdlc','*',"where type_tab='SECURITY SYSTEM' order by id_sdlc asc");
$getdatainodes = $this->query->getData('param_sdlc','*',"where type_tab='INNOVATION DESCRIPTION' order by id_sdlc ASC");
$getdatapiidoc = $this->query->getData('param_sdlc','*',"where type_tab='PERSONAL IDENTIFIABLE INFORMATION' order by id_sdlc ASC");
$getdatastgdoc = $this->query->getData('param_sdlc','*',"where type_tab='DATA SHARING TELKOM GROUP' order by id_sdlc ASC");
$btnsubmit =0;
?>
<style>
	.kt-wizard-v3 .kt-wizard-v3__wrapper .kt-form .kt-wizard-v3__content{
		padding-top:15px;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items{
	padding:0 2rem !important;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body{
		padding: 2rem 0.5rem 0rem;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item{
		flex: auto;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body .kt-wizard-v3__nav-label{
		font-size:12px;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body .kt-wizard-v3__nav-label span{
		font-size:13px;
	}
	.kt-wizard-v3 .kt-wizard-v3__wrapper .kt-form{
		width: 90%;
	    padding: 0rem 0 5rem;
	}
	.alert{
		padding :0rem 2rem !important;
	}
	 
	.mt15rem { margin-top: 9px !important; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	#jawaban .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	#history .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#history .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
	.kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 60px !important;
    	height: 60px !important;
	}
	/*#wfinfo .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#wfinfo .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}*/
	.nav-tabs.nav-tabs-line .nav-link{
		font-size:11px !important;
	}
	.tab-pane{
		font-size:12px !important;
		min-height:400px;
	} 
	.table-bordered th, .table-bordered td{
		border-color:#0b5e90;
	}
	p {
	    margin-top: 0;
	    margin-bottom: 0.2rem !important;
	}
</style>
<!-- MODAL PREVIEW -->
<div class="modal fade" id="modalpreview" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
				<!--div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">Dokumen</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div-->
				<div class="kt-portlet__body">
					<div class='row'>
						<div class="col-lg-12" style='min-height:500px;'>
							<iframe src="" id='embedsrc' type="application/pdf" width="100%" height="100%">
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END MODAL PREVIEW -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet">
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
				<div class="kt-grid__item">

					<!--begin: Form Wizard Nav -->
					<div class="kt-wizard-v3__nav">
						<div class="kt-wizard-v3__nav-items">
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" <?PHP echo $step1;?>>
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>1</span> Innovation Description
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>2</span> SPASI
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" <?PHP echo $step3;?>>
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>3</span> Technology Platform
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>4</span> Personal Identifiable Information
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>5</span> Security System
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>6</span> Data Sharing Telkom Group
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
						</div>
					</div>

					<!--end: Form Wizard Nav -->
				</div>
				<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

					<!--begin: Form Wizard Form-->
					<form class="kt-form" id="kt_form" method='POST' action='<?PHP echo base_url(); ?><?PHP echo $post_url; ?>'> 
						<!--begin: Form Wizard Step 1-->
						<input type='hidden' name='noinnovasi' value='<?PHP echo $norequest; ?>'>

						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" <?PHP echo $step1;?>>
							<div class="row">
									<div class="col-md-12">
										<h5>INNOVATION DESCRIPTION</h5><hr>
									</div>	 						
									<div class="col-lg-12">
										<div> 
										<table class='table table-bordered' style='border:solid 1px #013453;'>
											<tbody>
											<tr>
												<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>JUDUL:</b></td>
												<td><?PHP echo $judul;?></td>
												<td rowspan='6' colspan='2' style='padding: 0rem;'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
													<div style='padding:5px;'>
														<?PHP 
														foreach($getdatainodes as $inode){
															$getDoc_inode = $this->db->query("
																			SELECT a.*	
																			FROM data_innovasi_sdlc a
																			where 1=1 and noinnovasi ='".$norequest."' and type = '".$inode['id_sdlc']."'")->result_array();
															$doc_inode 		= array_shift($getDoc_inode);
														?>
														
														<center>
															<a class="example-image-link" href="<?PHP echo base_url();?>attachment/<?PHP echo $doc_inode['doc'];?>" data-lightbox="example-set" data-title="<?PHP echo $inode['nama_sdlc'];?>">
															<img class="example-image" src="<?PHP echo base_url();?>attachment/<?PHP echo $doc_inode['doc'];?>" height='100px' alt=""/>
															</a>
														</center>
														<hr> 
														<?PHP } ?>
													</div>
												</td>
											</tr>
											<tr>
												<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU:</b></td>
												<td><?PHP echo $cfu_fu;?></td>
												
											</tr>
											<tr>
												<td style='text-align:right;background-color:#013453;' class='text-white'><b>JENIS APLIKASI:</b></td>
												<td><?PHP echo $jenis_aplikasi;?></td>
											</tr>
											<tr>
												<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL:</b></td>
												<td><?PHP echo $url;?></td>
											</tr>
											<tr>
												<td style='text-align:right;background-color:#013453;' class='text-white'><b>DOMAIN:</b></td>
												<td><?PHP echo $domain;?></td>
											</tr> 
											<tr>
												<td colspan='2' style='width:50%;padding: 0rem'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
													<div style='padding:5px;'>
														<b>Deskripsi:</b>
														<?PHP echo str_replace("<div><br></div>", "", $deskripsi);?> 
														<hr>
														<b>Tujuan:</b>
														<?PHP echo str_replace("<div><br></div>", "", $tujuan);?> 
													</div>
												</td>
											</tr>
		 									<tr>
												<td colspan='2' style='padding: 0rem'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
													<div class="row" style='padding:5px;'>
														<div class="col-md-6">
															<b>Jenis Data:</b>
															<?PHP echo str_replace("<div><br></div>", "", $jenisdata);?> 
														</div>
														<div class="col-md-6">
															<b>Stakeholder:</b>
															<?PHP echo str_replace("<div><br></div>", "", $stakholder);?> 
														</div>
													</div>
												</td>
												<td colspan='2' style='padding: 0rem;'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasan Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
													<div style='padding:5px;'>
														<?PHP echo $penjelasan?>
													</div>
												</td>
											 
											</tr>
											</tbody>
										</table>
									</div>
									</div>
									
									<?PHP  
									if($level == '2' and ($status_data_innovasi== '0' or $status_data_innovasi == null)){
										$btnsubmit++;
									?>
									<div class="col-lg-12"><hr></div>
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-12"> 
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label"><strong>Action :</strong></label>
														<div class="col-9">
															<div class="kt-radio-inline">
																<label class="kt-radio">
																	<input type="radio" name="status_data_innovasi" value='1'> Comply
																	<span></span>
																</label>
																<label class="kt-radio">
																	<input type="radio" name="status_data_innovasi" value='2'> Not Comply
																	<span></span>
																</label> 
															</div>
														</div>
													</div> 
												</div> 
												<div class="col-md-12"> 
													<div class="form-group row">
														<label for="example-text-input" class="col-3 col-form-label"><strong>Komentar :</strong></label>
														<div class="col-9">
															 <textarea name="komentar_status_data_innovasi" class="summernote" id="komentar_status_data_innovasi"></textarea>
														</div>
													</div> 
												</div>
											</div>										
										</div> 
									<?PHP
									}else{ 
										$ghistory = "SELECT *, (select name from mi.user where userid = a.created_by) create_by
													FROM mi.data_innovasi_history a where a.judul ='Review Inovasi Description' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
										$gethistory = $this->db->query($ghistory)->result_array();
										foreach($gethistory as $rowhistory){
											if($rowhistory['action']=='COMPLY'){
												$color ='success';
											}else{
												$color ='danger';
											}
									?>
										<div class="kt-portlet kt-portlet--mobile"> 
											<div class="kt-portlet__body"> 
													<div class="kt-widget3">
														<div class="kt-widget3__item">
															<div class="kt-widget3__header">
																<div class="kt-widget3__user-img">
																	<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
																</div>
																<div class="kt-widget3__info">
																	<a href="#" class="kt-widget3__username">
																		<?PHP echo $rowhistory['create_by'];?>
																	</a><br>
																	<span class="kt-widget3__time">
																		<?PHP echo $rowhistory['created_at'];?>
																	</span>
																</div>

																<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
																	<?PHP echo $rowhistory['action'];?>
																</span>
															</div>
															<div class="kt-widget3__body">
																<?PHP echo $rowhistory['comment'];?>
															</div>
														</div> 
													</div>
											</div>
										</div> 
									<?PHP 
										} 
									}
									?> 
							</div>
						</div>

						<!--end: Form Wizard Step 1-->

						<!--begin: Form Wizard Step 2-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-12">
									<h5>DOKUMEN SPASI</h5><hr>							 
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
									<?PHP 
									$n=0;
									foreach($getdataspasi as $spasi){
										$n++;
									?>
									<div class="col-md-4">
										<div class="row">
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-9 col-form-label"><strong><?PHP echo $n;?>. Dokumen <?PHP echo $spasi['nama_sdlc'];?> :</strong></label>
													<div class="col-3">
														<div class="input-group">	
														<?PHP 
															$qDoc = $this->db->query("
																			SELECT a.*	
																			FROM data_innovasi_sdlc a
																			where 1=1 and noinnovasi ='".$norequest."' and type = '".$spasi['id_sdlc']."'");
															$getDoc = $qDoc->result_array($qDoc);
															$numDoc = $qDoc->num_rows($qDoc);
															$doc 	= array_shift($getDoc);
															$idDoc 	= $spasi['id_sdlc'];
															$statusdoc = $doc['status'];
															if($numDoc<1){
																echo '-';
															}else{
			 													echo '<button type="button" class="btn btn-sm btn-brand btn-pill btnpreview" data-toggle="modal" data-target="#modalpreview" data-file="'.$doc['doc'].'">Preview</button>';
			 												}
														?>
														</div>
													</div>
												</div> 
											</div> 
										</div> 
										<?PHP 
										if($level == '2' and ($statusdoc =='0' or $statusdoc ==null or $statusdoc=='' or !isset($statusdoc))){
											$btnsubmit++;
										?>
										<div class="row"> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Action :</strong></label>
													<div class="col-8">
														<div class="kt-radio-inline">
															<label class="kt-radio">
																<input type="radio" name="status<?PHP echo $idDoc;?>" value='1'> Comply
																<span></span>
															</label>
															<label class="kt-radio">
																<input type="radio" name="status<?PHP echo $idDoc;?>" value='2'> Not Comply
																<span></span>
															</label> 
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Komentar :</strong></label>
													<div class="col-8">
														 <textarea name="komentar<?PHP echo $idDoc;?>" class="summernote" id="komentar<?PHP echo $idDoc;?>"></textarea>
													</div>
												</div> 
											</div>
										</div>
										<?PHP
										}else{
										?>
										
												<?PHP 
												$keyword = 'REVIEW '.$spasi['type_tab'].' | '.$spasi['nama_sdlc'].'';
												$ghistory = "SELECT *, (select name from mi.user where userid = a.created_by) create_by
															FROM mi.data_innovasi_history a where a.judul ='".$keyword."' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
												$gethistory = $this->db->query($ghistory)->result_array();
												foreach($gethistory as $rowhistory){
													if($rowhistory['action']=='COMPLY'){
														$color ='success';
													}else{
														$color ='danger';
													}
												?>
												<div class="kt-portlet kt-portlet--mobile"> 
													<div class="kt-portlet__body"> 
														<div class="kt-widget3">
															<div class="kt-widget3__item">
																<div class="kt-widget3__header">
																	<div class="kt-widget3__user-img">
																		<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
																	</div>
																	<div class="kt-widget3__info">
																		<a href="#" class="kt-widget3__username">
																			<?PHP echo $rowhistory['create_by'];?>
																		</a><br>
																		<span class="kt-widget3__time">
																			<?PHP echo $rowhistory['created_at'];?>
																		</span>
																	</div>
																	<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
																		<?PHP echo $rowhistory['action'];?>
																	</span>
																</div>
																<div class="kt-widget3__body">
																	 
																		<?PHP echo $rowhistory['comment'];?>
																	 
																</div>
															</div> 
														</div> 
													</div>
												</div>   
												<?PHP 
												}
												if($doc['status']==2 OR !isset($doc['status'])){ 
													$notcomply++;
												}
												?>
												
										<?PHP
										} 
										?>  	
									</div>
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
									<?PHP 
									} 
									?>  
									</div>
								</div>
							</div>							 
						</div>

						<!--end: Form Wizard Step 2-->

						<!--begin: Form Wizard Step 3-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" <?PHP echo $step3;?>>
							<div class="row">
								<div class="col-md-12">
									<h5>TEKNOLOGI PLATFORM</h5>	<hr>						 
								</div> 
								<div class="col-md-12"> 
								</div>	
								<div class="col-md-6">
									<h6>LANGUAGE & FRAMEWORK</h6>							 
									<?PHP if($showalertfr == true) { ?>
									<div class="col-md-12">
										<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Not Standard Language & Framework</div>
									</div>						
									</div>
									<?PHP } else { ?>
									<div class="col-md-12">
										<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">Standard Language & Framework</div>
									</div>						
									</div>
									<?PHP } ?>
									<ul style='color:black'>
									<?PHP
										echo $li_fr;
									?>
									</ul>
								</div>
								<div class="col-md-6">
									<h6>DATABASE</h6>							 
									<?PHP if($showalertdb == true and $status_data_innovasi==1) { ?>
									<div class="col-md-12">
										<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Not Standard Database</div>
									</div>						
									</div>
									<?PHP } else { ?>
									<div class="col-md-12">
										<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">Standard Database</div>
									</div>						
									</div>
									<?PHP } ?>
									<ul>
									<?PHP
										echo $li_db;
									?>
									</ul>

								</div>						 
							</div>	
							<?PHP 
							if($level == '3' and $status_data_innovasi== '1'){
								if($status_upti ==0 or $status_upti==NULL){
									$btnsubmit++;
							?>
							<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
							<div class="row">  
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Action :</strong></label>
										<div class="col-9">
											<div class="kt-radio-inline">
												<label class="kt-radio">
													<input type="radio" name="status_upti" value='1'> Comply
													<span></span>
												</label>
												<label class="kt-radio">
													<input type="radio" name="status_upti" value='2'> Not Comply
													<span></span>
												</label> 
											</div>
										</div>
									</div> 
								</div> 
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Komentar :</strong></label>
										<div class="col-9">
											 <textarea name="komentar_upti" class="summernote" id="komentar_upti"></textarea>
										</div>
									</div> 
								</div>
							</div>
							<?PHP 
								}else{
							?>
									<?PHP  
									$ghistory = "SELECT 
										*,
									(select name from mi.user where userid = a.created_by) create_by
									FROM mi.data_innovasi_history a where a.judul ='REVIEW TEKNOLOGI PLATFORM' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
									//echo $ghistory;
									$gethistory = $this->db->query($ghistory)->result_array();
									$rowhistory = array_shift($gethistory);	
									if($rowhistory['action']=='COMPLY'){
											$color ='success';
										}else{
											$color ='danger';
										}
									?>
									<div class="kt-portlet kt-portlet--mobile"> 
										<div class="kt-portlet__body"> 
											<div class="kt-widget3">
												<div class="kt-widget3__item">
													<div class="kt-widget3__header">
														<div class="kt-widget3__user-img">
															<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
														</div>
														<div class="kt-widget3__info">
															<a href="#" class="kt-widget3__username">
																<?PHP echo $rowhistory['create_by'];?>
															</a><br>
															<span class="kt-widget3__time">
																<?PHP echo $rowhistory['created_at'];?>
															</span>
														</div>
														<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
															<?PHP echo $rowhistory['action'];?>
														</span>
													</div>
													<div class="kt-widget3__body">
														 
															<?PHP echo $rowhistory['comment'];?>
														 
													</div>
												</div> 
											</div>
										</div>
									</div>							
							<?PHP
								}
							} 
							?>
						</div>

						<!--end: Form Wizard Step 3-->

						<!--begin: Form Wizard Step 4-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-9">
									<h5>PERSONAL IDENTIFIABLE INFORMATION</h5><hr>						 
								</div>
								
							</div>
							<div class="row">
								<?PHP 
								foreach($getdatapiidoc as $piidoc){
									$qDoc_pii = $this->db->query("
													SELECT a.*	
													FROM data_innovasi_sdlc a
													where 1=1 and noinnovasi ='".$norequest."' and type = '".$piidoc['id_sdlc']."'");
									$getDoc_pii = $qDoc_pii->result_array();
									$jmdocpii 	= $qDoc_pii->num_rows();
									$docpii 	= array_shift($getDoc_pii);
									$idpii 		= $piidoc['id_sdlc'];
									$statuspii 	= $docpii['status']; 
								?>
									<?PHP 
										if($level == '2' and ($statuspii !=0 or $statuspii !=null)){ 
									?>
											<div class="kt-portlet kt-portlet--mobile"> 
													<div class="kt-portlet__body">
														<div class="kt-widget3">
														<?PHP 
														$keyword = 'REVIEW '.$piidoc['type_tab'].' | '.$piidoc['nama_sdlc'].'';
														$ghistory = "SELECT *, (select name from mi.user where userid = a.created_by) create_by
																	FROM mi.data_innovasi_history a where a.judul ='".$keyword."' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
														$gethistory = $this->db->query($ghistory)->result_array();
														foreach($gethistory as $rowhistory){
															if($rowhistory['action']=='COMPLY'){
																$color ='success';
																$showform = false;
															}else{
																$color ='danger';
																$showform = true;
															}
														?>
														<div class="kt-widget3__item">
															<div class="kt-widget3__header">
																<div class="kt-widget3__user-img">
																	<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
																</div>
																<div class="kt-widget3__info">
																	<a href="#" class="kt-widget3__username">
																		<?PHP echo $rowhistory['create_by'];?>
																	</a><br>
																	<span class="kt-widget3__time">
																		<?PHP echo $rowhistory['created_at'];?>
																	</span>
																</div>
																<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
																	<?PHP echo $rowhistory['action'];?>
																</span>
															</div>
															<div class="kt-widget3__body">
																 
																	<?PHP echo $rowhistory['comment'];?>
																 
															</div>
														</div> 
														<?PHP 
														}
														?> 
													</div>
												</div>
											</div> 	 
																				
									<?PHP
										}	 
									if($jmdocpii < 1){
										echo '
										<div class="col-md-12">
											<div class="alert alert-warning" role="alert">
												<div class="alert-icon"><i class="flaticon-warning"></i></div>
												<div class="alert-text">Dokumen Persetujuan Owner Tidak ditemukan.</div>
											</div>						
										</div>
										';
									}else{
										echo '
										<!--div class="col-md-12">
											<div class="alert alert-success" role="alert">
												<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
												<div class="alert-text">Dokumen Persetujuan Owner ditemukan</div>
											</div>						
										</div-->
										<div class="col-md-12">
											<object data="'.base_url().'attachment/'.$docpii['doc'].'" type="application/pdf" width="100%" height="400px">
											</object>
										</div>
										';
									} 
								} 
								?> 
								<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								
							</div>
							<?PHP 
							if($level == '2' and ($statuspii ==0 or $statuspii ==null) and $showform=true){
								$btnsubmit++;
							?>
							<div class="row">  
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Action :</strong></label>
										<div class="col-9">
											<div class="kt-radio-inline">
												<label class="kt-radio">
													<input type="radio" name="status<?PHP echo $idpii;?>" value='1'> Comply
													<span></span>
												</label>
												<label class="kt-radio">
													<input type="radio" name="status<?PHP echo $idpii;?>" value='2'> Not Comply
													<span></span>
												</label> 
											</div>
										</div>
									</div> 
								</div> 
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Komentar :</strong></label>
										<div class="col-9">
											 <textarea name="komentar<?PHP echo $idpii;?>" class="summernote" id="komentar<?PHP echo $idpii;?>"></textarea>
										</div>
									</div> 
								</div>
							</div>
							<?PHP
							} 
							?> 
						</div>

						<!--end: Form Wizard Step 4-->

						<!--begin: Form Wizard Step 5-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-12">
									<h5>SECURITY SYSTEM</h5><hr>							 
								</div>
								
								<?PHP if($security_status == true) { ?>
								<!--div class="col-md-12">
									<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Security System Not Comply</div>
									</div>						
								</div-->
								<?PHP } ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
									<?PHP 
									$n=0;
									foreach($getdatasecurity as $security){
										$n++;
									?>
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-9 col-form-label"><strong><?PHP echo $n;?>. Dokumen <?PHP echo $security['nama_sdlc'];?> :</strong></label>
													<div class="col-3">
														<div class="input-group">	
														<?PHP 
															$qdocs = $this->db->query("
																			SELECT a.*	
																			FROM data_innovasi_sdlc a
																			where 1=1 and noinnovasi ='".$norequest."' and type = '".$security['id_sdlc']."'");
															$getDocs = $qdocs->result_array();
															$numDocs = $qdocs->num_rows();
															$docs 	 = array_shift($getDocs);
															$idDocs  = $security['id_sdlc'];
															$statusDocs = $docs['status'];
															if($numDocs < 1){
																echo "-";
															}else{
																	echo '<button type="button" class="btn btn-sm btn-brand btn-pill btnpreview" data-toggle="modal" data-target="#modalpreview" data-file="'.$docs['doc'].'">Preview</button>';													
															}
														?>
														</div>
													</div>
												</div> 
											</div> 
										</div>
										<?PHP 
										if($level == '2' and ($statusDocs =='0' or $statusDocs==NULL)){
											$btnsubmit++;
										?>
										<div class="row">  
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Action :</strong></label>
													<div class="col-8">
														<div class="kt-radio-inline">
															<label class="kt-radio">
																<input type="radio" name="status<?PHP echo $idDocs;?>" value='1'> Comply
																<span></span>
															</label>
															<label class="kt-radio">
																<input type="radio" name="status<?PHP echo $idDocs;?>" value='2'> Not Comply
																<span></span>
															</label> 
														</div>
													</div>
												</div> 
											</div> 
											<div class="col-md-12"> 
												<div class="form-group row">
													<label for="example-text-input" class="col-4 col-form-label"><strong>Komentar :</strong></label>
													<div class="col-8">
														 <textarea name="komentar<?PHP echo $idDocs;?>" class="summernote" id="komentar<?PHP echo $idDocs;?>"></textarea>
													</div>
												</div> 
											</div>
										</div>
										<?PHP
										} else { 
											$keyword = 'REVIEW '.$security['type_tab'].' | '.$security['nama_sdlc'].'';
											$ghistory = "SELECT *, (select name from mi.user where userid = a.created_by) create_by
														FROM mi.data_innovasi_history a where a.judul ='".$keyword."' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
											$gethistory = $this->db->query($ghistory)->result_array();
											foreach($gethistory as $rowhistory){
												if($rowhistory['action']=='COMPLY'){
													$color ='success'; 
												}else{
													$color ='danger'; 
												}
											?>
											<div class="kt-portlet kt-portlet--mobile"> 
												<div class="kt-portlet__body"> 
													<div class="kt-widget3">
														<div class="kt-widget3__item">
															<div class="kt-widget3__header">
																<div class="kt-widget3__user-img">
																	<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
																</div>
																<div class="kt-widget3__info">
																	<a href="#" class="kt-widget3__username">
																		<?PHP echo $rowhistory['create_by'];?>
																	</a><br>
																	<span class="kt-widget3__time">
																		<?PHP echo $rowhistory['created_at'];?>
																	</span>
																</div>
																<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
																	<?PHP echo $rowhistory['action'];?>
																</span>
															</div>
															<div class="kt-widget3__body">
																 
																	<?PHP echo $rowhistory['comment'];?> 
															</div>
														</div>
													</div>
												</div>
											</div>
										<?PHP 
											} 
										}
										?>
									</div>
									<!--div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div-->
									<?PHP 
									} 
									?> 
									</div>
								</div>
							</div>
						</div>

						<!--begin: Form Wizard Step 5-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">								
								<div class="col-md-12">
									<h5>DATA SHARING TELKOM GROUP</h5><hr>								 
								</div> 
							</div>
							<div class="row">
							
								<?PHP 
								foreach($getdatastgdoc as $stgdoc){
									$qDoc_stg = $this->db->query("
													SELECT a.*	
													FROM data_innovasi_sdlc a
													where 1=1 and noinnovasi ='".$norequest."' and type = '".$stgdoc['id_sdlc']."'");
									$getDoc_stg = $qDoc_stg->result_array();
									$jmdocstg = $qDoc_stg->num_rows();
									$docstg		= array_shift($getDoc_stg);
									$status 	= $docstg['status'];
									$idstg 		= $stgdoc['id_sdlc']; 
									
									if($status!='0' or $status!=NULL){
								?>
										
													<?PHP  
													$ghistory = "SELECT *, (select name from mi.user where userid = a.created_by) create_by
																FROM mi.data_innovasi_history a where a.judul ='REVIEW DATA SHARING TELKOM GROUP | MoM DG Council Meeting' and a.noinnovasi='".$norequest."' order by created_at desc limit 1";
													$gethistory = $this->db->query($ghistory)->result_array();
													foreach($gethistory as $rowhistory){
														if($rowhistory['action']=='COMPLY'){
															$color ='success';
															$showform = false;
														}else{
															$color ='danger';
															$showform = true;
														}
													?>
													<div class='col-lg-12'>
														<div class="kt-portlet kt-portlet--mobile"> 
															<div class="kt-portlet__body">
																<div class="kt-widget3">
																	<div class="kt-widget3__item">
																		<div class="kt-widget3__header">
																			<div class="kt-widget3__user-img">
																				<img class="kt-widget3__img" src="<?PHP echo base_url();?>images/user/1601750535defaultuserimage.png" alt="">
																			</div>
																			<div class="kt-widget3__info">
																				<a href="#" class="kt-widget3__username">
																					<?PHP echo $rowhistory['create_by'];?>
																				</a><br>
																				<span class="kt-widget3__time">
																					<?PHP echo $rowhistory['created_at'];?>
																				</span>
																			</div>
																			<span class="kt-widget3__status kt-font-<?PHP echo $color;?>">
																				<?PHP echo $rowhistory['action'];?>
																			</span>
																		</div>
																		<div class="kt-widget3__body"> 
																				<?PHP echo $rowhistory['comment'];?> 
																		</div>
																	</div> 
																</div>
															</div>
														</div>
													</div> 
													<?PHP 
													}
													?>
													
								<?PHP							
									}  
									if($jmdocstg < 1){
										echo '
										<!--div class="col-md-12">
											<div class="alert alert-warning" role="alert">
												<div class="alert-icon"><i class="flaticon-warning"></i></div>
												<div class="alert-text">Dok MoM DG Council Meeting tidak ditemukan.</div>
											</div>						
										</div-->
										';
									}else{
										echo '
										<!--div class="col-md-12">
											<div class="alert alert-success" role="alert">
												<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
												<div class="alert-text">MoM DG Council Meeting ditemukan.</div>
											</div>						
										</div-->
										<div class="col-md-12">
											<object data="'.base_url().'attachment/'.$docstg['doc'].'" type="application/pdf" width="100%" height="400px">
											</object>
										</div>
										';
									} 
								} 
								?> 	 

							</div> 
							<?PHP  
							if($level == '2' and ($status=='0' or $status==NULL) and $showform==true){ 
								$btnsubmit++;
							?>
							<div class="row">  
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Action :</strong></label>
										<div class="col-9">
											<div class="kt-radio-inline">
												<label class="kt-radio">
													<input type="radio" name="status<?PHP echo $idstg; ?>" value='1'> Comply
													<span></span>
												</label>
												<label class="kt-radio">
													<input type="radio" name="status<?PHP echo $idstg; ?>" value='2'> Not Comply
													<span></span>
												</label> 
											</div>
										</div>
									</div> 
								</div> 
								<div class="col-md-12"> 
									<div class="form-group row">
										<label for="example-text-input" class="col-3 col-form-label"><strong>Komentar :</strong></label>
										<div class="col-9">
											 <textarea name="komentar<?PHP echo $idstg; ?>" class="summernote" id="komentar<?PHP $idstg; ?>"></textarea>
										</div>
									</div> 
								</div>
							</div>
							<?PHP
							}
							?>
						</div> 
						<!--end: Form Wizard Step 5-->

						<!--begin: Form Actions -->
						<div class="kt-form__actions">
							<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
								Previous
							</div>
							<?PHP if($btnsubmit !=0){?>
							<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
								Submit
							</div> 
							<?PHP } ?>
							<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
								Next Step
							</div>
						</div>

						<!--end: Form Actions -->
					</form>

					<!--end: Form Wizard Form-->
				</div>
			</div>
		</div>
	</div>
</div> 
