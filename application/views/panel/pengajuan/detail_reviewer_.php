<?PHP
error_reporting(0);
$userdata		= $this->session->userdata('sesspwt'); 
$userid 		= $userdata['userid'];
$req 			= str_replace('-','/',$id);
$norequest 		= str_replace('_','-',$req);

$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$level			= $dUser['step'];
$aksesCreate 	= $dUser['actionbtn'];

$getInnovation = $this->db->query("
				SELECT a.*,
				(SELECT picture from mi.user where userid=a.created_by) pictcreated,
				(select name from mi.user where userid=a.created_by)as usercreate,
				(SELECT name from mi.user where userid=a.created_by)as namecreated
				FROM data_innovasi a
				where 1=1 and noinnovasi ='".$norequest."'")->result_array();
$row 			= array_shift($getInnovation);

if ($level==1) {
	if ($row['status']!=0 or $row['status']!=2) {
		$hideact = 'kt-hidden';
	} else {
		$hideact = '';
	}
} else if ($level!=1) {
	if ($row['current']==$level) {
		$hideact = '';
	} else {
		$hideact = 'kt-hidden';
	}
} else {
	$hideact = 'kt-hidden';
}

if ($row['pictcreated']=='') {
	$hidden 	= 'kt-hidden';
} else {
	$hidden 	= '';
}

$pathfile 		= base_url().'attachment/';

$judul 			= $row['judul'];
$cfu_fu 		= $row['cfu_fu'];
$url 			= '<a href="'.$row['url_aplikasi'].'">'.$row['url_aplikasi'].'</a>';
$deskripsi 		= $row['deskripsi'];
$tujuan 		= $row['tujuan'];
$jenisdata 		= $row['jenis_data'];
$stakholder  	= $row['stakholder'];
$diagram 		= $pathfile.$row['diagram'];
$diagram2 		= $pathfile.$row['diagram2'];
$penjelasan	 	= $row['penjelasan_singkat'];
$pii_status 	= $row['pii_status'];
$sharing_group_status = $row['sharing_telkom_group'];

$sdlc_required= 0;
//GET DATA DOC REQUIRMENT
$docreq = getdatadoc('Lihat Dokumen Requirment',$norequest,'Dok.Requirment');
$docdesign = getdatadoc('Lihat Dokumen Design',$norequest,'Dok.Design');
$docsop = getdatadoc('Lihat Dokumen SOP',$norequest,'Dok.SOP');
$docsopd2p = getdatadoc('Lihat Dokumen SOP D2P',$norequest,'Dok.SOP D2P');
$docsmp = getdatadoc('Lihat Dokumen SMP',$norequest,'Dok.SMP');
$doctes = getdatadoc('Lihat Dokumen Test Report / Test Plan',$norequest,'Dok.Test Plan/Report');
if($docreq=='-'){}else{$sdlc_required++;}
if($docdesign=='-'){}else{$sdlc_required++;}
if($docsop=='-'){}else{$sdlc_required++;}
if($docsopd2p=='-'){}else{$sdlc_required++;}
if($docsmp=='-'){}else{$sdlc_required++;}
if($doctes=='-'){}else{$sdlc_required++;}

if($sdlc_required < 6){
	$sdlc_status =true;
}else{
	$sdlc_status =false;
}

//GET DATA UPTI 
$li_fr='';
$stdstatus_fr = 0;
$notstdstatus_fr = 0;
$getupti = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Framework'")->result_array();	
foreach($getupti as $row_fr){
	$cekupti = $this->db->query("select * from param_framework where framework='".$row_fr['name_upti']."'")->num_rows();
	if($cekupti > 0){
		$stdstatus_fr++;
		$stdfr = 'STANDARD';
	}else{
		$notstdstatus_fr++;
		$stdfr = 'NOT STANDARD';
	}
	$li_fr .= "<li>".$row_fr['name_upti']." <small>(".$stdfr.")</small></li>";
} 
if($stdstatus_fr > 0){
	$showalertfr = false;
}else{
	$showalertfr = true;								
}

$li_db = '';
$stdstatus_db = 0;
$notstdstatus_db = 0;
$getupti2 = $this->db->query("select * from data_innovasi_upti where noinnovasi='".$norequest."' and type='Database'")->result_array();	
foreach($getupti2 as $row_db){
	$cekdb = $this->db->query("select * from param_database where nama_database='".$row_db['name_upti']."'")->num_rows();
	if($cekdb > 0){
		$stdstatus_db++;
		$stddb = 'STANDARD';
	}else{
		$notstdstatus_db++;
		$stddb = 'NOT STANDARD';
	}
	$li_db .= "<li>".$row_db['name_upti']." <small>(".$stddb.")</small></li>";
} 
if($stdstatus_db > 0){
	$showalertdv = false;
}else{
	$showalertdb = true;								
}

//GET DATA SECUITY SYSTEM
$security_count = 0;
$docToU = getdatadoc('Lihat Dokumen Evidance ToU (Terms of Use)',$norequest,'Evidance.ToU');
$docTFA = getdatadoc('Lihat Dokumen Multi Factor Authentication',$norequest,'Evidance.TFA');
$docNDE = getdatadoc('Lihat Dokumen NDE Hasil VA Test',$norequest,'Evidance.NDE VA Test');
$docCustomer = getdatadoc('Lihat Dokumen Customer Consent',$norequest,'Evidance.Customer Consent');

if($docToU=='-'){}else{$security_count++;}
if($docTFA=='-'){}else{$security_count++;}
if($docNDE=='-'){}else{$security_count++;}

if($security_count < 3){
	$security_status =true;
}else{
	$security_status =false;
}
?>
<style>
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item{
		flex :1 0 0;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items{
		padding:0 2em;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body{
		padding: 2rem 0rem 0rem 0.5rem !important;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body .kt-wizard-v3__nav-label{
		font-size:12px;
	}
	.kt-wizard-v3 .kt-wizard-v3__nav .kt-wizard-v3__nav-items .kt-wizard-v3__nav-item .kt-wizard-v3__nav-body .kt-wizard-v3__nav-label span{
		font-size:12px;
	}
	.alert{
		padding :0rem 2rem !important;
	}
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.kt-wizard-v3 .kt-wizard-v3__wrapper .kt-form{
		width:90%;
		padding:1rem 0 5rem;
	}
	.mt15rem { margin-top: 9px !important; }
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	#jawaban .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	#history .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#history .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}
	.kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 60px !important;
    	height: 60px !important;
	}
	/*#wfinfo .kt-timeline-v2 .kt-timeline-v2__items .kt-timeline-v2__item .kt-timeline-v2__item-time {
		padding: 0rem;
	}
	#wfinfo .kt-user-card .kt-user-card__avatar .kt-badge, .kt-user-card .kt-user-card__avatar img {
		width: 40px;
    	height: 40px;
	}*/
	.nav-tabs.nav-tabs-line .nav-link{
		font-size:11px !important;
	}
	.tab-pane{
		font-size:12px !important;
		min-height:400px;
	} 
	.table-bordered th, .table-bordered td{
		border-color:#0b5e90;
	}
	p {
	    margin-top: 0;
	    margin-bottom: 0.2rem !important;
	}
</style>
<!-- MODAL PREVIEW -->
<div class="modal fade" id="modalpreview" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
				<!--div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">Dokumen</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
					</div>
				</div-->
				<div class="kt-portlet__body">
					<div class='row'>
						<div class="col-lg-12" style='min-height:500px;'>
							<iframe src="" id='embedsrc' type="application/pdf" width="100%" height="100%">
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END MODAL PREVIEW -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet">
		<div class="kt-portlet__body kt-portlet__body--fit">
			<div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
				<div class="kt-grid__item">

					<!--begin: Form Wizard Nav -->
					<div class="kt-wizard-v3__nav">
						<div class="kt-wizard-v3__nav-items">
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>1</span> Innovation Description
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>2</span> SDLC
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>3</span> Technology Platform
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label" style='font-size:10px;'>
										<span>4</span> Personal Identifiable Information
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>5</span> Security System
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
							<a class="kt-wizard-v3__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v3__nav-body">
									<div class="kt-wizard-v3__nav-label">
										<span>6</span> Data Sharing Telkom Group
									</div>
									<div class="kt-wizard-v3__nav-bar"></div>
								</div>
							</a>
						</div>
					</div>

					<!--end: Form Wizard Nav -->
				</div>
				<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">

					<!--begin: Form Wizard Form-->
					<form class="kt-form" id="kt_form" method='POST' action='<?PHP echo base_url(); ?>/ctrl_innovasi/review_innovasi'> 
						<!--begin: Form Wizard Step 1-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
							<div class="row">
									<div class="col-md-9">
										<h5>INNOVATION DESCRIPTION</h5>
									</div>
									<div class="col-md-3">
										<div class="kt-radio-inline">
											<input type='hidden' name='noinnovasi' value='<?PHP echo $norequest; ?>'>
											<label class="kt-radio">
												<input type="radio" name="status_data_innovasi" value='1'> Comply
												<span></span>
											</label>
											<label class="kt-radio">
												<input type="radio" name="status_data_innovasi" value='2'> Not Comply
												<span></span>
											</label> 
										</div>
									</div> 
									<div class="col-lg-12"><hr></div>
									<div class="col-lg-12">
										<div>
										<table class='table table-bordered' style='border:solid 1px #013453;'>
											<tbody>
											<tr>
												<td width='12%' style='text-align:right;background-color:#013453;' class='text-white'><b>Judul:</b></td>
												<td><?PHP echo $judul;?></td>
												<td rowspan='4' colspan='2' style='padding: 0rem;'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Arsitektur & Flow Proses Aplikasi</b></div>
													<div style='padding:5px;'>
														Arsitektur Aplikasi :
														<a class="example-image-link" href="<?PHP echo $diagram; ?>" data-lightbox="example-set" data-title="Arsitektur Aplikasi">
															<img class="example-image" src="<?PHP echo $diagram; ?>" height='100px' alt=""/>
														</a>
														<hr>
														Flow Proses Aplikasi :
														<a class="example-image-link" href="<?PHP echo $diagram2; ?>" data-lightbox="example-set" data-title="Flow Proses Aplikasi">
															<img class="example-image" src="<?PHP echo $diagram2; ?>" height='100px' alt=""/>
														</a>
													</div>
												</td>
											</tr>
											<tr>
												<td style='text-align:right;background-color:#013453;' class='text-white'><b>CFU/FU:</b></td>
												<td><?PHP echo $cfu_fu;?></td>
												
											</tr>
											<tr>
												<td style='text-align:right;background-color:#013453;' class='text-white'><b>URL:</b></td>
												<td><?PHP echo $url;?></td>
											</tr>
											<tr>
												<td colspan='2' style='width:50%;padding: 0rem'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Deskripsi & Tujuan</b></div>
													<div style='padding:5px;'>
														<b>Deskripsi:</b>
														<?PHP echo str_replace("<div><br></div>", "", $deskripsi);?> 
														<hr>
														<b>Tujuan:</b>
														<?PHP echo str_replace("<div><br></div>", "", $tujuan);?> 
													</div>
												</td>
											</tr>

		 									<tr>
												<td colspan='2' style='padding: 0rem'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Jenis Data & Stakeholder</b></div> 
													<div class="row" style='padding:5px;'>
														<div class="col-md-6">
															<b>Jenis Data:</b>
															<?PHP echo str_replace("<div><br></div>", "", $jenisdata);?> 
														</div>
														<div class="col-md-6">
															<b>Stakeholder:</b>
															<?PHP echo str_replace("<div><br></div>", "", $stakholder);?> 
														</div>
													</div>
												</td>
												<td colspan='2' style='padding: 0rem;'>
													<div style='background-color:#013453;padding:5px;' class='text-white'><b>Penjelasang Singkat Arsitektur & Flow Proses Aplikasi</b></div> 
													<div style='padding:5px;'>
														<?PHP echo $penjelasan?>
													</div>
												</td>
											 
											</tr>
											</tbody>
										</table>
									</div>
									</div>
							</div>
						</div>

						<!--end: Form Wizard Step 1-->

						<!--begin: Form Wizard Step 2-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-9">
									<h5>AGILE SDLC</h5>							 
								</div>
								<div class="col-md-3">
									<div class="kt-radio-inline">
										<label class="kt-radio">
											<input type="radio" name="status_sdlc" value='1'> Comply
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="status_sdlc" value='2'> Not Comply
											<span></span>
										</label> 
									</div>
								</div>
								<div class="col-md-12">	 
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								</div>
								<?PHP if($sdlc_status == true) { ?>
								<div class="col-md-12">
									<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Required Not Comply</div>
									</div>						
								</div>
								<?PHP } ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul>
										<li>
											Requirment
											<div>
											<?PHP  echo $docreq; ?> 
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

										</li>
										<li>
											Architecture & Design
											<div>
											<?PHP  echo $docdesign; ?> 
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										</li>
										<li>
											Development
											<div>
											<?PHP  echo $docsop.$docsopd2p.$docsmp; ?> 
											</div>

											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										</li>
										<li>
											Test & Feedback
											<div>
											<?PHP  echo $doctes; ?> 
											</div>

											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										</li>
									</ul>							 
								</div>
							</div>							 
						</div>

						<!--end: Form Wizard Step 2-->

						<!--begin: Form Wizard Step 3-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-9">
									<h5>TEKNOLOGI PLATFORM</h5>							 
								</div>
								<div class="col-md-3">
									<div class="kt-radio-inline">
										<label class="kt-radio">
											<input type="radio" name="status_upti" value='1'> Comply
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="status_upti" value='2'> Not Comply
											<span></span>
										</label> 
									</div>
								</div>
								<div class="col-md-12">
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								</div>
								<div class="col-md-6">
									<h6>LANGUAGE & FRAMEWORK</h6>							 
									<?PHP if($showalertfr == true) { ?>
									<div class="col-md-12">
										<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Not Standard Language & Framework</div>
									</div>						
									</div>
									<?PHP } else { ?>
									<div class="col-md-12">
										<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">Standard Language & Framework</div>
									</div>						
									</div>
									<?PHP } ?>
									<ul style='color:black'>
									<?PHP
										echo $li_fr;
									?>
									</ul>
								</div>
								<div class="col-md-6">
									<h6>DATABASE</h6>							 
									<?PHP if($showalertdb == true) { ?>
									<div class="col-md-12">
										<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Not Standard Database</div>
									</div>						
									</div>
									<?PHP } else { ?>
									<div class="col-md-12">
										<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">Standard Database</div>
									</div>						
									</div>
									<?PHP } ?>
									<ul>
									<?PHP
										echo $li_db;
									?>
									</ul>

								</div>						 
							</div>	 
						</div>

						<!--end: Form Wizard Step 3-->

						<!--begin: Form Wizard Step 4-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-9">
									<h5>PERSONAL IDENTIFIABLE INFORMATION</h5>							 
								</div>
								<div class="col-md-3">
									<div class="kt-radio-inline">
										<label class="kt-radio">
											<input type="radio" name="status_pii" value='1'> Comply
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="status_pii" value='2'> Not Comply
											<span></span>
										</label> 
									</div>
								</div>
								<div class="col-md-12">
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								</div>
							</div>
							<div class="row">
								<?PHP 
								$file_pii = './attachment/'.$row['pii_evidance'];
								if (file_exists($file_pii)) {
								    $file_pii_exi = true;
								} else {
								    $file_pii_exi = false;
								}
								?>
								<?PHP if($file_pii_exi == true) { ?>
								<div class="col-md-12">
									<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Dokumen Persetujuan Owner Tidak ditemukan.</div>
									</div>						
								</div>
								<?PHP } else { ?>
								<div class="col-md-12">
									<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">Dokumen Persetujuan Owner ditemukan</div>
									</div>						
								</div>
								<?PHP  
								if($file_pii_exi == true){
								?>
								<div class="col-md-12">
									<object data="<?PHP echo base_url();?>attachment/<?PHP echo $row['pii_evidance'];?>" type="application/pdf" width="100%" height="400px">
									</object>
								</div>
								<?PHP 
								}else{
									echo "<center>File Broken Or Not Exist , Please Contact Administrator !!!</center>";
								}
								?>
								<?PHP } ?> 
							</div>	 
						</div>

						<!--end: Form Wizard Step 4-->

						<!--begin: Form Wizard Step 5-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">
								<div class="col-md-9">
									<h5>SECURITY SYSTEM</h5>							 
								</div>
								<div class="col-md-3">
									<div class="kt-radio-inline">
										<label class="kt-radio">
											<input type="radio" name="status_security" value='1'> Comply
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="status_security" value='2'> Not Comply
											<span></span>
										</label> 
									</div>
								</div>
								<div class="col-md-12">
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								</div>
								<?PHP if($security_status == true) { ?>
								<div class="col-md-12">
									<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Security System Not Comply</div>
									</div>						
								</div>
								<?PHP } ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<ul>
										<li>
											Evidance ToU (Terms of Use)
											<div>
											<?PHP  echo $docToU; ?> 
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

										</li>
										<li>
											Evidance Multifactor Authentication
											<div>
											<?PHP  echo $docTFA; ?> 
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										</li>
										<li>
											Evidance Customer Consent
											<div>
											<?PHP  echo $docCustomer; ?> 
											</div>

											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										</li>
										<li>
											Evidance NDE Hasil VA Test
											<div>
											<?PHP  echo $docNDE; ?> 
											</div>

											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
										</li>
									</ul>							 
								</div>
							</div>
						</div>

						<!--begin: Form Wizard Step 5-->
						<div class="kt-wizard-v3__content" data-ktwizard-type="step-content">
							<div class="row">								
								<div class="col-md-9">
									<h5>DATA SHARING TELKOM GROUP</h5>								 
								</div>
								<div class="col-md-3">
									<div class="kt-radio-inline">
										<label class="kt-radio">
											<input type="radio" name="status_data_telkom_group" value='1'> Comply
											<span></span>
										</label>
										<label class="kt-radio">
											<input type="radio" name="status_data_telkom_group" value='2'> Not Comply
											<span></span>
										</label> 
									</div>
								</div>
								<div class="col-md-12">					 
									<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
								</div>
							</div>
							<div class="row">
								<?PHP
								$file_stg = './attachment/'.$row['sharing_telkom_group'];
								if (file_exists($file_stg)) {
								    $sharing_group_status = true;
								} else {
								    $sharing_group_status = false;
								}
								?>
								<?PHP if($sharing_group_status == false) { ?>
								<div class="col-md-12">
									<div class="alert alert-warning" role="alert">
										<div class="alert-icon"><i class="flaticon-warning"></i></div>
										<div class="alert-text">Dok MoM DG Council Meeting tidak ditemukan.</div>
									</div>						
								</div>
								<?PHP } else { ?>
								<div class="col-md-12">
									<div class="alert alert-success" role="alert">
										<div class="alert-icon"><i class="flaticon2-checkmark"></i></div>
										<div class="alert-text">MoM DG Council Meeting ditemukan.</div>
									</div>						
								</div>
								<div class="col-md-12">
									<object data="<?PHP echo base_url();?>attachment/<?PHP echo $row['sharing_telkom_group'];?>" type="application/pdf" width="100%" height="400px">
									</object>
								</div>
								<?PHP } ?>
							</div> 
						</div>


						<!--end: Form Wizard Step 5-->

						<!--begin: Form Actions -->
						<div class="kt-form__actions">
							<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev">
								Previous
							</div>
							<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit">
								Submit
							</div>
							<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next">
								Next Step
							</div>
						</div>

						<!--end: Form Actions -->
					</form>

					<!--end: Form Wizard Form-->
				</div>
			</div>
		</div>
	</div>
</div> 
