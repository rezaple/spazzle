<?PHP
$userdata		= $this->session->userdata('sessSpazzle'); 
$userid 		= $userdata['userid'];
$getUser 		= $this->db->query("
					SELECT a.*, 
						(SELECT name from mi.level_user where id_level=a.level_user) namelevel,
						(SELECT level from mi.level_user where id_level=a.level_user) step,
						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
					FROM mi.user a where userid='$userid'
				")->result_array();
$dUser 			= array_shift($getUser);
$nameuser		= $dUser['name'];
$namelevel		= $dUser['namelevel'];
$level			= $dUser['step'];
$level_user		= $dUser['level_user'];
$cond 			= "
				and (noinnovasi in (
					SELECT noinnovasi from mi.data_innovasi_history where created_by='$userid' or send_to='$userid'
				) or created_by='$userid')
				";
if($level == 0){
	$cond = '';
}else if($level == 1){
	//$cond = "and created_by='$userid'";
	$cond = '';
}else{
	$cond = '';
}
$getComply=0;
$getPending=0;
$getNotComply=0;
$getinovasi = $this->db->query("SELECT noinnovasi from data_innovasi where 1=1 and status !=3 $cond")->result_array();
foreach($getinovasi as $di){
			$cekstatus ="select 
							noinnovasi ,status_data_innovasi,status_upti,
							case when status_security =jumlah_security then 1 else 0 end as final_status_security,
							case when status_spasi =jumlah_spasi then 1 else 0 end as final_status_spasi
							from(
								select 
								*, 
								(select count(*) from mi.param_sdlc where type_tab='SPASI') as jumlah_spasi,
								(select count(*) from mi.param_sdlc where type_tab='SECURITY SYSTEM')as jumlah_security
								from (
								select 
									a.noinnovasi ,
									a.status_data_innovasi ,
									a.status_upti, 
									(select SUM(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on sd.id_sdlc = cast(dis.type as integer)
									where noinnovasi =a.noinnovasi and type_tab = 'SECURITY SYSTEM' and status='1'
									)as status_security,
									(select SUM(status) from mi.data_innovasi_sdlc dis left join mi.param_sdlc sd on sd.id_sdlc = cast(dis.type as integer) 
									where noinnovasi =a.noinnovasi and type_tab = 'SPASI' and status='1') as status_spasi
								from mi.data_innovasi a
								where a.noinnovasi ='".$di['noinnovasi']."' 
								)as master
							)as master2 ";
			$arr_status = $this->db->query($cekstatus)->result_array();
			$rs = array_shift($arr_status);
			 
			if($rs['status_data_innovasi']==1 
				and $rs['status_upti']==1 
				and $rs['final_status_security']==1 
				and $rs['final_status_spasi']==1 
			){
				$getComply++;
			}else if(
				$rs['status_data_innovasi']==NULL
				and $rs['status_upti']==NULL 
				and $rs['final_status_security']==0 
				and $rs['final_status_spasi']==0 ){
				$getPending++;				

			}else{
				$getNotComply++;		

			} 
			
}
// $getPending 	= $this->db->query("SELECT * from data_innovasi where status in (0) $cond")->num_rows();
// $getComply 		= $this->db->query("SELECT * from data_innovasi where status in (7) $cond")->num_rows();
// $getNotComply 	= $this->db->query("SELECT * from data_innovasi where status not in (0,7) $cond")->num_rows();


$aksesCreate 	= $dUser['actionbtn'];

if($level_user !='1'){ 
	$textheader = '<span class="first-text">'.$namelevel.'</span>';
}else{
	$textheader = '<span class="first-text">Innovator Clinic</span> <br><span class="second-text">Register Your Innovation</span>';
}
?>
<style> 
	/* 
	  ##Device = Laptops, Desktops
	  ##Screen = B/w 1025px to 1280px
	*/
	@media (min-width: 1025px) and (max-width: 1280px) {
	  	.first-text {
			font-size:18px !important;
		}
		.second-text {
			font-size:12px !important;
		}
		#orange { 
			background-size: 130% 93% !important;
		}
		.btn.btn-wide{
			padding-left: 1.25rem !important;
   			padding-right: 1.25rem !important;
		}
		.mt15rem{
			font-size:8px !important;
		}	  
	}
	/* 
	  ##Device = Tablets, Ipads (portrait)
	  ##Screen = B/w 768px to 1024px
	*/
	@media (min-width: 768px) and (max-width: 1024px) { 
	}
	/* 
	  ##Device = Tablets, Ipads (landscape)
	  ##Screen = B/w 768px to 1024px
	*/
	@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
	}
	/* 
	  ##Device = Low Resolution Tablets, Mobiles (Landscape)
	  ##Screen = B/w 481px to 767px
	*/
	@media (min-width: 481px) and (max-width: 767px) {
	}
	/* 
	  ##Device = Most of the Smartphones Mobiles (Portrait)
	  ##Screen = B/w 320px to 479px
	*/
	@media (min-width: 320px) and (max-width: 480px) {
	}
	.first-text {
		font-size:22px;
	}
	.second-text {
		font-size:14px;
	}
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	.mt15rem {font-size:9px; }
	.text-pending { color:#ad6627 !important;font-size: 28px; }
	.text-comply { color:#368360 !important;font-size: 28px; }
	.text-nocomply { color:#a53641 !important;font-size: 28px; }	
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
	.kt-portlet.kt-portlet--solid-danger {
		background: #c0392b!important;
	}
	.kt-portlet{
		border-radius:10px !important;
	}
	#indigo {
		background:#013453;
	}
	#orange {
		min-height: 150px;
		object-fit: contain;
		background-color:#013453;
		background-image: url(<?PHP echo base_url();?>/assets/rocket-blue_small.png) ; 
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-repeat:no-repeat;
		background-size: 120% 93%;
    	background-position:50% 400%;
	}
	#indigo-transparent{
		background-color: #eeeeee;
	}
	.text-app2{
		height: 70px;
		font-size: 24px;
		font-weight: bold;
		font-stretch: normal;
		font-style: normal;
		line-height: 1.11;
		letter-spacing: normal;
		text-align: left;
		color: #555555;
	}
	#btncreate{
		width:100% !important;
		border-radius: 20px;
		box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
		background-color: #d8494f;
	}
	.nav-tabs.nav-tabs-line.nav-tabs-line-danger.nav.nav-tabs .nav-link:hover > i, .nav-tabs.nav-tabs-line.nav-tabs-line-danger.nav.nav-tabs .nav-link.active > i, .nav-tabs.nav-tabs-line.nav-tabs-line-danger a.nav-link:hover > i, .nav-tabs.nav-tabs-line.nav-tabs-line-danger a.nav-link.active > i{
		color:#512857 !important;		
	}
	.nav-tabs.nav-tabs-line.nav-tabs-line-danger.nav.nav-tabs .nav-link:hover, .nav-tabs.nav-tabs-line.nav-tabs-line-danger.nav.nav-tabs .nav-link.active, .nav-tabs.nav-tabs-line.nav-tabs-line-danger a.nav-link:hover, .nav-tabs.nav-tabs-line.nav-tabs-line-danger a.nav-link.active{
		color:#512857 !important;
		border-bottom: 1px solid #512857 !important;
	}
	.pad2{
		padding-right:2px !important;padding-left:2px !important
	}
	.margin-bottom-0{
		margin-bottom:0px;
	}
	#tabledata{
		font-size:12px;
	}
	.dataTables_wrapper .dataTable th, .dataTables_wrapper .dataTable td{
		color :#0e0e0e;
	}
</style>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3 col-sm-12" style='max-width:20%'>
			<div class="kt-portlet kt-portlet--mobile" id="orange"> 
				<div class="kt-portlet__body">
					<div class="row"> 
						<div class="col-md-12 col-sm-12">
							<h6 class="text-app2 text-white"><?PHP echo $textheader;?></h6>
						</div>
					</div>
					<div class="row"> 
						<div class="col-md-12 col-sm-12">
							<!--img src='<?PHP echo base_url();?>assets/rocket-blue.png' height='180em'-->
						</div>
					</div>
					<div class="row" style="margin-top:12em;"> 
						<div class="col-md-1 col-sm-1">
						</div>
						<div class="col-md-10 col-sm-10">
							<?PHP if($level_user!=3){?>
						 	<a href = '<?PHP echo base_url();?>panel/input_inovasi'>
								<button class="btn btn-danger btn-wide btn-md btn-icon-md" id='btncreate' style='font-size:10px;'>
									<i class="flaticon flaticon-plus"></i>
									Create New 
								</button>
							</a>
							<?PHP } else { ?>
								<!--div style='height:40px;'>&nbsp;</div-->
							<?PHP } ?>
						</div>
						<div class="col-md-1 col-sm-1">
						</div>
					</div>
				</div>
			</div>
			<div class="kt-portlet kt-portlet--mobile" id="indigo"> 
				<div class="kt-portlet__body" style='padding:20px'>
					<div class="row"> 
						<div class="col-md-12 col-sm-12">
							<h6 class="text-white">APPS Innovation :</h6>
						</div>
					</div>
					<div class="row margin-bottom-0"> 
						<div class="col-md-4 col-sm-12 pad2 margin-bottom-0">
							<div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid" id='indigo-transparent'>
								<div class="kt-portlet__body" style='padding:15px;'>
									<div class="row">
										<div class="col-sm-12 mt15rem pad2"><center>Comply</center></div>
										<div class="col-sm-12">
											<h4 style="margin-top: 0.5rem;">
												<a class="text-comply" id="jmlApproved"><center><?PHP echo $getComply;?></center></a>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 pad2 margin-bottom-0">
							<div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid" id='indigo-transparent'>
								<div class="kt-portlet__body" style='padding:15px;'>
									<div class="row">
										<div class="col-sm-12 mt15rem pad2"><center>Pending</center></div>
										<div class="col-sm-12">
											<h4 style="margin-top: 0.5rem;">
												<a class="text-pending" id="jmlPending"><center><?PHP echo $getPending;?></center></a>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 pad2 margin-bottom-0">
							<div class="kt-portlet kt-portlet--solid-default kt-portlet--height-fluid" id='indigo-transparent'>
								<div class="kt-portlet__body" style='padding:15px;'>
									<div class="row">
										<div class="col-sm-12 mt15rem pad2"><center>Not Comply</center></div>
										<div class="col-sm-12">
											<h4 style="margin-top: 0.5rem;">
												<a class="text-nocomply" id="jmlRejected"><center><?PHP echo $getNotComply;?></center></a>
											</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9 col-sm-12">
			<div class="row">
				<div class="kt-portlet kt-portlet--mobile">
					<!--div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h3 class="kt-portlet__head-title">
								<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-danger" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#draft" role="tab"><i class="flaticon flaticon-file-2"></i> INNOVATION </a>
									</li>
								</ul>
							</h3>
						</div>
						<div id="bgkonten" class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-toolbar-wrapper">
								<?PHP //echo getRoleBtnAction($aksesCreate,'modalinsertinovasi','Buat Pengajuan INOVASI','submit','btn-info btn-sm','flaticon flaticon-doc');?>
								<?PHP //echo getRoleBtnAction($aksesCreate,'modalinsertE','Buat Pengajuan CFUE-E','submit','btn-info btn-sm','flaticon flaticon-doc');?>
								
							</div>
						</div>
					</div-->

					<!-- MODAL INSERT -->
					<div class="modal fade" id="modalinsert" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">

								<form class="kt-form kt-form--label-right" id="forminsertsingle" enctype="multipart/form-data">
									<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">Form Pengajuan</h3>
											</div>
											<div class="kt-portlet__head-toolbar">
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class='row'>
												<div class="col-lg-5">
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Nama Project *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="namaproject" class="form-control" id="namaproject" placeholder="Nama Project">
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Subject *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="subject" class="form-control" id="subject" placeholder="Subject">
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-5"> 
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Pelanggan *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<select name="pelanggan" class="form-control getCustomer" id="pelanggan" data-placeholder="Pelanggan" style="width: 100%;">
																	<option value=""></option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Service ID *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="serviceid" class="form-control" id="serviceid" placeholder="Service ID">
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-5">
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">No Order (NCX) *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="no_order_ncx" class="form-control" id="no_order_ncx" placeholder="No Order (NCX)">
															</div>
														</div>
													</div> 
												</div>
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
											
											<div class="row">
												<?PHP
												$getSelAppr 	= $this->db->query("SELECT * from mi.level_user where is_select=1 order by level")->result_array();
												foreach ($getSelAppr as $apprflow) {
													$level 		= $apprflow['level'];
													if(strpos(strtolower($apprflow['action']),strtolower('approve')) !== false){
														$labeldis = 'Disetujui';
													} else {
														$labeldis = 'Di-Review';
													}
												?>
												<div class="col-lg-3">
													<label class="col-form-label col-sm-12"><?PHP echo $labeldis; ?> Oleh *<br>(<?PHP echo $apprflow['name']; ?>)</label>
													<div class="col-sm-12">
														<div class='input-group'>
															<select name="appflow[]" class="form-control getAF<?PHP echo $level; ?>" id="appflow<?PHP echo $level; ?>" data-placeholder="Pilih..." style="width: 100%;">
															</select>
														</div>
													</div>
												</div>
												<?PHP } ?>
											</div><br>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
											
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Latar Belakang *</label>
														<div class="col-sm-12">
															<textarea name="latarbelakang" class="summernote" id="latarbelakang"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Aspek Strategis *</label>
														<div class="col-sm-12">
															<textarea name="aspekstrategis" class="summernote" id="aspekstrategis"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Aspek Finansial *</label>
														<div class="col-sm-12">
															<textarea name="aspekfinansial" class="summernote" id="aspekfinansial"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Aspek Kompetisi *</label>
														<div class="col-sm-12">
															<textarea name="aspekkompetisi" class="summernote" id="aspekkompetisi"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Konfigurasi Teknis *</label>
														<div class="col-sm-12">
															<textarea name="konfigurasiteknis" class="summernote" id="konfigurasiteknis"></textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Analisa Teknis /Bisnis *</label>
														<div class="col-sm-12">
															<textarea name="analisateknis" class="summernote" id="analisateknis"></textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
											<div class="input_fields_wrap">
												<div class="row">
													<div class="col-lg-12"> 
														<button type="button" class="btn btn-label-dark btn-pill add_field_button pull-right">Add More Fields</button>
													</div>
												</div>
												<div class="row">
														<div class="col-lg-3"> 
															<div class="form-group row">
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<label>Layanan *</label>
																	<div class='input-group'>
																		<input type="text" name="layanan[]" class="form-control" id="layanan" placeholder="Layanan">
																	</div>
																</div>
															</div> 
														</div>
														<div class="col-lg-3"> 
															<div class="form-group row">
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<label>Volume *</label>
																	<div class='input-group'>
																		<input type="text" name="volume[]" class="form-control" id="volume" placeholder="Volume">
																	</div>
																</div>
															</div> 
														</div>
														<div class="col-lg-3"> 
															<div class="form-group row">
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<label>Tarif *</label>
																	<div class='input-group'>
																		<input type="text" name="tarif[]" class="form-control" id="tarif" placeholder="Tarif">
																	</div>
																</div>
															</div> 
														</div>
														<div class="col-lg-3"> 
															<div class="form-group row">
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<label>Price *</label>
																	<div class='input-group'>
																		<input type="text" name="price[]" class="form-control" id="price" placeholder="Price">
																	</div>
																</div>
															</div>
														</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div class="row">
														<label class="col-form-label col-lg-4 col-sm-12">Attachment *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div id="bgattach">
																<div class="input-group">
												                    <input type="file" class="form-control" name="attach[]" id="attach">
												                    <a href="#" class="col-lg-1">
												                    </a>
												                </div>
															</div>
															<br>
															<button type="button" class="btn btn-sm btn-default btnAddAttach">
																<i class="flaticon flaticon-plus"></i> Tambah Attachment
															</button>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" id="savedraft" class="btn btn-primary"><i class="la la-save"></i> Save as Draft</button>
										<button type="submit" id="savepublish" class="btn btn-success"><i class="la la-rocket"></i> Save and Publish</button>
									</div>
								</form>

							</div>
						</div>
					</div>
					<!-- END MODAL INSERT -->

					<!-- MODAL UPDATE -->
					<div class="modal fade" id="update" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">

								<form class="kt-form kt-form--label-right" id="formupdate" enctype="multipart/form-data">
									<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">Update Data : <b id="updatename"></b></h3>
											</div>
											<div class="kt-portlet__head-toolbar">
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class='row'>
												<div class="col-lg-5">
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Nama Project *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="hidden" name="ed_id" id="ed_id">
																<input type="hidden" name="ed_norequest" id="ed_norequest">
																<input type="hidden" name="statusbefore" id="statusbefore">
																<input type="text" name="ed_namaproject" class="form-control" id="ed_namaproject" placeholder="Nama Project">
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Subject *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="ed_subject" class="form-control" id="ed_subject" placeholder="Subject">
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-5"> 
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Pelanggan *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<select name="ed_pelanggan" class="form-control getCustomer" id="ed_pelanggan" data-placeholder="Pelanggan" style="width: 100%;">
																	<option value=""></option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Service ID *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="ed_serviceid" class="form-control" id="ed_serviceid" placeholder="Service ID">
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-5">
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">No Order (NCX) *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'>
																<input type="text" name="ed_no_order_ncx" class="form-control" id="ed_no_order_ncx" placeholder="No Order (NCX)">
															</div>
														</div>
													</div> 
												</div>
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>

											<div id="ed_bgappflow" class="row"></div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
											
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Latar Belakang *</label>
														<div class="col-sm-12">
															<textarea name="ed_latarbelakang" class="summernote" id="ed_latarbelakang"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Aspek Strategis *</label>
														<div class="col-sm-12">
															<textarea name="ed_aspekstrategis" class="summernote" id="ed_aspekstrategis"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Aspek Finansial *</label>
														<div class="col-sm-12">
															<textarea name="ed_aspekfinansial" class="summernote" id="ed_aspekfinansial"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Aspek Kompetisi *</label>
														<div class="col-sm-12">
															<textarea name="ed_aspekkompetisi" class="summernote" id="ed_aspekkompetisi"></textarea>
														</div>
													</div>

													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Konfigurasi Teknis *</label>
														<div class="col-sm-12">
															<textarea name="ed_konfigurasiteknis" class="summernote" id="ed_konfigurasiteknis"></textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-form-label text-left col-sm-12">Analisa Teknis /Bisnis *</label>
														<div class="col-sm-12">
															<textarea name="ed_analisateknis" class="summernote" id="ed_analisateknis"></textarea>
														</div>
													</div>
												</div>
											</div>
											<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
											<div class="row">
												<div class="col-lg-12"> 
													<button type="button" class="btn btn-label-dark btn-pill ed_add_field_button pull-right">Add More Fields</button>
												</div>
											</div>
											<div class="ed_input_fields_wrap" id='loadLayanan'> 
											</div>
											<div class="row">
												<div class="col-lg-6">
													<div id="atteksis"></div>
													<div class="row">
														<label class="col-form-label col-lg-4 col-sm-12">Attachment *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div id="ed_bgattach">
																<div class="input-group">
												                    <input type="file" class="form-control" name="ed_attach[]" id="ed_attach">
												                    <a href="#" class="col-lg-1">
												                    </a>
												                </div>
															</div>
															<br>
															<button type="button" class="btn btn-sm btn-default ed_btnAddAttach">
																<i class="flaticon flaticon-plus"></i> Tambah Attachment
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" id="saveupdatedraft" class="btn btn-primary"><i class="la la-save"></i> Update as Draft</button>
										<button type="submit" id="saveupdatepublish" class="btn btn-success"><i class="la la-rocket"></i> Update and Publish</button>
									</div>
								</form>

							</div>
						</div>
					</div>
					<!-- END MODAL UPDATE -->

					<!-- MODAL DELETE -->
					<div class="modal fade" id="modalsubmit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm" role="document">
							<div class="modal-content">
								<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
									<div class="swal2-header">
										<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
										<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure want to submit data?</h2>
									</div>
									<div class="swal2-content">
										<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
									</div>
									<div class="swal2-actions" style="display: flex;">
										<form method="POST">
										<input type="hidden" name="idsubmit" id="idsubmit" value="<?PHP echo $userid; ?>">
										<center>
										<button type="button" id="submitBtn" class="swal2-styled btn btn-success" aria-label="">
											Yes, submit it!
										</button>
										<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
										</center>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END MODAL DELETE -->

					<!-- MODAL ACTION -->
					<div class="modal fade" id="modalaction" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-md" role="document">
							<form method="POST">
								<div class="modal-content row">
									<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show col-sm-12" style="display: flex;">
										<div class="swal2-header">
											<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
											<h2 class="swal2-title" id="swal2-title">Are you sure want to <b class="labaction"></b> SBR?</h2>
										</div>
										<div class="swal2-content">
											<div id="swal2-content">You won't be able to revert this!</div>
										</div>
										<div class="swal2-actions">
											<input type="hidden" name="idsubmitact" id="idsubmitact">
											<input type="hidden" name="typesubmitact" id="typesubmitact">
											<div class="form-group row">
												<label class="col-form-label text-left col-sm-12">Write a comment *</label>
												<div class="col-sm-12">
													<textarea name="comment" class="form-control" style="width: 100%;" id="comment"></textarea>
												</div>
											</div>
											<center>
											<button type="submit" id="submitActBtn" class="swal2-styled btn btn-brand" aria-label="">
												Yes, <span class="labaction"></span> it!
											</button>
											<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
											</center>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- END MODAL ACTION -->

					<!-- MODAL APPROVAL -->
					<div class="modal fade" id="modalapprove" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-md" role="document">
							<form method="POST">
							<div class="modal-content row">
								<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show col-sm-12" style="display: flex;">
									<div class="swal2-header">
										<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
										<h2 class="swal2-title" id="swal2-title">Are you sure want to <b class="labaction"></b> SBR?</h2>
									</div>
									<div class="swal2-content">
										<div id="swal2-content">You won't be able to revert this!</div>
									</div>
									<div class="swal2-actions">
										<input type="hidden" name="idsubmitapprove" id="idsubmitapprove">
										<input type="hidden" name="id_no_answer" id="id_no_answer">
										<input type="hidden" name="typesubmitact" id="typesubmitact">
										<div class="form-group row">
											<label class="col-form-label text-left col-sm-12">Write a comment *</label>
											<div class="col-sm-12">
												<textarea name="comment" class="form-control" style="width: 100%;" id="comment"></textarea>
											</div>
										</div>
										<center>
										<button type="submit" id="saveapprove" class="swal2-styled btn btn-brand" aria-label="">
											Yes, <span class="labaction"></span> it!
										</button>
										<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
										</center>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
					<!-- END MODAL APPROVAL -->

					<!-- MODAL UPLOAD DOK -->
					<div class="modal fade" id="modaluploaddok" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">

								<form class="kt-form kt-form--label-right" id="formupload" enctype="multipart/form-data">
									<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">Upload Assign Dok Pengajuan : <b id="uploadname"></b></h3>
											</div>
											<div class="kt-portlet__head-toolbar">
											</div>
										</div>
										<div class="kt-portlet__body">
											<div class='row'>
												<div class="col-lg-8" id="dokexist">
												</div>
												<div class="col-lg-8">
													<div class="form-group row">
														<label class="col-form-label col-lg-4 col-sm-12">Dok Pengajuan *</label>
														<div class="col-lg-8 col-md-8 col-sm-12">
															<div class='input-group'> 
																<input type="hidden" name="idreq" class="form-control" id="idreq" value="">
																<input type="file" name="dokpengajuan" class="form-control" id="dokpengajuan" placeholder="FILE">
															</div>
														</div>
													</div> 
												</div> 
												<div class="kt-separator kt-separator--space-lg kt-separator--border-dashed"></div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" id="uploadPengajuan" class="btn btn-success"><i class="la la-rocket"></i> Update Assign Dok Pengajuan</button>
									</div>
								</form>

							</div>
						</div>
					</div> 
					<!-- END MODAL UPLOAD DOK -->

					<div class="kt-portlet__body">
						<!--begin: Datatable -->
						<table class="table table-striped- table-striped table-hover table-checkable" id="tabledata">
							<thead>
								<tr>
									<th>Judul</th>
									<th>CFU/FU</th>
									<th>URL</th>								
									<th>Stakeholder</th>								
									<th>Created By</th> 
									<th>Created At</th>
									<th>Status</th>
									<!--th>Actions</th-->
								</tr>
							</thead>
							<!--tfoot>
								<tr>
									<th>Judul</th>
									<th>CFU/FU</th>
									<th>Jenis Data</th>								
									<th>Stackholder</th>								
									<th>Create By</th> 
									<th>Created At</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</tfoot-->
						</table>
						<!--end: Datatable -->  
					</div>

					<!-- MODAL DELETE -->
					<div class="modal fade" id="delete" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm" role="document">
							<div class="modal-content">
								<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
									<div class="swal2-header">
										<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
										<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure?</h2>
									</div>
									<div class="swal2-content">
										<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
									</div>
									<div class="swal2-actions" style="display: flex;">
										<form method="POST">
										<input type="hidden" name="iddel" id="iddel" value="">
										<center>
										<button type="button" id="deleteBtn" class="swal2-styled btn btn-danger" aria-label="">
											Yes, delete it!
										</button>
										<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
										</center>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END MODAL DELETE -->

					<!-- MODAL DELETE ATTACHMENT -->
					<div class="modal fade" id="deleteAtt" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm" role="document">
							<div class="modal-content">
								<div aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-popup swal2-modal swal2-show" style="display: flex;">
									<div class="swal2-header">
										<div class="swal2-icon swal2-warning swal2-animate-warning-icon" style="display: flex;"></div>
										<h2 class="swal2-title" id="swal2-title" style="display: flex;">Are you sure?</h2>
									</div>
									<div class="swal2-content">
										<div id="swal2-content" style="display: block;">You won't be able to revert this!</div>
									</div>
									<div class="swal2-actions" style="display: flex;">
										<form method="POST">
										<input type="hidden" name="iddelatt" id="iddelatt" value="">
										<center>
										<button type="button" id="deleteBtnAtt" class="swal2-styled btn btn-danger" aria-label="">
											Yes, delete it!
										</button>
										<button type="button" class="swal2-cancel swal2-styled btn btn-secondary" data-dismiss="modal">Cancel</button>
										</center>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END MODAL DELETE ATTACHMENT -->
				</div>
			</div>
		</div>
	</div>

	
</div>