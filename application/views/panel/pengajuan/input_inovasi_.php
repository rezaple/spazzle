<script src="<?PHP echo base_url(); ?>assets/zxcvbn.js"></script>
<style>
.sizemidle {
	max-width: 140px!important;
    max-height: 140px!important;
}
.kt-invoice-1 .kt-invoice__wrapper .kt-invoice__head .kt-invoice__container .kt-invoice__logo {
	padding-top: 5rem!important;
}
.bghead {
    background: url('<?PHP echo base_url(); ?>images/sidebar.png') no-repeat!important;
    background-size: 100% auto!important;
    background-position: 0px 30%!important;
}
</style>
<style>
.kt-portlet{
	border-radius:10px !important;
}
meter {
    /* Reset the default appearance */
    -webkit-appearance: none;
       -moz-appearance: none;
            appearance: none;
            
    margin: 0 auto 1em;
    width: 100%;
    height: .5em;
    
    /* Applicable only to Firefox */
    background: none;
    background-color: rgba(0,0,0,0.1);
}

meter::-webkit-meter-bar {
    background: none;
    background-color: rgba(0,0,0,0.1);
}

meter[value="1"]::-webkit-meter-optimum-value { background: red; }
meter[value="2"]::-webkit-meter-optimum-value { background: yellow; }
meter[value="3"]::-webkit-meter-optimum-value { background: orange; }
meter[value="4"]::-webkit-meter-optimum-value { background: green; }

meter[value="1"]::-moz-meter-bar { background: red; }
meter[value="2"]::-moz-meter-bar { background: yellow; }
meter[value="3"]::-moz-meter-bar { background: orange; }
meter[value="4"]::-moz-meter-bar { background: green; }

.feedback {
    color: #9ab;
    font-size: 90%;
    padding: 0 .25em;
    padding-left: 0em;
    margin-top: 1em;
}
.kt-wizard-v2 .kt-wizard-v2__wrapper .kt-form{
	padding: 2rem 3rem 3rem !important;
}
#formwizard a{
	color:#000 !important;
}
#formwizard a:hover{
	color:#000 !important;
}
#formwizard a:active{
	color:#000 !important;
}

#formwizard .kt-wizard-v2 .kt-wizard-v2__aside .kt-wizard-v2__nav .kt-wizard-v2__nav-items .kt-wizard-v2__nav-item[data-ktwizard-state="current"] .kt-wizard-v2__nav-icon{
	color:#ff7a61;
}
#formwizard .kt-wizard-v2 .kt-wizard-v2__aside .kt-wizard-v2__nav .kt-wizard-v2__nav-items .kt-wizard-v2__nav-item[data-ktwizard-state="current"] .kt-wizard-v2__nav-body .kt-wizard-v2__nav-label .kt-wizard-v2__nav-label-title{
	color:#ff7a61;

}
#formwizard .kt-wizard-v2 .kt-wizard-v2__aside .kt-wizard-v2__nav .kt-wizard-v2__nav-items .kt-wizard-v2__nav-item[data-ktwizard-state="current"] .kt-wizard-v2__nav-body .kt-wizard-v2__nav-label .kt-wizard-v2__nav-label-desc{
	color:#ff7a61;

}
#btn-prev {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
}
#btn-submit {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
  background-color: #d8494f;
  border-color:#d8494f;;
}
#btn-next {
  border-radius: 20px;
  box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
  background-color: #d8494f;
  border-color:#d8494f;;

}
.kt-wizard-v2 .kt-wizard-v2__aside .kt-wizard-v2__nav .kt-wizard-v2__nav-items .kt-wizard-v2__nav-item .kt-wizard-v2__nav-body .kt-wizard-v2__nav-label .kt-wizard-v2__nav-label-title{
	font-size:1rem;
}
</style>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div class="kt-portlet" id='formwizard'>
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<a href = '<?PHP echo base_url();?>inovasi'>
						<i class="flaticon flaticon2-back"></i> Innovation Data
					</a>
				</h3>
			</div> 
		</div>
		<div class="kt-portlet__body kt-portlet__body--fit">
			 <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
				<div class="kt-grid__item kt-wizard-v2__aside">

					<!--begin: Form Wizard Nav -->
					<div class="kt-wizard-v2__nav">
						<div class="kt-wizard-v2__nav-items">
							<a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step" data-ktwizard-state="current">
								<div class="kt-wizard-v2__nav-body">
									<div class="kt-wizard-v2__nav-icon">
										<i class="fa fa-wrench"></i>
									</div>
									<div class="kt-wizard-v2__nav-label">
										<div class="kt-wizard-v2__nav-label-title">
											INNOVATION DESCRIPTION
										</div>
										<div class="kt-wizard-v2__nav-label-desc">
											<!--Setup Your Account Details-->
										</div>
									</div>
								</div>
							</a>
							<a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v2__nav-body">
									<div class="kt-wizard-v2__nav-icon">
										<i class="flaticon-refresh"></i>
									</div>
									<div class="kt-wizard-v2__nav-label">
										<div class="kt-wizard-v2__nav-label-title">
											SDLC
										</div>
										<div class="kt-wizard-v2__nav-label-desc">
											<!--SYSTEM DEVELOPMENT LIFE CYCLE-->
										</div>
									</div>
								</div>
							</a>
							<a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v2__nav-body">
									<div class="kt-wizard-v2__nav-icon">
										<i class="flaticon-responsive"></i>
									</div>
									<div class="kt-wizard-v2__nav-label">
										<div class="kt-wizard-v2__nav-label-title">
											TEKNOLOGI PLATFORM
										</div>
										<div class="kt-wizard-v2__nav-label-desc">
										</div>
									</div>
								</div>
							</a>
							<a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v2__nav-body">
									<div class="kt-wizard-v2__nav-icon">
										<i class="flaticon-map"></i>
									</div>
									<div class="kt-wizard-v2__nav-label">
										<div class="kt-wizard-v2__nav-label-title">
											PERSONAL IDENTIFIABLE INFORMATION
										</div>
										<div class="kt-wizard-v2__nav-label-desc">
											<!--PERSONAL IDENTIFIABLE INFORMATION-->
										</div>
									</div>
								</div>
							</a>
							<a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v2__nav-body">
									<div class="kt-wizard-v2__nav-icon">
										<i class="flaticon-safe-shield-protection"></i>
									</div>
									<div class="kt-wizard-v2__nav-label">
										<div class="kt-wizard-v2__nav-label-title">
											SECURITY SYSTEM
										</div>
										<div class="kt-wizard-v2__nav-label-desc">
											
										</div>
									</div>
								</div>
							</a>
							<a class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
								<div class="kt-wizard-v2__nav-body">
									<div class="kt-wizard-v2__nav-icon">
										<i class="fa fa-share-alt"></i>
									</div>
									<div class="kt-wizard-v2__nav-label">
										<div class="kt-wizard-v2__nav-label-title">
											DATA SHARING TELKOM GROUP
										</div>
										<div class="kt-wizard-v2__nav-label-desc">
											
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<!--end: Form Wizard Nav -->
				</div>
				<div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">

					<!--begin: Form Wizard Form-->
					<form class="kt-form" id="kt_form" method='POST' action='<?PHP echo base_url(); ?>/ctrl_innovasi/submit_innovasi'>

						<!--begin: Form Wizard Step 1-->
						<div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
							<div class="kt-heading kt-heading--md">Form Innovation Description</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v2__form">
									<div class="form-group">
										<label>Judul*</label>
										<input type="text" class="form-control" id="judul" name="judul" placeholder="Input Judul INOVASI" value="">
										<!--span class="form-text text-muted">Please enter your first name.</span-->
									</div>
									<div class="form-group">
										<label>CFU / FU*</label>
										<input type="text" class="form-control" id="cfu_fu" name="cfu_fu" placeholder="CFU FU">
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div>
									<div class="form-group">
										<label>URL Aplikasi*</label>
										<input type="text" class="form-control" id="url" name="url" placeholder="URL Aplikasi">
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div>
									
									<div class="form-group">
										<label>Deskripsi*</label>
										<textarea name="deskripsi" class="summernote" id="deskripsi"></textarea>
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div>
									<div class="form-group">
										<label>Tujuan*</label>
										<textarea name="tujuan" class="summernote" id="tujuan"></textarea>
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div>

									<div class="form-group">
										<label>Jenis Data*</label>
										<textarea name="jenis_data" class="summernote" id="jenis_data"></textarea>
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div>
									<div class="form-group">
										<label>Stakeholder*</label>
										<textarea name="stakholder" class="summernote" id="stakholder"></textarea>
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="row">
												<label class="col-form-label col-lg-4 col-sm-12">Arsitektur Aplikasi *</label>
												<div class="col-lg-8 col-md-8 col-sm-12">
													<div id="bgattach">
														<div class="input-group">
										                    <input type="file" class="form-control" name="diagram" id="diagram" accept="image/*">
										                    <a href="#" class="col-lg-1">
										                    </a>
										                </div>
													</div>
													<br>
													<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
														<i class="flaticon flaticon-plus"></i> Tambah Attachment
													</button-->
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="row">
												<label class="col-form-label col-lg-4 col-sm-12">Flow proses Aplikasi *</label>
												<div class="col-lg-8 col-md-8 col-sm-12">
													<div id="bgattach">
														<div class="input-group">
										                    <input type="file" class="form-control" name="diagram2" id="diagram2" accept="image/*">
										                    <a href="#" class="col-lg-1">
										                    </a>
										                </div>
													</div>
													<br>
													<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
														<i class="flaticon flaticon-plus"></i> Tambah Attachment
													</button-->
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Penjelasan Singkat Arsitektur & Flow proses aplikasi *</label>
										<textarea name="penjelasan" class="summernote" id="penjelasan"></textarea>
										<!--span class="form-text text-muted">Please enter your last name.</span-->
									</div> 
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 1-->

						<!--begin: Form Wizard Step 2-->
						<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
							<div class="kt-heading kt-heading--md">SDLC<br><small>System Development Life Cycle</small></div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v2__form">
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc Requirment*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docrequirmen[]" id="docrequirmen" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc Design*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docdesign[]" id="docdesign" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc Test Report & Plan *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doctesreport[]" id="doctesreport" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc SMP*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docsmp[]" id="docsmp" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div>
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc SOP*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docsop[]" id="docsop">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
										<div class="col-xl-6">
											<div class="form-group">
												<label>Doc SOP D2P*</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docsopd2p[]" id="docsopd2p" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 

									</div>
									 
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 2-->

						<!--begin: Form Wizard Step 3-->
						<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
							<div class="kt-heading kt-heading--md">TEKNOLOGI PLATFORM</small></div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v2__form">
										<div class="row">
											<div class="col-6">
												<div class="form-group row">
													<label class="col-12 col-form-label">Language & Framework</label>
													<div class="col-12">
														<div class="kt-checkbox-list">
															<?PHP
															foreach($getdataframework as $gframe){
															?>
															<label class="kt-checkbox">
																<input type="checkbox" name='framework[]' value='<?PHP echo $gframe['framework'];?>'> <?PHP echo $gframe['framework'];?>
																<span></span>
															</label>
															<?PHP														
															} 
															?>
															
															<label class="kt-checkbox">
																<input type="checkbox" id='other_framework'> Other
																<span></span>
															</label>

														</div>
													</div>
												</div>
												<div class="form-group row" id='fr_wrapper' style='display:none;'> 
													<label class="col-form-label col-lg-12 col-sm-12">Other Language & Framework *</label>
													<div class="col-lg-12 col-md-9 col-sm-12">
														<div id="bgframework">
															<div class="input-group">
											                    <input type="text" class="form-control" name="otherframe[]" id="otherframe"> 
											                </div>
														</div>
														<br>
														<button type="button" class="btn btn-sm btn-default btnframework">
															<i class="flaticon flaticon-plus"></i> Tambah
														</button>
													</div> 
												</div>												
											</div>
											<div class="col-6">
												<div class="form-group row">
													<label class="col-12 col-form-label">Database</label>
													<div class="col-12">
														<div class="kt-checkbox-list">
															<?PHP
															foreach($getdatadatabase as $gbase){
															?>
															<label class="kt-checkbox">
																<input type="checkbox" name='database[]' value='<?PHP echo $gbase['nama_database'];?>'> <?PHP echo $gbase['nama_database'];?>
																<span></span>
															</label>
															<?PHP														
															} 
															?>
															<label class="kt-checkbox">
																<input type="checkbox" id='other_database'> Other
																<span></span>
															</label>

														</div>
													</div>
												</div>
												<div class="form-group row" id='db_wrapper' style='display:none;'> 
													<label class="col-form-label col-lg-12 col-sm-12">Other Database *</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div id="bgdatabase">
															<div class="input-group">
											                    <input type="text" class="form-control" name="otherdb[]" id="otherdb"> 
											                </div>
														</div>
														<br>
														<button type="button" class="btn btn-sm btn-default btndatabase">
															<i class="flaticon flaticon-plus"></i> Tambah
														</button>
													</div> 
												</div>
											</div>
										</div>  
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 3-->

						<!--begin: Form Wizard Step 4-->
						<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
							<div class="kt-heading kt-heading--md">PERSONAL IDENTIFIABLE INFORMATION</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v2__form"> 
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label class="kt-checkbox">
													<input type="checkbox" name='pii' id='pii' value='1'>PII
													<span></span>
												</label>
												<span class="form-text text-muted">Pilih ceklist jika inovasi memenuhi ketentuan berikut.</span>

											</div>
										</div> 
									</div> 
									<div class="row" id='uepo' style='display:none;'>
										<div class="col-xl-6">
											<div class="form-group">
												<label>Upload Evidence Persetujuan Owner *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docpii" id="docpii">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<h6>Persetujuan Data Owner via NDE ditujukan kepada :</h6>
										<div class="col-xl-12">
											<ul>
												<li>Direktorat Customer : VP Planning and Resource Management</li>
												<li>Direktorat Enterprise &  Business Service : VP Enterprise Business Development</li>
												<li>Direktorat Wholesale & International Service : VP Wholsale and Intl.Development</li>
												<li>Direktorat Customer : VP Planning and Resource Management</li>
												<li>Subsidiary : Pejabat yang bertanggung jawab sebagai Data Owner Subsidiary</li>
											</ul>
										</div>
									</div>
									<div class="row">
										<table class='table table-responsive table-striped table-bordered'>
											<thead>
													<th>No</th>
													<th>PII</th>
													<th>Keterangan / Contoh</th>
											</thead>
											<tbody>
													<tr>
														<td>1</td>
														<td>ID Berlangganan</td>
														<td>Termasuk No.Telepon Telkom, No. Indihome,No. Speedy dll.</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Nama</td>
														<td>Nama Perorangan , Nama Pelanggan Korporat dll.</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Tempat/Tanggal Lahir</td>
														<td>Termasuk No.Telepon Telkom, No. Indihome,No. Speedy dll.</td>
													</tr>
													<tr>
														<td>4</td>
														<td>No.HP</td>
														<td>Termasuk No.HP yang muncul dari sistem apapun (Pengaduan via Twitter, MyIndihome dll)</td>
													</tr>
													<tr>
														<td>5</td>
														<td>E-Mail</td>
														<td>Termasuk E-Mail yang muncul dari sistem manapun</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Alamat Pelanggan</td>
														<td>Alamat rumah / kantor, Koordinasi GPS(long/lat) dll.</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Info Sosial Media</td>
														<td>Akun / User ID Facebook , Instagram , Twitter dll, Termasuk jumlah follower.</td>
													</tr>
													<tr>
														<td>8</td>
														<td>No. Identitas Resmi</td>
														<td>Termasuk No.KTP, nomor Kartu Keluarga, NPWP dll.</td>
													</tr>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 4-->

						<!--begin: Form Wizard Step 5-->
						<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
							<div class="kt-heading kt-heading--md">Security System</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v2__form">
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence ToU (Terms Of Use) *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doctou[]" id="doctou" accept="image/*">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document Scrennshot PNG/JPG</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence Costumer Consent</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doccustcons[]" id="doccustcons" accept="image/*">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document Scrennshot PNG/JPG</span>
											</div>
										</div> 
									</div> 

									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence Multi Factor Authentication *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="doctfa[]" id="doctfa" accept="image/*">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<span class="form-text text-muted">Type Document Scrennshot PNG/JPG</span>
											</div>
										</div> 
									</div>
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Evidence NDE Hasil VA Test *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docss[]" id="docss" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div>

								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 5-->

						<!--begin: Form Wizard Step 6-->
						<div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
							<div class="kt-heading kt-heading--md">Data Sharing Telkom Group</div>
							<div class="kt-form__section kt-form__section--first">
								<div class="kt-wizard-v2__form">
									<div class="row">
										<div class="col-xl-6">
											<div class="form-group">
												<label>Upload MoM DG Council Meeting *</label>
												<div id="bgattach">
													<div class="input-group">
									                    <input type="file" class="form-control" name="docstg" id="docstg" accept="application/pdf">
									                    <a href="#" class="col-lg-1">
									                    </a>
									                </div>
												</div>
												<br>
												<!--button type="button" class="btn btn-sm btn-default btnAddAttach">
													<i class="flaticon flaticon-plus"></i> Tambah Document
												</button-->
												<span class="form-text text-muted">Type Document PDF</span>
											</div>
										</div> 
									</div>
									 <div class="row">
										
										<div class="col-xl-12">
											<h6>Data Governance Council Meeting</h6>
											<span>
												Pertukaran Data antara Entitas Telkom Group : 
											</span>
											<ul>
												<li>Telkom -> Subsidiary</li>
												<li>Subsidiary -> Subsdiary</li> 
												<li>Pihak Eksternal (Third Party) </li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--end: Form Wizard Step 6-->

						<!--begin: Form Actions -->
						<div class="kt-form__actions">
							<div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-prev" id='btn-prev'>
								Previous
							</div>
							<div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-submit" id='btn-submit'>
								Submit
							</div>
							<div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" data-ktwizard-type="action-next" id='btn-next'>
								Next Step
							</div>
						</div>

						<!--end: Form Actions -->
					</form>

					<!--end: Form Wizard Form-->
				</div>
			</div>
		</div>
	</div>
</div>
 