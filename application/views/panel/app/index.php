<!-- begin:: Content -->
<style>
	#theads{
		background-color:#d9e1f2; 
	} 
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text">Success!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head kt-portlet__head--lg">
			<div class="kt-portlet__head-label">
				<span class="kt-portlet__head-icon">
				</span>
				<h3 class="kt-portlet__head-title">
					List Application
				</h3>
			</div> 
		</div>  
		<div class="kt-portlet__body"> 
			<div class='row'> 
				<div class='col-lg-12 col-md-12 col-sm-12'>  
					<table class="table table-striped- table-bordered table-hover table-checkable" id="tabledata" style='font-size:11px;'>
						<thead id='theads'>
							<tr>
								<th>No</th>
								<th>Nama Aplikasi</th>
								<th width='10%'>Deskripsi</th>
								<th>Benefit Value</th>
								<th>Unit</th>
								<th>Tahun terbit Inovasi</th>
								<th>Kategori</th>
								<th>Inovator+Developer</th>
								<th>PIC</th>
								<th>Tipe Inovasi</th>
								<th>Url</th>
								<th>Scope Implementasi</th>
								<th>User</th> 
								<th>Action</th>
							</tr>
						</thead> 
						<tbody>
						</tbody>
					</table> 
				</div>  
			</div>
			<!-- MODAL UPDATE -->
			<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Update Data : <b id="nameroles"></b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							</button>
						</div>

						<form class="kt-form kt-form--label-left" id="formupdate" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="kt-form__content">
									<div class="kt-alert m-alert--icon alert alert-danger kt-hidden" role="alert" id="kt_form_1_msg">
										<div class="kt-alert__icon">
											<i class="la la-warning"></i>
										</div>
										<div class="kt-alert__text">
											Oh snap! Change a few things up and try submitting again.
										</div>
										<div class="kt-alert__close">
											<button type="button" class="close" data-close="alert" aria-label="Close">
											</button>
										</div>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Question *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<input type="hidden" value="" name="ed_id_faq" id="ed_id_faq">
											<input type="text" name="ed_question" class="form-control" placeholder="Question" id="ed_question" value="">
										</div>
										<span class="form-text text-muted">Type Question</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-3 col-sm-12">Answer *</label>
									<div class="col-lg-9 col-md-9 col-sm-12">
										<div class='input-group'>
											<textarea name="ed_answer" class="summernote" id="ed_answer"></textarea> 
										</div>
										<span class="form-text text-muted">Type Answer</span>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" id="saveupdate" class="btn btn-primary">Update</button>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- END MODAL UPDATE --> 
		</div>
	</div>
</div>

<!-- end:: Content -->