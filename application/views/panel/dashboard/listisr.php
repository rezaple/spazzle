<?PHP
$userdata		= $this->session->userdata('sesspwt'); 
$userid			= $userdata['userid'];
$actionTypeAP 	= 'NEW-ISR';

$qClientID 		= $this->db->query("SELECT (select clientid from auth where authid=a.authid) clientid from user a where userid='$userid'")->result_array();
$getClientID	= array_shift($qClientID);
$Client_ID 		= $getClientID['clientid'];

$getService 	= $this->query->getDatabyQ("SELECT * FROM m2m_service order by 1");
$getSubService 	= $this->query->getDatabyQ("SELECT * FROM m2m_subservice order by 1");

$qD				= "
				SELECT *
				from isrm2m.m2m_application where userid='$userid' and actionTypeAP='$actionTypeAP' and codeResult in ('') 
				order by 1 desc
				";
$dataD			= $this->db->query($qD)->num_rows();

$qR				= "
				SELECT *
				from isrm2m.m2m_application where userid='$userid' and actionTypeAP='$actionTypeAP' and codeResult in ('-1') 
				order by 1 desc
				";
$dataR			= $this->db->query($qR)->num_rows();

$qPS			= "
				SELECT *
				from isrm2m.m2m_application where userid='$userid' and actionTypeAP='$actionTypeAP' and codeResult not in ('','-1') 
				";
$dataPS			= $this->db->query($qPS)->num_rows();

$qB			= "
				SELECT *
				from isrm2m.m2m_batch where userid='$userid' and action_ap='$actionTypeAP' 
				";
$dataB			= $this->db->query($qB)->num_rows();
?>
<style>
	.kt-separator.kt-separator--space-lg {
	    margin: 1.5rem 0;
	}
	h1.number {
		color: #b94e4e;
	    border: 3px solid rgb(173, 69, 69, .3);
	    border-radius: 100%;
	    width: 64px;
	    height: 64px;
	    line-height: 4.5rem;
	    margin: 0 auto;
	    font-size: 2rem;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	h4 span {
		font-size: 14px;
	}
	#detail .form-group {
		margin-bottom: -1rem;
	}
</style>

<!-- <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="padding-bottom: 5px;">
	<div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Filter Data
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-group">
					<a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
				</div>
			</div>
		</div>
		<div class="kt-portlet__body">
			<form id="filterdata">
				<div class="row">
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">Service_ID</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<select name="layanan" class="form-control" id="layanan" placeholder="LAYANAN">
										<?PHP foreach($getService as $service) { ?>
										<option value="<?PHP echo $service['id']; ?>"><?PHP echo $service['name']; ?></option>
										<?PHP } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">Area_of_Service</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<input type="text" name="Area_of_Service" class="form-control" id="Area_of_Service" placeholder="Area_of_Service">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">Client_ID</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<input type="text" name="Client_ID" class="form-control" id="Client_ID" placeholder="Client_ID">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">Station_Name</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<input type="text" name="Station_Name" class="form-control" id="Station_Name" placeholder="Station_Name">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">AppRef</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<input type="text" name="appRef" class="form-control" id="appRef" placeholder="AppRef">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">LicenseNumber</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<input type="text" name="licenseNumber" class="form-control" id="licenseNumber" placeholder="LicenseNumber">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">TransactionId</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class='input-group'>
									<input type="text" name="transactionId" class="form-control" id="transactionId" placeholder="TransactionId">
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 ">
						<div class="form-group row">
							<label class="col-form-label col-lg-12 col-sm-12">&nbsp;</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<button type="button" id="showdata" class="btn btn-primary"><i class="flaticon flaticon-search-1"></i> Filter Data</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
 -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div id="gagalinsert" class="alert alert-warning alert-elevate kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-warning"></i></div>
		<div class="alert-text" id="textfailed">
			<strong>Failed!</strong> Change a few things up and try submitting again.
		</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesinsert" class="alert alert-success fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-black"></i></div>
		<div class="alert-text" id="textsuccess"><strong>Success!</strong></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesdelete" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been deleted!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<div id="suksesclear" class="alert alert-secondary fade show kt-hidden" role="alert">
		<div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
		<div class="alert-text">Your data has been cleared!</div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>

	<!--begin::Portlet-->
	<div class="kt-portlet kt-portlet--mobile">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					<ul class="nav nav-tabs  nav-tabs-line nav-tabs-line-danger" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#station" role="tab"><i class="flaticon flaticon-file-1"></i> Applications</a>
						</li>
					</ul>
				</h3>
			</div>
			<div id="bgkonten" class="kt-portlet__head-toolbar">
				<div class="kt-portlet__head-toolbar-wrapper">
					
				</div>
			</div>
		</div>

		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="station" role="tabpanel">
					<!--begin: Datatable -->
					<table class="table table-striped- table-bordered table-hover table-checkable" id="tabledatastation">
						<thead>
							<tr>
								<th>Unduh</th>
								<th>Tipe</th>
								<th>Ap_Name_Reference</th>
								<th>Status</th>
								<th>LicenseNumber</th>
								<th>AppRef</th>
								<th>BatchNo</th>
								<th>Service_ID</th>
								<th>SubService_ID</th>
								<th>Created By</th>
								<th>Created Date</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Unduh</th>
								<th>Tipe</th>
								<th>Ap_Name_Reference</th>
								<th>Status</th>
								<th>LicenseNumber</th>
								<th>AppRef</th>
								<th>BatchNo</th>
								<th>Service_ID</th>
								<th>SubService_ID</th>
								<th>Created By</th>
								<th>Created Date</th>
							</tr>
						</tfoot>
					</table>

					<!--end: Datatable -->
				</div>
			</div>

		</div>

		<!-- MODAL DETAIL -->
		<div class="modal fade" id="detail" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body kt-portlet kt-portlet--tabs" style="margin-bottom: 0px;">
						<div id="loadDetailData"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<!-- <button type="submit" id="saveinsert" class="btn btn-primary">Save</button> -->
					</div>
				</div>
			</div>
		</div>
		<!-- END MODAL DETAIL -->
	</div>
</div>