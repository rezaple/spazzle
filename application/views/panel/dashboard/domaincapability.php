<?PHP 
// $userdata		= $this->session->userdata('sesspwt'); 
// $userid 		= $userdata['userid'];

// $getUser 		= $this->db->query("
// 					SELECT a.*, 
// 						(SELECT level from mi.level_user where id_level=a.level_user) step,
// 						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
// 					FROM mi.user a where userid='$userid'
// 				")->result_array();
// $dUser 			= array_shift($getUser);

// $aksesCreate 	= $dUser['actionbtn'];

// $getPengaju		= $this->db->query("SELECT * from mi.user where level_user=1")->result_array();
// //$getNoRequest	= $this->db->query("SELECT * from sbrdoc where status not in (0)")->num_rows();
// $getTotal 		= $this->db->query("SELECT * from sbrdoc where status not in (0)")->num_rows();
// $getPending 	= $this->db->query("SELECT * from sbrdoc where status not in (0,4,5)")->num_rows();
// $getApproved 	= $this->db->query("SELECT * from sbrdoc where status in (4)")->num_rows();
// $getRejected 	= $this->db->query("SELECT * from sbrdoc where status in (5)")->num_rows();
?>
<link rel="stylesheet" href="<?PHP echo base_url();?>assets/tree/Treant.css">
<link rel="stylesheet" href="<?PHP echo base_url();?>assets/tree/basic-example.css">
<style>
	.Treant .collapse-switch {
	    width: 13px;
	    height: 13px;
	    display: block;
	    border: 1px solid #8bd7bd;
	    position: absolute;
	    top: 45%;
	    left: -6px;
	    cursor: pointer;
	    background: #313d63;
	    border-radius: 100%;
	}
	.Treant .collapsed .collapse-switch {
	    background-color: #013453;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	.mainchart{
		border:0px !important;
	}
	.mainchart .collapse-switch{
		display:none;
	}
	.chartactive{
		width: 150px;
		height: 54px;
		border-radius: 20px;
		box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
		background-color: #004874;
		color:white;
	}

	.chartactive2{
		width: 150px;
		height: 54px;
		border-radius: 20px;
		box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
		background-color: #a74350;
		color:white;
	}
	.node2 {
	  width: 80px;
	  height: 40px;
	  border-radius: 11px;
	  background-color: #95a5a6;
	  font-size:10px;
	}
	.node3 {
	  width: 120px;
	  height: 40px;
	  border-radius: 11px;
	  background-color: #95a5a6;
	  font-size:10px;
	}
	.mt2rem {margin-top: 2rem;}
	.valuebgdash {margin-top: 0.5rem; margin-bottom: -1rem;}
	.kt-portlet--solid-danger { background: #c0392b!important; }
	#circlebg {
		font-size: 2rem;
	    line-height: 1rem;
	    text-align: center;
	    box-shadow: 0px 3px 10px rgba(0,0,0,.3);
	    z-index: 2;
	    width: 150px;
	    height: 150px;
	    border-radius: 100%;
	    position: absolute;
	    left: 36%;
    	top: 21%;
	    padding-top: 10%;
	}
	#circlebg span {font-size: 1rem;}
	#loadermini {
		width: 100%;
		height: calc(100% - 20px);
		background: #FFF url('<?PHP echo base_url(); ?>images/loadermini.gif') center no-repeat;
		background-size: 80px auto;
	}
	.steps {
	  list-style-type: none;
	  padding: 0;
	}
	.steps li {
	  display: inline-block;
	  margin-bottom: 3px;
	}
	.steps li a, .steps li p {
	  background: #ff6d00;
	  padding: 8px 10px;
	  color: #fff;
	  display: block;
	  font-size: 11px;
	  font-weight: bold;
	  position: relative;
	  text-indent: 12px;
	}
	.steps li a:hover, .steps li p:hover {
	  text-decoration: none;
	}
	.steps li a:before, .steps li p:before {
	  border-bottom: 18px solid transparent;
	  border-left: 12px solid #fff;
	  border-top: 18px solid transparent;
	  content: "";
	  height: 0;
	  position: absolute;
	  left: 0;
	  top: 50%;
	  width: 0;
	  margin-top: -18px;
	}
	.steps li a:after, .steps li p:after {
	  border-bottom: 18px solid transparent;
	  border-left: 12px solid #ff6d00;
	  border-top: 18px solid transparent;
	  content: "";
	  height: 0;
	  position: absolute;
	  /*right: -12px;*/
	  left:100%;
	  top: 50%;
	  width: 0;
	  margin-top: -18px;
	  z-index: 1;
	}
	.steps li.active a, .steps li.active p {
	  background: #fd8933;
	  color: #fff;
	}
	.steps li.active a:after, .steps li.active p:after {
	  border-left: 12px solid #fd8933;
	}
	.steps li.active2 a, .steps li.active2 p {
	  background: #d87765;
	  color: #fff;
	}
	.steps li.active2 a:after, .steps li.active2 p:after {
	  border-left: 12px solid #d87765;
	}
	.steps li.active3 a, .steps li.active3 p {
	  background: #c1898c;
	  color: #fff;
	}
	.steps li.active3 a:after, .steps li.active3 p:after {
	  border-left: 12px solid #c1898c;
	}
	.steps li.undone a, .steps li.undone p {
	  background: #eee;
	  color: #333;
	}
	.steps li.undone a:after, .steps li.undone p:after {
	  border-left: 12px solid #eee;
	}
	.steps li.undone p {
	  color: #aaa;
	}
	.alert{
		padding:5px !important;
		margin:0 0 5px 0 !important;
	}
	.node {
		cursor: pointer;
	}

	.node circle {
	  fill: #fff;
	  stroke: steelblue;
	  stroke-width: 3px;
	}

	.node text {
	  font: 12px sans-serif;
	}

	.link {
	  fill: none;
	  stroke: #ccc;
	  stroke-width: 2px;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<!--Begin::Dashboard 1-->

	<!--Begin::Section-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--responsive-mobile">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#detail" role="tab">
										<!-- <b><i class="flaticon flaticon-file-2"></i> TEL.1/INNOVASI/A/10/2020</b> -->
										<b><i class="flaticon flaticon-file-2"></i> Domain</b>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#sdlc" role="tab">
										<!-- <b><i class="flaticon flaticon-file-2"></i> TEL.1/INNOVASI/A/10/2020</b> -->
										<i class="flaticon flaticon-file-2"></i> CFU/FU
									</a>
								</li> 
							</ul>
						</h3>
					</div>

				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="detail" role="tabpanel">	
							<div id='treedomain'></div>				 
						</div>
						<div class="tab-pane" id="sdlc" role="tabpanel"> 
							<div id='treecfu'></div>	 
						</div> 
					</div> 
				</div>
			</div>
		</div>
	</div>

	 
</div>
<script src="<?PHP echo base_url();?>assets/tree/vendor/raphael.js"></script>
<script src="<?PHP echo base_url();?>assets/tree/Treant.js"></script>
<script>
	loaddomain();
	function loaddomain(){
	    var config = {
	        container: "#treedomain",
	        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
	        scrollbar: "fancy",
	        animateOnInit: true,
	        siblingSeparation:   40,
	        subTeeSeparation:    30,
	            
	        node: {
	            collapsable: true
	        },
	        animation: {
	            nodeAnimation: "easeOutBounce",
	            nodeSpeed: 700,
	            connectorsAnimation: "bounce",
	            connectorsSpeed: 700
	        },
	        
	        connectors: {
	            type: 'step',
	            style: {
	                // "stroke-width": 2,
	                "stroke": "#013453",
	                "arrow-end": "classic-wide-long"
	            }
	        },
	        node: {
	            HTMLclass: 'nodeExample1'
	        }
	    },

	    base55 = {
	        text: {
	            name: "",
	            title: "",
	        },
	        HTMLid: "base",
	        HTMLclass: "mainchart",
		        image: "<?PHP echo base_url(); ?>assets/tree/innovation.png"
		    },

	    
	                int1 = {
	                        parent: base55,
	                        text:{
	                            name: "Partner Mgmt",
	                            title: "",
	                        },   
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                }, 

			                int1_1 = {
			                        parent: int1,
			                        text:{
			                            name: "TREG-1",
			                            title: "",
			                        },   
			                        stackChildren: true,
			                        collapsable: true,
			                        HTMLid: "base",
			                        HTMLclass: "node2",
			                }, 
				                int1_1_1 = {
				                        parent: int1_1,
				                        text:{
				                            name: "Application-1",
				                            title: "",
				                        },   
				                        stackChildren: true,
				                        collapsable: true,
				                        HTMLid: "base",
				                        HTMLclass: "node3",
				                }, 
				                int1_1_2 = {
				                        parent: int1_1,
				                        text:{
				                            name: "Application-N",
				                            title: "",
				                        },   
				                        stackChildren: true,
				                        collapsable: true,
				                        HTMLid: "base",
				                        HTMLclass: "node3",
				                }, 
			                int1_2 = {
			                        parent: int1,
			                        text:{
			                            name: "TREG-2",
			                            title: "",
			                        },   
			                        stackChildren: true,
			                        collapsable: true,
			                        HTMLid: "base",
			                        HTMLclass: "node2",
			                }, 
								int1_2_1 = {
								        parent: int1_2,
								        text:{
								            name: "Application-1",
								            title: "",
								        },   
								        stackChildren: true,
								        collapsable: true,
								        HTMLid: "base",
								        HTMLclass: "node3",
								}, 
								int1_2_2 = {
								        parent: int1_2,
								        text:{
								            name: "Application-N",
								            title: "",
								        },   
								        stackChildren: true,
								        collapsable: true,
								        HTMLid: "base",
								        HTMLclass: "node3",
								}, 
			              	int1_3 = {
			                        parent: int1,
			                        text:{
			                            name: "TREG-N",
			                            title: "",
			                        },   
			                        stackChildren: true,
			                        collapsable: true,
			                        HTMLid: "base",
			                        HTMLclass: "node2",
			                }, 
		                		int1_3_1 = {
								        parent: int1_3,
								        text:{
								            name: "Application-1",
								            title: "",
								        },   
								        stackChildren: true,
								        collapsable: true,
								        HTMLid: "base",
								        HTMLclass: "node3",
								}, 
								int1_3_2 = {
								        parent: int1_3,
								        text:{
								            name: "Application-N",
								            title: "",
								        },   
								        stackChildren: true,
								        collapsable: true,
								        HTMLid: "base",
								        HTMLclass: "node3",
								}, 

	                int2 = {
	                        parent: base55,
	                        text:{
	                            name: "Analytics",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                }, 
							 

	                int3 = {
	                        parent: base55,
	                        text:{
	                            name: "Security Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	            
	            	int4 = {
	                        parent: base55,
	                        text:{
	                            name: "Performance Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },

	                int5 = {
	                        parent: base55,
	                        text:{
	                            name: "Product Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },

	                int6 = {
	                        parent: base55,
	                        text:{
	                            name: "Fullfilment Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	               
	               	int7 = {
	                        parent: base55,
	                        text:{
	                            name: "Assurance Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },

	                int8 = {
	                        parent: base55,
	                        text:{
	                            name: "Data & Inventory Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	                

	    chart_config = [
	        config,
	        base55,
	        int1,
		        int1_1,
		        	int1_1_1,int1_1_2, 
		        int1_2,
		        	int1_2_1,int1_2_2,
		        int1_3,
		        	int1_3_1,int1_3_2,
	        int2, 
	        int3,
	        int4,
	        int5,
	        int6,
	        int7,
	        int8,
	        
	          
	     ];

	    new Treant( chart_config );
	}


    function loadcfu(){
        var config2 = {
	        container: "#treecfu",
	        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
	        scrollbar: "fancy",
	        animateOnInit: true,
	        siblingSeparation:   40,
	        subTeeSeparation:    30,
	            
	        node: {
	            collapsable: true
	        },
	        animation: {
	            nodeAnimation: "easeOutBounce",
	            nodeSpeed: 700,
	            connectorsAnimation: "bounce",
	            connectorsSpeed: 700
	        },
	        
	        connectors: {
	            type: 'step',
	            style: {
	                // "stroke-width": 2,
	                "stroke": "#013453",
	                "arrow-end": "classic-wide-long"
	            }
	        },
	        node: {
	            HTMLclass: 'nodeExample1'
	        }
	    },

	    base552 = {
	        text: {
	            name: "",
	            title: "",
	        },
	        HTMLid: "base",
	        HTMLclass: "mainchart",
		        image: "<?PHP echo base_url(); ?>assets/tree/innovation.png"
		    },

	    
	                Xint1 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-1",
	                            title: "",
	                        },   
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                }, 

			                Xint1_1 = {
			                        parent: Xint1,
			                        text:{
			                            name: "Application-1",
			                            title: "",
			                        },   
			                        stackChildren: true,
			                        collapsable: true,
			                        HTMLid: "base",
			                        HTMLclass: "node3",
			                }, 
				                
			                Xint1_2 = {
			                        parent: Xint1,
			                        text:{
			                            name: "Application-2",
			                            title: "",
			                        },   
			                        stackChildren: true,
			                        collapsable: true,
			                        HTMLid: "base",
			                        HTMLclass: "node3",
			                }, 
								
			              	Xint1_3 = {
			                        parent: Xint1,
			                        text:{
			                            name: "Application-N",
			                            title: "",
			                        },   
			                        stackChildren: true,
			                        collapsable: true,
			                        HTMLid: "base",
			                        HTMLclass: "node3",
			                }, 
		                		

	                Xint2 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-2",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                }, 
							 

	                Xint3 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-3",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },
	            
	            	Xint4 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-4",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },

	                Xint5 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-5",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },

	                Xint6 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-6",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },
	               
	               	Xint7 = {
	                        parent: base552,
	                        text:{
	                            name: "TREG-7",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },

	                Xint8 = {
	                        parent: base552,
	                        text:{
	                            name: "DBS",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },

	                Xint9 = {
	                        parent: base552,
	                        text:{
	                            name: "DES",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },

	                Xint10 = {
	                        parent: base552,
	                        text:{
	                            name: "DGS",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },

	                Xint11 = {
	                        parent: base552,
	                        text:{
	                            name: "Other CFU",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive2",
	                },
	                

	    chart_config2 = [
	        config2,
	        base552,
	        Xint1,
		        Xint1_1,
		        Xint1_2,
		        Xint1_3,
	        Xint2, 
	        Xint3,
	        Xint4,
	        Xint5,
	        Xint6,
	        Xint7,
	        Xint8,
	        Xint9,
	        Xint10,
	        Xint11,	         
	     ];

	    new Treant( chart_config2 );	
    }

</script>     