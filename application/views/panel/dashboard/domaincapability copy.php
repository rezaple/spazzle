<?PHP 
// $userdata		= $this->session->userdata('sesspwt'); 
// $userid 		= $userdata['userid'];

// $getUser 		= $this->db->query("
// 					SELECT a.*, 
// 						(SELECT level from mi.level_user where id_level=a.level_user) step,
// 						(SELECT action from mi.level_user where id_level=a.level_user) actionbtn
// 					FROM mi.user a where userid='$userid'
// 				")->result_array();
// $dUser 			= array_shift($getUser);

// $aksesCreate 	= $dUser['actionbtn'];

// $getPengaju		= $this->db->query("SELECT * from mi.user where level_user=1")->result_array();
// //$getNoRequest	= $this->db->query("SELECT * from sbrdoc where status not in (0)")->num_rows();
// $getTotal 		= $this->db->query("SELECT * from sbrdoc where status not in (0)")->num_rows();
// $getPending 	= $this->db->query("SELECT * from sbrdoc where status not in (0,4,5)")->num_rows();
// $getApproved 	= $this->db->query("SELECT * from sbrdoc where status in (4)")->num_rows();
// $getRejected 	= $this->db->query("SELECT * from sbrdoc where status in (5)")->num_rows();
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<style>
	.mt2rem {margin-top: 2rem;}
	.valuebgdash {margin-top: 0.5rem; margin-bottom: -1rem;}
	.kt-portlet--solid-danger { background: #c0392b!important; }
	#circlebg {
		font-size: 2rem;
	    line-height: 1rem;
	    text-align: center;
	    box-shadow: 0px 3px 10px rgba(0,0,0,.3);
	    z-index: 2;
	    width: 150px;
	    height: 150px;
	    border-radius: 100%;
	    position: absolute;
	    left: 36%;
    	top: 21%;
	    padding-top: 10%;
	}
	#circlebg span {font-size: 1rem;}
	#loadermini {
		width: 100%;
		height: calc(100% - 20px);
		background: #FFF url('<?PHP echo base_url(); ?>images/loadermini.gif') center no-repeat;
		background-size: 80px auto;
	}
	.steps {
	  list-style-type: none;
	  padding: 0;
	}
	.steps li {
	  display: inline-block;
	  margin-bottom: 3px;
	}
	.steps li a, .steps li p {
	  background: #ff6d00;
	  padding: 8px 10px;
	  color: #fff;
	  display: block;
	  font-size: 11px;
	  font-weight: bold;
	  position: relative;
	  text-indent: 12px;
	}
	.steps li a:hover, .steps li p:hover {
	  text-decoration: none;
	}
	.steps li a:before, .steps li p:before {
	  border-bottom: 18px solid transparent;
	  border-left: 12px solid #fff;
	  border-top: 18px solid transparent;
	  content: "";
	  height: 0;
	  position: absolute;
	  left: 0;
	  top: 50%;
	  width: 0;
	  margin-top: -18px;
	}
	.steps li a:after, .steps li p:after {
	  border-bottom: 18px solid transparent;
	  border-left: 12px solid #ff6d00;
	  border-top: 18px solid transparent;
	  content: "";
	  height: 0;
	  position: absolute;
	  /*right: -12px;*/
	  left:100%;
	  top: 50%;
	  width: 0;
	  margin-top: -18px;
	  z-index: 1;
	}
	.steps li.active a, .steps li.active p {
	  background: #fd8933;
	  color: #fff;
	}
	.steps li.active a:after, .steps li.active p:after {
	  border-left: 12px solid #fd8933;
	}
	.steps li.active2 a, .steps li.active2 p {
	  background: #d87765;
	  color: #fff;
	}
	.steps li.active2 a:after, .steps li.active2 p:after {
	  border-left: 12px solid #d87765;
	}
	.steps li.active3 a, .steps li.active3 p {
	  background: #c1898c;
	  color: #fff;
	}
	.steps li.active3 a:after, .steps li.active3 p:after {
	  border-left: 12px solid #c1898c;
	}
	.steps li.undone a, .steps li.undone p {
	  background: #eee;
	  color: #333;
	}
	.steps li.undone a:after, .steps li.undone p:after {
	  border-left: 12px solid #eee;
	}
	.steps li.undone p {
	  color: #aaa;
	}
	.alert{
		padding:5px !important;
		margin:0 0 5px 0 !important;
	}
	.node {
		cursor: pointer;
	}

	.node circle {
	  fill: #fff;
	  stroke: steelblue;
	  stroke-width: 3px;
	}

	.node text {
	  font: 12px sans-serif;
	}

	.link {
	  fill: none;
	  stroke: #ccc;
	  stroke-width: 2px;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<!--Begin::Dashboard 1-->

	<!--Begin::Section-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--responsive-mobile">
				<div class="kt-portlet__body">
				<div class="row">
					<div class="col-lg-6">
						 <div class="row">
							<div class="col-lg-12" style='margin-bottom:20px;color:#fd6c56;'>
								<center><h4>Domain Capability Matrix Focusing On Digitizing NITS Operational Process</h4></center>
							</div>
						 </div>
						 <div class="row">
							<div class="col-lg-3">
								<div class="kt-portlet kt-portlet--mobile" style='background-color:#013453 !important;margin-bottom:5px;'>
									<div class="kt-portlet__body text-white" style='padding:10px;'>
										<center>
											<h6>
												Product Mgt
											</h6>
											<hr style='border-color:white'>
												Performance Mgt
										</center>
									</div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="kt-portlet kt-portlet--mobile" style='background-color:#013453 !important;margin-bottom:5px;'>
									<div class="kt-portlet__body text-white" style='padding:10px;'>
										<center>
											<h6>
												Fullfilment Mgt
											</h6>
											<hr style='border-color:white' >
												Security Mgt
										</center>
									</div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="kt-portlet kt-portlet--mobile" style='background-color:#013453 !important;margin-bottom:5px;'>
									<div class="kt-portlet__body text-white" style='padding:10px;'>
										<center>
											<h6>
												Assurance Mgt
											</h6>
											<hr style='border-color:white'>
												Analytics Mgt
										</center>
									</div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="kt-portlet kt-portlet--mobile" style='background-color:#013453 !important;margin-bottom:5px;'>
									<div class="kt-portlet__body text-white" style='padding:10px;'>
										<center>
											<h6>
												Data & Inventory Mgt
											</h6>
											<hr style='border-color:white'>
												Partner Mgt
										</center>
									</div>
								</div>
							</div> 
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="alert alert-warning" role="alert" style='width:100%'>
									<div class="alert-text" style='text-align:center;'>44 Program Inovasi</div>
								</div>
							</div>
						</div>
						<div class="row"> 
	 						<div class="col-lg-12">
	 							<table class='table table-bordered'>
	 								<thead>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>ITSG</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>ISG</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>ISP</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>IMA</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>PND</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>DSO</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>DIT</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white'>DSS</th>
	 								</thead>
	 								<tbody>
	 									<tr>
	 										<td style='text-align:center;'>0 Inovasi</td>
	 										<td style='text-align:center;'>0 Inovasi</td>
	 										<td style='text-align:center;'>0 Inovasi</td>
	 										<td style='text-align:center;'>5 Inovasi</td>
	 										<td style='text-align:center;'>10 Inovasi</td>
	 										<td style='text-align:center;'>25 Inovasi</td>
	 										<td style='text-align:center;'>0 Inovasi</td>
	 										<td style='text-align:center;'>4 Inovasi</td>
	 									</tr>
	 								</tbody>
	 							</table>
	 							
	 						</div>	
	 					</div>
	 					<div class="row"> 
	 						<div class="col-lg-12">
	 							<hr>
		 					</div>
		 				</div>
	 					<div class="row"> 
	 						<hr>
	 						<div class="col-lg-6">
	 							<h5 style='color:#fd6c56;'>RESOURCE MANAGEMENT</h5>
	 							<table class='table table-bordered'>
	 								<thead>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white' width='30%'>RESOURCE</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white' width='30%'>Managed by</th>
	 									<th style='text-align:center;background-color:#013453 !important;' class='text-white' width='30%'># of Resource</th>
	 								</thead> 
	 							</table>
	 							<h5>Developer</h5>
	 							<table class='table table-bordered'>
	 								<tbody>
	 									<tr>
	 										<td width='30%' class='text-white' style='background-color:#013453 !important;'>Prohire & Outcsource</td>
	 										<td width='30%'>ITSG</td>
	 										<td width='30%'>7</td>
	 									</tr>
	 									<tr>
	 										<td width='30%' class='text-white' style='background-color:#013453 !important;'>Mitra</td>
	 										<td colspan='2'></td>
	 									</tr>
	 									<tr>
	 										<td width='30%'>Triklin</td>
	 										<td width='30%'>ITSG</td>
	 										<td width='30%'>3</td>
	 									</tr>
	 									<tr>
	 										<td width='30%'>TBS,Amal Solution</td>
	 										<td width='30%'>DSS</td>
	 										<td width='30%'>10</td>
	 									</tr>
	 									<tr>
	 										<td width='30%'>TMA,Telinfra,TA</td>
	 										<td width='30%'>DSO</td>
	 										<td width='30%'>8</td>
	 									</tr>
	 									<tr>
	 										<td width='30%'>NEC, Denggan, Amoeba</td>
	 										<td width='30%'>PND</td>
	 										<td width='30%'>9</td>
	 									</tr>
	 									<tr>
	 										<td width='30%'></td>
	 										<td width='30%'>TOTAL</td>
	 										<td width='30%'><b>40</b></td>
	 									</tr>

	 								</tbody> 
	 							</table>
	 							<h5>IT Infrastructure</h5>
	 							<table class='table table-bordered'>
	 								<tbody>
	 									<tr>
	 										<td width='30%' class='text-white' style='background-color:#013453 !important;'>
	 											AON Environment
	 											<ul>
	 												<li>Server</li>
	 												<li>Database</li>
	 												<li>Storage</li>
	 											</ul>
	 										</td>
	 										<td width='30%'>DIT</td>
	 										<td width='30%'>
	 											Server 4 Cores <br>
	 											Memory 8GB <br>
	 											Apache/2.4.29<br>
	 											PHP 7.2.24<br>
	 											HD 150 GB 
	 										</td>
	 									</tr>
	 								</tbody> 
	 							</table>
	 						</div>
	 						<div class="col-lg-6">
	 							<h5><span style='color:#fd6c56;'>Governance</span> (Architecture, Data & Process Management)</h5>
	 							<h5 style='color:#e76271;'>Main Activity</h5>
	 							<!--ul class="steps">
							      <li class="done"><a href="">Gathering Requirment</a></li>
							      <li class="active"><p>Analysis & Design</p></li>
							      <li class="active2"><a href="">Development</a></li>
							      <li class="active3"><a href="">Testing & VA</a></li>
							      <li class="undone"><p>D2P</p></li>
							    </ul-->
	 						</div>
	 					</div>
					</div>
					<div class="col-sm-6" id="tree" style="overflow-x:scroll">
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	 
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js"></script>
<script>

var treeData = [
  {
    "name": "Inovasi",
    "parent": "null",
    "children": [
      {
        "name": "TR-1",
        "parent": "Inovasi",
        "children": [
          {
            "name": "Ac-Promonis",
            "parent": "TR-1"
          },
          {
            "name": "T_Gold",
            "parent": "TR-1"
          },
          {
            "name": "Simidun",
            "parent": "TR-1"
          },
          {
            "name": "Kuitansi ONLINE PDD",
            "parent": "TR-1"
          },
          {
            "name": "NEXTONE",
            "parent": "TR-1"
          }
        ]
      },
      {
        "name": "DES",
        "parent": "Inovasi",
        "children": [
          {
            "name": "AM-360",
            "parent": "DES"
          },
          {
            "name": "IWT",
            "parent": "DES"
          }
        ]
      },{
        "name": "DGS",
        "parent": "Inovasi",
        "children": [
          {
            "name": "Anggun",
            "parent": "DGS"
          },
          {
            "name": "RALM",
            "parent": "DGS"
          }
        ]
      }
    ]
  }
];


// ************** Generate the tree diagram	 *****************
var margin = {top: 20, right: 120, bottom: 20, left: 120},
	width = 960 - margin.right - margin.left,
	height = 500 - margin.top - margin.bottom;
	
var i = 0,
	duration = 750,
	root;

var tree = d3.layout.tree()
	.size([height, width]);

var diagonal = d3.svg.diagonal()
	.projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("#tree").append("svg")
	.attr("width", width + margin.right + margin.left)
	.attr("height", height + margin.top + margin.bottom)
  .append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

root = treeData[0];
root.x0 = height / 2;
root.y0 = 0;
  
update(root);

d3.select(self.frameElement).style("height", "500px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
	  links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
	  .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
	  .attr("class", "node")
	  .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
	  .on("click", click);

  nodeEnter.append("circle")
	  .attr("r", 1e-6)
	  .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
	  .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
	  .attr("dy", ".35em")
	  .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
	  .text(function(d) { return d.name; })
	  .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
	  .duration(duration)
	  .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
	  .attr("r", 10)
	  .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
	  .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
	  .duration(duration)
	  .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
	  .remove();

  nodeExit.select("circle")
	  .attr("r", 1e-6);

  nodeExit.select("text")
	  .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
	  .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
	  .attr("class", "link")
	  .attr("d", function(d) {
		var o = {x: source.x0, y: source.y0};
		return diagonal({source: o, target: o});
	  });

  // Transition links to their new position.
  link.transition()
	  .duration(duration)
	  .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
	  .duration(duration)
	  .attr("d", function(d) {
		var o = {x: source.x, y: source.y};
		return diagonal({source: o, target: o});
	  })
	  .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
	d.x0 = d.x;
	d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
	d._children = d.children;
	d.children = null;
  } else {
	d.children = d._children;
	d._children = null;
  }
  update(d);
}

</script>