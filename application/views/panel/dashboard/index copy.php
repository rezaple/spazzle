<?PHP
$q 		= $this->db->query("
		SELECT * from (
			SELECT a.*, 
				(SELECT codeResult from isrm2m.m2m_application where ap_name_reference=a.app_id) status 
			from isrm2m.m2m_station a
		) as base
		where status in ('1','3','-4','-7')")->result_array();
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.6/proj4.js"></script>
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/stock/modules/data.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>
<script src="https://code.highcharts.com/modules/marker-clusters.js"></script>
<script src="https://code.highcharts.com/modules/coloraxis.js"></script>

<style>
.imgdummy {
	max-height: 300px;
}
#logomap {
	position: absolute;
    right: 2rem;
    z-index: 1;
    bottom: 2.5rem;
}
#logomap img {
    max-height: 2rem;
    opacity: 0.5;
}
#maps1 { height: 100%; }
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

	<div class="kt-portlet kt-portlet--mobile" style="height: 100%;">
		<div class="kt-portlet__body" style="height: 100%;">
			<div class="row" style="height: 100%;">
				<div class="col-md-12" style="padding:0">
					<div id="logomap"><img src="<?PHP echo base_url(); ?>images/logoisrm2m-b.png"></div>
					<div id="maps1" class="col-md-12"></div>
					<script>
						var chart;
						var x = [],
						  y = [],
						  zoomY,
						  zoomX;

					    Highcharts.mapChart('maps1', {
					        chart: {
					            map: 'countries/id/id-all',
					            backgroundColor: '#d1e5fd',
					            zoomType: 'xy',
							    events: {
							      load: function() {
							        x.push(this.xAxis[0].getExtremes().min);
							        x.push(this.xAxis[0].getExtremes().max);
							        y.push(this.yAxis[0].getExtremes().min);
							        y.push(this.yAxis[0].getExtremes().max);
							        x.push(this.xAxis[0].getExtremes().min);
							        x.push(this.xAxis[0].getExtremes().max);
							        y.push(this.yAxis[0].getExtremes().min);
							        y.push(this.yAxis[0].getExtremes().max);
							        // console.log(x, y);
							      }
							    }
					        },
					        title: {
					            text: ''
					        },
					        credits: {
					            enabled: false
				          	},
					        subtitle: {
					            text: ''
					        },
					       	mapNavigation: {
							    enabled: true,
							    buttons: {
							      zoomIn: {
							        onclick: function() {
							          this.mapZoom(0.5);
							        }
							      },
							      zoomOut: {
							        onclick: function() {
							          this.mapZoom(2);
							        }
							      }
							    }
							},
							xAxis: {
							    events: {
							      afterSetExtremes: function() {}
							    }
							},
					  		yAxis: {
							    events: {
							      afterSetExtremes: function() {}
							    }
							},
					        tooltip: {
					            headerFormat: '',
					            pointFormat: '<b>{point.name}</b><br>Lat: {point.lat:.2f}, Lon: {point.lon:.2f}'
					        },
					        colorAxis: {
					        	visible: false,
					            min: 1,
					            max: 100,
					            minColor: 'rgba(182,49,40,.5)',
					            maxColor: 'rgba(182,49,40,.5)',
					        },
					        // plotOptions: {
					        //     mappoint: {
					        //         cluster: {
					        //             enabled: true,
					        //             allowOverlap: false,
					        //             animation: {
					        //                 duration: 450
					        //             },
					        //             layoutAlgorithm: {
					        //                 type: 'grid',
					        //                 gridSize: 70
					        //             },
					        //         },
					        //         tooltip: {
						       //          headerFormat: '<b>{series.name}</b><br>',
						       //          pointFormat: '<b>{point.name}</b><br>Lat: {point.lat:.2f}, Lon: {point.lon:.2f}',
						       //          clusterFormat: 'Total: {point.clusterPointsAmount}'
						       //      },
					        //     }
					        // },
					        series: [{
					            name: 'Basemap',
					            borderColor: 'rgba(160,160,160,.5)',
					            nullColor: '#eaeaea',
					            showInLegend: false,
					        }, {
					            type: 'mappoint',
					            enableMouseTracking: true,
					            colorKey: 'clusterPointsAmount',
					            name: 'ISR',
					            marker: {
					            	symbol: 'url(<?PHP echo base_url(); ?>images/towersmall_m.png)'
					            },
					            data: [
					            	<?PHP 
					            	$i = 0;
					            	foreach ($q as $data) { $i++;
					            		// FUNGSI CONVERT COORDINATE DMS/DES TO DECIMAL
										$lat_deg 	= $data['LAT_DEG'];
										$lat_min 	= $data['LAT_MIN'];
										$lat_sec 	= $data['LAT_SEC'];
										$lat_dir 	= $data['LAT_DIR_IND_N_S'];
										$lat 		= round($this->formula->DMS2Decimal($lat_deg, $lat_min, $lat_sec, $lat_dir),4);

										$lon_deg 	= $data['LONG_DEG'];
										$lon_min 	= $data['LONG_MIN'];
										$lon_sec 	= $data['LONG_SEC'];
										$lon_dir 	= $data['LONG_DIR_IND_E_W'];
										$lon 		= round($this->formula->DMS2Decimal($lon_deg, $lon_min, $lon_sec, $lon_dir),4);
					            	?>
					            	{
								      name: '<?PHP echo $data['Station_Name']; ?>',
								      color: "rgba(182,49,40,.5)",
								      lat: <?PHP echo $lat; ?>,
								      lon: <?PHP echo $lon; ?>,
								      index: <?PHP echo $i; ?>,
								    },
								    <?PHP } ?>
					            ]
					        }]
					    });
					</script>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<!--End::Section-->

	<!--End::Dashboard 1-->
</div>