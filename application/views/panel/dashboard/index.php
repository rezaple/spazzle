<?PHP 
function looptreg($init,$root,$name){
	$data = '';
	$datachild = '';
	$x=0;
	$CI =& get_instance();
	$getcfu = $CI->db->query("select cfu from (
									select * from (
										select trim(cfu)as cfu,judul,url,domain from (
											select 
											unnest(string_to_array(cfu_fu, ',')) AS cfu,judul,url_aplikasi as url,domain from mi.data_innovasi di 
										)as master order by cfu 
									)as inherit1 
									)as master where domain = '".$name."'")->result_array();
	$n =0;
	foreach($getcfu as $cfu){
    		$n++;
    		$codeid = $init.$n;
    		$data .= '
		    		'.$codeid.' = {
	                        parent: '.$root.',
	                        text:{
	                            name: "'.$cfu['cfu'].'",
	                            title: "",
	                        },   
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "node2",
	                }, 
    		';
    		$CI =& get_instance();
    		$get = $CI->db->query("select * from (
											select * from (
												select trim(cfu)as cfu,judul,url,domain from (
													select 
													unnest(string_to_array(cfu_fu, ',')) AS cfu,judul,url_aplikasi as url,domain from mi.data_innovasi di 
												)as master order by cfu 
											)as inherit1 where cfu ='".$cfu['cfu']."'
											)as master where domain = '".$name."'")->result_array();
    		foreach($get as $row){
    			$x++;
    			$getcodeid = $init.$n.$x;
    			$datachild .= '
			    		'.$getcodeid.' = {
		                        parent: '.$codeid.',
		                        text:{
		                            name: "'.$row['judul'].'",
		                            title: "",
		                        },  
		                        link: {
						            href: "'.$row['url'].'",
						            target: "_blank"
						        },   
		                        stackChildren: true,
		                        collapsable: true,
		                        HTMLid: "base",
		                        HTMLclass: "node3",
		                }, 
	    		';
    		}
	    	 
	}
	return $data.$datachild;
};
function looptregcomma($init,$root,$name){
	$codeid = array();
	$getcodeid = array();
	$x = 0;
	$CI =& get_instance();
	$getcfu = $CI->db->query("select cfu from (
									select * from (
										select trim(cfu)as cfu,judul,url,domain from (
											select 
											unnest(string_to_array(cfu_fu, ',')) AS cfu,judul,url_aplikasi as url,domain from mi.data_innovasi di 
										)as master order by cfu 
									)as inherit1 
									)as master where domain = '".$name."'")->result_array();
	$n =0;
	foreach($getcfu as $cfu){
    		$n++;
    		$codeid[] = $init.$n;
    		$CI =& get_instance();
    		$get = $CI->db->query("select * from (
											select * from (
												select trim(cfu)as cfu,judul,url,domain from (
													select 
													unnest(string_to_array(cfu_fu, ',')) AS cfu,judul,url_aplikasi as url,domain from mi.data_innovasi di 
												)as master order by cfu 
											)as inherit1 where cfu ='".$cfu['cfu']."'
											)as master where domain = '".$name."'")->result_array();
    		foreach($get as $row){
    			$x++;
    			$getcodeid[] = $init.$n.$x;
    			
    		}
	}
	$data = array_merge($codeid,$getcodeid);
	$jm = count($data);
	$datax = implode(',',array_filter($data));
	if($jm > 0){
		return $datax.',';				
	}
};
?>
<link rel="stylesheet" href="<?PHP echo base_url();?>assets/tree/Treant.css">
<link rel="stylesheet" href="<?PHP echo base_url();?>assets/tree/basic-example.css">
<style>
	.Treant a.node{
		color:white !important;
	}
	.Treant .collapse-switch {
	    width: 13px;
	    height: 13px;
	    display: block;
	    border: 1px solid #8bd7bd;
	    position: absolute;
	    top: 36%;
	    left: -6px;
	    cursor: pointer;
	    background: #313d63;
	    border-radius: 100%;
	}
	.Treant .collapsed .collapse-switch {
	    background-color: #013453;
	}
	.kt-portlet__head .nav-tabs.nav-tabs-line {
		margin: 0 0 -15px 0;
	}
	.mainchart{
		border:0px !important;
	}
	.mainchart .collapse-switch{
		display:none;
	}
	.chartactive{
		width: auto;
		height: auto;
		min-width:100px;
		border-radius: 20px;
		box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
		background-color: #004874;
		color:white;
		border:0px;
	}

	.chartactive2{
		width: auto;
		height: auto;
		min-width:100px;
		border-radius: 20px;
		box-shadow: 0 8px 4px 0 rgba(0, 0, 0, 0.16);
		background-color: #a74350;
		color:white;
		border:0px;
	}
	.node2 {
	  width: auto;
	  height: auto;
	  max-width:200px;
	  border-radius: 11px;
	  background-color: #95a5a6;
	  font-size:10px;
	  border:0px;
	}
	.node3 {
	  width: auto;
	  height: auto;
	  max-width:200px;
	  border-radius: 11px;
	  background-color: #95a5a6;
	  font-size:10px;
	  color:white;
	  border:0px;
	}
	.mt2rem {margin-top: 2rem;}
	.valuebgdash {margin-top: 0.5rem; margin-bottom: -1rem;}
	.kt-portlet--solid-danger { background: #c0392b!important; }
	#circlebg {
		font-size: 2rem;
	    line-height: 1rem;
	    text-align: center;
	    box-shadow: 0px 3px 10px rgba(0,0,0,.3);
	    z-index: 2;
	    width: 150px;
	    height: 150px;
	    border-radius: 100%;
	    position: absolute;
	    left: 36%;
    	top: 21%;
	    padding-top: 10%;
	}
	#circlebg span {font-size: 1rem;}
	#loadermini {
		width: 100%;
		height: calc(100% - 20px);
		background: #FFF url('<?PHP echo base_url(); ?>images/loadermini.gif') center no-repeat;
		background-size: 80px auto;
	}
	.steps {
	  list-style-type: none;
	  padding: 0;
	}
	.steps li {
	  display: inline-block;
	  margin-bottom: 3px;
	}
	.steps li a, .steps li p {
	  background: #ff6d00;
	  padding: 8px 10px;
	  color: #fff;
	  display: block;
	  font-size: 11px;
	  font-weight: bold;
	  position: relative;
	  text-indent: 12px;
	}
	.steps li a:hover, .steps li p:hover {
	  text-decoration: none;
	}
	.steps li a:before, .steps li p:before {
	  border-bottom: 18px solid transparent;
	  border-left: 12px solid #fff;
	  border-top: 18px solid transparent;
	  content: "";
	  height: 0;
	  position: absolute;
	  left: 0;
	  top: 50%;
	  width: 0;
	  margin-top: -18px;
	}
	.steps li a:after, .steps li p:after {
	  border-bottom: 18px solid transparent;
	  border-left: 12px solid #ff6d00;
	  border-top: 18px solid transparent;
	  content: "";
	  height: 0;
	  position: absolute;
	  /*right: -12px;*/
	  left:100%;
	  top: 50%;
	  width: 0;
	  margin-top: -18px;
	  z-index: 1;
	}
	.steps li.active a, .steps li.active p {
	  background: #fd8933;
	  color: #fff;
	}
	.steps li.active a:after, .steps li.active p:after {
	  border-left: 12px solid #fd8933;
	}
	.steps li.active2 a, .steps li.active2 p {
	  background: #d87765;
	  color: #fff;
	}
	.steps li.active2 a:after, .steps li.active2 p:after {
	  border-left: 12px solid #d87765;
	}
	.steps li.active3 a, .steps li.active3 p {
	  background: #c1898c;
	  color: #fff;
	}
	.steps li.active3 a:after, .steps li.active3 p:after {
	  border-left: 12px solid #c1898c;
	}
	.steps li.undone a, .steps li.undone p {
	  background: #eee;
	  color: #333;
	}
	.steps li.undone a:after, .steps li.undone p:after {
	  border-left: 12px solid #eee;
	}
	.steps li.undone p {
	  color: #aaa;
	}
	.alert{
		padding:5px !important;
		margin:0 0 5px 0 !important;
	}
	.node {
		cursor: pointer;
	}

	.node circle {
	  fill: #fff;
	  stroke: steelblue;
	  stroke-width: 3px;
	}

	.node text {
	  font: 12px sans-serif;
	}

	.link {
	  fill: none;
	  stroke: #ccc;
	  stroke-width: 2px;
	}
</style>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<!--Begin::Dashboard 1-->

	<!--Begin::Section-->
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="kt-portlet kt-portlet--responsive-mobile">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<ul class="nav nav-tabs  nav-tabs-line " role="tablist">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#sdlc" role="tab">
										<!-- <b><i class="flaticon flaticon-file-2"></i> TEL.1/INNOVASI/A/10/2020</b> -->
										<i class="flaticon flaticon-file-2"></i> CFU/FU
									</a>
								</li> 

								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#detail" role="tab">
										<!-- <b><i class="flaticon flaticon-file-2"></i> TEL.1/INNOVASI/A/10/2020</b> -->
										<b><i class="flaticon flaticon-file-2"></i> Domain</b>
									</a>
								</li>
							</ul>
						</h3>
					</div>

				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane" id="detail" role="tabpanel">	
							<div id='treedomain' style='height:420px'></div>				 
						</div>
						<div class="tab-pane active" id="sdlc" role="tabpanel"> 
							<div id='treecfu' style='height:420px'></div>	 
						</div> 
					</div> 
				</div>
			</div>
		</div>
	</div>

	 
</div>
<script src="<?PHP echo base_url();?>assets/tree/vendor/raphael.js"></script>
<script src="<?PHP echo base_url();?>assets/tree/Treant.js"></script>
<script>
	loadcfu();
	function loaddomain(){
	    var config = {
	        container: "#treedomain",
	        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
	        scrollbar: "fancy",
	        animateOnInit: true,
	        siblingSeparation:   40,
	        subTeeSeparation:    30,
	            
	        node: {
	            collapsable: true
	        },
	        animation: {
	            nodeAnimation: "easeOutBounce",
	            nodeSpeed: 700,
	            connectorsAnimation: "bounce",
	            connectorsSpeed: 700
	        },
	        
	        connectors: {
	            type: 'step',
	            style: {
	                // "stroke-width": 2,
	                "stroke": "#013453",
	                "arrow-end": "classic-wide-long"
	            }
	        },
	        node: {
	            HTMLclass: 'nodeExample1'
	        }
	    },

	    base55 = {
	        text: {
	            name: "",
	            title: "",
	        },
	        HTMLid: "base",
	        HTMLclass: "mainchart",
		        image: "<?PHP echo base_url(); ?>assets/tree/innovation.png"
		    },

	    
	                int1 = {
	                        parent: base55,
	                        text:{
	                            name: "Partner Mgmt",
	                            title: "",
	                        },   
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                }, 
	                	<?php
	                	echo looptreg('int1','int1','Partner Mgmt');
	                	?>
	                int2 = {
	                        parent: base55,
	                        text:{
	                            name: "Analytics",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                }, 
						<?php
	                	echo looptreg('int2','int2','Analytics');
	                	?> 

	                int3 = {
	                        parent: base55,
	                        text:{
	                            name: "Security Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	            		<?php
	                	echo looptreg('int3','int3','Security Mgmt');
	                	?> 

	            	int4 = {
	                        parent: base55,
	                        text:{
	                            name: "Performance Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	                	<?php
	                	echo looptreg('int4','int4','Performance Mgmt');
	                	?> 

	                int5 = {
	                        parent: base55,
	                        text:{
	                            name: "Product Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	                	<?php
	                	echo looptreg('int5','int5','Product Mgmt');
	                	?> 

	                int6 = {
	                        parent: base55,
	                        text:{
	                            name: "Fullfilment Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	               		<?php
	                	echo looptreg('int6','int6','Fullfilment Mgmt');
	                	?> 

	               	int7 = {
	                        parent: base55,
	                        text:{
	                            name: "Assurance Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
						<?php
	                	echo looptreg('int7','int7','Assurance Mgmt');
	                	?> 

	                int8 = {
	                        parent: base55,
	                        text:{
	                            name: "Data & Inventory Mgmt",
	                            title: "",
	                        },  
	                        stackChildren: true,
	                        collapsable: true,
	                        HTMLid: "base",
	                        HTMLclass: "chartactive",
	                },
	                	<?php
	                	echo looptreg('int8','int8','Data & Inventory Mgmt');
	                	?> 


	    chart_config = [
	        config,
	        base55,
	        int1, 
	        int2, 
	        int3,
	        int4,
	        int5,
	        int6,
	        int7,
	        int8,
	        <?PHP echo looptregcomma('int1','int1','Partner Mgmt'); ?>
			<?PHP echo looptregcomma('int2','int2','Analytics'); ?>	        
			<?PHP echo looptregcomma('int3','int3','Security Mgmt'); ?>	          
	        <?PHP echo looptregcomma('int4','int4','Performance Mgmt'); ?>
			<?PHP echo looptregcomma('int5','int5','Product Mgmt'); ?>	        
			<?PHP echo looptregcomma('int6','int6','Fullfilment Mgmt'); ?>         
	        <?PHP echo looptregcomma('int7','int7','Assurance Mgmt'); ?>
			<?PHP echo looptregcomma('int8','int8','Data & Inventory Mgmt'); ?>	        

	     ];

	    new Treant( chart_config );
	}


    function loadcfu(){
        var config2 = {
	        container: "#treecfu",
	        rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
	        scrollbar: "fancy",
	        animateOnInit: true,
	        siblingSeparation:   40,
	        subTeeSeparation:    30,
	            
	        node: {
	            collapsable: true
	        },
	        animation: {
	            nodeAnimation: "easeOutBounce",
	            nodeSpeed: 700,
	            connectorsAnimation: "bounce",
	            connectorsSpeed: 700
	        },
	        
	        connectors: {
	            type: 'step',
	            style: {
	                // "stroke-width": 2,
	                "stroke": "#013453",
	                "arrow-end": "classic-wide-long"
	            }
	        },
	        node: {
	            HTMLclass: 'nodeExample1'
	        }
	    },

	    base552 = {
	        text: {
	            name: "",
	            title: "",
	        },
	        HTMLid: "base",
	        HTMLclass: "mainchart",
		        image: "<?PHP echo base_url(); ?>assets/tree/innovation.png"
		    },

	    
	        <?PHP 
	    	$getparent = $this->db->query("select cfu,count(judul)as jml from (
									select trim(cfu)as cfu,judul from (
										select 
										unnest(string_to_array(cfu_fu, ',')) AS cfu,judul from mi.data_innovasi di 
									)as master order by cfu 
								)as inherit1 group by cfu")->result_array();
	    	$no =0;
	    	$noc = 0;
	    	$listcodeid = array();
	    	$child_listcodeid = array();
	    	foreach($getparent as $parent){
	    		$no++;
	    		$codeid = "Xint".$no;
	    		echo '
			    		'.$codeid.' = {
		                        parent: base552,
		                        text:{
		                            name: "'.$parent['cfu'].' ( '.$parent['jml'].' )",
		                            title: "",
		                        },   
		                        stackChildren: true,
		                        collapsable: true,
		                        HTMLid: "base",
		                        HTMLclass: "chartactive2",
		                }, 
	    		';
	    		if($parent['jml'] > 0){
	    			$getchild = $this->db->query("select * from (
													select trim(cfu)as cfu,judul,url from (
														select 
														unnest(string_to_array(cfu_fu, ',')) AS cfu,judul,url_aplikasi as url from mi.data_innovasi di 
													)as master order by cfu 
												)as inherit1 where cfu ='".$parent['cfu']."'")->result_array();
	    			foreach($getchild as $child){
	    				$noc++;
	    				$child_codeid = "CXint".$noc;
	    				echo '
					    		'.$child_codeid.' = {
				                        parent: '.$codeid.',
				                        text:{
				                            name: "'.$child['judul'].'",
				                            title: ""
				                        },  
				                        link: {
								            href: "'.$child['url'].'",
								            target: "_blank"
								        }, 
				                        stackChildren: true,
				                        collapsable: true,
				                        HTMLid: "base",
				                        HTMLclass: "node3",
				                }, 
			    		';
			    		$child_listcodeid[] =  $child_codeid;
	    			}
	    		}
	    		$listcodeid[] = $codeid;
	    	}
	    	$configcodeid = implode(',',$listcodeid);
	    	$configcodeid2 = implode(',',$child_listcodeid);
			?>

		    chart_config2 = [
		        config2,
		        base552,	        
		        <?PHP echo $configcodeid.','.$configcodeid2;?> 
		     ];


	    new Treant( chart_config2 );	
    }

</script>     